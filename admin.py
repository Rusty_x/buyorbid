import os
import uwsgi
import gevent.monkey

gevent.monkey.patch_all()

from factories import create_app, initialize_api, initialize_blueprints

app = create_app('buyorbid', 'config.ProdConfig')

with app.app_context():
    from buyorbid.views.admin import admin_bp
    from buyorbid import api

    # Import all subscriptions to initialize them
    from buyorbid.subscribers import search

    initialize_api(app, api)

    # Initialize the app blueprints
    initialize_blueprints(app, admin_bp)

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
