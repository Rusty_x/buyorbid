--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE address (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    phone character varying(200),
    street text,
    city character varying(200),
    lat character varying(200),
    lng character varying(200),
    zipcode text,
    customer_id integer,
    user_id integer,
    state_id integer NOT NULL,
    country_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE address OWNER TO postgres;

--
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE address_id_seq OWNER TO postgres;

--
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE address_id_seq OWNED BY address.id;


--
-- Name: admin_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE admin_message (
    id integer NOT NULL,
    full_name character varying(200) NOT NULL,
    email character varying(200) NOT NULL,
    phone character varying(200) NOT NULL,
    subject text,
    is_read boolean,
    user_read boolean,
    has_parent boolean,
    is_replied boolean,
    user_id integer,
    date_replied timestamp without time zone,
    body text,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE admin_message OWNER TO postgres;

--
-- Name: admin_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin_message_id_seq OWNER TO postgres;

--
-- Name: admin_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin_message_id_seq OWNED BY admin_message.id;


--
-- Name: admin_message_response; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE admin_message_response (
    id integer NOT NULL,
    admin_message_id integer NOT NULL,
    body text,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE admin_message_response OWNER TO postgres;

--
-- Name: admin_message_response_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin_message_response_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin_message_response_id_seq OWNER TO postgres;

--
-- Name: admin_message_response_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin_message_response_id_seq OWNED BY admin_message_response.id;


--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE alembic_version OWNER TO postgres;

--
-- Name: auction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auction (
    id integer NOT NULL,
    name character varying(200),
    description text,
    other_information text,
    title character varying(200),
    auction_information text,
    value double precision,
    starting_bid double precision,
    max_bid double precision,
    bid_cost double precision,
    bid_increment double precision,
    variant_id integer,
    cover_image_id integer,
    on_bid boolean,
    bid_start timestamp without time zone,
    bid_end timestamp without time zone,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE auction OWNER TO postgres;

--
-- Name: auction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auction_id_seq OWNER TO postgres;

--
-- Name: auction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auction_id_seq OWNED BY auction.id;


--
-- Name: auction_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auction_request (
    id integer NOT NULL,
    product_id integer NOT NULL,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE auction_request OWNER TO postgres;

--
-- Name: auction_request_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auction_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auction_request_id_seq OWNER TO postgres;

--
-- Name: auction_request_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auction_request_id_seq OWNED BY auction_request.id;


--
-- Name: bank; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE bank (
    id integer NOT NULL,
    code character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    branch character varying(200) NOT NULL,
    country_code character varying(200) NOT NULL,
    country_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE bank OWNER TO postgres;

--
-- Name: bank_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bank_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank_id_seq OWNER TO postgres;

--
-- Name: bank_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bank_id_seq OWNED BY bank.id;


--
-- Name: bid; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE bid (
    id integer NOT NULL,
    auction_id integer NOT NULL,
    price double precision,
    bid_time timestamp without time zone,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE bid OWNER TO postgres;

--
-- Name: bid_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bid_id_seq OWNER TO postgres;

--
-- Name: bid_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bid_id_seq OWNED BY bid.id;


--
-- Name: cart; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cart (
    id integer NOT NULL,
    first_name character varying(200),
    last_name character varying(200),
    delivery_charge double precision,
    phone character varying(200),
    email character varying(200),
    address_line1 character varying(200),
    address_line2 character varying(200),
    city character varying(200),
    user_id integer,
    state_id integer,
    country_id integer,
    payment_option_id integer,
    delivery_option_id integer,
    shipment_name character varying(200),
    shipment_phone character varying(200),
    shipment_address character varying(300),
    shipment_city character varying(200),
    shipment_description character varying(200),
    shipment_state character varying(200),
    shipment_country character varying(200),
    purchase_session_token character varying(100),
    quantity integer,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE cart OWNER TO postgres;

--
-- Name: cart_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cart_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cart_id_seq OWNER TO postgres;

--
-- Name: cart_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cart_id_seq OWNED BY cart.id;


--
-- Name: cart_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cart_item (
    id integer NOT NULL,
    customer_id integer,
    product_id integer NOT NULL,
    variant_id integer,
    quantity integer,
    cart_id integer NOT NULL,
    user_id integer,
    price double precision,
    total double precision,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE cart_item OWNER TO postgres;

--
-- Name: cart_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cart_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cart_item_id_seq OWNER TO postgres;

--
-- Name: cart_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cart_item_id_seq OWNED BY cart_item.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categories (
    category_id integer,
    product_id integer
);


ALTER TABLE categories OWNER TO postgres;

--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE category (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(200) NOT NULL,
    description character varying(200),
    visibility boolean,
    permanent boolean,
    view_count integer,
    cover_image_id integer,
    level integer NOT NULL,
    parent_id integer,
    detail_tag character varying(300),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE category OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE category_id_seq OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE category_id_seq OWNED BY category.id;


--
-- Name: city; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE city (
    id integer NOT NULL,
    name character varying(200),
    zipcode character varying(200),
    slug character varying(200) NOT NULL,
    state_id integer NOT NULL,
    state_code character varying(200) NOT NULL,
    country_code character varying(200) NOT NULL,
    country_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE city OWNER TO postgres;

--
-- Name: city_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE city_id_seq OWNER TO postgres;

--
-- Name: city_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE city_id_seq OWNED BY city.id;


--
-- Name: conversation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE conversation (
    id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE conversation OWNER TO postgres;

--
-- Name: conversation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE conversation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE conversation_id_seq OWNER TO postgres;

--
-- Name: conversation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE conversation_id_seq OWNED BY conversation.id;


--
-- Name: conversee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE conversee (
    id integer NOT NULL,
    conversation_id integer NOT NULL,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE conversee OWNER TO postgres;

--
-- Name: conversee_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE conversee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE conversee_id_seq OWNER TO postgres;

--
-- Name: conversee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE conversee_id_seq OWNED BY conversee.id;


--
-- Name: country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE country (
    id integer NOT NULL,
    code character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    slug character varying(200) NOT NULL,
    phone_code character varying,
    enabled boolean,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE country OWNER TO postgres;

--
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE country_id_seq OWNER TO postgres;

--
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE country_id_seq OWNED BY country.id;


--
-- Name: coupon; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE coupon (
    id integer NOT NULL,
    order_id integer,
    type character varying(200) NOT NULL,
    amount double precision,
    min_spend double precision,
    category_id integer,
    is_used boolean,
    is_expired boolean,
    expiry_date timestamp without time zone NOT NULL,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE coupon OWNER TO postgres;

--
-- Name: coupon_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE coupon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coupon_id_seq OWNER TO postgres;

--
-- Name: coupon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE coupon_id_seq OWNED BY coupon.id;


--
-- Name: credit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE credit (
    id integer NOT NULL,
    amount double precision,
    price double precision,
    is_awarded boolean,
    is_purchased boolean,
    is_credit boolean,
    credit_type_id integer,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE credit OWNER TO postgres;

--
-- Name: credit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE credit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE credit_id_seq OWNER TO postgres;

--
-- Name: credit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE credit_id_seq OWNED BY credit.id;


--
-- Name: credit_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE credit_type (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(200) NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE credit_type OWNER TO postgres;

--
-- Name: credit_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE credit_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE credit_type_id_seq OWNER TO postgres;

--
-- Name: credit_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE credit_type_id_seq OWNED BY credit_type.id;


--
-- Name: currency; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE currency (
    id integer NOT NULL,
    code character varying(200) NOT NULL,
    name character varying(200),
    enabled boolean,
    symbol character varying(200),
    payment_code character varying(200),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE currency OWNER TO postgres;

--
-- Name: currency_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE currency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE currency_id_seq OWNER TO postgres;

--
-- Name: currency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE currency_id_seq OWNED BY currency.id;


--
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    email character varying(200) NOT NULL,
    accept_marketing boolean,
    user_id integer,
    notes text,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE customer OWNER TO postgres;

--
-- Name: customer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_id_seq OWNER TO postgres;

--
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer_id_seq OWNED BY customer.id;


--
-- Name: delivery_option; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE delivery_option (
    id integer NOT NULL,
    code character varying(50),
    name character varying(200),
    description character varying(200),
    is_enabled boolean,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE delivery_option OWNER TO postgres;

--
-- Name: delivery_option_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE delivery_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE delivery_option_id_seq OWNER TO postgres;

--
-- Name: delivery_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE delivery_option_id_seq OWNED BY delivery_option.id;


--
-- Name: delivery_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE delivery_status (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(200) NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE delivery_status OWNER TO postgres;

--
-- Name: delivery_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE delivery_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE delivery_status_id_seq OWNER TO postgres;

--
-- Name: delivery_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE delivery_status_id_seq OWNED BY delivery_status.id;


--
-- Name: earning; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE earning (
    id integer NOT NULL,
    amount integer,
    is_credit boolean,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE earning OWNER TO postgres;

--
-- Name: earning_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE earning_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE earning_id_seq OWNER TO postgres;

--
-- Name: earning_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE earning_id_seq OWNED BY earning.id;


--
-- Name: email; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE email (
    id integer NOT NULL,
    subject character varying(400) NOT NULL,
    html text,
    text text,
    send_at timestamp without time zone,
    sent_at timestamp without time zone,
    status character varying(200),
    message_id character varying(200),
    "to" character varying(200) NOT NULL,
    from_ character varying(200) NOT NULL,
    notification_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE email OWNER TO postgres;

--
-- Name: email_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE email_id_seq OWNER TO postgres;

--
-- Name: email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE email_id_seq OWNED BY email.id;


--
-- Name: image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE image (
    id integer NOT NULL,
    name character varying(200),
    height double precision,
    width double precision,
    url character varying(200) NOT NULL,
    slug character varying(200),
    auction_id integer,
    product_id integer,
    category_id integer,
    is_cover boolean,
    variant_id integer,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE image OWNER TO postgres;

--
-- Name: image_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE image_id_seq OWNER TO postgres;

--
-- Name: image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE image_id_seq OWNED BY image.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE message (
    id integer NOT NULL,
    text text,
    receiver_id integer NOT NULL,
    sender_id integer NOT NULL,
    conversation_id integer NOT NULL,
    product_id integer,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE message OWNER TO postgres;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO postgres;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE notification (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    email text,
    sms text,
    push text,
    code character varying(200) NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE notification OWNER TO postgres;

--
-- Name: notification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notification_id_seq OWNER TO postgres;

--
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE notification_id_seq OWNED BY notification.id;


--
-- Name: order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "order" (
    id integer NOT NULL,
    customer_id integer,
    first_name character varying(200),
    last_name character varying(200),
    discount double precision,
    shipping_charge double precision,
    code character varying(200) NOT NULL,
    coupon_code character varying(200),
    message text,
    verified_status boolean,
    payment_option_id integer,
    payment_status_id integer,
    delivery_option_id integer,
    delivery_status_id integer,
    order_status_id integer,
    email character varying(200) NOT NULL,
    phone character varying(200) NOT NULL,
    address_line1 character varying(200) NOT NULL,
    address_line2 character varying(200),
    city character varying(200) NOT NULL,
    user_id integer,
    state_id integer NOT NULL,
    country_id integer NOT NULL,
    auto_set_shipment boolean,
    shipment_name character varying(200),
    shipment_phone character varying(200),
    shipment_address character varying(300),
    shipment_city character varying(200),
    shipment_description character varying(200),
    shipment_state character varying(200),
    shipment_country character varying(200),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE "order" OWNER TO postgres;

--
-- Name: order_entry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE order_entry (
    id integer NOT NULL,
    order_id integer NOT NULL,
    user_id integer,
    variant_id integer NOT NULL,
    product_id integer NOT NULL,
    quantity integer,
    price double precision,
    order_status_id integer,
    date_cancelled date,
    date_cust_cancelled date,
    date_delivered date,
    date_processed date,
    date_returned date,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE order_entry OWNER TO postgres;

--
-- Name: order_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_entry_id_seq OWNER TO postgres;

--
-- Name: order_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_entry_id_seq OWNED BY order_entry.id;


--
-- Name: order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_id_seq OWNER TO postgres;

--
-- Name: order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_id_seq OWNED BY "order".id;


--
-- Name: order_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE order_status (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(200) NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE order_status OWNER TO postgres;

--
-- Name: order_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_status_id_seq OWNER TO postgres;

--
-- Name: order_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_status_id_seq OWNED BY order_status.id;


--
-- Name: payment_channel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE payment_channel (
    id integer NOT NULL,
    code character varying(200) NOT NULL,
    name character varying(200),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE payment_channel OWNER TO postgres;

--
-- Name: payment_channel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE payment_channel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE payment_channel_id_seq OWNER TO postgres;

--
-- Name: payment_channel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE payment_channel_id_seq OWNED BY payment_channel.id;


--
-- Name: payment_option; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE payment_option (
    id integer NOT NULL,
    code character varying(200) NOT NULL,
    name character varying(200),
    type character varying(50),
    is_enabled boolean,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE payment_option OWNER TO postgres;

--
-- Name: payment_option_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE payment_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE payment_option_id_seq OWNER TO postgres;

--
-- Name: payment_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE payment_option_id_seq OWNED BY payment_option.id;


--
-- Name: payment_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE payment_status (
    id integer NOT NULL,
    code character varying(200) NOT NULL,
    name character varying(200),
    description character varying(200),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE payment_status OWNER TO postgres;

--
-- Name: payment_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE payment_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE payment_status_id_seq OWNER TO postgres;

--
-- Name: payment_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE payment_status_id_seq OWNED BY payment_status.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    description text,
    sku character varying(200) NOT NULL,
    handle character varying(200),
    quantity integer,
    regular_price double precision NOT NULL,
    sale_price double precision,
    weight double precision,
    has_variants boolean,
    on_sale boolean,
    is_featured boolean,
    variant_attributes text,
    user_id integer,
    view_count integer,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE product OWNER TO postgres;

--
-- Name: product_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_detail (
    id integer NOT NULL,
    name character varying(200),
    content text,
    type_id integer,
    "position" integer,
    product_id integer,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE product_detail OWNER TO postgres;

--
-- Name: product_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_detail_id_seq OWNER TO postgres;

--
-- Name: product_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_detail_id_seq OWNED BY product_detail.id;


--
-- Name: product_detail_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_detail_type (
    id integer NOT NULL,
    code character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    description text,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE product_detail_type OWNER TO postgres;

--
-- Name: product_detail_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_detail_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_detail_type_id_seq OWNER TO postgres;

--
-- Name: product_detail_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_detail_type_id_seq OWNED BY product_detail_type.id;


--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_id_seq OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_id_seq OWNED BY product.id;


--
-- Name: product_view; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_view (
    id integer NOT NULL,
    view_count integer,
    product_id integer,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE product_view OWNER TO postgres;

--
-- Name: product_view_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_view_id_seq OWNER TO postgres;

--
-- Name: product_view_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_view_id_seq OWNED BY product_view.id;


--
-- Name: recurrent_card; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE recurrent_card (
    id integer NOT NULL,
    token character varying(200) NOT NULL,
    mask character varying(300),
    brand character varying(300),
    exp_month integer,
    exp_year integer,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE recurrent_card OWNER TO postgres;

--
-- Name: recurrent_card_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE recurrent_card_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recurrent_card_id_seq OWNER TO postgres;

--
-- Name: recurrent_card_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE recurrent_card_id_seq OWNED BY recurrent_card.id;


--
-- Name: referral; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE referral (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    email character varying(200) NOT NULL,
    is_verified boolean,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE referral OWNER TO postgres;

--
-- Name: referral_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE referral_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE referral_id_seq OWNER TO postgres;

--
-- Name: referral_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE referral_id_seq OWNED BY referral.id;


--
-- Name: sms; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE sms (
    id integer NOT NULL,
    text text,
    "to" character varying(200) NOT NULL,
    from_ character varying(200) NOT NULL,
    send_at timestamp without time zone,
    status character varying(200),
    message_id character varying(200),
    notification_id integer NOT NULL,
    sent_at timestamp without time zone,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE sms OWNER TO postgres;

--
-- Name: sms_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sms_id_seq OWNER TO postgres;

--
-- Name: sms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sms_id_seq OWNED BY sms.id;


--
-- Name: state; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE state (
    id integer NOT NULL,
    code character varying(200) NOT NULL,
    name character varying(200),
    slug character varying(200) NOT NULL,
    country_code character varying(200) NOT NULL,
    country_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE state OWNER TO postgres;

--
-- Name: state_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE state_id_seq OWNER TO postgres;

--
-- Name: state_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE state_id_seq OWNED BY state.id;


--
-- Name: timezone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE timezone (
    id integer NOT NULL,
    name character varying(200),
    code character varying(200),
    "offset" character varying(200),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE timezone OWNER TO postgres;

--
-- Name: timezone_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE timezone_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE timezone_id_seq OWNER TO postgres;

--
-- Name: timezone_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE timezone_id_seq OWNED BY timezone.id;


--
-- Name: transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE transaction (
    id integer NOT NULL,
    customer_id integer,
    cart_id integer,
    code character varying(200) NOT NULL,
    description text,
    payment_option_id integer NOT NULL,
    transaction_status_id integer,
    order_id integer,
    amount double precision,
    phone character varying(200),
    email character varying(200),
    address_line1 character varying(200),
    city character varying(200),
    state character varying(200),
    country character varying(200),
    auto_set_shipment boolean,
    shipment_name character varying(200),
    shipment_phone character varying(200),
    shipment_address character varying(300),
    shipment_city character varying(200),
    shipment_description character varying(200),
    shipment_state character varying(200),
    shipment_country character varying(200),
    user_id integer,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE transaction OWNER TO postgres;

--
-- Name: transaction_entry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE transaction_entry (
    id integer NOT NULL,
    variant_id integer NOT NULL,
    transaction_id integer NOT NULL,
    quantity integer,
    price double precision,
    user_id integer,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE transaction_entry OWNER TO postgres;

--
-- Name: transaction_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE transaction_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transaction_entry_id_seq OWNER TO postgres;

--
-- Name: transaction_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE transaction_entry_id_seq OWNED BY transaction_entry.id;


--
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transaction_id_seq OWNER TO postgres;

--
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE transaction_id_seq OWNED BY transaction.id;


--
-- Name: transaction_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE transaction_status (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(200) NOT NULL,
    message character varying(200),
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE transaction_status OWNER TO postgres;

--
-- Name: transaction_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE transaction_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transaction_status_id_seq OWNER TO postgres;

--
-- Name: transaction_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE transaction_status_id_seq OWNED BY transaction_status.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "user" (
    id integer NOT NULL,
    username character varying(200),
    email character varying(200),
    alt_email character varying(200),
    first_name character varying(200) NOT NULL,
    last_name character varying(200),
    gender character varying(200),
    phone character varying(200),
    alt_phone character varying(200),
    date_of_birth date,
    password text,
    is_enabled boolean,
    is_verified boolean,
    phone_verified boolean,
    email_verified boolean,
    is_staff boolean,
    is_admin boolean,
    login_count integer,
    last_login_at timestamp without time zone,
    current_login_at timestamp without time zone,
    last_login_ip character varying(200),
    current_login_ip character varying(200),
    awarded_credit integer,
    purchased_credit integer,
    user_earning double precision,
    referral_code character varying(200),
    profile_pic text,
    verification_hash character varying(200),
    otp character varying(50),
    is_incognito boolean,
    referral_count integer,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE "user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: variant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE variant (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    attribute_keys character varying(200),
    description text,
    height double precision,
    width double precision,
    regular_price double precision,
    sale_price double precision,
    weight double precision,
    tax_class character varying(200),
    sku character varying(200),
    shipping_class character varying(200),
    is_saved boolean,
    on_sale boolean,
    quantity integer,
    availability boolean,
    view_count integer,
    user_id integer,
    product_id integer,
    last_updated timestamp without time zone,
    date_created timestamp without time zone,
    length double precision
);


ALTER TABLE variant OWNER TO postgres;

--
-- Name: variant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE variant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE variant_id_seq OWNER TO postgres;

--
-- Name: variant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE variant_id_seq OWNED BY variant.id;


--
-- Name: viewed_product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE viewed_product (
    id integer NOT NULL,
    view_count integer,
    product_id integer NOT NULL,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE viewed_product OWNER TO postgres;

--
-- Name: viewed_product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE viewed_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE viewed_product_id_seq OWNER TO postgres;

--
-- Name: viewed_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE viewed_product_id_seq OWNED BY viewed_product.id;


--
-- Name: wishlist; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE wishlist (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    description text,
    slug character varying(200),
    is_private boolean,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE wishlist OWNER TO postgres;

--
-- Name: wishlist_entry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE wishlist_entry (
    id integer NOT NULL,
    product_id integer NOT NULL,
    wishlist_id integer NOT NULL,
    user_id integer NOT NULL,
    last_updated timestamp without time zone,
    date_created timestamp without time zone
);


ALTER TABLE wishlist_entry OWNER TO postgres;

--
-- Name: wishlist_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE wishlist_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wishlist_entry_id_seq OWNER TO postgres;

--
-- Name: wishlist_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE wishlist_entry_id_seq OWNED BY wishlist_entry.id;


--
-- Name: wishlist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE wishlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wishlist_id_seq OWNER TO postgres;

--
-- Name: wishlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE wishlist_id_seq OWNED BY wishlist.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_message ALTER COLUMN id SET DEFAULT nextval('admin_message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_message_response ALTER COLUMN id SET DEFAULT nextval('admin_message_response_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auction ALTER COLUMN id SET DEFAULT nextval('auction_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auction_request ALTER COLUMN id SET DEFAULT nextval('auction_request_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank ALTER COLUMN id SET DEFAULT nextval('bank_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bid ALTER COLUMN id SET DEFAULT nextval('bid_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart ALTER COLUMN id SET DEFAULT nextval('cart_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart_item ALTER COLUMN id SET DEFAULT nextval('cart_item_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category ALTER COLUMN id SET DEFAULT nextval('category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY city ALTER COLUMN id SET DEFAULT nextval('city_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY conversation ALTER COLUMN id SET DEFAULT nextval('conversation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY conversee ALTER COLUMN id SET DEFAULT nextval('conversee_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY country ALTER COLUMN id SET DEFAULT nextval('country_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY coupon ALTER COLUMN id SET DEFAULT nextval('coupon_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY credit ALTER COLUMN id SET DEFAULT nextval('credit_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY credit_type ALTER COLUMN id SET DEFAULT nextval('credit_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY currency ALTER COLUMN id SET DEFAULT nextval('currency_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer ALTER COLUMN id SET DEFAULT nextval('customer_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY delivery_option ALTER COLUMN id SET DEFAULT nextval('delivery_option_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY delivery_status ALTER COLUMN id SET DEFAULT nextval('delivery_status_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY earning ALTER COLUMN id SET DEFAULT nextval('earning_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email ALTER COLUMN id SET DEFAULT nextval('email_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image ALTER COLUMN id SET DEFAULT nextval('image_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notification ALTER COLUMN id SET DEFAULT nextval('notification_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order" ALTER COLUMN id SET DEFAULT nextval('order_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_entry ALTER COLUMN id SET DEFAULT nextval('order_entry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_status ALTER COLUMN id SET DEFAULT nextval('order_status_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payment_channel ALTER COLUMN id SET DEFAULT nextval('payment_channel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payment_option ALTER COLUMN id SET DEFAULT nextval('payment_option_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payment_status ALTER COLUMN id SET DEFAULT nextval('payment_status_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product ALTER COLUMN id SET DEFAULT nextval('product_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_detail ALTER COLUMN id SET DEFAULT nextval('product_detail_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_detail_type ALTER COLUMN id SET DEFAULT nextval('product_detail_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_view ALTER COLUMN id SET DEFAULT nextval('product_view_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recurrent_card ALTER COLUMN id SET DEFAULT nextval('recurrent_card_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY referral ALTER COLUMN id SET DEFAULT nextval('referral_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sms ALTER COLUMN id SET DEFAULT nextval('sms_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY state ALTER COLUMN id SET DEFAULT nextval('state_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY timezone ALTER COLUMN id SET DEFAULT nextval('timezone_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction ALTER COLUMN id SET DEFAULT nextval('transaction_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction_entry ALTER COLUMN id SET DEFAULT nextval('transaction_entry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction_status ALTER COLUMN id SET DEFAULT nextval('transaction_status_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY variant ALTER COLUMN id SET DEFAULT nextval('variant_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY viewed_product ALTER COLUMN id SET DEFAULT nextval('viewed_product_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wishlist ALTER COLUMN id SET DEFAULT nextval('wishlist_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wishlist_entry ALTER COLUMN id SET DEFAULT nextval('wishlist_entry_id_seq'::regclass);


--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY address (id, name, phone, street, city, lat, lng, zipcode, customer_id, user_id, state_id, country_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('address_id_seq', 1, false);


--
-- Data for Name: admin_message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admin_message (id, full_name, email, phone, subject, is_read, user_read, has_parent, is_replied, user_id, date_replied, body, last_updated, date_created) FROM stdin;
\.


--
-- Name: admin_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin_message_id_seq', 1, false);


--
-- Data for Name: admin_message_response; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admin_message_response (id, admin_message_id, body, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: admin_message_response_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin_message_response_id_seq', 1, false);


--
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY alembic_version (version_num) FROM stdin;
46fd449a7f20
\.


--
-- Data for Name: auction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auction (id, name, description, other_information, title, auction_information, value, starting_bid, max_bid, bid_cost, bid_increment, variant_id, cover_image_id, on_bid, bid_start, bid_end, last_updated, date_created) FROM stdin;
\.


--
-- Name: auction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auction_id_seq', 1, false);


--
-- Data for Name: auction_request; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auction_request (id, product_id, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: auction_request_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auction_request_id_seq', 1, false);


--
-- Data for Name: bank; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bank (id, code, name, branch, country_code, country_id, last_updated, date_created) FROM stdin;
1	011	FIRST BANK PLC	011151003	NG	1	2017-10-29 17:02:50.974928	2017-10-29 17:02:50.974956
2	014	MAINSTREET BANK	014150331	NG	1	2017-10-29 17:02:50.98655	2017-10-29 17:02:50.986564
3	023	NIGERIA INTERNATIONAL BANK LIMITED	023150005	NG	1	2017-10-29 17:02:50.99624	2017-10-29 17:02:50.996256
4	033	UNITED BANK FOR AFRICA PLC	033153513	NG	1	2017-10-29 17:02:51.01624	2017-10-29 17:02:51.016269
5	035	WEMA BANK PLC	035150103	NG	1	2017-10-29 17:02:51.026389	2017-10-29 17:02:51.026404
6	044	ACCESS BANK PLC	044150149	NG	1	2017-10-29 17:02:51.034953	2017-10-29 17:02:51.034968
7	050	ECOBANK NIGERIA PLC	050150010	NG	1	2017-10-29 17:02:51.045209	2017-10-29 17:02:51.045235
8	056	OCEANIC BANK INTERNATIONAL PLC	056080016	NG	1	2017-10-29 17:02:51.054092	2017-10-29 17:02:51.054107
9	057	ZENITH BANK PLC	057150013	NG	1	2017-10-29 17:02:51.063998	2017-10-29 17:02:51.064013
10	058	GTBANK PLC	058152036	NG	1	2017-10-29 17:02:51.073633	2017-10-29 17:02:51.073648
11	063	DIAMOND BANK PLC	063150162	NG	1	2017-10-29 17:02:51.082794	2017-10-29 17:02:51.082819
12	068	STANDARD CHARTERED BANK NIGERIA LIMITED	068150015	NG	1	2017-10-29 17:02:51.09249	2017-10-29 17:02:51.092509
13	070	FIDELITY BANK PLC	070150003	NG	1	2017-10-29 17:02:51.100729	2017-10-29 17:02:51.100743
14	076	SKYE BANK PLC	076151006	NG	1	2017-10-29 17:02:51.109192	2017-10-29 17:02:51.109209
15	082	KEYSTONE BANK	082150017	NG	1	2017-10-29 17:02:51.117134	2017-10-29 17:02:51.117148
16	084	ENTERPRISE BANK	084150015	NG	1	2017-10-29 17:02:51.125949	2017-10-29 17:02:51.125964
17	214	FIRST CITY MONUMENT BANK PLC	214150018	NG	1	2017-10-29 17:02:51.134263	2017-10-29 17:02:51.134276
18	215	UNITY BANK PLC	215154097	NG	1	2017-10-29 17:02:51.142098	2017-10-29 17:02:51.14211
19	221	STANBIC IBTC BANK PLC	221159522	NG	1	2017-10-29 17:02:51.149655	2017-10-29 17:02:51.149666
20	232	STERLING BANK PLC	232150016	NG	1	2017-10-29 17:02:51.156969	2017-10-29 17:02:51.156979
\.


--
-- Name: bank_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bank_id_seq', 20, true);


--
-- Data for Name: bid; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bid (id, auction_id, price, bid_time, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: bid_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bid_id_seq', 1, false);


--
-- Data for Name: cart; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cart (id, first_name, last_name, delivery_charge, phone, email, address_line1, address_line2, city, user_id, state_id, country_id, payment_option_id, delivery_option_id, shipment_name, shipment_phone, shipment_address, shipment_city, shipment_description, shipment_state, shipment_country, purchase_session_token, quantity, last_updated, date_created) FROM stdin;
1	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-29 17:06:13.566557	2017-10-29 17:06:12.388619
2	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-29 18:56:43.579115	2017-10-29 18:56:43.579142
3	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-29 19:42:34.680464	2017-10-29 19:42:34.680489
4	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 00:12:36.87491	2017-10-30 00:12:36.874927
5	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 00:56:39.937649	2017-10-30 00:56:39.937672
6	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 05:43:34.049834	2017-10-30 05:43:34.049854
7	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 06:21:51.455728	2017-10-30 06:21:51.455746
8	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-30 07:04:53.879813	2017-10-30 07:04:52.466687
9	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 07:45:24.085337	2017-10-30 07:45:24.085369
10	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-30 10:07:57.923266	2017-10-30 10:07:57.177812
11	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-30 10:31:35.790721	2017-10-30 10:31:35.0664
12	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 13:08:55.868654	2017-10-30 13:08:55.86867
13	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-30 13:09:14.896638	2017-10-30 13:09:12.980827
14	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-30 14:38:07.304313	2017-10-30 14:38:02.063986
15	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 15:09:50.060373	2017-10-30 15:09:50.060392
16	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-30 15:40:42.073161	2017-10-30 15:40:36.196035
17	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 15:47:01.756669	2017-10-30 15:47:01.756692
18	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-30 16:00:57.868666	2017-10-30 16:00:52.796211
19	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 16:06:04.641705	2017-10-30 16:06:04.641728
20	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 18:04:15.127791	2017-10-30 18:04:15.127806
21	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-30 21:22:17.113722	2017-10-30 21:22:16.136457
22	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 21:57:30.095978	2017-10-30 21:57:30.095998
23	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-30 22:02:01.756511	2017-10-30 22:02:01.756543
24	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-30 22:16:08.392077	2017-10-30 22:16:06.286055
25	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-31 00:41:28.741195	2017-10-31 00:41:28.74122
26	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-31 01:58:55.440727	2017-10-31 01:58:55.440786
27	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-31 01:58:56.334675	2017-10-31 01:58:56.334711
28	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-31 02:22:53.96245	2017-10-31 02:22:53.96247
29	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-31 02:49:16.320199	2017-10-31 02:49:16.320231
30	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-31 02:49:18.662877	2017-10-31 02:49:18.6629
31	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-31 08:49:56.712126	2017-10-31 08:49:46.164178
32	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-31 09:09:52.047437	2017-10-31 09:09:48.247287
33	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-31 09:33:59.289308	2017-10-31 09:33:56.200841
34	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-31 09:35:54.813169	2017-10-31 09:35:53.781981
35	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-31 09:37:35.672094	2017-10-31 09:37:35.672145
36	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-31 09:37:49.312653	2017-10-31 09:37:49.287316
37	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-10-31 09:37:49.749297	2017-10-31 09:37:49.73493
38	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	2017-10-31 10:40:51.978216	2017-10-31 10:40:51.978242
\.


--
-- Name: cart_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cart_id_seq', 38, true);


--
-- Data for Name: cart_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cart_item (id, customer_id, product_id, variant_id, quantity, cart_id, user_id, price, total, last_updated, date_created) FROM stdin;
\.


--
-- Name: cart_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cart_item_id_seq', 1, false);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY categories (category_id, product_id) FROM stdin;
17	1
5	2
5	3
2	4
5	5
5	6
2	7
2	8
\.


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY category (id, name, code, description, visibility, permanent, view_count, cover_image_id, level, parent_id, detail_tag, last_updated, date_created) FROM stdin;
1	Women	women	all things that concern women	f	\N	0	\N	0	\N	\N	2017-10-29 17:02:34.598421	2017-10-29 17:02:34.598448
2	Dresses	w_dresses	all things that concern dresses	f	\N	0	\N	1	1	\N	2017-10-29 17:02:34.613896	2017-10-29 17:02:34.613922
3	Blouses, Shirts & Tops	blouses_shirts_tops	all things that concern Blouses, Shirts & Tops	f	\N	0	\N	1	1	\N	2017-10-29 17:02:34.63273	2017-10-29 17:02:34.63274
4	Bra's, Briefs & Lingerie	bras_brief_lingerie	all things that concern Bra's, Briefs & Lingerie	f	\N	0	\N	1	1	\N	2017-10-29 17:02:34.649645	2017-10-29 17:02:34.649667
5	Shoes	w_shoes	all things that concern women shoes	f	\N	0	\N	1	1	\N	2017-10-29 17:02:34.66586	2017-10-29 17:02:34.665871
6	Pants & Jeans	w_pants_jeans	all things that concern ...	f	\N	0	\N	1	1	\N	2017-10-29 17:02:34.681308	2017-10-29 17:02:34.681319
7	Jewelry & Accessories	w_jewelry_accessories	all things that concern ...	f	\N	0	\N	1	1	\N	2017-10-29 17:02:34.698382	2017-10-29 17:02:34.698393
8	Sunglasses & Watches	w_sunglasses_watches	all things that concern ...	f	\N	0	\N	1	1	\N	2017-10-29 17:02:34.716453	2017-10-29 17:02:34.716463
9	Men	Men	all things that concern men	f	\N	0	\N	0	\N	\N	2017-10-29 17:02:34.735457	2017-10-29 17:02:34.735474
10	Shirts & Tops	m_shirts_tops	all things that concern ...	f	\N	0	\N	1	9	\N	2017-10-29 17:02:34.748564	2017-10-29 17:02:34.748577
11	Boxer's, Briefs & Socks	boxers_briefs_socks	all things that concern ...	f	\N	0	\N	1	9	\N	2017-10-29 17:02:34.764665	2017-10-29 17:02:34.764677
12	Shoes	m_shoes	all things that concern men shoes	f	\N	0	\N	1	9	\N	2017-10-29 17:02:34.780256	2017-10-29 17:02:34.780269
13	Pants & Jeans	m_pants_jeans	all things that concern ...	f	\N	0	\N	1	9	\N	2017-10-29 17:02:34.797897	2017-10-29 17:02:34.797915
14	Jewelry & Accessories	m_jewelry_accessories	all things that concern ...	f	\N	0	\N	1	9	\N	2017-10-29 17:02:34.818303	2017-10-29 17:02:34.818314
15	Sunglasses & Watches	m_sunglasses_watches	all things that concern ...	f	\N	0	\N	1	9	\N	2017-10-29 17:02:34.833987	2017-10-29 17:02:34.833997
16	Phone & Accessories	phone_accessories	all things that concern phone and accessories	f	\N	0	\N	0	\N	\N	2017-10-29 17:02:34.851058	2017-10-29 17:02:34.851067
17	Smart Phones	smart_phones	all things that concern ...	f	\N	0	\N	1	16	\N	2017-10-29 17:02:34.861885	2017-10-29 17:02:34.861906
18	Feature Phones	feature_phones	all things that concern ...	f	\N	0	\N	1	16	\N	2017-10-29 17:02:34.879308	2017-10-29 17:02:34.879323
19	Cases & Covers	p_cases_cover	all things that concern ...	f	\N	0	\N	1	16	\N	2017-10-29 17:02:34.897473	2017-10-29 17:02:34.897484
20	Phone Accessories	p_accessories	all things that concern ...	f	\N	0	\N	1	16	\N	2017-10-29 17:02:34.910516	2017-10-29 17:02:34.910529
21	Computer & Office	computer_office	all things that concern computer and office	f	\N	0	\N	0	\N	\N	2017-10-29 17:02:34.923598	2017-10-29 17:02:34.923608
22	Laptop & Tablets	laptop_tablets	all things that concern ...	f	\N	0	\N	1	21	\N	2017-10-29 17:02:34.935832	2017-10-29 17:02:34.935849
23	Laptop & Tablet Accessories	l_t_accessories	all things that concern ...	f	\N	0	\N	1	21	\N	2017-10-29 17:02:34.954457	2017-10-29 17:02:34.954469
24	Storage Devices	storage_devices	all things that concern ...	f	\N	0	\N	1	21	\N	2017-10-29 17:02:34.982452	2017-10-29 17:02:34.982469
25	Electronics	electronics	all things that concern electronics	f	\N	0	\N	0	\N	\N	2017-10-29 17:02:34.99959	2017-10-29 17:02:34.999604
26	Home Audio & Video	home_audio_video	all things that concern ...	f	\N	0	\N	1	25	\N	2017-10-29 17:02:35.013945	2017-10-29 17:02:35.013961
27	Smart Electronics	smart_electronics	all things that concern ...	f	\N	0	\N	1	25	\N	2017-10-29 17:02:35.028274	2017-10-29 17:02:35.028287
28	Audio & Video	audio_video	all things that concern ...	f	\N	0	\N	1	25	\N	2017-10-29 17:02:35.047311	2017-10-29 17:02:35.047327
29	Parts & Accessories	parts_accessories	all things that concern ...	f	\N	0	\N	1	25	\N	2017-10-29 17:02:35.060022	2017-10-29 17:02:35.060032
30	Home & kitchen	home_kitchen	all things that concern home and kitchen	f	\N	0	\N	0	\N	\N	2017-10-29 17:02:35.072507	2017-10-29 17:02:35.072518
31	Baby, Kids & Toys	baby_kids_toys	all things that concern baby, kids and toys	f	\N	0	\N	0	\N	\N	2017-10-29 17:02:35.080432	2017-10-29 17:02:35.080442
32	Health & Sports	health_sport	all things that concern health and sports	f	\N	0	\N	0	\N	\N	2017-10-29 17:02:35.086261	2017-10-29 17:02:35.086271
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('category_id_seq', 32, true);


--
-- Data for Name: city; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY city (id, name, zipcode, slug, state_id, state_code, country_code, country_id, last_updated, date_created) FROM stdin;
1	Birnin Gwari	\N	birnin-gwari	1	KDA	NG	1	2017-10-29 17:02:38.517503	2017-10-29 17:02:38.517521
2	Chikun	\N	chikun	1	KDA	NG	1	2017-10-29 17:02:38.530595	2017-10-29 17:02:38.530607
3	Giwa	\N	giwa	1	KDA	NG	1	2017-10-29 17:02:38.541268	2017-10-29 17:02:38.54128
4	Igabi	\N	igabi	1	KDA	NG	1	2017-10-29 17:02:38.552842	2017-10-29 17:02:38.552854
5	Ikara	\N	ikara	1	KDA	NG	1	2017-10-29 17:02:38.563254	2017-10-29 17:02:38.563266
6	Jaba	\N	jaba	1	KDA	NG	1	2017-10-29 17:02:38.574192	2017-10-29 17:02:38.574204
7	Jema''a	\N	jema-a	1	KDA	NG	1	2017-10-29 17:02:38.585124	2017-10-29 17:02:38.585136
8	Kachia	\N	kachia	1	KDA	NG	1	2017-10-29 17:02:38.597037	2017-10-29 17:02:38.597048
9	Kaduna North	\N	kaduna-north	1	KDA	NG	1	2017-10-29 17:02:38.607241	2017-10-29 17:02:38.607253
10	Kaduna South	\N	kaduna-south	1	KDA	NG	1	2017-10-29 17:02:38.617749	2017-10-29 17:02:38.617761
11	Kagarko	\N	kagarko	1	KDA	NG	1	2017-10-29 17:02:38.627851	2017-10-29 17:02:38.627862
12	Kajuru	\N	kajuru	1	KDA	NG	1	2017-10-29 17:02:38.63832	2017-10-29 17:02:38.638332
13	Kaura	\N	kaura	1	KDA	NG	1	2017-10-29 17:02:38.648591	2017-10-29 17:02:38.648602
14	Kauru	\N	kauru	1	KDA	NG	1	2017-10-29 17:02:38.659418	2017-10-29 17:02:38.659431
15	Kubau	\N	kubau	1	KDA	NG	1	2017-10-29 17:02:38.670464	2017-10-29 17:02:38.670476
16	Kudan	\N	kudan	1	KDA	NG	1	2017-10-29 17:02:38.681182	2017-10-29 17:02:38.681193
17	Lere	\N	lere	1	KDA	NG	1	2017-10-29 17:02:38.691581	2017-10-29 17:02:38.691593
18	Makarfi	\N	makarfi	1	KDA	NG	1	2017-10-29 17:02:38.702274	2017-10-29 17:02:38.702285
19	Sabon Gari	\N	sabon-gari	1	KDA	NG	1	2017-10-29 17:02:38.712606	2017-10-29 17:02:38.712617
20	Sanga	\N	sanga	1	KDA	NG	1	2017-10-29 17:02:38.724032	2017-10-29 17:02:38.724043
21	Soba	\N	soba	1	KDA	NG	1	2017-10-29 17:02:38.736703	2017-10-29 17:02:38.736715
22	Zangon Kataf	\N	zangon-kataf	1	KDA	NG	1	2017-10-29 17:02:38.747279	2017-10-29 17:02:38.74729
23	Zaria	\N	zaria	1	KDA	NG	1	2017-10-29 17:02:38.757536	2017-10-29 17:02:38.757547
24	Ajingi	\N	ajingi	2	KAN	NG	1	2017-10-29 17:02:38.76862	2017-10-29 17:02:38.768632
25	Albasu	\N	albasu	2	KAN	NG	1	2017-10-29 17:02:38.778753	2017-10-29 17:02:38.778764
26	Bagwai	\N	bagwai	2	KAN	NG	1	2017-10-29 17:02:38.789085	2017-10-29 17:02:38.789096
27	Bebeji	\N	bebeji	2	KAN	NG	1	2017-10-29 17:02:38.799037	2017-10-29 17:02:38.799048
28	Bichi	\N	bichi	2	KAN	NG	1	2017-10-29 17:02:38.809662	2017-10-29 17:02:38.809674
29	Bunkure	\N	bunkure	2	KAN	NG	1	2017-10-29 17:02:38.820398	2017-10-29 17:02:38.820409
30	Dala	\N	dala	2	KAN	NG	1	2017-10-29 17:02:38.832699	2017-10-29 17:02:38.832714
31	Dambatta	\N	dambatta	2	KAN	NG	1	2017-10-29 17:02:38.84458	2017-10-29 17:02:38.844593
32	Dawakin Kudu	\N	dawakin-kudu	2	KAN	NG	1	2017-10-29 17:02:38.857261	2017-10-29 17:02:38.857275
33	Dawakin Tofa	\N	dawakin-tofa	2	KAN	NG	1	2017-10-29 17:02:38.870815	2017-10-29 17:02:38.87083
34	Doguwa	\N	doguwa	2	KAN	NG	1	2017-10-29 17:02:38.881563	2017-10-29 17:02:38.881575
35	Fagge	\N	fagge	2	KAN	NG	1	2017-10-29 17:02:38.894458	2017-10-29 17:02:38.894472
36	Gabasawa	\N	gabasawa	2	KAN	NG	1	2017-10-29 17:02:38.9111	2017-10-29 17:02:38.911115
37	Garko	\N	garko	2	KAN	NG	1	2017-10-29 17:02:38.928214	2017-10-29 17:02:38.928229
38	Garun Mallam	\N	garun-mallam	2	KAN	NG	1	2017-10-29 17:02:38.942042	2017-10-29 17:02:38.942054
39	Gaya	\N	gaya	2	KAN	NG	1	2017-10-29 17:02:38.955667	2017-10-29 17:02:38.955679
40	Gezawa	\N	gezawa	2	KAN	NG	1	2017-10-29 17:02:38.973918	2017-10-29 17:02:38.973932
41	Gwale	\N	gwale	2	KAN	NG	1	2017-10-29 17:02:38.988298	2017-10-29 17:02:38.988313
42	Gwarzo	\N	gwarzo	2	KAN	NG	1	2017-10-29 17:02:39.005017	2017-10-29 17:02:39.005033
43	Kabo	\N	kabo	2	KAN	NG	1	2017-10-29 17:02:39.018736	2017-10-29 17:02:39.01875
44	Kano Municipal	\N	kano-municipal	2	KAN	NG	1	2017-10-29 17:02:39.034532	2017-10-29 17:02:39.034547
45	Karaye	\N	karaye	2	KAN	NG	1	2017-10-29 17:02:39.049205	2017-10-29 17:02:39.049217
46	Kibiya	\N	kibiya	2	KAN	NG	1	2017-10-29 17:02:39.063957	2017-10-29 17:02:39.063974
47	Kiru	\N	kiru	2	KAN	NG	1	2017-10-29 17:02:39.079037	2017-10-29 17:02:39.07906
48	Kumbotso	\N	kumbotso	2	KAN	NG	1	2017-10-29 17:02:39.096013	2017-10-29 17:02:39.096028
49	Kunchi	\N	kunchi	2	KAN	NG	1	2017-10-29 17:02:39.109336	2017-10-29 17:02:39.109351
50	Kura	\N	kura	2	KAN	NG	1	2017-10-29 17:02:39.123043	2017-10-29 17:02:39.123056
51	Madobi	\N	madobi	2	KAN	NG	1	2017-10-29 17:02:39.136465	2017-10-29 17:02:39.13648
52	Makoda	\N	makoda	2	KAN	NG	1	2017-10-29 17:02:39.155045	2017-10-29 17:02:39.155058
53	Minjibir	\N	minjibir	2	KAN	NG	1	2017-10-29 17:02:39.170258	2017-10-29 17:02:39.170271
54	Nasarawa, Kano State	\N	nasarawa-kano-state	2	KAN	NG	1	2017-10-29 17:02:39.184085	2017-10-29 17:02:39.184097
55	Rano	\N	rano	2	KAN	NG	1	2017-10-29 17:02:39.199746	2017-10-29 17:02:39.199764
56	Rimin Gado	\N	rimin-gado	2	KAN	NG	1	2017-10-29 17:02:39.223161	2017-10-29 17:02:39.223174
57	Rogo	\N	rogo	2	KAN	NG	1	2017-10-29 17:02:39.238241	2017-10-29 17:02:39.238252
58	Shanono	\N	shanono	2	KAN	NG	1	2017-10-29 17:02:39.252391	2017-10-29 17:02:39.252403
59	Sumaila	\N	sumaila	2	KAN	NG	1	2017-10-29 17:02:39.263165	2017-10-29 17:02:39.263177
60	Takai	\N	takai	2	KAN	NG	1	2017-10-29 17:02:39.27447	2017-10-29 17:02:39.274483
61	Tarauni	\N	tarauni	2	KAN	NG	1	2017-10-29 17:02:39.284485	2017-10-29 17:02:39.284497
62	Tofa	\N	tofa	2	KAN	NG	1	2017-10-29 17:02:39.298245	2017-10-29 17:02:39.298267
63	Tsanyawa	\N	tsanyawa	2	KAN	NG	1	2017-10-29 17:02:39.30999	2017-10-29 17:02:39.310004
64	Tudun Wada	\N	tudun-wada	2	KAN	NG	1	2017-10-29 17:02:39.321656	2017-10-29 17:02:39.321669
65	Ungogo	\N	ungogo	2	KAN	NG	1	2017-10-29 17:02:39.337481	2017-10-29 17:02:39.337494
66	Warawa	\N	warawa	2	KAN	NG	1	2017-10-29 17:02:39.347503	2017-10-29 17:02:39.347513
67	Wudil	\N	wudil	2	KAN	NG	1	2017-10-29 17:02:39.357325	2017-10-29 17:02:39.357335
68	Agatu	\N	agatu	3	BNE	NG	1	2017-10-29 17:02:39.372154	2017-10-29 17:02:39.372166
69	Apa	\N	apa	3	BNE	NG	1	2017-10-29 17:02:39.381675	2017-10-29 17:02:39.381685
70	Ado	\N	ado	3	BNE	NG	1	2017-10-29 17:02:39.391079	2017-10-29 17:02:39.391089
71	Buruku	\N	buruku	3	BNE	NG	1	2017-10-29 17:02:39.42013	2017-10-29 17:02:39.420141
72	Gboko	\N	gboko	3	BNE	NG	1	2017-10-29 17:02:39.431015	2017-10-29 17:02:39.431027
73	Guma	\N	guma	3	BNE	NG	1	2017-10-29 17:02:39.441382	2017-10-29 17:02:39.441394
74	Gwer East	\N	gwer-east	3	BNE	NG	1	2017-10-29 17:02:39.451687	2017-10-29 17:02:39.451699
75	Gwer West	\N	gwer-west	3	BNE	NG	1	2017-10-29 17:02:39.461933	2017-10-29 17:02:39.461952
76	Katsina-Ala	\N	katsina-ala	3	BNE	NG	1	2017-10-29 17:02:39.474166	2017-10-29 17:02:39.474177
77	Konshisha	\N	konshisha	3	BNE	NG	1	2017-10-29 17:02:39.484557	2017-10-29 17:02:39.48457
78	Kwande	\N	kwande	3	BNE	NG	1	2017-10-29 17:02:39.498895	2017-10-29 17:02:39.49891
79	Logo	\N	logo	3	BNE	NG	1	2017-10-29 17:02:39.512786	2017-10-29 17:02:39.512804
80	Makurdi	\N	makurdi	3	BNE	NG	1	2017-10-29 17:02:39.526594	2017-10-29 17:02:39.526607
81	Obi, Benue State	\N	obi-benue-state	3	BNE	NG	1	2017-10-29 17:02:39.53991	2017-10-29 17:02:39.53992
82	Ogbadibo	\N	ogbadibo	3	BNE	NG	1	2017-10-29 17:02:39.552861	2017-10-29 17:02:39.552876
83	Ohimini	\N	ohimini	3	BNE	NG	1	2017-10-29 17:02:39.565852	2017-10-29 17:02:39.565867
84	Oju	\N	oju	3	BNE	NG	1	2017-10-29 17:02:39.582557	2017-10-29 17:02:39.582574
85	Okpokwu	\N	okpokwu	3	BNE	NG	1	2017-10-29 17:02:39.60609	2017-10-29 17:02:39.606113
86	Oturkpo	\N	oturkpo	3	BNE	NG	1	2017-10-29 17:02:39.623156	2017-10-29 17:02:39.623169
87	Tarka	\N	tarka	3	BNE	NG	1	2017-10-29 17:02:39.638056	2017-10-29 17:02:39.638072
88	Ukum	\N	ukum	3	BNE	NG	1	2017-10-29 17:02:39.653992	2017-10-29 17:02:39.654005
89	Ushongo	\N	ushongo	3	BNE	NG	1	2017-10-29 17:02:39.667627	2017-10-29 17:02:39.667641
90	Vandeikya	\N	vandeikya	3	BNE	NG	1	2017-10-29 17:02:39.682934	2017-10-29 17:02:39.68295
91	Akko	\N	akko	4	GMB	NG	1	2017-10-29 17:02:39.696968	2017-10-29 17:02:39.696984
92	Balanga	\N	balanga	4	GMB	NG	1	2017-10-29 17:02:39.71308	2017-10-29 17:02:39.713095
93	Billiri	\N	billiri	4	GMB	NG	1	2017-10-29 17:02:39.729633	2017-10-29 17:02:39.729647
94	Dukku	\N	dukku	4	GMB	NG	1	2017-10-29 17:02:39.741344	2017-10-29 17:02:39.741357
95	Funakaye	\N	funakaye	4	GMB	NG	1	2017-10-29 17:02:39.756229	2017-10-29 17:02:39.756253
96	Gombe	\N	gombe	4	GMB	NG	1	2017-10-29 17:02:39.774108	2017-10-29 17:02:39.774122
97	Kaltungo	\N	kaltungo	4	GMB	NG	1	2017-10-29 17:02:39.786261	2017-10-29 17:02:39.786275
98	Kwami	\N	kwami	4	GMB	NG	1	2017-10-29 17:02:39.799068	2017-10-29 17:02:39.799082
99	Nafada	\N	nafada	4	GMB	NG	1	2017-10-29 17:02:39.813488	2017-10-29 17:02:39.813504
100	Shongom	\N	shongom	4	GMB	NG	1	2017-10-29 17:02:39.831235	2017-10-29 17:02:39.831253
101	Yamaltu/Deba	\N	yamaltu-deba	4	GMB	NG	1	2017-10-29 17:02:39.845874	2017-10-29 17:02:39.845892
102	Abakaliki	\N	abakaliki	5	EBN	NG	1	2017-10-29 17:02:39.859705	2017-10-29 17:02:39.85972
103	Afikpo North	\N	afikpo-north	5	EBN	NG	1	2017-10-29 17:02:39.876231	2017-10-29 17:02:39.876259
104	Afikpo South	\N	afikpo-south	5	EBN	NG	1	2017-10-29 17:02:39.892395	2017-10-29 17:02:39.892426
105	Ebonyi	\N	ebonyi	5	EBN	NG	1	2017-10-29 17:02:39.906407	2017-10-29 17:02:39.906422
106	Ezza North	\N	ezza-north	5	EBN	NG	1	2017-10-29 17:02:39.918403	2017-10-29 17:02:39.918417
107	Ezza South	\N	ezza-south	5	EBN	NG	1	2017-10-29 17:02:39.931073	2017-10-29 17:02:39.931086
108	Ikwo	\N	ikwo	5	EBN	NG	1	2017-10-29 17:02:39.9455	2017-10-29 17:02:39.945514
109	Ishielu	\N	ishielu	5	EBN	NG	1	2017-10-29 17:02:39.960648	2017-10-29 17:02:39.960661
110	Ivo	\N	ivo	5	EBN	NG	1	2017-10-29 17:02:39.975605	2017-10-29 17:02:39.975627
111	Izzi	\N	izzi	5	EBN	NG	1	2017-10-29 17:02:39.992407	2017-10-29 17:02:39.992423
112	Ohaozara	\N	ohaozara	5	EBN	NG	1	2017-10-29 17:02:40.010048	2017-10-29 17:02:40.010066
113	Ohaukwu	\N	ohaukwu	5	EBN	NG	1	2017-10-29 17:02:40.027328	2017-10-29 17:02:40.027345
114	Onicha	\N	onicha	5	EBN	NG	1	2017-10-29 17:02:40.045692	2017-10-29 17:02:40.045736
115	Aniocha North	\N	aniocha-north	6	DEL	NG	1	2017-10-29 17:02:40.060295	2017-10-29 17:02:40.060307
116	Aniocha South	\N	aniocha-south	6	DEL	NG	1	2017-10-29 17:02:40.077495	2017-10-29 17:02:40.077516
117	Bomadi	\N	bomadi	6	DEL	NG	1	2017-10-29 17:02:40.08991	2017-10-29 17:02:40.089925
118	Burutu	\N	burutu	6	DEL	NG	1	2017-10-29 17:02:40.108277	2017-10-29 17:02:40.108296
119	Ethiope East	\N	ethiope-east	6	DEL	NG	1	2017-10-29 17:02:40.123362	2017-10-29 17:02:40.123379
120	Ethiope West	\N	ethiope-west	6	DEL	NG	1	2017-10-29 17:02:40.146778	2017-10-29 17:02:40.146794
121	Ika North East	\N	ika-north-east	6	DEL	NG	1	2017-10-29 17:02:40.15961	2017-10-29 17:02:40.159625
122	Ika South	\N	ika-south	6	DEL	NG	1	2017-10-29 17:02:40.171601	2017-10-29 17:02:40.171615
123	Isoko North	\N	isoko-north	6	DEL	NG	1	2017-10-29 17:02:40.185414	2017-10-29 17:02:40.185437
124	Isoko South	\N	isoko-south	6	DEL	NG	1	2017-10-29 17:02:40.206377	2017-10-29 17:02:40.206392
125	Ndokwa East	\N	ndokwa-east	6	DEL	NG	1	2017-10-29 17:02:40.227348	2017-10-29 17:02:40.227363
126	Ndokwa West	\N	ndokwa-west	6	DEL	NG	1	2017-10-29 17:02:40.263663	2017-10-29 17:02:40.263685
127	Okpe	\N	okpe	6	DEL	NG	1	2017-10-29 17:02:40.285435	2017-10-29 17:02:40.285451
128	Oshimili North	\N	oshimili-north	6	DEL	NG	1	2017-10-29 17:02:40.301652	2017-10-29 17:02:40.30167
129	Oshimili South	\N	oshimili-south	6	DEL	NG	1	2017-10-29 17:02:40.316082	2017-10-29 17:02:40.316096
130	Patani	\N	patani	6	DEL	NG	1	2017-10-29 17:02:40.330479	2017-10-29 17:02:40.33055
131	Sapele, Delta	\N	sapele-delta	6	DEL	NG	1	2017-10-29 17:02:40.344399	2017-10-29 17:02:40.344416
132	Udu	\N	udu	6	DEL	NG	1	2017-10-29 17:02:40.359854	2017-10-29 17:02:40.359868
133	Ughelli North	\N	ughelli-north	6	DEL	NG	1	2017-10-29 17:02:40.372676	2017-10-29 17:02:40.372688
134	Ughelli South	\N	ughelli-south	6	DEL	NG	1	2017-10-29 17:02:40.387083	2017-10-29 17:02:40.387096
135	Ukwuani	\N	ukwuani	6	DEL	NG	1	2017-10-29 17:02:40.401169	2017-10-29 17:02:40.401183
136	Uvwie	\N	uvwie	6	DEL	NG	1	2017-10-29 17:02:40.423006	2017-10-29 17:02:40.423037
137	Warri North	\N	warri-north	6	DEL	NG	1	2017-10-29 17:02:40.440933	2017-10-29 17:02:40.440954
138	Warri South	\N	warri-south	6	DEL	NG	1	2017-10-29 17:02:40.453952	2017-10-29 17:02:40.453962
139	Warri South West	\N	warri-south-west	6	DEL	NG	1	2017-10-29 17:02:40.46413	2017-10-29 17:02:40.46414
140	Bade	\N	bade	7	YBE	NG	1	2017-10-29 17:02:40.474867	2017-10-29 17:02:40.474877
141	Bursari	\N	bursari	7	YBE	NG	1	2017-10-29 17:02:40.490278	2017-10-29 17:02:40.49029
142	Damaturu	\N	damaturu	7	YBE	NG	1	2017-10-29 17:02:40.500859	2017-10-29 17:02:40.500871
143	Fika	\N	fika	7	YBE	NG	1	2017-10-29 17:02:40.511016	2017-10-29 17:02:40.511028
144	Fune	\N	fune	7	YBE	NG	1	2017-10-29 17:02:40.522816	2017-10-29 17:02:40.522831
145	Geidam	\N	geidam	7	YBE	NG	1	2017-10-29 17:02:40.533998	2017-10-29 17:02:40.534011
146	Gujba	\N	gujba	7	YBE	NG	1	2017-10-29 17:02:40.544567	2017-10-29 17:02:40.54458
147	Gulani	\N	gulani	7	YBE	NG	1	2017-10-29 17:02:40.55596	2017-10-29 17:02:40.555973
148	Jakusko	\N	jakusko	7	YBE	NG	1	2017-10-29 17:02:40.570152	2017-10-29 17:02:40.570172
149	Karasuwa	\N	karasuwa	7	YBE	NG	1	2017-10-29 17:02:40.582554	2017-10-29 17:02:40.58257
150	Machina	\N	machina	7	YBE	NG	1	2017-10-29 17:02:40.593893	2017-10-29 17:02:40.593907
151	Nangere	\N	nangere	7	YBE	NG	1	2017-10-29 17:02:40.606054	2017-10-29 17:02:40.606076
152	Nguru	\N	nguru	7	YBE	NG	1	2017-10-29 17:02:40.621051	2017-10-29 17:02:40.621066
153	Potiskum	\N	potiskum	7	YBE	NG	1	2017-10-29 17:02:40.633014	2017-10-29 17:02:40.633028
154	Tarmuwa	\N	tarmuwa	7	YBE	NG	1	2017-10-29 17:02:40.645945	2017-10-29 17:02:40.645962
155	Yunusari	\N	yunusari	7	YBE	NG	1	2017-10-29 17:02:40.660887	2017-10-29 17:02:40.660902
156	Yusufari	\N	yusufari	7	YBE	NG	1	2017-10-29 17:02:40.674648	2017-10-29 17:02:40.674662
157	Anka	\N	anka	8	ZFR	NG	1	2017-10-29 17:02:40.688582	2017-10-29 17:02:40.688607
158	Bakura	\N	bakura	8	ZFR	NG	1	2017-10-29 17:02:40.702176	2017-10-29 17:02:40.702192
159	Birnin Magaji/Kiyaw	\N	birnin-magaji-kiyaw	8	ZFR	NG	1	2017-10-29 17:02:40.715707	2017-10-29 17:02:40.715722
160	Bukkuyum	\N	bukkuyum	8	ZFR	NG	1	2017-10-29 17:02:40.742866	2017-10-29 17:02:40.742899
161	Bungudu	\N	bungudu	8	ZFR	NG	1	2017-10-29 17:02:40.756727	2017-10-29 17:02:40.756739
162	Gummi	\N	gummi	8	ZFR	NG	1	2017-10-29 17:02:40.77436	2017-10-29 17:02:40.77438
163	Gusau	\N	gusau	8	ZFR	NG	1	2017-10-29 17:02:40.791075	2017-10-29 17:02:40.791088
164	Kaura Namoda	\N	kaura-namoda	8	ZFR	NG	1	2017-10-29 17:02:40.806182	2017-10-29 17:02:40.806198
165	Maradun	\N	maradun	8	ZFR	NG	1	2017-10-29 17:02:40.822357	2017-10-29 17:02:40.822372
166	Maru	\N	maru	8	ZFR	NG	1	2017-10-29 17:02:40.835129	2017-10-29 17:02:40.835151
167	Shinkafi	\N	shinkafi	8	ZFR	NG	1	2017-10-29 17:02:40.846948	2017-10-29 17:02:40.846963
168	Talata Mafara	\N	talata-mafara	8	ZFR	NG	1	2017-10-29 17:02:40.858979	2017-10-29 17:02:40.859001
169	Chafe	\N	chafe	8	ZFR	NG	1	2017-10-29 17:02:40.874012	2017-10-29 17:02:40.874025
170	Zurmi	\N	zurmi	8	ZFR	NG	1	2017-10-29 17:02:40.886699	2017-10-29 17:02:40.886713
171	Ado Ekiti	\N	ado-ekiti	9	EKI	NG	1	2017-10-29 17:02:40.899867	2017-10-29 17:02:40.899882
172	Efon	\N	efon	9	EKI	NG	1	2017-10-29 17:02:40.912417	2017-10-29 17:02:40.912431
173	Ekiti East	\N	ekiti-east	9	EKI	NG	1	2017-10-29 17:02:40.925413	2017-10-29 17:02:40.925429
174	Ekiti South-West	\N	ekiti-south-west	9	EKI	NG	1	2017-10-29 17:02:40.938957	2017-10-29 17:02:40.93897
175	Ekiti West	\N	ekiti-west	9	EKI	NG	1	2017-10-29 17:02:40.950619	2017-10-29 17:02:40.950633
176	Emure	\N	emure	9	EKI	NG	1	2017-10-29 17:02:40.964287	2017-10-29 17:02:40.964301
177	Gbonyin	\N	gbonyin	9	EKI	NG	1	2017-10-29 17:02:40.978369	2017-10-29 17:02:40.978393
178	Ido Osi	\N	ido-osi	9	EKI	NG	1	2017-10-29 17:02:40.996476	2017-10-29 17:02:40.996489
179	Ijero	\N	ijero	9	EKI	NG	1	2017-10-29 17:02:41.0133	2017-10-29 17:02:41.013316
180	Ikere	\N	ikere	9	EKI	NG	1	2017-10-29 17:02:41.025949	2017-10-29 17:02:41.025963
181	Ikole	\N	ikole	9	EKI	NG	1	2017-10-29 17:02:41.037177	2017-10-29 17:02:41.037191
182	Ilejemeje	\N	ilejemeje	9	EKI	NG	1	2017-10-29 17:02:41.054427	2017-10-29 17:02:41.054441
183	Irepodun/Ifelodun	\N	irepodun-ifelodun	9	EKI	NG	1	2017-10-29 17:02:41.065741	2017-10-29 17:02:41.065759
184	Ise/Orun	\N	ise-orun	9	EKI	NG	1	2017-10-29 17:02:41.08331	2017-10-29 17:02:41.083326
185	Moba	\N	moba	9	EKI	NG	1	2017-10-29 17:02:41.098068	2017-10-29 17:02:41.098088
186	Oye	\N	oye	9	EKI	NG	1	2017-10-29 17:02:41.112737	2017-10-29 17:02:41.112788
187	Akoko-Edo	\N	akoko-edo	10	EDO	NG	1	2017-10-29 17:02:41.12533	2017-10-29 17:02:41.125345
188	Egor	\N	egor	10	EDO	NG	1	2017-10-29 17:02:41.138994	2017-10-29 17:02:41.13901
189	Esan Central	\N	esan-central	10	EDO	NG	1	2017-10-29 17:02:41.154073	2017-10-29 17:02:41.154086
190	Esan North-East	\N	esan-north-east	10	EDO	NG	1	2017-10-29 17:02:41.164291	2017-10-29 17:02:41.164301
191	Esan South-East	\N	esan-south-east	10	EDO	NG	1	2017-10-29 17:02:41.173855	2017-10-29 17:02:41.173868
192	Esan West	\N	esan-west	10	EDO	NG	1	2017-10-29 17:02:41.184362	2017-10-29 17:02:41.184373
193	Etsako Central	\N	etsako-central	10	EDO	NG	1	2017-10-29 17:02:41.197422	2017-10-29 17:02:41.197436
194	Etsako East	\N	etsako-east	10	EDO	NG	1	2017-10-29 17:02:41.21113	2017-10-29 17:02:41.211143
195	Etsako West	\N	etsako-west	10	EDO	NG	1	2017-10-29 17:02:41.221905	2017-10-29 17:02:41.221918
196	Igueben	\N	igueben	10	EDO	NG	1	2017-10-29 17:02:41.232731	2017-10-29 17:02:41.232743
197	Ikpoba Okha	\N	ikpoba-okha	10	EDO	NG	1	2017-10-29 17:02:41.245251	2017-10-29 17:02:41.245263
198	Orhionmwon	\N	orhionmwon	10	EDO	NG	1	2017-10-29 17:02:41.254962	2017-10-29 17:02:41.254972
199	Oredo	\N	oredo	10	EDO	NG	1	2017-10-29 17:02:41.267365	2017-10-29 17:02:41.267395
200	Ovia North-East	\N	ovia-north-east	10	EDO	NG	1	2017-10-29 17:02:41.285286	2017-10-29 17:02:41.285298
201	Ovia South-West	\N	ovia-south-west	10	EDO	NG	1	2017-10-29 17:02:41.299429	2017-10-29 17:02:41.299446
202	Owan East	\N	owan-east	10	EDO	NG	1	2017-10-29 17:02:41.319223	2017-10-29 17:02:41.319238
203	Owan West	\N	owan-west	10	EDO	NG	1	2017-10-29 17:02:41.336226	2017-10-29 17:02:41.336246
204	Uhunmwonde	\N	uhunmwonde	10	EDO	NG	1	2017-10-29 17:02:41.35106	2017-10-29 17:02:41.351073
205	Abi	\N	abi	11	CRI	NG	1	2017-10-29 17:02:41.363228	2017-10-29 17:02:41.363248
206	Akamkpa	\N	akamkpa	11	CRI	NG	1	2017-10-29 17:02:41.376522	2017-10-29 17:02:41.376533
207	Akpabuyo	\N	akpabuyo	11	CRI	NG	1	2017-10-29 17:02:41.389191	2017-10-29 17:02:41.389204
208	Bakassi	\N	bakassi	11	CRI	NG	1	2017-10-29 17:02:41.404629	2017-10-29 17:02:41.404642
209	Bekwarra	\N	bekwarra	11	CRI	NG	1	2017-10-29 17:02:41.419567	2017-10-29 17:02:41.419582
210	Biase	\N	biase	11	CRI	NG	1	2017-10-29 17:02:41.434538	2017-10-29 17:02:41.43455
211	Boki	\N	boki	11	CRI	NG	1	2017-10-29 17:02:41.446589	2017-10-29 17:02:41.446604
212	Calabar Municipal	\N	calabar-municipal	11	CRI	NG	1	2017-10-29 17:02:41.460619	2017-10-29 17:02:41.460632
213	Calabar South	\N	calabar-south	11	CRI	NG	1	2017-10-29 17:02:41.473679	2017-10-29 17:02:41.473689
214	Etung	\N	etung	11	CRI	NG	1	2017-10-29 17:02:41.486196	2017-10-29 17:02:41.486209
215	Ikom	\N	ikom	11	CRI	NG	1	2017-10-29 17:02:41.49948	2017-10-29 17:02:41.499492
216	Obanliku	\N	obanliku	11	CRI	NG	1	2017-10-29 17:02:41.51664	2017-10-29 17:02:41.516653
217	Obubra	\N	obubra	11	CRI	NG	1	2017-10-29 17:02:41.531714	2017-10-29 17:02:41.531724
218	Obudu	\N	obudu	11	CRI	NG	1	2017-10-29 17:02:41.543117	2017-10-29 17:02:41.543129
219	Odukpani	\N	odukpani	11	CRI	NG	1	2017-10-29 17:02:41.55409	2017-10-29 17:02:41.554103
220	Ogoja	\N	ogoja	11	CRI	NG	1	2017-10-29 17:02:41.569957	2017-10-29 17:02:41.56997
221	Yakuur	\N	yakuur	11	CRI	NG	1	2017-10-29 17:02:41.586706	2017-10-29 17:02:41.586724
222	Yala	\N	yala	11	CRI	NG	1	2017-10-29 17:02:41.599353	2017-10-29 17:02:41.599365
223	Auyo	\N	auyo	12	JIG	NG	1	2017-10-29 17:02:41.613233	2017-10-29 17:02:41.61325
224	Babura	\N	babura	12	JIG	NG	1	2017-10-29 17:02:41.62926	2017-10-29 17:02:41.629271
225	Biriniwa	\N	biriniwa	12	JIG	NG	1	2017-10-29 17:02:41.641544	2017-10-29 17:02:41.641558
226	Birnin Kudu	\N	birnin-kudu	12	JIG	NG	1	2017-10-29 17:02:41.656213	2017-10-29 17:02:41.656228
227	Buji	\N	buji	12	JIG	NG	1	2017-10-29 17:02:41.671382	2017-10-29 17:02:41.671402
228	Dutse	\N	dutse	12	JIG	NG	1	2017-10-29 17:02:41.69021	2017-10-29 17:02:41.690228
229	Gagarawa	\N	gagarawa	12	JIG	NG	1	2017-10-29 17:02:41.70616	2017-10-29 17:02:41.706178
230	Garki	\N	garki	12	JIG	NG	1	2017-10-29 17:02:41.726325	2017-10-29 17:02:41.726346
231	Gumel	\N	gumel	12	JIG	NG	1	2017-10-29 17:02:41.738832	2017-10-29 17:02:41.738845
232	Guri	\N	guri	12	JIG	NG	1	2017-10-29 17:02:41.750305	2017-10-29 17:02:41.750318
233	Gwaram	\N	gwaram	12	JIG	NG	1	2017-10-29 17:02:41.762247	2017-10-29 17:02:41.76226
234	Gwiwa	\N	gwiwa	12	JIG	NG	1	2017-10-29 17:02:41.775083	2017-10-29 17:02:41.775096
235	Hadejia	\N	hadejia	12	JIG	NG	1	2017-10-29 17:02:41.788516	2017-10-29 17:02:41.788529
236	Jahun	\N	jahun	12	JIG	NG	1	2017-10-29 17:02:41.805994	2017-10-29 17:02:41.806006
237	Kafin Hausa	\N	kafin-hausa	12	JIG	NG	1	2017-10-29 17:02:41.818973	2017-10-29 17:02:41.819005
238	Kazaure	\N	kazaure	12	JIG	NG	1	2017-10-29 17:02:41.835737	2017-10-29 17:02:41.83575
239	Kiri Kasama	\N	kiri-kasama	12	JIG	NG	1	2017-10-29 17:02:41.849876	2017-10-29 17:02:41.849886
240	Kiyawa	\N	kiyawa	12	JIG	NG	1	2017-10-29 17:02:41.864791	2017-10-29 17:02:41.864804
241	Kaugama	\N	kaugama	12	JIG	NG	1	2017-10-29 17:02:41.877176	2017-10-29 17:02:41.877188
242	Maigatari	\N	maigatari	12	JIG	NG	1	2017-10-29 17:02:41.890605	2017-10-29 17:02:41.890617
243	Malam Madori	\N	malam-madori	12	JIG	NG	1	2017-10-29 17:02:41.903655	2017-10-29 17:02:41.903667
244	Miga	\N	miga	12	JIG	NG	1	2017-10-29 17:02:41.918957	2017-10-29 17:02:41.918968
245	Ringim	\N	ringim	12	JIG	NG	1	2017-10-29 17:02:41.932451	2017-10-29 17:02:41.932463
246	Roni	\N	roni	12	JIG	NG	1	2017-10-29 17:02:41.946147	2017-10-29 17:02:41.94616
247	Sule Tankarkar	\N	sule-tankarkar	12	JIG	NG	1	2017-10-29 17:02:41.959951	2017-10-29 17:02:41.959963
248	Taura	\N	taura	12	JIG	NG	1	2017-10-29 17:02:41.976801	2017-10-29 17:02:41.976812
249	Yankwashi	\N	yankwashi	12	JIG	NG	1	2017-10-29 17:02:41.989496	2017-10-29 17:02:41.989507
250	Abadam	\N	abadam	13	BOR	NG	1	2017-10-29 17:02:42.000199	2017-10-29 17:02:42.000209
251	Askira/Uba	\N	askira-uba	13	BOR	NG	1	2017-10-29 17:02:42.012409	2017-10-29 17:02:42.012433
252	Bama	\N	bama	13	BOR	NG	1	2017-10-29 17:02:42.025925	2017-10-29 17:02:42.025938
253	Bayo	\N	bayo	13	BOR	NG	1	2017-10-29 17:02:42.035814	2017-10-29 17:02:42.035825
254	Biu	\N	biu	13	BOR	NG	1	2017-10-29 17:02:42.045447	2017-10-29 17:02:42.045458
255	Chibok	\N	chibok	13	BOR	NG	1	2017-10-29 17:02:42.055067	2017-10-29 17:02:42.055078
256	Damboa	\N	damboa	13	BOR	NG	1	2017-10-29 17:02:42.065408	2017-10-29 17:02:42.065418
257	Dikwa	\N	dikwa	13	BOR	NG	1	2017-10-29 17:02:42.075101	2017-10-29 17:02:42.075111
258	Gubio	\N	gubio	13	BOR	NG	1	2017-10-29 17:02:42.084677	2017-10-29 17:02:42.084686
259	Guzamala	\N	guzamala	13	BOR	NG	1	2017-10-29 17:02:42.097542	2017-10-29 17:02:42.097553
260	Gwoza	\N	gwoza	13	BOR	NG	1	2017-10-29 17:02:42.107704	2017-10-29 17:02:42.107714
261	Hawul	\N	hawul	13	BOR	NG	1	2017-10-29 17:02:42.116906	2017-10-29 17:02:42.116916
262	Jere	\N	jere	13	BOR	NG	1	2017-10-29 17:02:42.126624	2017-10-29 17:02:42.126634
263	Kaga	\N	kaga	13	BOR	NG	1	2017-10-29 17:02:42.136491	2017-10-29 17:02:42.136501
264	Kala/Balge	\N	kala-balge	13	BOR	NG	1	2017-10-29 17:02:42.152866	2017-10-29 17:02:42.152884
265	Konduga	\N	konduga	13	BOR	NG	1	2017-10-29 17:02:42.162888	2017-10-29 17:02:42.162898
266	Kukawa	\N	kukawa	13	BOR	NG	1	2017-10-29 17:02:42.172879	2017-10-29 17:02:42.17289
267	Kwaya Kusar	\N	kwaya-kusar	13	BOR	NG	1	2017-10-29 17:02:42.182622	2017-10-29 17:02:42.182632
268	Mafa	\N	mafa	13	BOR	NG	1	2017-10-29 17:02:42.197226	2017-10-29 17:02:42.19724
269	Magumeri	\N	magumeri	13	BOR	NG	1	2017-10-29 17:02:42.209083	2017-10-29 17:02:42.209097
270	Maiduguri	\N	maiduguri	13	BOR	NG	1	2017-10-29 17:02:42.22379	2017-10-29 17:02:42.22381
271	Marte	\N	marte	13	BOR	NG	1	2017-10-29 17:02:42.237782	2017-10-29 17:02:42.237795
272	Mobbar	\N	mobbar	13	BOR	NG	1	2017-10-29 17:02:42.248459	2017-10-29 17:02:42.248469
273	Monguno	\N	monguno	13	BOR	NG	1	2017-10-29 17:02:42.258142	2017-10-29 17:02:42.258153
274	Ngala	\N	ngala	13	BOR	NG	1	2017-10-29 17:02:42.268895	2017-10-29 17:02:42.268911
275	Nganzai	\N	nganzai	13	BOR	NG	1	2017-10-29 17:02:42.284109	2017-10-29 17:02:42.284128
276	Shani	\N	shani	13	BOR	NG	1	2017-10-29 17:02:42.302109	2017-10-29 17:02:42.302125
277	Abua/Odual	\N	abua-odual	14	RVS	NG	1	2017-10-29 17:02:42.316009	2017-10-29 17:02:42.316024
278	Ahoada East	\N	ahoada-east	14	RVS	NG	1	2017-10-29 17:02:42.329545	2017-10-29 17:02:42.329561
279	Ahoada West	\N	ahoada-west	14	RVS	NG	1	2017-10-29 17:02:42.344371	2017-10-29 17:02:42.344384
280	Akuku-Toru	\N	akuku-toru	14	RVS	NG	1	2017-10-29 17:02:42.356156	2017-10-29 17:02:42.356168
281	Andoni	\N	andoni	14	RVS	NG	1	2017-10-29 17:02:42.372114	2017-10-29 17:02:42.372128
282	Asari-Toru	\N	asari-toru	14	RVS	NG	1	2017-10-29 17:02:42.386069	2017-10-29 17:02:42.386083
283	Bonny	\N	bonny	14	RVS	NG	1	2017-10-29 17:02:42.401531	2017-10-29 17:02:42.401545
284	Degema	\N	degema	14	RVS	NG	1	2017-10-29 17:02:42.417961	2017-10-29 17:02:42.417992
285	Eleme	\N	eleme	14	RVS	NG	1	2017-10-29 17:02:42.429965	2017-10-29 17:02:42.429978
286	Emuoha	\N	emuoha	14	RVS	NG	1	2017-10-29 17:02:42.441246	2017-10-29 17:02:42.441259
287	Etche	\N	etche	14	RVS	NG	1	2017-10-29 17:02:42.453395	2017-10-29 17:02:42.453407
288	Gokana	\N	gokana	14	RVS	NG	1	2017-10-29 17:02:42.46529	2017-10-29 17:02:42.465303
289	Ikwerre	\N	ikwerre	14	RVS	NG	1	2017-10-29 17:02:42.476417	2017-10-29 17:02:42.476429
290	Khana	\N	khana	14	RVS	NG	1	2017-10-29 17:02:42.487208	2017-10-29 17:02:42.48722
291	Obio/Akpor	\N	obio-akpor	14	RVS	NG	1	2017-10-29 17:02:42.502735	2017-10-29 17:02:42.502751
292	Ogba/Egbema/Ndoni	\N	ogba-egbema-ndoni	14	RVS	NG	1	2017-10-29 17:02:42.521749	2017-10-29 17:02:42.521771
293	Ogu/Bolo	\N	ogu-bolo	14	RVS	NG	1	2017-10-29 17:02:42.537807	2017-10-29 17:02:42.537824
294	Okrika	\N	okrika	14	RVS	NG	1	2017-10-29 17:02:42.552113	2017-10-29 17:02:42.552127
295	Omuma	\N	omuma	14	RVS	NG	1	2017-10-29 17:02:42.572327	2017-10-29 17:02:42.572343
296	Opobo/Nkoro	\N	opobo-nkoro	14	RVS	NG	1	2017-10-29 17:02:42.589464	2017-10-29 17:02:42.589479
297	Oyigbo	\N	oyigbo	14	RVS	NG	1	2017-10-29 17:02:42.605844	2017-10-29 17:02:42.605865
298	Port Harcourt	\N	port-harcourt	14	RVS	NG	1	2017-10-29 17:02:42.622433	2017-10-29 17:02:42.622453
299	Tai	\N	tai	14	RVS	NG	1	2017-10-29 17:02:42.636833	2017-10-29 17:02:42.636845
300	Bokkos	\N	bokkos	15	PLT	NG	1	2017-10-29 17:02:42.655672	2017-10-29 17:02:42.655692
301	Barkin Ladi	\N	barkin-ladi	15	PLT	NG	1	2017-10-29 17:02:42.675273	2017-10-29 17:02:42.675292
302	Bassa	\N	bassa	15	PLT	NG	1	2017-10-29 17:02:42.690378	2017-10-29 17:02:42.690396
303	Jos East	\N	jos-east	15	PLT	NG	1	2017-10-29 17:02:42.706592	2017-10-29 17:02:42.706604
304	Jos North	\N	jos-north	15	PLT	NG	1	2017-10-29 17:02:42.720161	2017-10-29 17:02:42.720176
305	Jos South	\N	jos-south	15	PLT	NG	1	2017-10-29 17:02:42.73394	2017-10-29 17:02:42.733953
306	Kanam	\N	kanam	15	PLT	NG	1	2017-10-29 17:02:42.744149	2017-10-29 17:02:42.74416
307	Kanke	\N	kanke	15	PLT	NG	1	2017-10-29 17:02:42.754223	2017-10-29 17:02:42.754235
308	Langtang South	\N	langtang-south	15	PLT	NG	1	2017-10-29 17:02:42.764922	2017-10-29 17:02:42.764934
309	Langtang North	\N	langtang-north	15	PLT	NG	1	2017-10-29 17:02:42.774851	2017-10-29 17:02:42.774862
310	Mangu	\N	mangu	15	PLT	NG	1	2017-10-29 17:02:42.785109	2017-10-29 17:02:42.785122
311	Mikang	\N	mikang	15	PLT	NG	1	2017-10-29 17:02:42.797943	2017-10-29 17:02:42.797958
312	Pankshin	\N	pankshin	15	PLT	NG	1	2017-10-29 17:02:42.809667	2017-10-29 17:02:42.80968
313	Qua''an Pan	\N	qua-an-pan	15	PLT	NG	1	2017-10-29 17:02:42.820195	2017-10-29 17:02:42.820209
314	Riyom	\N	riyom	15	PLT	NG	1	2017-10-29 17:02:42.833742	2017-10-29 17:02:42.833763
315	Shendam	\N	shendam	15	PLT	NG	1	2017-10-29 17:02:42.845649	2017-10-29 17:02:42.845662
316	Wase	\N	wase	15	PLT	NG	1	2017-10-29 17:02:42.85874	2017-10-29 17:02:42.858753
317	Binji	\N	binji	16	SOK	NG	1	2017-10-29 17:02:42.872788	2017-10-29 17:02:42.872805
318	Bodinga	\N	bodinga	16	SOK	NG	1	2017-10-29 17:02:42.885303	2017-10-29 17:02:42.885323
319	Dange Shuni	\N	dange-shuni	16	SOK	NG	1	2017-10-29 17:02:42.900523	2017-10-29 17:02:42.900536
320	Gada	\N	gada	16	SOK	NG	1	2017-10-29 17:02:42.916631	2017-10-29 17:02:42.916648
321	Goronyo	\N	goronyo	16	SOK	NG	1	2017-10-29 17:02:42.932133	2017-10-29 17:02:42.932147
322	Gudu	\N	gudu	16	SOK	NG	1	2017-10-29 17:02:42.945266	2017-10-29 17:02:42.945281
323	Gwadabawa	\N	gwadabawa	16	SOK	NG	1	2017-10-29 17:02:42.957188	2017-10-29 17:02:42.957202
324	Illela	\N	illela	16	SOK	NG	1	2017-10-29 17:02:42.970843	2017-10-29 17:02:42.970863
325	Isa	\N	isa	16	SOK	NG	1	2017-10-29 17:02:42.984565	2017-10-29 17:02:42.98458
326	Kebbe	\N	kebbe	16	SOK	NG	1	2017-10-29 17:02:42.998342	2017-10-29 17:02:42.998358
327	Kware	\N	kware	16	SOK	NG	1	2017-10-29 17:02:43.010137	2017-10-29 17:02:43.01015
328	Rabah	\N	rabah	16	SOK	NG	1	2017-10-29 17:02:43.022699	2017-10-29 17:02:43.022712
329	Sabon Birni	\N	sabon-birni	16	SOK	NG	1	2017-10-29 17:02:43.033523	2017-10-29 17:02:43.033536
330	Shagari	\N	shagari	16	SOK	NG	1	2017-10-29 17:02:43.045263	2017-10-29 17:02:43.045278
331	Silame	\N	silame	16	SOK	NG	1	2017-10-29 17:02:43.060203	2017-10-29 17:02:43.060217
332	Sokoto North	\N	sokoto-north	16	SOK	NG	1	2017-10-29 17:02:43.07526	2017-10-29 17:02:43.075277
333	Sokoto South	\N	sokoto-south	16	SOK	NG	1	2017-10-29 17:02:43.089657	2017-10-29 17:02:43.089674
334	Tambuwal	\N	tambuwal	16	SOK	NG	1	2017-10-29 17:02:43.102599	2017-10-29 17:02:43.102614
335	Tangaza	\N	tangaza	16	SOK	NG	1	2017-10-29 17:02:43.115167	2017-10-29 17:02:43.115191
336	Tureta	\N	tureta	16	SOK	NG	1	2017-10-29 17:02:43.135137	2017-10-29 17:02:43.135154
337	Wamako	\N	wamako	16	SOK	NG	1	2017-10-29 17:02:43.148299	2017-10-29 17:02:43.148314
338	Wurno	\N	wurno	16	SOK	NG	1	2017-10-29 17:02:43.162052	2017-10-29 17:02:43.162065
339	Yabo	\N	yabo	16	SOK	NG	1	2017-10-29 17:02:43.174833	2017-10-29 17:02:43.174846
340	Aninri	\N	aninri	17	ENU	NG	1	2017-10-29 17:02:43.187561	2017-10-29 17:02:43.187574
341	Awgu	\N	awgu	17	ENU	NG	1	2017-10-29 17:02:43.199602	2017-10-29 17:02:43.199615
342	Enugu East	\N	enugu-east	17	ENU	NG	1	2017-10-29 17:02:43.21075	2017-10-29 17:02:43.210762
343	Enugu North	\N	enugu-north	17	ENU	NG	1	2017-10-29 17:02:43.221812	2017-10-29 17:02:43.221827
344	Enugu South	\N	enugu-south	17	ENU	NG	1	2017-10-29 17:02:43.235937	2017-10-29 17:02:43.235952
345	Ezeagu	\N	ezeagu	17	ENU	NG	1	2017-10-29 17:02:43.251953	2017-10-29 17:02:43.251975
346	Igbo Etiti	\N	igbo-etiti	17	ENU	NG	1	2017-10-29 17:02:43.268942	2017-10-29 17:02:43.26897
347	Igbo Eze North	\N	igbo-eze-north	17	ENU	NG	1	2017-10-29 17:02:43.279234	2017-10-29 17:02:43.279245
348	Igbo Eze South	\N	igbo-eze-south	17	ENU	NG	1	2017-10-29 17:02:43.292026	2017-10-29 17:02:43.292041
349	Isi Uzo	\N	isi-uzo	17	ENU	NG	1	2017-10-29 17:02:43.302595	2017-10-29 17:02:43.302607
350	Nkanu East	\N	nkanu-east	17	ENU	NG	1	2017-10-29 17:02:43.312498	2017-10-29 17:02:43.312511
351	Nkanu West	\N	nkanu-west	17	ENU	NG	1	2017-10-29 17:02:43.329871	2017-10-29 17:02:43.329895
352	Nsukka	\N	nsukka	17	ENU	NG	1	2017-10-29 17:02:43.341612	2017-10-29 17:02:43.341626
353	Oji River	\N	oji-river	17	ENU	NG	1	2017-10-29 17:02:43.351698	2017-10-29 17:02:43.351709
354	Udenu	\N	udenu	17	ENU	NG	1	2017-10-29 17:02:43.379795	2017-10-29 17:02:43.379808
355	Udi	\N	udi	17	ENU	NG	1	2017-10-29 17:02:43.391718	2017-10-29 17:02:43.391739
356	Uzo Uwani	\N	uzo-uwani	17	ENU	NG	1	2017-10-29 17:02:43.404928	2017-10-29 17:02:43.404941
357	Abaji	\N	abaji	18	ABV	NG	1	2017-10-29 17:02:43.416555	2017-10-29 17:02:43.416568
358	Bwari	\N	bwari	18	ABV	NG	1	2017-10-29 17:02:43.427242	2017-10-29 17:02:43.427255
359	Gwagwalada	\N	gwagwalada	18	ABV	NG	1	2017-10-29 17:02:43.437856	2017-10-29 17:02:43.437869
360	Kuje	\N	kuje	18	ABV	NG	1	2017-10-29 17:02:43.450358	2017-10-29 17:02:43.45037
361	Kwali	\N	kwali	18	ABV	NG	1	2017-10-29 17:02:43.460826	2017-10-29 17:02:43.460838
362	Municipal Area Council	\N	municipal-area-council	18	ABV	NG	1	2017-10-29 17:02:43.473084	2017-10-29 17:02:43.473106
363	Aboh Mbaise	\N	aboh-mbaise	19	IMO	NG	1	2017-10-29 17:02:43.487112	2017-10-29 17:02:43.487126
364	Ahiazu Mbaise	\N	ahiazu-mbaise	19	IMO	NG	1	2017-10-29 17:02:43.501518	2017-10-29 17:02:43.501532
365	Ehime Mbano	\N	ehime-mbano	19	IMO	NG	1	2017-10-29 17:02:43.52085	2017-10-29 17:02:43.520871
366	Ezinihitte	\N	ezinihitte	19	IMO	NG	1	2017-10-29 17:02:43.535396	2017-10-29 17:02:43.53541
367	Ideato North	\N	ideato-north	19	IMO	NG	1	2017-10-29 17:02:43.546928	2017-10-29 17:02:43.54694
368	Ideato South	\N	ideato-south	19	IMO	NG	1	2017-10-29 17:02:43.558842	2017-10-29 17:02:43.558857
369	Ihitte/Uboma	\N	ihitte-uboma	19	IMO	NG	1	2017-10-29 17:02:43.572573	2017-10-29 17:02:43.572587
370	Ikeduru	\N	ikeduru	19	IMO	NG	1	2017-10-29 17:02:43.586358	2017-10-29 17:02:43.586373
371	Isiala Mbano	\N	isiala-mbano	19	IMO	NG	1	2017-10-29 17:02:43.598352	2017-10-29 17:02:43.598365
372	Isu	\N	isu	19	IMO	NG	1	2017-10-29 17:02:43.616622	2017-10-29 17:02:43.61664
373	Mbaitoli	\N	mbaitoli	19	IMO	NG	1	2017-10-29 17:02:43.630375	2017-10-29 17:02:43.630391
374	Ngor Okpala	\N	ngor-okpala	19	IMO	NG	1	2017-10-29 17:02:43.644919	2017-10-29 17:02:43.644934
375	Njaba	\N	njaba	19	IMO	NG	1	2017-10-29 17:02:43.663671	2017-10-29 17:02:43.663688
376	Nkwerre	\N	nkwerre	19	IMO	NG	1	2017-10-29 17:02:43.687325	2017-10-29 17:02:43.68735
377	Nwangele	\N	nwangele	19	IMO	NG	1	2017-10-29 17:02:43.701049	2017-10-29 17:02:43.701063
378	Obowo	\N	obowo	19	IMO	NG	1	2017-10-29 17:02:43.717719	2017-10-29 17:02:43.717734
379	Oguta	\N	oguta	19	IMO	NG	1	2017-10-29 17:02:43.734543	2017-10-29 17:02:43.734571
380	Ohaji/Egbema	\N	ohaji-egbema	19	IMO	NG	1	2017-10-29 17:02:43.750306	2017-10-29 17:02:43.750319
381	Okigwe	\N	okigwe	19	IMO	NG	1	2017-10-29 17:02:43.765477	2017-10-29 17:02:43.7655
382	Orlu	\N	orlu	19	IMO	NG	1	2017-10-29 17:02:43.780398	2017-10-29 17:02:43.780411
383	Orsu	\N	orsu	19	IMO	NG	1	2017-10-29 17:02:43.791709	2017-10-29 17:02:43.791721
384	Oru East	\N	oru-east	19	IMO	NG	1	2017-10-29 17:02:43.80349	2017-10-29 17:02:43.803502
385	Oru West	\N	oru-west	19	IMO	NG	1	2017-10-29 17:02:43.814582	2017-10-29 17:02:43.814595
386	Owerri Municipal	\N	owerri-municipal	19	IMO	NG	1	2017-10-29 17:02:43.826212	2017-10-29 17:02:43.826224
387	Owerri North	\N	owerri-north	19	IMO	NG	1	2017-10-29 17:02:43.840332	2017-10-29 17:02:43.840346
388	Owerri West	\N	owerri-west	19	IMO	NG	1	2017-10-29 17:02:43.854724	2017-10-29 17:02:43.854737
389	Unuimo	\N	unuimo	19	IMO	NG	1	2017-10-29 17:02:43.866975	2017-10-29 17:02:43.866989
390	Alimosho Akowonjo	\N	alimosho-akowonjo	20	LOS	NG	1	2017-10-29 17:02:43.878571	2017-10-29 17:02:43.878587
391	Agege Sango	\N	agege-sango	20	LOS	NG	1	2017-10-29 17:02:43.893559	2017-10-29 17:02:43.89359
392	Ajeromi Ifelodun Ajegunle	\N	ajeromi-ifelodun-ajegunle	20	LOS	NG	1	2017-10-29 17:02:43.907965	2017-10-29 17:02:43.907979
393	Apapa	\N	apapa	20	LOS	NG	1	2017-10-29 17:02:43.924497	2017-10-29 17:02:43.924523
394	Amuwo Odofin	\N	amuwo-odofin	20	LOS	NG	1	2017-10-29 17:02:43.945437	2017-10-29 17:02:43.945479
395	Badagry	\N	badagry	20	LOS	NG	1	2017-10-29 17:02:43.958714	2017-10-29 17:02:43.958729
396	Ita-Maru Epe	\N	ita-maru-epe	20	LOS	NG	1	2017-10-29 17:02:43.97099	2017-10-29 17:02:43.971004
397	Igbo-Ifon, Eti Osa West	\N	igbo-ifon-eti-osa-west	20	LOS	NG	1	2017-10-29 17:02:43.983407	2017-10-29 17:02:43.983421
398	Lekki	\N	lekki	20	LOS	NG	1	2017-10-29 17:02:43.997926	2017-10-29 17:02:43.997947
399	Ibeju-Lekki	\N	ibeju-lekki	20	LOS	NG	1	2017-10-29 17:02:44.016382	2017-10-29 17:02:44.016402
400	Ifako Ijaiye	\N	ifako-ijaiye	20	LOS	NG	1	2017-10-29 17:02:44.031002	2017-10-29 17:02:44.031016
401	Ikeja	\N	ikeja	20	LOS	NG	1	2017-10-29 17:02:44.042925	2017-10-29 17:02:44.042938
402	Ikorodu	\N	ikorodu	20	LOS	NG	1	2017-10-29 17:02:44.05496	2017-10-29 17:02:44.054974
403	Kosofe Ojota	\N	kosofe-ojota	20	LOS	NG	1	2017-10-29 17:02:44.066387	2017-10-29 17:02:44.0664
404	Lagos Mainland	\N	lagos-mainland	20	LOS	NG	1	2017-10-29 17:02:44.082826	2017-10-29 17:02:44.08284
405	Lagos Island	\N	lagos-island	20	LOS	NG	1	2017-10-29 17:02:44.096332	2017-10-29 17:02:44.096344
406	Mushin	\N	mushin	20	LOS	NG	1	2017-10-29 17:02:44.108274	2017-10-29 17:02:44.108287
407	Ojo	\N	ojo	20	LOS	NG	1	2017-10-29 17:02:44.122473	2017-10-29 17:02:44.122486
408	Oshodi Isolo	\N	oshodi-isolo	20	LOS	NG	1	2017-10-29 17:02:44.137523	2017-10-29 17:02:44.137536
409	Shomolu	\N	shomolu	20	LOS	NG	1	2017-10-29 17:02:44.154066	2017-10-29 17:02:44.15408
410	Surulere	\N	surulere	20	LOS	NG	1	2017-10-29 17:02:44.171381	2017-10-29 17:02:44.171395
411	Abule Egba	\N	abule-egba	20	LOS	NG	1	2017-10-29 17:02:44.18662	2017-10-29 17:02:44.186633
412	Alapere, Ketu	\N	alapere-ketu	20	LOS	NG	1	2017-10-29 17:02:44.201638	2017-10-29 17:02:44.201652
413	Badiya, Apapa Iganmu	\N	badiya-apapa-iganmu	20	LOS	NG	1	2017-10-29 17:02:44.215865	2017-10-29 17:02:44.215877
414	Ayobo, Ipaja	\N	ayobo-ipaja	20	LOS	NG	1	2017-10-29 17:02:44.232768	2017-10-29 17:02:44.232781
415	Badagry West Kankon	\N	badagry-west-kankon	20	LOS	NG	1	2017-10-29 17:02:44.246417	2017-10-29 17:02:44.246436
416	Pedro Bariga	\N	pedro-bariga	20	LOS	NG	1	2017-10-29 17:02:44.262053	2017-10-29 17:02:44.262066
417	Coker Aguda	\N	coker-aguda	20	LOS	NG	1	2017-10-29 17:02:44.276927	2017-10-29 17:02:44.276942
418	Isheri-Olofin, Egbe Idimu	\N	isheri-olofin-egbe-idimu	20	LOS	NG	1	2017-10-29 17:02:44.292685	2017-10-29 17:02:44.292698
419	Eredo, Epe/Ijebu	\N	eredo-epe-ijebu	20	LOS	NG	1	2017-10-29 17:02:44.305771	2017-10-29 17:02:44.305798
420	Oyonka, Iba	\N	oyonka-iba	20	LOS	NG	1	2017-10-29 17:02:44.32118	2017-10-29 17:02:44.321199
421	Eti-Osa East, Ajah	\N	eti-osa-east-ajah	20	LOS	NG	1	2017-10-29 17:02:44.335858	2017-10-29 17:02:44.335873
422	Ifelodun, Amukoko	\N	ifelodun-amukoko	20	LOS	NG	1	2017-10-29 17:02:44.352857	2017-10-29 17:02:44.352872
423	Igbogbo Baiyeku	\N	igbogbo-baiyeku	20	LOS	NG	1	2017-10-29 17:02:44.365679	2017-10-29 17:02:44.365692
424	Ijede, Maidan	\N	ijede-maidan	20	LOS	NG	1	2017-10-29 17:02:44.379591	2017-10-29 17:02:44.379609
425	Odogunyan, Ikorodu North	\N	odogunyan-ikorodu-north	20	LOS	NG	1	2017-10-29 17:02:44.412037	2017-10-29 17:02:44.412056
426	Owutu, Ikorodu West	\N	owutu-ikorodu-west	20	LOS	NG	1	2017-10-29 17:02:44.428384	2017-10-29 17:02:44.428406
427	Ikosi Ejinrin, Agbowa	\N	ikosi-ejinrin-agbowa	20	LOS	NG	1	2017-10-29 17:02:44.444263	2017-10-29 17:02:44.44428
428	Ikosi Isheri	\N	ikosi-isheri	20	LOS	NG	1	2017-10-29 17:02:44.458933	2017-10-29 17:02:44.458948
429	Igando, Ikotun	\N	igando-ikotun	20	LOS	NG	1	2017-10-29 17:02:44.47093	2017-10-29 17:02:44.470942
430	Ikoyi Obalende	\N	ikoyi-obalende	20	LOS	NG	1	2017-10-29 17:02:44.481827	2017-10-29 17:02:44.481838
431	Imota Ebute-Ajebo	\N	imota-ebute-ajebo	20	LOS	NG	1	2017-10-29 17:02:44.49213	2017-10-29 17:02:44.49214
432	Victoria Island	\N	victoria-island	20	LOS	NG	1	2017-10-29 17:02:44.503493	2017-10-29 17:02:44.503505
433	Isolo	\N	isolo	20	LOS	NG	1	2017-10-29 17:02:44.513492	2017-10-29 17:02:44.513503
434	Ikate Itire	\N	ikate-itire	20	LOS	NG	1	2017-10-29 17:02:44.523448	2017-10-29 17:02:44.523458
435	Kakawa, Lagos Island East	\N	kakawa-lagos-island-east	20	LOS	NG	1	2017-10-29 17:02:44.533139	2017-10-29 17:02:44.533149
436	Mosan Okunola	\N	mosan-okunola	20	LOS	NG	1	2017-10-29 17:02:44.543658	2017-10-29 17:02:44.543668
437	Odi-Olowo, Ilupeju	\N	odi-olowo-ilupeju	20	LOS	NG	1	2017-10-29 17:02:44.553294	2017-10-29 17:02:44.553305
438	Oke-Ira, Ojodu	\N	oke-ira-ojodu	20	LOS	NG	1	2017-10-29 17:02:44.563127	2017-10-29 17:02:44.563137
439	Ojokoro, Ijaye	\N	ojokoro-ijaye	20	LOS	NG	1	2017-10-29 17:02:44.57486	2017-10-29 17:02:44.574873
440	Olorunda, Iworo	\N	olorunda-iworo	20	LOS	NG	1	2017-10-29 17:02:44.587221	2017-10-29 17:02:44.587233
441	Onigbongbo, Opebi	\N	onigbongbo-opebi	20	LOS	NG	1	2017-10-29 17:02:44.59725	2017-10-29 17:02:44.597262
442	Oriade Ijegun-Ibasa	\N	oriade-ijegun-ibasa	20	LOS	NG	1	2017-10-29 17:02:44.607033	2017-10-29 17:02:44.607045
443	Abekoko, Orile Agege	\N	abekoko-orile-agege	20	LOS	NG	1	2017-10-29 17:02:44.616894	2017-10-29 17:02:44.616905
444	Oto Awori, Ijaniki	\N	oto-awori-ijaniki	20	LOS	NG	1	2017-10-29 17:02:44.627695	2017-10-29 17:02:44.627707
445	Adekunle, Yaba	\N	adekunle-yaba	20	LOS	NG	1	2017-10-29 17:02:44.637807	2017-10-29 17:02:44.637819
446	Adavi	\N	adavi	21	KGI	NG	1	2017-10-29 17:02:44.648218	2017-10-29 17:02:44.648231
447	Ajaokuta	\N	ajaokuta	21	KGI	NG	1	2017-10-29 17:02:44.658662	2017-10-29 17:02:44.658674
448	Ankpa	\N	ankpa	21	KGI	NG	1	2017-10-29 17:02:44.674189	2017-10-29 17:02:44.674203
449	Bassa, Kogi State	\N	bassa-kogi-state	21	KGI	NG	1	2017-10-29 17:02:44.688927	2017-10-29 17:02:44.688943
450	Dekina	\N	dekina	21	KGI	NG	1	2017-10-29 17:02:44.699278	2017-10-29 17:02:44.699289
451	Ibaji	\N	ibaji	21	KGI	NG	1	2017-10-29 17:02:44.708832	2017-10-29 17:02:44.708842
452	Idah	\N	idah	21	KGI	NG	1	2017-10-29 17:02:44.723721	2017-10-29 17:02:44.723737
453	Igalamela Odolu	\N	igalamela-odolu	21	KGI	NG	1	2017-10-29 17:02:44.736064	2017-10-29 17:02:44.73608
454	Ijumu	\N	ijumu	21	KGI	NG	1	2017-10-29 17:02:44.749308	2017-10-29 17:02:44.749331
455	Kabba/Bunu	\N	kabba-bunu	21	KGI	NG	1	2017-10-29 17:02:44.765064	2017-10-29 17:02:44.765077
456	Kogi	\N	kogi	21	KGI	NG	1	2017-10-29 17:02:44.78188	2017-10-29 17:02:44.781893
457	Lokoja	\N	lokoja	21	KGI	NG	1	2017-10-29 17:02:44.798275	2017-10-29 17:02:44.79829
458	Mopa Muro	\N	mopa-muro	21	KGI	NG	1	2017-10-29 17:02:44.81155	2017-10-29 17:02:44.811564
459	Ofu	\N	ofu	21	KGI	NG	1	2017-10-29 17:02:44.823524	2017-10-29 17:02:44.823541
460	Ogori/Magongo	\N	ogori-magongo	21	KGI	NG	1	2017-10-29 17:02:44.837951	2017-10-29 17:02:44.837965
461	Okehi	\N	okehi	21	KGI	NG	1	2017-10-29 17:02:44.853044	2017-10-29 17:02:44.853067
462	Okene	\N	okene	21	KGI	NG	1	2017-10-29 17:02:44.872122	2017-10-29 17:02:44.872143
463	Olamaboro	\N	olamaboro	21	KGI	NG	1	2017-10-29 17:02:44.884612	2017-10-29 17:02:44.884623
464	Omala	\N	omala	21	KGI	NG	1	2017-10-29 17:02:44.89782	2017-10-29 17:02:44.897835
465	Yagba East	\N	yagba-east	21	KGI	NG	1	2017-10-29 17:02:44.909497	2017-10-29 17:02:44.909512
466	Yagba West	\N	yagba-west	21	KGI	NG	1	2017-10-29 17:02:44.92169	2017-10-29 17:02:44.921722
467	Aleiro	\N	aleiro	22	KEB	NG	1	2017-10-29 17:02:44.934392	2017-10-29 17:02:44.934405
468	Arewa Dandi	\N	arewa-dandi	22	KEB	NG	1	2017-10-29 17:02:44.945812	2017-10-29 17:02:44.945824
469	Argungu	\N	argungu	22	KEB	NG	1	2017-10-29 17:02:44.95722	2017-10-29 17:02:44.957233
470	Augie	\N	augie	22	KEB	NG	1	2017-10-29 17:02:44.968149	2017-10-29 17:02:44.968162
471	Bagudo	\N	bagudo	22	KEB	NG	1	2017-10-29 17:02:44.979771	2017-10-29 17:02:44.979785
472	Birnin Kebbi	\N	birnin-kebbi	22	KEB	NG	1	2017-10-29 17:02:44.991983	2017-10-29 17:02:44.991996
473	Bunza	\N	bunza	22	KEB	NG	1	2017-10-29 17:02:45.004858	2017-10-29 17:02:45.004871
474	Dandi	\N	dandi	22	KEB	NG	1	2017-10-29 17:02:45.019477	2017-10-29 17:02:45.019497
475	Fakai	\N	fakai	22	KEB	NG	1	2017-10-29 17:02:45.039852	2017-10-29 17:02:45.039866
476	Gwandu	\N	gwandu	22	KEB	NG	1	2017-10-29 17:02:45.052979	2017-10-29 17:02:45.052992
477	Jega	\N	jega	22	KEB	NG	1	2017-10-29 17:02:45.064927	2017-10-29 17:02:45.064941
478	Kalgo	\N	kalgo	22	KEB	NG	1	2017-10-29 17:02:45.077426	2017-10-29 17:02:45.077439
479	Koko/Besse	\N	koko-besse	22	KEB	NG	1	2017-10-29 17:02:45.091888	2017-10-29 17:02:45.091902
480	Maiyama	\N	maiyama	22	KEB	NG	1	2017-10-29 17:02:45.106292	2017-10-29 17:02:45.106305
481	Ngaski	\N	ngaski	22	KEB	NG	1	2017-10-29 17:02:45.118387	2017-10-29 17:02:45.118401
482	Sakaba	\N	sakaba	22	KEB	NG	1	2017-10-29 17:02:45.131667	2017-10-29 17:02:45.13168
483	Shanga	\N	shanga	22	KEB	NG	1	2017-10-29 17:02:45.146147	2017-10-29 17:02:45.146173
484	Suru	\N	suru	22	KEB	NG	1	2017-10-29 17:02:45.161791	2017-10-29 17:02:45.161805
485	Wasagu/Danko	\N	wasagu-danko	22	KEB	NG	1	2017-10-29 17:02:45.174655	2017-10-29 17:02:45.174666
486	Yauri	\N	yauri	22	KEB	NG	1	2017-10-29 17:02:45.191505	2017-10-29 17:02:45.191522
487	Zuru	\N	zuru	22	KEB	NG	1	2017-10-29 17:02:45.209152	2017-10-29 17:02:45.209167
488	Bakori	\N	bakori	23	KAT	NG	1	2017-10-29 17:02:45.224474	2017-10-29 17:02:45.224487
489	Batagarawa	\N	batagarawa	23	KAT	NG	1	2017-10-29 17:02:45.236425	2017-10-29 17:02:45.236438
490	Batsari	\N	batsari	23	KAT	NG	1	2017-10-29 17:02:45.247993	2017-10-29 17:02:45.248006
491	Baure	\N	baure	23	KAT	NG	1	2017-10-29 17:02:45.274061	2017-10-29 17:02:45.274075
492	Bindawa	\N	bindawa	23	KAT	NG	1	2017-10-29 17:02:45.288973	2017-10-29 17:02:45.288986
493	Charanchi	\N	charanchi	23	KAT	NG	1	2017-10-29 17:02:45.302323	2017-10-29 17:02:45.302334
494	Dandume	\N	dandume	23	KAT	NG	1	2017-10-29 17:02:45.316121	2017-10-29 17:02:45.316135
495	Danja	\N	danja	23	KAT	NG	1	2017-10-29 17:02:45.332264	2017-10-29 17:02:45.332282
496	Dan Musa	\N	dan-musa	23	KAT	NG	1	2017-10-29 17:02:45.347836	2017-10-29 17:02:45.347855
497	Daura	\N	daura	23	KAT	NG	1	2017-10-29 17:02:45.361803	2017-10-29 17:02:45.361816
498	Dutsi	\N	dutsi	23	KAT	NG	1	2017-10-29 17:02:45.381768	2017-10-29 17:02:45.381787
499	Dutsin Ma	\N	dutsin-ma	23	KAT	NG	1	2017-10-29 17:02:45.438174	2017-10-29 17:02:45.438196
500	Faskari	\N	faskari	23	KAT	NG	1	2017-10-29 17:02:45.451974	2017-10-29 17:02:45.451988
501	Funtua	\N	funtua	23	KAT	NG	1	2017-10-29 17:02:45.474769	2017-10-29 17:02:45.474801
502	Ingawa	\N	ingawa	23	KAT	NG	1	2017-10-29 17:02:45.490123	2017-10-29 17:02:45.490136
503	Jibia	\N	jibia	23	KAT	NG	1	2017-10-29 17:02:45.5032	2017-10-29 17:02:45.503214
504	Kafur	\N	kafur	23	KAT	NG	1	2017-10-29 17:02:45.515732	2017-10-29 17:02:45.515744
505	Kaita	\N	kaita	23	KAT	NG	1	2017-10-29 17:02:45.526542	2017-10-29 17:02:45.526554
506	Kankara	\N	kankara	23	KAT	NG	1	2017-10-29 17:02:45.537155	2017-10-29 17:02:45.537166
507	Kankia	\N	kankia	23	KAT	NG	1	2017-10-29 17:02:45.547995	2017-10-29 17:02:45.548006
508	Katsina	\N	katsina	23	KAT	NG	1	2017-10-29 17:02:45.55972	2017-10-29 17:02:45.559731
509	Kurfi	\N	kurfi	23	KAT	NG	1	2017-10-29 17:02:45.572868	2017-10-29 17:02:45.572882
510	Kusada	\N	kusada	23	KAT	NG	1	2017-10-29 17:02:45.586989	2017-10-29 17:02:45.587002
511	Mai''Adua	\N	mai-adua	23	KAT	NG	1	2017-10-29 17:02:45.600769	2017-10-29 17:02:45.600781
512	Malumfashi	\N	malumfashi	23	KAT	NG	1	2017-10-29 17:02:45.616724	2017-10-29 17:02:45.61674
513	Mani	\N	mani	23	KAT	NG	1	2017-10-29 17:02:45.630841	2017-10-29 17:02:45.630854
514	Mashi	\N	mashi	23	KAT	NG	1	2017-10-29 17:02:45.643258	2017-10-29 17:02:45.643271
515	Matazu	\N	matazu	23	KAT	NG	1	2017-10-29 17:02:45.656895	2017-10-29 17:02:45.656908
516	Musawa	\N	musawa	23	KAT	NG	1	2017-10-29 17:02:45.671241	2017-10-29 17:02:45.671255
517	Rimi	\N	rimi	23	KAT	NG	1	2017-10-29 17:02:45.686306	2017-10-29 17:02:45.686321
518	Sabuwa	\N	sabuwa	23	KAT	NG	1	2017-10-29 17:02:45.69951	2017-10-29 17:02:45.699521
519	Safana	\N	safana	23	KAT	NG	1	2017-10-29 17:02:45.710136	2017-10-29 17:02:45.710146
520	Sandamu	\N	sandamu	23	KAT	NG	1	2017-10-29 17:02:45.723085	2017-10-29 17:02:45.723098
521	Zango	\N	zango	23	KAT	NG	1	2017-10-29 17:02:45.733937	2017-10-29 17:02:45.733948
522	Aguata	\N	aguata	24	ABR	NG	1	2017-10-29 17:02:45.748301	2017-10-29 17:02:45.748312
523	Anambra East	\N	anambra-east	24	ABR	NG	1	2017-10-29 17:02:45.764268	2017-10-29 17:02:45.764287
524	Anambra West	\N	anambra-west	24	ABR	NG	1	2017-10-29 17:02:45.786151	2017-10-29 17:02:45.786171
525	Anaocha	\N	anaocha	24	ABR	NG	1	2017-10-29 17:02:45.802295	2017-10-29 17:02:45.802309
526	Awka North	\N	awka-north	24	ABR	NG	1	2017-10-29 17:02:45.820356	2017-10-29 17:02:45.82037
527	Awka South	\N	awka-south	24	ABR	NG	1	2017-10-29 17:02:45.838952	2017-10-29 17:02:45.838965
528	Ayamelum	\N	ayamelum	24	ABR	NG	1	2017-10-29 17:02:45.851566	2017-10-29 17:02:45.851579
529	Dunukofia	\N	dunukofia	24	ABR	NG	1	2017-10-29 17:02:45.866219	2017-10-29 17:02:45.866239
530	Ekwusigo	\N	ekwusigo	24	ABR	NG	1	2017-10-29 17:02:45.879981	2017-10-29 17:02:45.879992
531	Idemili North	\N	idemili-north	24	ABR	NG	1	2017-10-29 17:02:45.890419	2017-10-29 17:02:45.890432
532	Idemili South	\N	idemili-south	24	ABR	NG	1	2017-10-29 17:02:45.902417	2017-10-29 17:02:45.90243
533	Ihiala	\N	ihiala	24	ABR	NG	1	2017-10-29 17:02:45.913163	2017-10-29 17:02:45.913174
534	Njikoka	\N	njikoka	24	ABR	NG	1	2017-10-29 17:02:45.928795	2017-10-29 17:02:45.928808
535	Nnewi North	\N	nnewi-north	24	ABR	NG	1	2017-10-29 17:02:45.942416	2017-10-29 17:02:45.942438
536	Nnewi South	\N	nnewi-south	24	ABR	NG	1	2017-10-29 17:02:45.956071	2017-10-29 17:02:45.956086
537	Ogbaru	\N	ogbaru	24	ABR	NG	1	2017-10-29 17:02:45.967995	2017-10-29 17:02:45.968008
538	Onitsha North	\N	onitsha-north	24	ABR	NG	1	2017-10-29 17:02:45.981332	2017-10-29 17:02:45.981366
539	Onitsha South	\N	onitsha-south	24	ABR	NG	1	2017-10-29 17:02:45.998609	2017-10-29 17:02:45.998623
540	Orumba North	\N	orumba-north	24	ABR	NG	1	2017-10-29 17:02:46.013246	2017-10-29 17:02:46.013261
541	Orumba South	\N	orumba-south	24	ABR	NG	1	2017-10-29 17:02:46.028542	2017-10-29 17:02:46.028558
542	Oyi	\N	oyi	24	ABR	NG	1	2017-10-29 17:02:46.04418	2017-10-29 17:02:46.044201
543	Asa	\N	asa	25	KWA	NG	1	2017-10-29 17:02:46.060103	2017-10-29 17:02:46.060119
544	Baruten	\N	baruten	25	KWA	NG	1	2017-10-29 17:02:46.078924	2017-10-29 17:02:46.078954
545	Edu	\N	edu	25	KWA	NG	1	2017-10-29 17:02:46.091751	2017-10-29 17:02:46.091765
546	Ekiti, Kwara State	\N	ekiti-kwara-state	25	KWA	NG	1	2017-10-29 17:02:46.10716	2017-10-29 17:02:46.107174
547	Ifelodun, Kwara State	\N	ifelodun-kwara-state	25	KWA	NG	1	2017-10-29 17:02:46.12042	2017-10-29 17:02:46.120433
548	Ilorin East	\N	ilorin-east	25	KWA	NG	1	2017-10-29 17:02:46.13504	2017-10-29 17:02:46.135054
549	Ilorin South	\N	ilorin-south	25	KWA	NG	1	2017-10-29 17:02:46.152043	2017-10-29 17:02:46.152056
550	Ilorin West	\N	ilorin-west	25	KWA	NG	1	2017-10-29 17:02:46.166431	2017-10-29 17:02:46.166456
551	Irepodun, Kwara State	\N	irepodun-kwara-state	25	KWA	NG	1	2017-10-29 17:02:46.180794	2017-10-29 17:02:46.180807
552	Isin	\N	isin	25	KWA	NG	1	2017-10-29 17:02:46.19403	2017-10-29 17:02:46.194053
553	Kaiama	\N	kaiama	25	KWA	NG	1	2017-10-29 17:02:46.208138	2017-10-29 17:02:46.208152
554	Moro	\N	moro	25	KWA	NG	1	2017-10-29 17:02:46.227214	2017-10-29 17:02:46.227231
555	Offa	\N	offa	25	KWA	NG	1	2017-10-29 17:02:46.246673	2017-10-29 17:02:46.246688
556	Oke Ero	\N	oke-ero	25	KWA	NG	1	2017-10-29 17:02:46.260308	2017-10-29 17:02:46.26032
557	Oyun	\N	oyun	25	KWA	NG	1	2017-10-29 17:02:46.273346	2017-10-29 17:02:46.27336
558	Pategi	\N	pategi	25	KWA	NG	1	2017-10-29 17:02:46.287187	2017-10-29 17:02:46.287209
559	Aba North	\N	aba-north	26	ABI	NG	1	2017-10-29 17:02:46.303421	2017-10-29 17:02:46.303436
560	Aba South	\N	aba-south	26	ABI	NG	1	2017-10-29 17:02:46.317983	2017-10-29 17:02:46.317999
561	Arochukwu	\N	arochukwu	26	ABI	NG	1	2017-10-29 17:02:46.33729	2017-10-29 17:02:46.33731
562	Bende	\N	bende	26	ABI	NG	1	2017-10-29 17:02:46.353962	2017-10-29 17:02:46.353975
563	Ikwuano	\N	ikwuano	26	ABI	NG	1	2017-10-29 17:02:46.365894	2017-10-29 17:02:46.365908
564	Isiala Ngwa North	\N	isiala-ngwa-north	26	ABI	NG	1	2017-10-29 17:02:46.380181	2017-10-29 17:02:46.380195
565	Isiala Ngwa South	\N	isiala-ngwa-south	26	ABI	NG	1	2017-10-29 17:02:46.392063	2017-10-29 17:02:46.392076
566	Isuikwuato	\N	isuikwuato	26	ABI	NG	1	2017-10-29 17:02:46.411296	2017-10-29 17:02:46.411321
567	Obi Ngwa	\N	obi-ngwa	26	ABI	NG	1	2017-10-29 17:02:46.430379	2017-10-29 17:02:46.430393
568	Ohafia	\N	ohafia	26	ABI	NG	1	2017-10-29 17:02:46.442924	2017-10-29 17:02:46.442937
569	Osisioma	\N	osisioma	26	ABI	NG	1	2017-10-29 17:02:46.456172	2017-10-29 17:02:46.456235
570	Ugwunagbo	\N	ugwunagbo	26	ABI	NG	1	2017-10-29 17:02:46.473837	2017-10-29 17:02:46.47385
571	Ukwa East	\N	ukwa-east	26	ABI	NG	1	2017-10-29 17:02:46.487807	2017-10-29 17:02:46.487819
572	Ukwa West	\N	ukwa-west	26	ABI	NG	1	2017-10-29 17:02:46.500846	2017-10-29 17:02:46.500857
573	Umuahia North	\N	umuahia-north	26	ABI	NG	1	2017-10-29 17:02:46.510887	2017-10-29 17:02:46.510897
574	Umuahia South	\N	umuahia-south	26	ABI	NG	1	2017-10-29 17:02:46.52082	2017-10-29 17:02:46.52083
575	Umu Nneochi	\N	umu-nneochi	26	ABI	NG	1	2017-10-29 17:02:46.531067	2017-10-29 17:02:46.531077
576	Demsa	\N	demsa	27	ADA	NG	1	2017-10-29 17:02:46.543938	2017-10-29 17:02:46.543953
577	Fufure	\N	fufure	27	ADA	NG	1	2017-10-29 17:02:46.5551	2017-10-29 17:02:46.555112
578	Ganye	\N	ganye	27	ADA	NG	1	2017-10-29 17:02:46.565642	2017-10-29 17:02:46.565654
579	Gayuk	\N	gayuk	27	ADA	NG	1	2017-10-29 17:02:46.575943	2017-10-29 17:02:46.575954
580	Gombi	\N	gombi	27	ADA	NG	1	2017-10-29 17:02:46.58683	2017-10-29 17:02:46.586841
581	Grie	\N	grie	27	ADA	NG	1	2017-10-29 17:02:46.596569	2017-10-29 17:02:46.596579
582	Hong	\N	hong	27	ADA	NG	1	2017-10-29 17:02:46.606926	2017-10-29 17:02:46.606937
583	Jada	\N	jada	27	ADA	NG	1	2017-10-29 17:02:46.617301	2017-10-29 17:02:46.617312
584	Larmurde	\N	larmurde	27	ADA	NG	1	2017-10-29 17:02:46.628256	2017-10-29 17:02:46.628267
585	Madagali	\N	madagali	27	ADA	NG	1	2017-10-29 17:02:46.644536	2017-10-29 17:02:46.644548
586	Maiha	\N	maiha	27	ADA	NG	1	2017-10-29 17:02:46.663816	2017-10-29 17:02:46.663836
587	Mayo Belwa	\N	mayo-belwa	27	ADA	NG	1	2017-10-29 17:02:46.6822	2017-10-29 17:02:46.682214
588	Michika	\N	michika	27	ADA	NG	1	2017-10-29 17:02:46.696745	2017-10-29 17:02:46.696758
589	Mubi North	\N	mubi-north	27	ADA	NG	1	2017-10-29 17:02:46.710097	2017-10-29 17:02:46.71011
590	Mubi South	\N	mubi-south	27	ADA	NG	1	2017-10-29 17:02:46.726671	2017-10-29 17:02:46.726683
591	Numan	\N	numan	27	ADA	NG	1	2017-10-29 17:02:46.739271	2017-10-29 17:02:46.739285
592	Shelleng	\N	shelleng	27	ADA	NG	1	2017-10-29 17:02:46.754089	2017-10-29 17:02:46.754102
593	Song	\N	song	27	ADA	NG	1	2017-10-29 17:02:46.769662	2017-10-29 17:02:46.769678
594	Toungo	\N	toungo	27	ADA	NG	1	2017-10-29 17:02:46.787342	2017-10-29 17:02:46.787356
595	Yola North	\N	yola-north	27	ADA	NG	1	2017-10-29 17:02:46.800485	2017-10-29 17:02:46.800496
596	Yola South	\N	yola-south	27	ADA	NG	1	2017-10-29 17:02:46.811424	2017-10-29 17:02:46.811435
597	Brass	\N	brass	28	BAY	NG	1	2017-10-29 17:02:46.824816	2017-10-29 17:02:46.824826
598	Ekeremor	\N	ekeremor	28	BAY	NG	1	2017-10-29 17:02:46.834906	2017-10-29 17:02:46.834917
599	Kolokuma/Opokuma	\N	kolokuma-opokuma	28	BAY	NG	1	2017-10-29 17:02:46.844697	2017-10-29 17:02:46.844707
600	Nembe	\N	nembe	28	BAY	NG	1	2017-10-29 17:02:46.855107	2017-10-29 17:02:46.855117
601	Ogbia	\N	ogbia	28	BAY	NG	1	2017-10-29 17:02:46.866286	2017-10-29 17:02:46.866297
602	Sagbama	\N	sagbama	28	BAY	NG	1	2017-10-29 17:02:46.880352	2017-10-29 17:02:46.880365
603	Southern Ijaw	\N	southern-ijaw	28	BAY	NG	1	2017-10-29 17:02:46.89671	2017-10-29 17:02:46.896727
604	Yenagoa	\N	yenagoa	28	BAY	NG	1	2017-10-29 17:02:46.911794	2017-10-29 17:02:46.911809
605	Alkaleri	\N	alkaleri	29	BCH	NG	1	2017-10-29 17:02:46.927217	2017-10-29 17:02:46.927229
606	Bauchi	\N	bauchi	29	BCH	NG	1	2017-10-29 17:02:46.938066	2017-10-29 17:02:46.938078
607	Bogoro	\N	bogoro	29	BCH	NG	1	2017-10-29 17:02:46.949142	2017-10-29 17:02:46.949153
608	Damban	\N	damban	29	BCH	NG	1	2017-10-29 17:02:46.96082	2017-10-29 17:02:46.960831
609	Darazo	\N	darazo	29	BCH	NG	1	2017-10-29 17:02:46.972283	2017-10-29 17:02:46.972296
610	Dass	\N	dass	29	BCH	NG	1	2017-10-29 17:02:46.986834	2017-10-29 17:02:46.986854
611	Gamawa	\N	gamawa	29	BCH	NG	1	2017-10-29 17:02:47.003312	2017-10-29 17:02:47.003327
612	Ganjuwa	\N	ganjuwa	29	BCH	NG	1	2017-10-29 17:02:47.01817	2017-10-29 17:02:47.018183
613	Giade	\N	giade	29	BCH	NG	1	2017-10-29 17:02:47.035803	2017-10-29 17:02:47.035822
614	Itas/Gadau	\N	itas-gadau	29	BCH	NG	1	2017-10-29 17:02:47.047917	2017-10-29 17:02:47.047927
615	Jama''are	\N	jama-are	29	BCH	NG	1	2017-10-29 17:02:47.058894	2017-10-29 17:02:47.058906
616	Katagum	\N	katagum	29	BCH	NG	1	2017-10-29 17:02:47.069722	2017-10-29 17:02:47.069733
617	Kirfi	\N	kirfi	29	BCH	NG	1	2017-10-29 17:02:47.079432	2017-10-29 17:02:47.079442
618	Misau	\N	misau	29	BCH	NG	1	2017-10-29 17:02:47.089134	2017-10-29 17:02:47.089144
619	Ningi	\N	ningi	29	BCH	NG	1	2017-10-29 17:02:47.098844	2017-10-29 17:02:47.098856
620	Shira	\N	shira	29	BCH	NG	1	2017-10-29 17:02:47.109299	2017-10-29 17:02:47.10931
621	Tafawa Balewa	\N	tafawa-balewa	29	BCH	NG	1	2017-10-29 17:02:47.118826	2017-10-29 17:02:47.118836
622	Toro	\N	toro	29	BCH	NG	1	2017-10-29 17:02:47.128985	2017-10-29 17:02:47.128996
623	Warji	\N	warji	29	BCH	NG	1	2017-10-29 17:02:47.142936	2017-10-29 17:02:47.142954
624	Zaki	\N	zaki	29	BCH	NG	1	2017-10-29 17:02:47.160729	2017-10-29 17:02:47.160747
625	Abak	\N	abak	30	AKI	NG	1	2017-10-29 17:02:47.176285	2017-10-29 17:02:47.176303
626	Eastern Obolo	\N	eastern-obolo	30	AKI	NG	1	2017-10-29 17:02:47.194069	2017-10-29 17:02:47.194081
627	Eket	\N	eket	30	AKI	NG	1	2017-10-29 17:02:47.209286	2017-10-29 17:02:47.209298
628	Esit Eket	\N	esit-eket	30	AKI	NG	1	2017-10-29 17:02:47.22442	2017-10-29 17:02:47.224431
629	Essien Udim	\N	essien-udim	30	AKI	NG	1	2017-10-29 17:02:47.238856	2017-10-29 17:02:47.238874
630	Etim Ekpo	\N	etim-ekpo	30	AKI	NG	1	2017-10-29 17:02:47.249641	2017-10-29 17:02:47.249651
631	Etinan	\N	etinan	30	AKI	NG	1	2017-10-29 17:02:47.259747	2017-10-29 17:02:47.259757
632	Ibeno	\N	ibeno	30	AKI	NG	1	2017-10-29 17:02:47.27121	2017-10-29 17:02:47.271221
633	Ibesikpo Asutan	\N	ibesikpo-asutan	30	AKI	NG	1	2017-10-29 17:02:47.280974	2017-10-29 17:02:47.280983
634	Ibiono-Ibom	\N	ibiono-ibom	30	AKI	NG	1	2017-10-29 17:02:47.290715	2017-10-29 17:02:47.290726
635	Ika	\N	ika	30	AKI	NG	1	2017-10-29 17:02:47.300139	2017-10-29 17:02:47.300149
636	Ikono	\N	ikono	30	AKI	NG	1	2017-10-29 17:02:47.31012	2017-10-29 17:02:47.31013
637	Ikot Abasi	\N	ikot-abasi	30	AKI	NG	1	2017-10-29 17:02:47.319904	2017-10-29 17:02:47.320057
638	Ikot Ekpene	\N	ikot-ekpene	30	AKI	NG	1	2017-10-29 17:02:47.336275	2017-10-29 17:02:47.336294
639	Ini	\N	ini	30	AKI	NG	1	2017-10-29 17:02:47.351538	2017-10-29 17:02:47.351549
640	Itu	\N	itu	30	AKI	NG	1	2017-10-29 17:02:47.36218	2017-10-29 17:02:47.3622
641	Mbo	\N	mbo	30	AKI	NG	1	2017-10-29 17:02:47.372512	2017-10-29 17:02:47.372525
642	Mkpat-Enin	\N	mkpat-enin	30	AKI	NG	1	2017-10-29 17:02:47.382837	2017-10-29 17:02:47.382848
643	Nsit-Atai	\N	nsit-atai	30	AKI	NG	1	2017-10-29 17:02:47.392785	2017-10-29 17:02:47.392797
644	Nsit-Ibom	\N	nsit-ibom	30	AKI	NG	1	2017-10-29 17:02:47.403648	2017-10-29 17:02:47.40366
645	Nsit-Ubium	\N	nsit-ubium	30	AKI	NG	1	2017-10-29 17:02:47.413605	2017-10-29 17:02:47.413616
646	Obot Akara	\N	obot-akara	30	AKI	NG	1	2017-10-29 17:02:47.423671	2017-10-29 17:02:47.423683
647	Okobo	\N	okobo	30	AKI	NG	1	2017-10-29 17:02:47.433395	2017-10-29 17:02:47.433406
648	Onna	\N	onna	30	AKI	NG	1	2017-10-29 17:02:47.44375	2017-10-29 17:02:47.44376
649	Oron	\N	oron	30	AKI	NG	1	2017-10-29 17:02:47.453323	2017-10-29 17:02:47.453333
650	Oruk Anam	\N	oruk-anam	30	AKI	NG	1	2017-10-29 17:02:47.463069	2017-10-29 17:02:47.463079
651	Udung-Uko	\N	udung-uko	30	AKI	NG	1	2017-10-29 17:02:47.472697	2017-10-29 17:02:47.472708
652	Ukanafun	\N	ukanafun	30	AKI	NG	1	2017-10-29 17:02:47.48293	2017-10-29 17:02:47.48294
653	Uruan	\N	uruan	30	AKI	NG	1	2017-10-29 17:02:47.492621	2017-10-29 17:02:47.492631
654	Urue-Offong/Oruko	\N	urue-offong-oruko	30	AKI	NG	1	2017-10-29 17:02:47.502323	2017-10-29 17:02:47.502334
655	Uyo	\N	uyo	30	AKI	NG	1	2017-10-29 17:02:47.511939	2017-10-29 17:02:47.511949
656	Ardo Kola	\N	ardo-kola	31	TAR	NG	1	2017-10-29 17:02:47.522198	2017-10-29 17:02:47.522209
657	Bali	\N	bali	31	TAR	NG	1	2017-10-29 17:02:47.534169	2017-10-29 17:02:47.534181
658	Donga	\N	donga	31	TAR	NG	1	2017-10-29 17:02:47.546188	2017-10-29 17:02:47.5462
659	Gashaka	\N	gashaka	31	TAR	NG	1	2017-10-29 17:02:47.559824	2017-10-29 17:02:47.559836
660	Gassol	\N	gassol	31	TAR	NG	1	2017-10-29 17:02:47.576553	2017-10-29 17:02:47.576576
661	Ibi	\N	ibi	31	TAR	NG	1	2017-10-29 17:02:47.595478	2017-10-29 17:02:47.595498
662	Jalingo	\N	jalingo	31	TAR	NG	1	2017-10-29 17:02:47.617507	2017-10-29 17:02:47.617521
663	Karim Lamido	\N	karim-lamido	31	TAR	NG	1	2017-10-29 17:02:47.631753	2017-10-29 17:02:47.631769
664	Kumi	\N	kumi	31	TAR	NG	1	2017-10-29 17:02:47.648007	2017-10-29 17:02:47.648022
665	Lau	\N	lau	31	TAR	NG	1	2017-10-29 17:02:47.664424	2017-10-29 17:02:47.664436
666	Sardauna	\N	sardauna	31	TAR	NG	1	2017-10-29 17:02:47.678762	2017-10-29 17:02:47.678774
667	Takum	\N	takum	31	TAR	NG	1	2017-10-29 17:02:47.692921	2017-10-29 17:02:47.692936
668	Ussa	\N	ussa	31	TAR	NG	1	2017-10-29 17:02:47.706584	2017-10-29 17:02:47.706597
669	Wukari	\N	wukari	31	TAR	NG	1	2017-10-29 17:02:47.716869	2017-10-29 17:02:47.716881
670	Yorro	\N	yorro	31	TAR	NG	1	2017-10-29 17:02:47.726618	2017-10-29 17:02:47.726628
671	Zing	\N	zing	31	TAR	NG	1	2017-10-29 17:02:47.736839	2017-10-29 17:02:47.736849
672	Atakunmosa East	\N	atakunmosa-east	32	OSU	NG	1	2017-10-29 17:02:47.747687	2017-10-29 17:02:47.747696
673	Atakunmosa West	\N	atakunmosa-west	32	OSU	NG	1	2017-10-29 17:02:47.757382	2017-10-29 17:02:47.757393
674	Aiyedaade	\N	aiyedaade	32	OSU	NG	1	2017-10-29 17:02:47.76765	2017-10-29 17:02:47.767662
675	Aiyedire	\N	aiyedire	32	OSU	NG	1	2017-10-29 17:02:47.778065	2017-10-29 17:02:47.778076
676	Boluwaduro	\N	boluwaduro	32	OSU	NG	1	2017-10-29 17:02:47.79506	2017-10-29 17:02:47.795072
677	Boripe	\N	boripe	32	OSU	NG	1	2017-10-29 17:02:47.807379	2017-10-29 17:02:47.807391
678	Ede North	\N	ede-north	32	OSU	NG	1	2017-10-29 17:02:47.820591	2017-10-29 17:02:47.820603
679	Ede South	\N	ede-south	32	OSU	NG	1	2017-10-29 17:02:47.83434	2017-10-29 17:02:47.834358
680	Ife Central	\N	ife-central	32	OSU	NG	1	2017-10-29 17:02:47.852801	2017-10-29 17:02:47.85282
681	Ife East	\N	ife-east	32	OSU	NG	1	2017-10-29 17:02:47.869739	2017-10-29 17:02:47.869753
682	Ife North	\N	ife-north	32	OSU	NG	1	2017-10-29 17:02:47.883646	2017-10-29 17:02:47.883661
683	Ife South	\N	ife-south	32	OSU	NG	1	2017-10-29 17:02:47.903269	2017-10-29 17:02:47.903289
684	Egbedore	\N	egbedore	32	OSU	NG	1	2017-10-29 17:02:47.919668	2017-10-29 17:02:47.919681
685	Ejigbo	\N	ejigbo	32	OSU	NG	1	2017-10-29 17:02:47.934764	2017-10-29 17:02:47.934779
686	Ifedayo	\N	ifedayo	32	OSU	NG	1	2017-10-29 17:02:47.948428	2017-10-29 17:02:47.94844
687	Ifelodun	\N	ifelodun	32	OSU	NG	1	2017-10-29 17:02:47.961866	2017-10-29 17:02:47.961879
688	Ila	\N	ila	32	OSU	NG	1	2017-10-29 17:02:47.97597	2017-10-29 17:02:47.975984
689	Ilesa East	\N	ilesa-east	32	OSU	NG	1	2017-10-29 17:02:47.991145	2017-10-29 17:02:47.991161
690	Ilesa West	\N	ilesa-west	32	OSU	NG	1	2017-10-29 17:02:48.008834	2017-10-29 17:02:48.008854
691	Irepodun	\N	irepodun	32	OSU	NG	1	2017-10-29 17:02:48.024118	2017-10-29 17:02:48.024131
692	Irewole	\N	irewole	32	OSU	NG	1	2017-10-29 17:02:48.039744	2017-10-29 17:02:48.039763
693	Isokan	\N	isokan	32	OSU	NG	1	2017-10-29 17:02:48.056886	2017-10-29 17:02:48.056905
694	Iwo	\N	iwo	32	OSU	NG	1	2017-10-29 17:02:48.073867	2017-10-29 17:02:48.07388
695	Obokun	\N	obokun	32	OSU	NG	1	2017-10-29 17:02:48.087557	2017-10-29 17:02:48.087572
696	Odo Otin	\N	odo-otin	32	OSU	NG	1	2017-10-29 17:02:48.103586	2017-10-29 17:02:48.103599
697	Ola Oluwa	\N	ola-oluwa	32	OSU	NG	1	2017-10-29 17:02:48.116606	2017-10-29 17:02:48.116619
698	Olorunda	\N	olorunda	32	OSU	NG	1	2017-10-29 17:02:48.129888	2017-10-29 17:02:48.129901
699	Oriade	\N	oriade	32	OSU	NG	1	2017-10-29 17:02:48.145189	2017-10-29 17:02:48.145207
700	Orolu	\N	orolu	32	OSU	NG	1	2017-10-29 17:02:48.161518	2017-10-29 17:02:48.161531
701	Osogbo	\N	osogbo	32	OSU	NG	1	2017-10-29 17:02:48.17476	2017-10-29 17:02:48.174774
702	Afijio	\N	afijio	33	OYO	NG	1	2017-10-29 17:02:48.187945	2017-10-29 17:02:48.187958
703	Akinyele	\N	akinyele	33	OYO	NG	1	2017-10-29 17:02:48.202438	2017-10-29 17:02:48.20245
704	Atiba	\N	atiba	33	OYO	NG	1	2017-10-29 17:02:48.21798	2017-10-29 17:02:48.218013
705	Atisbo	\N	atisbo	33	OYO	NG	1	2017-10-29 17:02:48.237674	2017-10-29 17:02:48.237686
706	Egbeda	\N	egbeda	33	OYO	NG	1	2017-10-29 17:02:48.250433	2017-10-29 17:02:48.250447
707	Ibadan North	\N	ibadan-north	33	OYO	NG	1	2017-10-29 17:02:48.269622	2017-10-29 17:02:48.269636
708	Ibadan North-East	\N	ibadan-north-east	33	OYO	NG	1	2017-10-29 17:02:48.28816	2017-10-29 17:02:48.288175
709	Ibadan North-West	\N	ibadan-north-west	33	OYO	NG	1	2017-10-29 17:02:48.303746	2017-10-29 17:02:48.303759
710	Ibadan South-East	\N	ibadan-south-east	33	OYO	NG	1	2017-10-29 17:02:48.330029	2017-10-29 17:02:48.330057
711	Ibadan South-West	\N	ibadan-south-west	33	OYO	NG	1	2017-10-29 17:02:48.343409	2017-10-29 17:02:48.343422
712	Ibarapa Central	\N	ibarapa-central	33	OYO	NG	1	2017-10-29 17:02:48.360954	2017-10-29 17:02:48.360966
713	Ibarapa East	\N	ibarapa-east	33	OYO	NG	1	2017-10-29 17:02:48.381481	2017-10-29 17:02:48.381495
714	Ibarapa North	\N	ibarapa-north	33	OYO	NG	1	2017-10-29 17:02:48.392878	2017-10-29 17:02:48.392895
715	Ido	\N	ido	33	OYO	NG	1	2017-10-29 17:02:48.403206	2017-10-29 17:02:48.403216
716	Irepo	\N	irepo	33	OYO	NG	1	2017-10-29 17:02:48.414849	2017-10-29 17:02:48.41486
717	Iseyin	\N	iseyin	33	OYO	NG	1	2017-10-29 17:02:48.425419	2017-10-29 17:02:48.42543
718	Itesiwaju	\N	itesiwaju	33	OYO	NG	1	2017-10-29 17:02:48.43563	2017-10-29 17:02:48.43564
719	Iwajowa	\N	iwajowa	33	OYO	NG	1	2017-10-29 17:02:48.446105	2017-10-29 17:02:48.446115
720	Kajola	\N	kajola	33	OYO	NG	1	2017-10-29 17:02:48.457227	2017-10-29 17:02:48.457237
721	Lagelu	\N	lagelu	33	OYO	NG	1	2017-10-29 17:02:48.467979	2017-10-29 17:02:48.46799
722	Ogbomosho North	\N	ogbomosho-north	33	OYO	NG	1	2017-10-29 17:02:48.478398	2017-10-29 17:02:48.478408
723	Ogbomosho South	\N	ogbomosho-south	33	OYO	NG	1	2017-10-29 17:02:48.488928	2017-10-29 17:02:48.488939
724	Ogo Oluwa	\N	ogo-oluwa	33	OYO	NG	1	2017-10-29 17:02:48.500254	2017-10-29 17:02:48.500272
725	Olorunsogo	\N	olorunsogo	33	OYO	NG	1	2017-10-29 17:02:48.511135	2017-10-29 17:02:48.511146
726	Oluyole	\N	oluyole	33	OYO	NG	1	2017-10-29 17:02:48.522213	2017-10-29 17:02:48.522228
727	Ona Ara	\N	ona-ara	33	OYO	NG	1	2017-10-29 17:02:48.532523	2017-10-29 17:02:48.532533
728	Orelope	\N	orelope	33	OYO	NG	1	2017-10-29 17:02:48.543827	2017-10-29 17:02:48.543837
729	Ori Ire	\N	ori-ire	33	OYO	NG	1	2017-10-29 17:02:48.555162	2017-10-29 17:02:48.555179
730	Oyo	\N	oyo	33	OYO	NG	1	2017-10-29 17:02:48.566911	2017-10-29 17:02:48.566924
731	Oyo East	\N	oyo-east	33	OYO	NG	1	2017-10-29 17:02:48.579202	2017-10-29 17:02:48.579217
732	Saki East	\N	saki-east	33	OYO	NG	1	2017-10-29 17:02:48.593236	2017-10-29 17:02:48.593252
733	Saki West	\N	saki-west	33	OYO	NG	1	2017-10-29 17:02:48.605091	2017-10-29 17:02:48.605106
734	Surulere, Oyo State	\N	surulere-oyo-state	33	OYO	NG	1	2017-10-29 17:02:48.616201	2017-10-29 17:02:48.616215
735	Abeokuta North	\N	abeokuta-north	34	OGN	NG	1	2017-10-29 17:02:48.644554	2017-10-29 17:02:48.644571
736	Abeokuta South	\N	abeokuta-south	34	OGN	NG	1	2017-10-29 17:02:48.658709	2017-10-29 17:02:48.658723
737	Ado-Odo/Ota	\N	ado-odo-ota	34	OGN	NG	1	2017-10-29 17:02:48.670787	2017-10-29 17:02:48.6708
738	Egbado North	\N	egbado-north	34	OGN	NG	1	2017-10-29 17:02:48.681994	2017-10-29 17:02:48.682007
739	Egbado South	\N	egbado-south	34	OGN	NG	1	2017-10-29 17:02:48.69307	2017-10-29 17:02:48.693084
740	Ewekoro	\N	ewekoro	34	OGN	NG	1	2017-10-29 17:02:48.70531	2017-10-29 17:02:48.705323
741	Ifo	\N	ifo	34	OGN	NG	1	2017-10-29 17:02:48.716712	2017-10-29 17:02:48.716725
742	Ijebu East	\N	ijebu-east	34	OGN	NG	1	2017-10-29 17:02:48.727482	2017-10-29 17:02:48.727495
743	Ijebu North	\N	ijebu-north	34	OGN	NG	1	2017-10-29 17:02:48.738209	2017-10-29 17:02:48.73822
744	Ijebu North East	\N	ijebu-north-east	34	OGN	NG	1	2017-10-29 17:02:48.749652	2017-10-29 17:02:48.749665
745	Ijebu Ode	\N	ijebu-ode	34	OGN	NG	1	2017-10-29 17:02:48.760702	2017-10-29 17:02:48.760716
746	Ikenne	\N	ikenne	34	OGN	NG	1	2017-10-29 17:02:48.771804	2017-10-29 17:02:48.771818
747	Imeko Afon	\N	imeko-afon	34	OGN	NG	1	2017-10-29 17:02:48.782767	2017-10-29 17:02:48.78278
748	Ipokia	\N	ipokia	34	OGN	NG	1	2017-10-29 17:02:48.797337	2017-10-29 17:02:48.797367
749	Obafemi Owode	\N	obafemi-owode	34	OGN	NG	1	2017-10-29 17:02:48.809595	2017-10-29 17:02:48.809605
750	Odeda	\N	odeda	34	OGN	NG	1	2017-10-29 17:02:48.819584	2017-10-29 17:02:48.819594
751	Odogbolu	\N	odogbolu	34	OGN	NG	1	2017-10-29 17:02:48.830094	2017-10-29 17:02:48.830112
752	Ogun Waterside	\N	ogun-waterside	34	OGN	NG	1	2017-10-29 17:02:48.844045	2017-10-29 17:02:48.844057
753	Remo North	\N	remo-north	34	OGN	NG	1	2017-10-29 17:02:48.853877	2017-10-29 17:02:48.853887
754	Shagamu	\N	shagamu	34	OGN	NG	1	2017-10-29 17:02:48.863913	2017-10-29 17:02:48.863925
755	Akwanga	\N	akwanga	35	NAS	NG	1	2017-10-29 17:02:48.873901	2017-10-29 17:02:48.873911
756	Awe	\N	awe	35	NAS	NG	1	2017-10-29 17:02:48.884579	2017-10-29 17:02:48.884589
757	Doma	\N	doma	35	NAS	NG	1	2017-10-29 17:02:48.894381	2017-10-29 17:02:48.894391
758	Karu	\N	karu	35	NAS	NG	1	2017-10-29 17:02:48.904301	2017-10-29 17:02:48.904313
759	Keana	\N	keana	35	NAS	NG	1	2017-10-29 17:02:48.914838	2017-10-29 17:02:48.914849
760	Keffi	\N	keffi	35	NAS	NG	1	2017-10-29 17:02:48.925572	2017-10-29 17:02:48.925583
761	Kokona	\N	kokona	35	NAS	NG	1	2017-10-29 17:02:48.935657	2017-10-29 17:02:48.935668
762	Lafia	\N	lafia	35	NAS	NG	1	2017-10-29 17:02:48.947599	2017-10-29 17:02:48.947613
763	Nasarawa	\N	nasarawa	35	NAS	NG	1	2017-10-29 17:02:48.958925	2017-10-29 17:02:48.958942
764	Nasarawa Egon	\N	nasarawa-egon	35	NAS	NG	1	2017-10-29 17:02:48.973078	2017-10-29 17:02:48.973092
765	Obi, Nasarawa State	\N	obi-nasarawa-state	35	NAS	NG	1	2017-10-29 17:02:48.984562	2017-10-29 17:02:48.984576
766	Toto	\N	toto	35	NAS	NG	1	2017-10-29 17:02:48.997154	2017-10-29 17:02:48.997167
767	Wamba	\N	wamba	35	NAS	NG	1	2017-10-29 17:02:49.0079	2017-10-29 17:02:49.007912
768	Akoko North-East	\N	akoko-north-east	36	OND	NG	1	2017-10-29 17:02:49.019276	2017-10-29 17:02:49.019288
769	Akoko North-West	\N	akoko-north-west	36	OND	NG	1	2017-10-29 17:02:49.030148	2017-10-29 17:02:49.030158
770	Akoko South-West	\N	akoko-south-west	36	OND	NG	1	2017-10-29 17:02:49.040484	2017-10-29 17:02:49.040496
771	Akoko South-East	\N	akoko-south-east	36	OND	NG	1	2017-10-29 17:02:49.051042	2017-10-29 17:02:49.051052
772	Akure North	\N	akure-north	36	OND	NG	1	2017-10-29 17:02:49.062144	2017-10-29 17:02:49.062156
773	Akure South	\N	akure-south	36	OND	NG	1	2017-10-29 17:02:49.073032	2017-10-29 17:02:49.073043
774	Ese Odo	\N	ese-odo	36	OND	NG	1	2017-10-29 17:02:49.083142	2017-10-29 17:02:49.083153
775	Idanre	\N	idanre	36	OND	NG	1	2017-10-29 17:02:49.093957	2017-10-29 17:02:49.093968
776	Ifedore	\N	ifedore	36	OND	NG	1	2017-10-29 17:02:49.106472	2017-10-29 17:02:49.106539
777	Ilaje	\N	ilaje	36	OND	NG	1	2017-10-29 17:02:49.119312	2017-10-29 17:02:49.119343
778	Ile Oluji/Okeigbo	\N	ile-oluji-okeigbo	36	OND	NG	1	2017-10-29 17:02:49.132823	2017-10-29 17:02:49.132839
779	Irele	\N	irele	36	OND	NG	1	2017-10-29 17:02:49.144222	2017-10-29 17:02:49.144235
780	Odigbo	\N	odigbo	36	OND	NG	1	2017-10-29 17:02:49.157868	2017-10-29 17:02:49.157893
781	Okitipupa	\N	okitipupa	36	OND	NG	1	2017-10-29 17:02:49.175762	2017-10-29 17:02:49.175781
782	Ondo East	\N	ondo-east	36	OND	NG	1	2017-10-29 17:02:49.200747	2017-10-29 17:02:49.20076
783	Ondo West	\N	ondo-west	36	OND	NG	1	2017-10-29 17:02:49.211832	2017-10-29 17:02:49.211842
784	Ose	\N	ose	36	OND	NG	1	2017-10-29 17:02:49.222597	2017-10-29 17:02:49.222607
785	Owo	\N	owo	36	OND	NG	1	2017-10-29 17:02:49.232285	2017-10-29 17:02:49.232295
786	Agaie	\N	agaie	37	NIG	NG	1	2017-10-29 17:02:49.242301	2017-10-29 17:02:49.242312
787	Agwara	\N	agwara	37	NIG	NG	1	2017-10-29 17:02:49.252291	2017-10-29 17:02:49.252301
788	Bida	\N	bida	37	NIG	NG	1	2017-10-29 17:02:49.262485	2017-10-29 17:02:49.262529
789	Borgu	\N	borgu	37	NIG	NG	1	2017-10-29 17:02:49.272286	2017-10-29 17:02:49.272296
790	Bosso	\N	bosso	37	NIG	NG	1	2017-10-29 17:02:49.282065	2017-10-29 17:02:49.282076
791	Chanchaga	\N	chanchaga	37	NIG	NG	1	2017-10-29 17:02:49.291852	2017-10-29 17:02:49.291862
792	Edati	\N	edati	37	NIG	NG	1	2017-10-29 17:02:49.30247	2017-10-29 17:02:49.302482
793	Gbako	\N	gbako	37	NIG	NG	1	2017-10-29 17:02:49.312806	2017-10-29 17:02:49.312817
794	Gurara	\N	gurara	37	NIG	NG	1	2017-10-29 17:02:49.322628	2017-10-29 17:02:49.322638
795	Katcha	\N	katcha	37	NIG	NG	1	2017-10-29 17:02:49.332636	2017-10-29 17:02:49.332647
796	Kontagora	\N	kontagora	37	NIG	NG	1	2017-10-29 17:02:49.343077	2017-10-29 17:02:49.343087
797	Lapai	\N	lapai	37	NIG	NG	1	2017-10-29 17:02:49.352566	2017-10-29 17:02:49.352576
798	Lavun	\N	lavun	37	NIG	NG	1	2017-10-29 17:02:49.362411	2017-10-29 17:02:49.362421
799	Magama	\N	magama	37	NIG	NG	1	2017-10-29 17:02:49.372513	2017-10-29 17:02:49.372523
800	Mariga	\N	mariga	37	NIG	NG	1	2017-10-29 17:02:49.383623	2017-10-29 17:02:49.383635
801	Mashegu	\N	mashegu	37	NIG	NG	1	2017-10-29 17:02:49.393557	2017-10-29 17:02:49.393567
802	Mokwa	\N	mokwa	37	NIG	NG	1	2017-10-29 17:02:49.403534	2017-10-29 17:02:49.403544
803	Moya	\N	moya	37	NIG	NG	1	2017-10-29 17:02:49.413378	2017-10-29 17:02:49.413389
804	Paikoro	\N	paikoro	37	NIG	NG	1	2017-10-29 17:02:49.438215	2017-10-29 17:02:49.438226
805	Rafi	\N	rafi	37	NIG	NG	1	2017-10-29 17:02:49.448014	2017-10-29 17:02:49.448024
806	Rijau	\N	rijau	37	NIG	NG	1	2017-10-29 17:02:49.45831	2017-10-29 17:02:49.45832
807	Shiroro	\N	shiroro	37	NIG	NG	1	2017-10-29 17:02:49.468128	2017-10-29 17:02:49.468139
808	Suleja	\N	suleja	37	NIG	NG	1	2017-10-29 17:02:49.47924	2017-10-29 17:02:49.479252
809	Tafa	\N	tafa	37	NIG	NG	1	2017-10-29 17:02:49.489617	2017-10-29 17:02:49.489628
810	Wushishi	\N	wushishi	37	NIG	NG	1	2017-10-29 17:02:49.499708	2017-10-29 17:02:49.499718
811	Accra	\N	accra	38	GH-0	GH	56	2017-10-29 17:02:49.5095	2017-10-29 17:02:49.509511
812	Ashiaman	\N	ashiaman	38	GH-0	GH	56	2017-10-29 17:02:49.520296	2017-10-29 17:02:49.520307
813	Teshie	\N	teshie	38	GH-0	GH	56	2017-10-29 17:02:49.530157	2017-10-29 17:02:49.530168
814	Tema	\N	tema	38	GH-0	GH	56	2017-10-29 17:02:49.53997	2017-10-29 17:02:49.53998
815	Madina	\N	madina	38	GH-0	GH	56	2017-10-29 17:02:49.549715	2017-10-29 17:02:49.549735
816	Nungua	\N	nungua	38	GH-0	GH	56	2017-10-29 17:02:49.560418	2017-10-29 17:02:49.560429
817	Lashibi	\N	lashibi	38	GH-0	GH	56	2017-10-29 17:02:49.570092	2017-10-29 17:02:49.570103
818	Dome	\N	dome	38	GH-0	GH	56	2017-10-29 17:02:49.579951	2017-10-29 17:02:49.579961
819	Tema New Town	\N	tema-new-town	38	GH-0	GH	56	2017-10-29 17:02:49.589892	2017-10-29 17:02:49.589903
820	Gbawe	\N	gbawe	38	GH-0	GH	56	2017-10-29 17:02:49.600565	2017-10-29 17:02:49.600575
821	Adenta East	\N	adenta-east	38	GH-0	GH	56	2017-10-29 17:02:49.61027	2017-10-29 17:02:49.610281
822	Taifa	\N	taifa	38	GH-0	GH	56	2017-10-29 17:02:49.620146	2017-10-29 17:02:49.620157
823	Kumasi	\N	kumasi	39	GH-1	GH	56	2017-10-29 17:02:49.629989	2017-10-29 17:02:49.629999
824	Obuasi	\N	obuasi	39	GH-1	GH	56	2017-10-29 17:02:49.640907	2017-10-29 17:02:49.640918
825	Ejura	\N	ejura	39	GH-1	GH	56	2017-10-29 17:02:49.650799	2017-10-29 17:02:49.65081
826	Tafo	\N	tafo	39	GH-1	GH	56	2017-10-29 17:02:49.660676	2017-10-29 17:02:49.66069
827	Mampong	\N	mampong	39	GH-1	GH	56	2017-10-29 17:02:49.670711	2017-10-29 17:02:49.670723
828	Konongo	\N	konongo	39	GH-1	GH	56	2017-10-29 17:02:49.682235	2017-10-29 17:02:49.682248
829	Agogo	\N	agogo	39	GH-1	GH	56	2017-10-29 17:02:49.69274	2017-10-29 17:02:49.692751
830	Bekwai	\N	bekwai	39	GH-1	GH	56	2017-10-29 17:02:49.703116	2017-10-29 17:02:49.703127
831	Tamale	\N	tamale	40	GH-2	GH	56	2017-10-29 17:02:49.713274	2017-10-29 17:02:49.713286
832	Yendi	\N	yendi	40	GH-2	GH	56	2017-10-29 17:02:49.723905	2017-10-29 17:02:49.723917
833	Savelugu	\N	savelugu	40	GH-2	GH	56	2017-10-29 17:02:49.733866	2017-10-29 17:02:49.733878
834	Salaga	\N	salaga	40	GH-2	GH	56	2017-10-29 17:02:49.743946	2017-10-29 17:02:49.743958
835	Kpandae	\N	kpandae	40	GH-2	GH	56	2017-10-29 17:02:49.754556	2017-10-29 17:02:49.75457
836	Sekondi-Takoradi	\N	sekondi-takoradi	41	GH-3	GH	56	2017-10-29 17:02:49.766635	2017-10-29 17:02:49.766648
837	Prestea	\N	prestea	41	GH-3	GH	56	2017-10-29 17:02:49.77848	2017-10-29 17:02:49.778531
838	Effiakuma	\N	effiakuma	41	GH-3	GH	56	2017-10-29 17:02:49.78937	2017-10-29 17:02:49.789382
839	Tarkwa	\N	tarkwa	41	GH-3	GH	56	2017-10-29 17:02:49.800039	2017-10-29 17:02:49.800051
840	Axim	\N	axim	41	GH-3	GH	56	2017-10-29 17:02:49.81136	2017-10-29 17:02:49.811374
841	Shama	\N	shama	41	GH-3	GH	56	2017-10-29 17:02:49.822199	2017-10-29 17:02:49.822212
842	Bibiani	\N	bibiani	41	GH-3	GH	56	2017-10-29 17:02:49.832633	2017-10-29 17:02:49.832645
843	Aboso	\N	aboso	41	GH-3	GH	56	2017-10-29 17:02:49.842998	2017-10-29 17:02:49.843011
844	Sunyani	\N	sunyani	42	GH-4	GH	56	2017-10-29 17:02:49.853713	2017-10-29 17:02:49.853733
845	Techiman	\N	techiman	42	GH-4	GH	56	2017-10-29 17:02:49.864671	2017-10-29 17:02:49.864684
846	Berekum	\N	berekum	42	GH-4	GH	56	2017-10-29 17:02:49.875139	2017-10-29 17:02:49.875152
847	Kintampo	\N	kintampo	42	GH-4	GH	56	2017-10-29 17:02:49.885412	2017-10-29 17:02:49.885425
848	Wenchi	\N	wenchi	42	GH-4	GH	56	2017-10-29 17:02:49.901112	2017-10-29 17:02:49.90113
849	Bechem	\N	bechem	42	GH-4	GH	56	2017-10-29 17:02:49.916702	2017-10-29 17:02:49.916721
850	Duayaw Nkwanta	\N	duayaw-nkwanta	42	GH-4	GH	56	2017-10-29 17:02:49.930302	2017-10-29 17:02:49.930313
851	Japekrom	\N	japekrom	42	GH-4	GH	56	2017-10-29 17:02:49.941108	2017-10-29 17:02:49.94112
852	Banda Ahenkro	\N	banda-ahenkro	42	GH-4	GH	56	2017-10-29 17:02:49.952484	2017-10-29 17:02:49.952496
853	Cape Coast	\N	cape-coast	43	GH-5	GH	56	2017-10-29 17:02:49.975276	2017-10-29 17:02:49.975294
854	Oduponkpehe	\N	oduponkpehe	43	GH-5	GH	56	2017-10-29 17:02:49.988713	2017-10-29 17:02:49.988736
855	Agona Swedru	\N	agona-swedru	43	GH-5	GH	56	2017-10-29 17:02:50.00184	2017-10-29 17:02:50.001854
856	Winneba	\N	winneba	43	GH-5	GH	56	2017-10-29 17:02:50.013958	2017-10-29 17:02:50.013971
857	Elmina	\N	elmina	43	GH-5	GH	56	2017-10-29 17:02:50.02441	2017-10-29 17:02:50.024422
858	Dunkwa-on-Offin	\N	dunkwa-on-offin	43	GH-5	GH	56	2017-10-29 17:02:50.034894	2017-10-29 17:02:50.034907
859	Apam	\N	apam	43	GH-5	GH	56	2017-10-29 17:02:50.045771	2017-10-29 17:02:50.045784
860	Saltpond	\N	saltpond	43	GH-5	GH	56	2017-10-29 17:02:50.056989	2017-10-29 17:02:50.057001
861	Nyakrom	\N	nyakrom	43	GH-5	GH	56	2017-10-29 17:02:50.066977	2017-10-29 17:02:50.066989
862	Foso	\N	foso	43	GH-5	GH	56	2017-10-29 17:02:50.076861	2017-10-29 17:02:50.076872
863	Mumford	\N	mumford	43	GH-5	GH	56	2017-10-29 17:02:50.087417	2017-10-29 17:02:50.087432
864	Anomabu	\N	anomabu	43	GH-5	GH	56	2017-10-29 17:02:50.0986	2017-10-29 17:02:50.098611
865	Koforidua	\N	koforidua	44	GH-6	GH	56	2017-10-29 17:02:50.108742	2017-10-29 17:02:50.108756
866	Nkawkaw	\N	nkawkaw	44	GH-6	GH	56	2017-10-29 17:02:50.120735	2017-10-29 17:02:50.12075
867	Akim Oda	\N	akim-oda	44	GH-6	GH	56	2017-10-29 17:02:50.134161	2017-10-29 17:02:50.134175
868	Suhum	\N	suhum	44	GH-6	GH	56	2017-10-29 17:02:50.145945	2017-10-29 17:02:50.145959
869	Nsawam	\N	nsawam	44	GH-6	GH	56	2017-10-29 17:02:50.156655	2017-10-29 17:02:50.156669
870	Asamankese	\N	asamankese	44	GH-6	GH	56	2017-10-29 17:02:50.167251	2017-10-29 17:02:50.167265
871	Begoro	\N	begoro	44	GH-6	GH	56	2017-10-29 17:02:50.177595	2017-10-29 17:02:50.177608
872	Akwatia	\N	akwatia	44	GH-6	GH	56	2017-10-29 17:02:50.190142	2017-10-29 17:02:50.190156
873	Somanya	\N	somanya	44	GH-6	GH	56	2017-10-29 17:02:50.208108	2017-10-29 17:02:50.208124
874	Aburi	\N	aburi	44	GH-6	GH	56	2017-10-29 17:02:50.219941	2017-10-29 17:02:50.219955
875	Kade	\N	kade	44	GH-6	GH	56	2017-10-29 17:02:50.239845	2017-10-29 17:02:50.239859
876	Akropong	\N	akropong	44	GH-6	GH	56	2017-10-29 17:02:50.251649	2017-10-29 17:02:50.251659
877	Kibi	\N	kibi	44	GH-6	GH	56	2017-10-29 17:02:50.263658	2017-10-29 17:02:50.263668
878	Mpraeso	\N	mpraeso	44	GH-6	GH	56	2017-10-29 17:02:50.274341	2017-10-29 17:02:50.274351
879	Akim Swedru	\N	akim-swedru	44	GH-6	GH	56	2017-10-29 17:02:50.286691	2017-10-29 17:02:50.2867
880	Wa	\N	wa	45	GH-7	GH	56	2017-10-29 17:02:50.300201	2017-10-29 17:02:50.300217
881	Bawku	\N	bawku	46	GH-8	GH	56	2017-10-29 17:02:50.315005	2017-10-29 17:02:50.315021
882	Bolgatanga	\N	bolgatanga	46	GH-8	GH	56	2017-10-29 17:02:50.326576	2017-10-29 17:02:50.32659
883	Navrongo	\N	navrongo	46	GH-8	GH	56	2017-10-29 17:02:50.338726	2017-10-29 17:02:50.338739
884	Ho	\N	ho	47	GH-9	GH	56	2017-10-29 17:02:50.351335	2017-10-29 17:02:50.351349
885	Aflao	\N	aflao	47	GH-9	GH	56	2017-10-29 17:02:50.362091	2017-10-29 17:02:50.362103
886	Hohoe	\N	hohoe	47	GH-9	GH	56	2017-10-29 17:02:50.373041	2017-10-29 17:02:50.373054
887	Anloga	\N	anloga	47	GH-9	GH	56	2017-10-29 17:02:50.383762	2017-10-29 17:02:50.383785
888	Kpandu	\N	kpandu	47	GH-9	GH	56	2017-10-29 17:02:50.396685	2017-10-29 17:02:50.3967
889	Keta	\N	keta	47	GH-9	GH	56	2017-10-29 17:02:50.409466	2017-10-29 17:02:50.40948
890	Kete-Krachi	\N	kete-krachi	47	GH-9	GH	56	2017-10-29 17:02:50.420764	2017-10-29 17:02:50.420777
891	Mombasa	\N	mombasa	48	KE-0	KE	180	2017-10-29 17:02:50.431761	2017-10-29 17:02:50.431774
892	Kwale	\N	kwale	48	KE-0	KE	180	2017-10-29 17:02:50.443458	2017-10-29 17:02:50.443473
893	Kilifi	\N	kilifi	48	KE-0	KE	180	2017-10-29 17:02:50.453928	2017-10-29 17:02:50.453942
894	Tana River	\N	tana-river	48	KE-0	KE	180	2017-10-29 17:02:50.46443	2017-10-29 17:02:50.464444
895	Lamu	\N	lamu	48	KE-0	KE	180	2017-10-29 17:02:50.485405	2017-10-29 17:02:50.485432
896	Taita-Taveta	\N	taita-taveta	48	KE-0	KE	180	2017-10-29 17:02:50.500522	2017-10-29 17:02:50.500538
897	Garissa	\N	garissa	49	KE-1	KE	180	2017-10-29 17:02:50.514393	2017-10-29 17:02:50.514409
898	Wajir	\N	wajir	49	KE-1	KE	180	2017-10-29 17:02:50.528015	2017-10-29 17:02:50.528032
899	Mandera	\N	mandera	49	KE-1	KE	180	2017-10-29 17:02:50.540898	2017-10-29 17:02:50.540914
900	Marsabit	\N	marsabit	50	KE-2	KE	180	2017-10-29 17:02:50.555743	2017-10-29 17:02:50.555758
901	Isiolo	\N	isiolo	50	KE-2	KE	180	2017-10-29 17:02:50.567866	2017-10-29 17:02:50.56788
902	Meru	\N	meru	50	KE-2	KE	180	2017-10-29 17:02:50.578741	2017-10-29 17:02:50.578754
903	Tharaka-Nithi	\N	tharaka-nithi	50	KE-2	KE	180	2017-10-29 17:02:50.59084	2017-10-29 17:02:50.590855
904	Embu	\N	embu	50	KE-2	KE	180	2017-10-29 17:02:50.602081	2017-10-29 17:02:50.602102
905	Kitui	\N	kitui	50	KE-2	KE	180	2017-10-29 17:02:50.612297	2017-10-29 17:02:50.612311
906	Machakos	\N	machakos	50	KE-2	KE	180	2017-10-29 17:02:50.622804	2017-10-29 17:02:50.622816
907	Makueni	\N	makueni	50	KE-2	KE	180	2017-10-29 17:02:50.633938	2017-10-29 17:02:50.633952
908	Nyandarua	\N	nyandarua	51	KE-3	KE	180	2017-10-29 17:02:50.645729	2017-10-29 17:02:50.645753
909	Nyeri	\N	nyeri	51	KE-3	KE	180	2017-10-29 17:02:50.65726	2017-10-29 17:02:50.657274
910	Kirinyaga	\N	kirinyaga	51	KE-3	KE	180	2017-10-29 17:02:50.668388	2017-10-29 17:02:50.668402
911	Murang'a	\N	murang-a	51	KE-3	KE	180	2017-10-29 17:02:50.679412	2017-10-29 17:02:50.679426
912	Kiambu	\N	kiambu	51	KE-3	KE	180	2017-10-29 17:02:50.691208	2017-10-29 17:02:50.691223
913	Turkana	\N	turkana	52	KE-4	KE	180	2017-10-29 17:02:50.703931	2017-10-29 17:02:50.703947
914	West Pokot	\N	west-pokot	52	KE-4	KE	180	2017-10-29 17:02:50.715929	2017-10-29 17:02:50.715941
915	Samburu	\N	samburu	52	KE-4	KE	180	2017-10-29 17:02:50.725914	2017-10-29 17:02:50.725925
916	Trans Nzoia	\N	trans-nzoia	52	KE-4	KE	180	2017-10-29 17:02:50.737042	2017-10-29 17:02:50.737054
917	Uasin Gishu	\N	uasin-gishu	52	KE-4	KE	180	2017-10-29 17:02:50.747011	2017-10-29 17:02:50.747023
918	Elgeyo-Marakwet	\N	elgeyo-marakwet	52	KE-4	KE	180	2017-10-29 17:02:50.756826	2017-10-29 17:02:50.756838
919	Nandi	\N	nandi	52	KE-4	KE	180	2017-10-29 17:02:50.766809	2017-10-29 17:02:50.766821
920	Baringo	\N	baringo	52	KE-4	KE	180	2017-10-29 17:02:50.777913	2017-10-29 17:02:50.777925
921	Laikipia	\N	laikipia	52	KE-4	KE	180	2017-10-29 17:02:50.787903	2017-10-29 17:02:50.787914
922	Nakuru	\N	nakuru	52	KE-4	KE	180	2017-10-29 17:02:50.797443	2017-10-29 17:02:50.797455
923	Narok	\N	narok	52	KE-4	KE	180	2017-10-29 17:02:50.807343	2017-10-29 17:02:50.807355
924	Kajiado	\N	kajiado	52	KE-4	KE	180	2017-10-29 17:02:50.81834	2017-10-29 17:02:50.818353
925	Kericho	\N	kericho	52	KE-4	KE	180	2017-10-29 17:02:50.828203	2017-10-29 17:02:50.828214
926	Bomet	\N	bomet	52	KE-4	KE	180	2017-10-29 17:02:50.839826	2017-10-29 17:02:50.839837
927	Kakamega	\N	kakamega	53	KE-5	KE	180	2017-10-29 17:02:50.849439	2017-10-29 17:02:50.849449
928	Vihiga	\N	vihiga	53	KE-5	KE	180	2017-10-29 17:02:50.859367	2017-10-29 17:02:50.859377
929	Bungoma	\N	bungoma	53	KE-5	KE	180	2017-10-29 17:02:50.86902	2017-10-29 17:02:50.86903
930	Busia	\N	busia	53	KE-5	KE	180	2017-10-29 17:02:50.878602	2017-10-29 17:02:50.878614
931	Siaya	\N	siaya	54	KE-6	KE	180	2017-10-29 17:02:50.88833	2017-10-29 17:02:50.888346
932	Kisumu	\N	kisumu	54	KE-6	KE	180	2017-10-29 17:02:50.900243	2017-10-29 17:02:50.900257
933	Homa Bay	\N	homa-bay	54	KE-6	KE	180	2017-10-29 17:02:50.911601	2017-10-29 17:02:50.911615
934	Migori	\N	migori	54	KE-6	KE	180	2017-10-29 17:02:50.92242	2017-10-29 17:02:50.922433
935	Kisii	\N	kisii	54	KE-6	KE	180	2017-10-29 17:02:50.932971	2017-10-29 17:02:50.932984
936	Nyamira	\N	nyamira	54	KE-6	KE	180	2017-10-29 17:02:50.945009	2017-10-29 17:02:50.945023
937	Nairobi	\N	nairobi	55	KE-7	KE	180	2017-10-29 17:02:50.955458	2017-10-29 17:02:50.95547
\.


--
-- Name: city_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('city_id_seq', 937, true);


--
-- Data for Name: conversation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY conversation (id, last_updated, date_created) FROM stdin;
\.


--
-- Name: conversation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('conversation_id_seq', 1, false);


--
-- Data for Name: conversee; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY conversee (id, conversation_id, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: conversee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('conversee_id_seq', 1, false);


--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY country (id, code, name, slug, phone_code, enabled, last_updated, date_created) FROM stdin;
1	NG	Nigeria	nigeria	\N	t	2017-10-29 17:02:31.13374	2017-10-29 17:02:31.133763
2	BD	Bangladesh	bangladesh	\N	f	2017-10-29 17:02:31.140265	2017-10-29 17:02:31.140275
3	BE	Belgium	belgium	\N	f	2017-10-29 17:02:31.145258	2017-10-29 17:02:31.145268
4	BF	Burkina Faso	burkina-faso	\N	f	2017-10-29 17:02:31.150227	2017-10-29 17:02:31.150237
5	BG	Bulgaria	bulgaria	\N	f	2017-10-29 17:02:31.155382	2017-10-29 17:02:31.155393
6	BA	Bosnia and Herzegovina	bosnia-and-herzegovina	\N	f	2017-10-29 17:02:31.160748	2017-10-29 17:02:31.160758
7	BB	Barbados	barbados	\N	f	2017-10-29 17:02:31.165547	2017-10-29 17:02:31.165558
8	WF	Wallis and Futuna	wallis-and-futuna	\N	f	2017-10-29 17:02:31.172037	2017-10-29 17:02:31.172048
9	JT	Johnston Island	johnston-island	\N	f	2017-10-29 17:02:31.177772	2017-10-29 17:02:31.177782
10	BM	Bermuda	bermuda	\N	f	2017-10-29 17:02:31.183446	2017-10-29 17:02:31.183456
11	BN	Brunei	brunei	\N	f	2017-10-29 17:02:31.189142	2017-10-29 17:02:31.189152
12	BO	Bolivia	bolivia	\N	f	2017-10-29 17:02:31.194055	2017-10-29 17:02:31.194066
13	BH	Bahrain	bahrain	\N	f	2017-10-29 17:02:31.19888	2017-10-29 17:02:31.19889
14	BI	Burundi	burundi	\N	f	2017-10-29 17:02:31.204363	2017-10-29 17:02:31.204373
15	BJ	Benin	benin	\N	f	2017-10-29 17:02:31.209133	2017-10-29 17:02:31.209143
16	BT	Bhutan	bhutan	\N	f	2017-10-29 17:02:31.214039	2017-10-29 17:02:31.214049
17	JM	Jamaica	jamaica	\N	f	2017-10-29 17:02:31.218973	2017-10-29 17:02:31.218983
18	BV	Bouvet Island	bouvet-island	\N	f	2017-10-29 17:02:31.224225	2017-10-29 17:02:31.224235
19	BW	Botswana	botswana	\N	f	2017-10-29 17:02:31.229649	2017-10-29 17:02:31.22966
20	WS	Samoa	samoa	\N	f	2017-10-29 17:02:31.23501	2017-10-29 17:02:31.23502
21	BQ	British Antarctic Territory	british-antarctic-territory	\N	f	2017-10-29 17:02:31.239917	2017-10-29 17:02:31.239928
22	BR	Brazil	brazil	\N	f	2017-10-29 17:02:31.244879	2017-10-29 17:02:31.244889
23	BS	Bahamas	bahamas	\N	f	2017-10-29 17:02:31.250187	2017-10-29 17:02:31.250197
24	JE	Jersey	jersey	\N	f	2017-10-29 17:02:31.255377	2017-10-29 17:02:31.255388
25	WK	Wake Island	wake-island	\N	f	2017-10-29 17:02:31.260817	2017-10-29 17:02:31.260837
26	BY	Belarus	belarus	\N	f	2017-10-29 17:02:31.266727	2017-10-29 17:02:31.266739
27	BZ	Belize	belize	\N	f	2017-10-29 17:02:31.272247	2017-10-29 17:02:31.272258
28	RU	Russia	russia	\N	f	2017-10-29 17:02:31.27764	2017-10-29 17:02:31.277651
29	RW	Rwanda	rwanda	\N	f	2017-10-29 17:02:31.282939	2017-10-29 17:02:31.28295
30	PC	Pacific Islands Trust Territory	pacific-islands-trust-territory	\N	f	2017-10-29 17:02:31.288113	2017-10-29 17:02:31.288124
31	TL	Timor-Leste	timor-leste	\N	f	2017-10-29 17:02:31.293424	2017-10-29 17:02:31.293435
32	RE	Réunion	reunion	\N	f	2017-10-29 17:02:31.299042	2017-10-29 17:02:31.299053
33	TM	Turkmenistan	turkmenistan	\N	f	2017-10-29 17:02:31.304269	2017-10-29 17:02:31.304281
34	TJ	Tajikistan	tajikistan	\N	f	2017-10-29 17:02:31.30949	2017-10-29 17:02:31.3095
35	RO	Romania	romania	\N	f	2017-10-29 17:02:31.314762	2017-10-29 17:02:31.314772
36	TK	Tokelau	tokelau	\N	f	2017-10-29 17:02:31.320288	2017-10-29 17:02:31.320299
37	GW	Guinea-Bissau	guinea-bissau	\N	f	2017-10-29 17:02:31.32552	2017-10-29 17:02:31.325531
38	GU	Guam	guam	\N	f	2017-10-29 17:02:31.330613	2017-10-29 17:02:31.330623
39	GT	Guatemala	guatemala	\N	f	2017-10-29 17:02:31.335827	2017-10-29 17:02:31.335844
40	GS	South Georgia and the South Sandwich Islands	south-georgia-and-the-south-sandwich-islands	\N	f	2017-10-29 17:02:31.341054	2017-10-29 17:02:31.341065
41	GR	Greece	greece	\N	f	2017-10-29 17:02:31.346664	2017-10-29 17:02:31.346674
42	GQ	Equatorial Guinea	equatorial-guinea	\N	f	2017-10-29 17:02:31.351787	2017-10-29 17:02:31.351797
43	GP	Guadeloupe	guadeloupe	\N	f	2017-10-29 17:02:31.356883	2017-10-29 17:02:31.356893
44	JP	Japan	japan	\N	f	2017-10-29 17:02:31.361918	2017-10-29 17:02:31.361929
45	GY	Guyana	guyana	\N	f	2017-10-29 17:02:31.367256	2017-10-29 17:02:31.367267
46	GG	Guernsey	guernsey	\N	f	2017-10-29 17:02:31.372484	2017-10-29 17:02:31.372494
47	GF	French Guiana	french-guiana	\N	f	2017-10-29 17:02:31.377628	2017-10-29 17:02:31.377639
48	GE	Georgia	georgia	\N	f	2017-10-29 17:02:31.382898	2017-10-29 17:02:31.382909
49	GD	Grenada	grenada	\N	f	2017-10-29 17:02:31.38813	2017-10-29 17:02:31.388141
50	GB	United Kingdom	united-kingdom	\N	f	2017-10-29 17:02:31.393829	2017-10-29 17:02:31.393839
51	GA	Gabon	gabon	\N	f	2017-10-29 17:02:31.399173	2017-10-29 17:02:31.399184
52	GN	Guinea	guinea	\N	f	2017-10-29 17:02:31.404461	2017-10-29 17:02:31.404471
53	GM	Gambia	gambia	\N	f	2017-10-29 17:02:31.409637	2017-10-29 17:02:31.409648
54	GL	Greenland	greenland	\N	f	2017-10-29 17:02:31.41501	2017-10-29 17:02:31.415021
55	GI	Gibraltar	gibraltar	\N	f	2017-10-29 17:02:31.420156	2017-10-29 17:02:31.420167
56	GH	Ghana	ghana	\N	f	2017-10-29 17:02:31.425237	2017-10-29 17:02:31.425248
57	OM	Oman	oman	\N	f	2017-10-29 17:02:31.432226	2017-10-29 17:02:31.432241
58	TN	Tunisia	tunisia	\N	f	2017-10-29 17:02:31.438484	2017-10-29 17:02:31.438529
59	JO	Jordan	jordan	\N	f	2017-10-29 17:02:31.444966	2017-10-29 17:02:31.444977
60	HR	Croatia	croatia	\N	f	2017-10-29 17:02:31.450641	2017-10-29 17:02:31.450652
61	HT	Haiti	haiti	\N	f	2017-10-29 17:02:31.456014	2017-10-29 17:02:31.456025
62	HU	Hungary	hungary	\N	f	2017-10-29 17:02:31.461208	2017-10-29 17:02:31.461219
63	HK	Hong Kong SAR China	hong-kong-sar-china	\N	f	2017-10-29 17:02:31.466597	2017-10-29 17:02:31.466628
64	HN	Honduras	honduras	\N	f	2017-10-29 17:02:31.47302	2017-10-29 17:02:31.473035
65	SU	Union of Soviet Socialist Republics	union-of-soviet-socialist-republics	\N	f	2017-10-29 17:02:31.47871	2017-10-29 17:02:31.478723
66	HM	Heard Island and McDonald Islands	heard-island-and-mcdonald-islands	\N	f	2017-10-29 17:02:31.484387	2017-10-29 17:02:31.484399
67	AE	United Arab Emirates	united-arab-emirates	\N	f	2017-10-29 17:02:31.490639	2017-10-29 17:02:31.490652
68	VE	Venezuela	venezuela	\N	f	2017-10-29 17:02:31.496948	2017-10-29 17:02:31.49696
69	PR	Puerto Rico	puerto-rico	\N	f	2017-10-29 17:02:31.506415	2017-10-29 17:02:31.506436
70	PS	Palestinian Territories	palestinian-territories	\N	f	2017-10-29 17:02:31.512867	2017-10-29 17:02:31.512877
71	UA	Ukraine	ukraine	\N	f	2017-10-29 17:02:31.520534	2017-10-29 17:02:31.520552
72	PW	Palau	palau	\N	f	2017-10-29 17:02:31.525999	2017-10-29 17:02:31.52601
73	PT	Portugal	portugal	\N	f	2017-10-29 17:02:31.531088	2017-10-29 17:02:31.531217
74	KN	Saint Kitts and Nevis	saint-kitts-and-nevis	\N	f	2017-10-29 17:02:31.537175	2017-10-29 17:02:31.537189
75	PZ	Panama Canal Zone	panama-canal-zone	\N	f	2017-10-29 17:02:31.543103	2017-10-29 17:02:31.543114
76	AF	Afghanistan	afghanistan	\N	f	2017-10-29 17:02:31.548615	2017-10-29 17:02:31.548627
77	IQ	Iraq	iraq	\N	f	2017-10-29 17:02:31.555239	2017-10-29 17:02:31.555251
78	PA	Panama	panama	\N	f	2017-10-29 17:02:31.560258	2017-10-29 17:02:31.560268
79	PF	French Polynesia	french-polynesia	\N	f	2017-10-29 17:02:31.565092	2017-10-29 17:02:31.565102
80	PG	Papua New Guinea	papua-new-guinea	\N	f	2017-10-29 17:02:31.570236	2017-10-29 17:02:31.570247
81	PE	Peru	peru	\N	f	2017-10-29 17:02:31.577989	2017-10-29 17:02:31.578006
82	PK	Pakistan	pakistan	\N	f	2017-10-29 17:02:31.584216	2017-10-29 17:02:31.584226
83	PH	Philippines	philippines	\N	f	2017-10-29 17:02:31.589348	2017-10-29 17:02:31.589359
84	PN	Pitcairn Islands	pitcairn-islands	\N	f	2017-10-29 17:02:31.596196	2017-10-29 17:02:31.596214
85	PL	Poland	poland	\N	f	2017-10-29 17:02:31.60429	2017-10-29 17:02:31.604327
86	PM	Saint Pierre and Miquelon	saint-pierre-and-miquelon	\N	f	2017-10-29 17:02:31.60992	2017-10-29 17:02:31.609938
87	ZM	Zambia	zambia	\N	f	2017-10-29 17:02:31.620292	2017-10-29 17:02:31.62031
88	EH	Western Sahara	western-sahara	\N	f	2017-10-29 17:02:31.629324	2017-10-29 17:02:31.629341
89	EE	Estonia	estonia	\N	f	2017-10-29 17:02:31.63616	2017-10-29 17:02:31.636172
90	EG	Egypt	egypt	\N	f	2017-10-29 17:02:31.643547	2017-10-29 17:02:31.643562
91	ZA	South Africa	south-africa	\N	f	2017-10-29 17:02:31.651211	2017-10-29 17:02:31.651225
92	EC	Ecuador	ecuador	\N	f	2017-10-29 17:02:31.657965	2017-10-29 17:02:31.657978
93	IT	Italy	italy	\N	f	2017-10-29 17:02:31.664947	2017-10-29 17:02:31.664965
94	VN	Vietnam	vietnam	\N	f	2017-10-29 17:02:31.673954	2017-10-29 17:02:31.673972
95	ZZ	Unknown or Invalid Region	unknown-or-invalid-region	\N	f	2017-10-29 17:02:31.681203	2017-10-29 17:02:31.681215
96	SB	Solomon Islands	solomon-islands	\N	f	2017-10-29 17:02:31.687448	2017-10-29 17:02:31.687459
97	ET	Ethiopia	ethiopia	\N	f	2017-10-29 17:02:31.692825	2017-10-29 17:02:31.692835
98	SO	Somalia	somalia	\N	f	2017-10-29 17:02:31.698032	2017-10-29 17:02:31.698043
99	ZW	Zimbabwe	zimbabwe	\N	f	2017-10-29 17:02:31.70305	2017-10-29 17:02:31.70306
100	SA	Saudi Arabia	saudi-arabia	\N	f	2017-10-29 17:02:31.707962	2017-10-29 17:02:31.707973
101	ES	Spain	spain	\N	f	2017-10-29 17:02:31.714734	2017-10-29 17:02:31.714747
102	ER	Eritrea	eritrea	\N	f	2017-10-29 17:02:31.722317	2017-10-29 17:02:31.722331
103	ME	Montenegro	montenegro	\N	f	2017-10-29 17:02:31.729906	2017-10-29 17:02:31.72992
104	MD	Moldova	moldova	\N	f	2017-10-29 17:02:31.73707	2017-10-29 17:02:31.737084
105	MG	Madagascar	madagascar	\N	f	2017-10-29 17:02:31.745439	2017-10-29 17:02:31.745458
106	MF	Saint Martin	saint-martin	\N	f	2017-10-29 17:02:31.753253	2017-10-29 17:02:31.753266
107	MA	Morocco	morocco	\N	f	2017-10-29 17:02:31.761394	2017-10-29 17:02:31.761408
108	MC	Monaco	monaco	\N	f	2017-10-29 17:02:31.76912	2017-10-29 17:02:31.769134
109	UZ	Uzbekistan	uzbekistan	\N	f	2017-10-29 17:02:31.775941	2017-10-29 17:02:31.775953
110	MM	Myanmar [Burma]	myanmar-burma	\N	f	2017-10-29 17:02:31.782407	2017-10-29 17:02:31.782419
111	ML	Mali	mali	\N	f	2017-10-29 17:02:31.791185	2017-10-29 17:02:31.791198
112	MO	Macau SAR China	macau-sar-china	\N	f	2017-10-29 17:02:31.79882	2017-10-29 17:02:31.798831
113	MN	Mongolia	mongolia	\N	f	2017-10-29 17:02:31.807792	2017-10-29 17:02:31.807811
114	MI	Midway Islands	midway-islands	\N	f	2017-10-29 17:02:31.815747	2017-10-29 17:02:31.815759
115	MH	Marshall Islands	marshall-islands	\N	f	2017-10-29 17:02:31.821449	2017-10-29 17:02:31.821461
116	MK	Macedonia	macedonia	\N	f	2017-10-29 17:02:31.826924	2017-10-29 17:02:31.826935
117	MU	Mauritius	mauritius	\N	f	2017-10-29 17:02:31.83303	2017-10-29 17:02:31.833041
118	MT	Malta	malta	\N	f	2017-10-29 17:02:31.838397	2017-10-29 17:02:31.838408
119	MW	Malawi	malawi	\N	f	2017-10-29 17:02:31.843663	2017-10-29 17:02:31.843675
120	MV	Maldives	maldives	\N	f	2017-10-29 17:02:31.852149	2017-10-29 17:02:31.852166
121	MQ	Martinique	martinique	\N	f	2017-10-29 17:02:31.86081	2017-10-29 17:02:31.860827
122	MP	Northern Mariana Islands	northern-mariana-islands	\N	f	2017-10-29 17:02:31.868592	2017-10-29 17:02:31.868609
123	MS	Montserrat	montserrat	\N	f	2017-10-29 17:02:31.874141	2017-10-29 17:02:31.874152
124	MR	Mauritania	mauritania	\N	f	2017-10-29 17:02:31.879254	2017-10-29 17:02:31.879265
125	IM	Isle of Man	isle-of-man	\N	f	2017-10-29 17:02:31.884679	2017-10-29 17:02:31.88469
126	UG	Uganda	uganda	\N	f	2017-10-29 17:02:31.890174	2017-10-29 17:02:31.890185
127	MY	Malaysia	malaysia	\N	f	2017-10-29 17:02:31.898458	2017-10-29 17:02:31.898475
128	MX	Mexico	mexico	\N	f	2017-10-29 17:02:31.90613	2017-10-29 17:02:31.906142
129	IL	Israel	israel	\N	f	2017-10-29 17:02:31.913194	2017-10-29 17:02:31.913206
130	FQ	French Southern and Antarctic Territories	french-southern-and-antarctic-territories	\N	f	2017-10-29 17:02:31.920888	2017-10-29 17:02:31.92092
131	FR	France	france	\N	f	2017-10-29 17:02:31.930026	2017-10-29 17:02:31.93004
132	AW	Aruba	aruba	\N	f	2017-10-29 17:02:31.938708	2017-10-29 17:02:31.938725
133	FX	Metropolitan France	metropolitan-france	\N	f	2017-10-29 17:02:31.947425	2017-10-29 17:02:31.947438
134	SH	Saint Helena	saint-helena	\N	f	2017-10-29 17:02:31.955249	2017-10-29 17:02:31.955265
135	AX	Åland Islands	aland-islands	\N	f	2017-10-29 17:02:31.963834	2017-10-29 17:02:31.963852
136	SJ	Svalbard and Jan Mayen	svalbard-and-jan-mayen	\N	f	2017-10-29 17:02:31.972938	2017-10-29 17:02:31.972951
137	FI	Finland	finland	\N	f	2017-10-29 17:02:31.980612	2017-10-29 17:02:31.980623
138	FJ	Fiji	fiji	\N	f	2017-10-29 17:02:31.986095	2017-10-29 17:02:31.986106
139	FK	Falkland Islands	falkland-islands	\N	f	2017-10-29 17:02:31.991441	2017-10-29 17:02:31.991452
140	FM	Micronesia	micronesia	\N	f	2017-10-29 17:02:31.99939	2017-10-29 17:02:31.999408
141	FO	Faroe Islands	faroe-islands	\N	f	2017-10-29 17:02:32.006466	2017-10-29 17:02:32.006483
142	NI	Nicaragua	nicaragua	\N	f	2017-10-29 17:02:32.012093	2017-10-29 17:02:32.012103
143	NL	Netherlands	netherlands	\N	f	2017-10-29 17:02:32.021634	2017-10-29 17:02:32.021656
144	NO	Norway	norway	\N	f	2017-10-29 17:02:32.0289	2017-10-29 17:02:32.02891
145	NA	Namibia	namibia	\N	f	2017-10-29 17:02:32.03419	2017-10-29 17:02:32.0342
146	VU	Vanuatu	vanuatu	\N	f	2017-10-29 17:02:32.039629	2017-10-29 17:02:32.03964
147	NC	New Caledonia	new-caledonia	\N	f	2017-10-29 17:02:32.046573	2017-10-29 17:02:32.046586
148	NE	Niger	niger	\N	f	2017-10-29 17:02:32.053986	2017-10-29 17:02:32.054002
149	NF	Norfolk Island	norfolk-island	\N	f	2017-10-29 17:02:32.059557	2017-10-29 17:02:32.059568
150	NZ	New Zealand	new-zealand	\N	f	2017-10-29 17:02:32.065617	2017-10-29 17:02:32.065631
151	NP	Nepal	nepal	\N	f	2017-10-29 17:02:32.073239	2017-10-29 17:02:32.073256
152	NQ	Dronning Maud Land	dronning-maud-land	\N	f	2017-10-29 17:02:32.082212	2017-10-29 17:02:32.082232
153	NR	Nauru	nauru	\N	f	2017-10-29 17:02:32.088989	2017-10-29 17:02:32.089
154	NT	Neutral Zone	neutral-zone	\N	f	2017-10-29 17:02:32.094342	2017-10-29 17:02:32.094352
155	NU	Niue	niue	\N	f	2017-10-29 17:02:32.100617	2017-10-29 17:02:32.100635
156	CK	Cook Islands	cook-islands	\N	f	2017-10-29 17:02:32.108414	2017-10-29 17:02:32.108432
157	CI	Côte d’Ivoire	cote-divoire	\N	f	2017-10-29 17:02:32.116105	2017-10-29 17:02:32.116118
158	CH	Switzerland	switzerland	\N	f	2017-10-29 17:02:32.123098	2017-10-29 17:02:32.123112
159	CO	Colombia	colombia	\N	f	2017-10-29 17:02:32.129722	2017-10-29 17:02:32.129736
160	CN	China	china	\N	f	2017-10-29 17:02:32.137072	2017-10-29 17:02:32.13709
161	CM	Cameroon	cameroon	\N	f	2017-10-29 17:02:32.146328	2017-10-29 17:02:32.14634
162	CL	Chile	chile	\N	f	2017-10-29 17:02:32.153151	2017-10-29 17:02:32.153164
163	CC	Cocos [Keeling] Islands	cocos-keeling-islands	\N	f	2017-10-29 17:02:32.160743	2017-10-29 17:02:32.160761
164	CA	Canada	canada	\N	f	2017-10-29 17:02:32.169846	2017-10-29 17:02:32.169859
165	CG	Congo - Brazzaville	congo-brazzaville	\N	f	2017-10-29 17:02:32.176766	2017-10-29 17:02:32.176778
166	CF	Central African Republic	central-african-republic	\N	f	2017-10-29 17:02:32.18519	2017-10-29 17:02:32.18521
167	CD	Congo - Kinshasa	congo-kinshasa	\N	f	2017-10-29 17:02:32.19499	2017-10-29 17:02:32.195003
168	CZ	Czech Republic	czech-republic	\N	f	2017-10-29 17:02:32.201387	2017-10-29 17:02:32.201399
169	CY	Cyprus	cyprus	\N	f	2017-10-29 17:02:32.209099	2017-10-29 17:02:32.209112
170	CX	Christmas Island	christmas-island	\N	f	2017-10-29 17:02:32.216357	2017-10-29 17:02:32.21637
171	CS	Serbia and Montenegro	serbia-and-montenegro	\N	f	2017-10-29 17:02:32.227844	2017-10-29 17:02:32.22787
172	CR	Costa Rica	costa-rica	\N	f	2017-10-29 17:02:32.236484	2017-10-29 17:02:32.236499
173	PY	Paraguay	paraguay	\N	f	2017-10-29 17:02:32.243854	2017-10-29 17:02:32.243877
174	CV	Cape Verde	cape-verde	\N	f	2017-10-29 17:02:32.252223	2017-10-29 17:02:32.252241
175	CU	Cuba	cuba	\N	f	2017-10-29 17:02:32.260866	2017-10-29 17:02:32.260886
176	CT	Canton and Enderbury Islands	canton-and-enderbury-islands	\N	f	2017-10-29 17:02:32.268126	2017-10-29 17:02:32.268137
177	SZ	Swaziland	swaziland	\N	f	2017-10-29 17:02:32.274122	2017-10-29 17:02:32.274132
178	SY	Syria	syria	\N	f	2017-10-29 17:02:32.279387	2017-10-29 17:02:32.279397
179	KG	Kyrgyzstan	kyrgyzstan	\N	f	2017-10-29 17:02:32.284369	2017-10-29 17:02:32.284379
180	KE	Kenya	kenya	\N	f	2017-10-29 17:02:32.292087	2017-10-29 17:02:32.292105
181	SR	Suriname	suriname	\N	f	2017-10-29 17:02:32.300481	2017-10-29 17:02:32.300498
182	KI	Kiribati	kiribati	\N	f	2017-10-29 17:02:32.306013	2017-10-29 17:02:32.306023
183	KH	Cambodia	cambodia	\N	f	2017-10-29 17:02:32.311468	2017-10-29 17:02:32.311478
184	SV	El Salvador	el-salvador	\N	f	2017-10-29 17:02:32.317033	2017-10-29 17:02:32.31706
185	KM	Comoros	comoros	\N	f	2017-10-29 17:02:32.323112	2017-10-29 17:02:32.323123
186	ST	São Tomé and Príncipe	sao-tome-and-principe	\N	f	2017-10-29 17:02:32.329253	2017-10-29 17:02:32.329264
187	SK	Slovakia	slovakia	\N	f	2017-10-29 17:02:32.335213	2017-10-29 17:02:32.335237
188	KR	South Korea	south-korea	\N	f	2017-10-29 17:02:32.341444	2017-10-29 17:02:32.341455
189	SI	Slovenia	slovenia	\N	f	2017-10-29 17:02:32.348833	2017-10-29 17:02:32.348851
190	KP	North Korea	north-korea	\N	f	2017-10-29 17:02:32.356613	2017-10-29 17:02:32.35663
191	KW	Kuwait	kuwait	\N	f	2017-10-29 17:02:32.363509	2017-10-29 17:02:32.36352
192	SN	Senegal	senegal	\N	f	2017-10-29 17:02:32.36896	2017-10-29 17:02:32.368971
193	SM	San Marino	san-marino	\N	f	2017-10-29 17:02:32.374484	2017-10-29 17:02:32.374522
194	SL	Sierra Leone	sierra-leone	\N	f	2017-10-29 17:02:32.379898	2017-10-29 17:02:32.379908
195	SC	Seychelles	seychelles	\N	f	2017-10-29 17:02:32.386683	2017-10-29 17:02:32.386695
196	KZ	Kazakhstan	kazakhstan	\N	f	2017-10-29 17:02:32.392075	2017-10-29 17:02:32.392086
197	KY	Cayman Islands	cayman-islands	\N	f	2017-10-29 17:02:32.39766	2017-10-29 17:02:32.397671
198	SG	Singapore	singapore	\N	f	2017-10-29 17:02:32.403198	2017-10-29 17:02:32.403209
199	SE	Sweden	sweden	\N	f	2017-10-29 17:02:32.408547	2017-10-29 17:02:32.408558
200	SD	Sudan	sudan	\N	f	2017-10-29 17:02:32.413963	2017-10-29 17:02:32.413974
201	DO	Dominican Republic	dominican-republic	\N	f	2017-10-29 17:02:32.419645	2017-10-29 17:02:32.419656
202	DM	Dominica	dominica	\N	f	2017-10-29 17:02:32.425233	2017-10-29 17:02:32.425244
203	DJ	Djibouti	djibouti	\N	f	2017-10-29 17:02:32.432639	2017-10-29 17:02:32.432656
204	DK	Denmark	denmark	\N	f	2017-10-29 17:02:32.438166	2017-10-29 17:02:32.438176
205	DD	East Germany	east-germany	\N	f	2017-10-29 17:02:32.443802	2017-10-29 17:02:32.443813
206	DE	Germany	germany	\N	f	2017-10-29 17:02:32.449644	2017-10-29 17:02:32.449669
207	YE	Yemen	yemen	\N	f	2017-10-29 17:02:32.457503	2017-10-29 17:02:32.457522
208	YD	People's Democratic Republic of Yemen	people-s-democratic-republic-of-yemen	\N	f	2017-10-29 17:02:32.462669	2017-10-29 17:02:32.462679
209	DZ	Algeria	algeria	\N	f	2017-10-29 17:02:32.467579	2017-10-29 17:02:32.46759
210	US	United States	united-states	\N	f	2017-10-29 17:02:32.47334	2017-10-29 17:02:32.473351
211	UY	Uruguay	uruguay	\N	f	2017-10-29 17:02:32.479626	2017-10-29 17:02:32.479643
212	YT	Mayotte	mayotte	\N	f	2017-10-29 17:02:32.488923	2017-10-29 17:02:32.488968
213	UM	U.S. Minor Outlying Islands	u-s-minor-outlying-islands	\N	f	2017-10-29 17:02:32.496821	2017-10-29 17:02:32.496839
214	LB	Lebanon	lebanon	\N	f	2017-10-29 17:02:32.505186	2017-10-29 17:02:32.505204
215	LC	Saint Lucia	saint-lucia	\N	f	2017-10-29 17:02:32.511153	2017-10-29 17:02:32.511163
216	LA	Laos	laos	\N	f	2017-10-29 17:02:32.516161	2017-10-29 17:02:32.516171
217	TV	Tuvalu	tuvalu	\N	f	2017-10-29 17:02:32.521741	2017-10-29 17:02:32.521753
218	TW	Taiwan	taiwan	\N	f	2017-10-29 17:02:32.527368	2017-10-29 17:02:32.527379
219	TT	Trinidad and Tobago	trinidad-and-tobago	\N	f	2017-10-29 17:02:32.532844	2017-10-29 17:02:32.532855
220	TR	Turkey	turkey	\N	f	2017-10-29 17:02:32.540421	2017-10-29 17:02:32.540437
221	LK	Sri Lanka	sri-lanka	\N	f	2017-10-29 17:02:32.549081	2017-10-29 17:02:32.5491
222	LI	Liechtenstein	liechtenstein	\N	f	2017-10-29 17:02:32.557797	2017-10-29 17:02:32.557814
223	LV	Latvia	latvia	\N	f	2017-10-29 17:02:32.566742	2017-10-29 17:02:32.566756
224	TO	Tonga	tonga	\N	f	2017-10-29 17:02:32.573865	2017-10-29 17:02:32.57388
225	LT	Lithuania	lithuania	\N	f	2017-10-29 17:02:32.582222	2017-10-29 17:02:32.582236
226	LU	Luxembourg	luxembourg	\N	f	2017-10-29 17:02:32.589473	2017-10-29 17:02:32.589488
227	LR	Liberia	liberia	\N	f	2017-10-29 17:02:32.596649	2017-10-29 17:02:32.596661
228	LS	Lesotho	lesotho	\N	f	2017-10-29 17:02:32.603426	2017-10-29 17:02:32.603438
229	TH	Thailand	thailand	\N	f	2017-10-29 17:02:32.611654	2017-10-29 17:02:32.611672
230	TF	French Southern Territories	french-southern-territories	\N	f	2017-10-29 17:02:32.621074	2017-10-29 17:02:32.621086
231	TG	Togo	togo	\N	f	2017-10-29 17:02:32.630997	2017-10-29 17:02:32.631015
232	TD	Chad	chad	\N	f	2017-10-29 17:02:32.639828	2017-10-29 17:02:32.639845
233	TC	Turks and Caicos Islands	turks-and-caicos-islands	\N	f	2017-10-29 17:02:32.648728	2017-10-29 17:02:32.648745
234	LY	Libya	libya	\N	f	2017-10-29 17:02:32.655444	2017-10-29 17:02:32.655457
235	VA	Vatican City	vatican-city	\N	f	2017-10-29 17:02:32.662964	2017-10-29 17:02:32.662979
236	VC	Saint Vincent and the Grenadines	saint-vincent-and-the-grenadines	\N	f	2017-10-29 17:02:32.670944	2017-10-29 17:02:32.670975
237	VD	North Vietnam	north-vietnam	\N	f	2017-10-29 17:02:32.679069	2017-10-29 17:02:32.679079
238	AD	Andorra	andorra	\N	f	2017-10-29 17:02:32.684499	2017-10-29 17:02:32.684509
239	AG	Antigua and Barbuda	antigua-and-barbuda	\N	f	2017-10-29 17:02:32.69007	2017-10-29 17:02:32.69008
240	VG	British Virgin Islands	british-virgin-islands	\N	f	2017-10-29 17:02:32.696657	2017-10-29 17:02:32.696667
241	AI	Anguilla	anguilla	\N	f	2017-10-29 17:02:32.70238	2017-10-29 17:02:32.70239
242	VI	U.S. Virgin Islands	u-s-virgin-islands	\N	f	2017-10-29 17:02:32.707592	2017-10-29 17:02:32.707603
243	IS	Iceland	iceland	\N	f	2017-10-29 17:02:32.712845	2017-10-29 17:02:32.712855
244	IR	Iran	iran	\N	f	2017-10-29 17:02:32.718098	2017-10-29 17:02:32.718114
245	AM	Armenia	armenia	\N	f	2017-10-29 17:02:32.724626	2017-10-29 17:02:32.724638
246	AL	Albania	albania	\N	f	2017-10-29 17:02:32.731297	2017-10-29 17:02:32.731316
247	AO	Angola	angola	\N	f	2017-10-29 17:02:32.740117	2017-10-29 17:02:32.740134
248	AN	Netherlands Antilles	netherlands-antilles	\N	f	2017-10-29 17:02:32.74809	2017-10-29 17:02:32.748102
249	AQ	Antarctica	antarctica	\N	f	2017-10-29 17:02:32.754813	2017-10-29 17:02:32.754831
250	AS	American Samoa	american-samoa	\N	f	2017-10-29 17:02:32.763234	2017-10-29 17:02:32.763246
251	AR	Argentina	argentina	\N	f	2017-10-29 17:02:32.770834	2017-10-29 17:02:32.770848
252	AU	Australia	australia	\N	f	2017-10-29 17:02:32.777807	2017-10-29 17:02:32.777819
253	AT	Austria	austria	\N	f	2017-10-29 17:02:32.785832	2017-10-29 17:02:32.78585
254	IO	British Indian Ocean Territory	british-indian-ocean-territory	\N	f	2017-10-29 17:02:32.794673	2017-10-29 17:02:32.794685
255	IN	India	india	\N	f	2017-10-29 17:02:32.801379	2017-10-29 17:02:32.80139
256	TZ	Tanzania	tanzania	\N	f	2017-10-29 17:02:32.808113	2017-10-29 17:02:32.808126
257	AZ	Azerbaijan	azerbaijan	\N	f	2017-10-29 17:02:32.816522	2017-10-29 17:02:32.816539
258	IE	Ireland	ireland	\N	f	2017-10-29 17:02:32.825766	2017-10-29 17:02:32.825785
259	ID	Indonesia	indonesia	\N	f	2017-10-29 17:02:32.83364	2017-10-29 17:02:32.833653
260	RS	Serbia	serbia	\N	f	2017-10-29 17:02:32.840904	2017-10-29 17:02:32.840917
261	PU	U.S. Miscellaneous Pacific Islands	u-s-miscellaneous-pacific-islands	\N	f	2017-10-29 17:02:32.849596	2017-10-29 17:02:32.849614
262	BL	Saint Barthélemy	saint-barthelemy	\N	f	2017-10-29 17:02:32.85903	2017-10-29 17:02:32.859047
263	QA	Qatar	qatar	\N	f	2017-10-29 17:02:32.867518	2017-10-29 17:02:32.867534
264	MZ	Mozambique	mozambique	\N	f	2017-10-29 17:02:32.873628	2017-10-29 17:02:32.873639
\.


--
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('country_id_seq', 264, true);


--
-- Data for Name: coupon; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY coupon (id, order_id, type, amount, min_spend, category_id, is_used, is_expired, expiry_date, user_id, last_updated, date_created) FROM stdin;
1	\N	bob	500	2500	\N	f	f	2017-12-14 09:24:16.972966	1	2017-10-30 09:24:16.98872	2017-10-30 09:24:16.988751
2	\N	bob	500	2500	\N	f	f	2017-12-14 09:24:16.972966	1	2017-10-30 09:24:17.000884	2017-10-30 09:24:17.000895
3	\N	bob	500	2500	\N	f	f	2017-12-14 09:24:16.972966	1	2017-10-30 09:24:17.007511	2017-10-30 09:24:17.007521
4	\N	bob	500	2500	\N	f	f	2017-12-14 09:24:16.972966	1	2017-10-30 09:24:17.014237	2017-10-30 09:24:17.014247
5	\N	bob	1000	5000	\N	f	f	2017-12-14 09:24:16.972966	1	2017-10-30 09:24:17.021079	2017-10-30 09:24:17.021089
6	\N	bob	2000	10000	\N	f	f	2017-12-14 09:24:16.972966	1	2017-10-30 09:24:17.027397	2017-10-30 09:24:17.027406
7	\N	bob	5000	25000	\N	f	f	2017-12-14 09:24:16.972966	1	2017-10-30 09:24:17.033561	2017-10-30 09:24:17.033571
\.


--
-- Name: coupon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('coupon_id_seq', 7, true);


--
-- Data for Name: credit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY credit (id, amount, price, is_awarded, is_purchased, is_credit, credit_type_id, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: credit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('credit_id_seq', 1, false);


--
-- Data for Name: credit_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY credit_type (id, name, code, last_updated, date_created) FROM stdin;
\.


--
-- Name: credit_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('credit_type_id_seq', 1, false);


--
-- Data for Name: currency; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY currency (id, code, name, enabled, symbol, payment_code, last_updated, date_created) FROM stdin;
1	AED	UAE dirham	f	د.إ;	\N	2017-10-29 17:02:32.891549	2017-10-29 17:02:32.891568
2	AFN	Afghan afghani	f	Afs	\N	2017-10-29 17:02:32.900898	2017-10-29 17:02:32.900918
3	ALL	Albanian lek	f	L	\N	2017-10-29 17:02:32.91007	2017-10-29 17:02:32.910085
4	AMD	Armenian dram	f	AMD	\N	2017-10-29 17:02:32.919861	2017-10-29 17:02:32.919891
5	ANG	Netherlands Antillean gulden	f	NAƒ	\N	2017-10-29 17:02:32.929129	2017-10-29 17:02:32.929146
6	AOA	Angolan kwanza	f	Kz	\N	2017-10-29 17:02:32.938344	2017-10-29 17:02:32.938357
7	ARS	Argentine peso	f	$	\N	2017-10-29 17:02:32.944907	2017-10-29 17:02:32.94492
8	AUD	Australian dollar	f	$	\N	2017-10-29 17:02:32.951261	2017-10-29 17:02:32.951274
9	AWG	Aruban florin	f	ƒ	\N	2017-10-29 17:02:32.958287	2017-10-29 17:02:32.9583
10	AZN	Azerbaijani manat	f	AZN	\N	2017-10-29 17:02:32.965588	2017-10-29 17:02:32.965601
11	BAM	Bosnia and Herzegovina konvertibilna marka	f	KM	\N	2017-10-29 17:02:32.971971	2017-10-29 17:02:32.971983
12	BBD	Barbadian dollar	f	Bds$	\N	2017-10-29 17:02:32.979168	2017-10-29 17:02:32.979181
13	BDT	Bangladeshi taka	f	৳	\N	2017-10-29 17:02:32.986572	2017-10-29 17:02:32.986586
14	BGN	Bulgarian lev	f	BGN	\N	2017-10-29 17:02:32.993266	2017-10-29 17:02:32.99328
15	BHD	Bahraini dinar	f	.د.ب	\N	2017-10-29 17:02:33.000062	2017-10-29 17:02:33.000073
16	BIF	Burundi franc	f	FBu	\N	2017-10-29 17:02:33.00673	2017-10-29 17:02:33.006742
17	BMD	Bermudian dollar	f	BD$	\N	2017-10-29 17:02:33.013639	2017-10-29 17:02:33.013653
18	BND	Brunei dollar	f	B$	\N	2017-10-29 17:02:33.020629	2017-10-29 17:02:33.020643
19	BOB	Bolivian boliviano	f	Bs.	\N	2017-10-29 17:02:33.028851	2017-10-29 17:02:33.028867
20	BRL	Brazilian real	f	R$	\N	2017-10-29 17:02:33.036301	2017-10-29 17:02:33.036312
21	BSD	Bahamian dollar	f	B$	\N	2017-10-29 17:02:33.042058	2017-10-29 17:02:33.042069
22	BTN	Bhutanese ngultrum	f	Nu.	\N	2017-10-29 17:02:33.047193	2017-10-29 17:02:33.047204
23	BWP	Botswana pula	f	P	\N	2017-10-29 17:02:33.052382	2017-10-29 17:02:33.052393
24	BYR	Belarusian ruble	f	Br	\N	2017-10-29 17:02:33.062005	2017-10-29 17:02:33.062016
25	BZD	Belize dollar	f	BZ$	\N	2017-10-29 17:02:33.070905	2017-10-29 17:02:33.07092
26	CAD	Canadian dollar	f	$	\N	2017-10-29 17:02:33.07824	2017-10-29 17:02:33.078253
27	CDF	Congolese franc	f	F	\N	2017-10-29 17:02:33.089679	2017-10-29 17:02:33.089733
28	CHF	Swiss franc	f	Fr.	\N	2017-10-29 17:02:33.100705	2017-10-29 17:02:33.100726
29	CLP	Chilean peso	f	$	\N	2017-10-29 17:02:33.111241	2017-10-29 17:02:33.111262
30	CNY	Chinese/Yuan renminbi	f	¥	\N	2017-10-29 17:02:33.121048	2017-10-29 17:02:33.121067
31	COP	Colombian peso	f	Col$	\N	2017-10-29 17:02:33.130805	2017-10-29 17:02:33.130822
32	CRC	Costa Rican colon	f	₡	\N	2017-10-29 17:02:33.139752	2017-10-29 17:02:33.139769
33	CUC	Cuban peso	f	$	\N	2017-10-29 17:02:33.14762	2017-10-29 17:02:33.147633
34	CVE	Cape Verdean escudo	f	Esc	\N	2017-10-29 17:02:33.155604	2017-10-29 17:02:33.155621
35	CZK	Czech koruna	f	Kč	\N	2017-10-29 17:02:33.164299	2017-10-29 17:02:33.164316
36	DJF	Djiboutian franc	f	Fdj	\N	2017-10-29 17:02:33.172913	2017-10-29 17:02:33.172932
37	DKK	Danish krone	f	Kr	\N	2017-10-29 17:02:33.184648	2017-10-29 17:02:33.184682
38	DOP	Dominican peso	f	RD$	\N	2017-10-29 17:02:33.195834	2017-10-29 17:02:33.195847
39	DZD	Algerian dinar	f	د.ج	\N	2017-10-29 17:02:33.205282	2017-10-29 17:02:33.205301
40	EEK	Estonian kroon	f	KR	\N	2017-10-29 17:02:33.215932	2017-10-29 17:02:33.21595
41	EGP	Egyptian pound	f	£	\N	2017-10-29 17:02:33.224815	2017-10-29 17:02:33.224833
42	ERN	Eritrean nakfa	f	Nfa	\N	2017-10-29 17:02:33.23215	2017-10-29 17:02:33.232164
43	ETB	Ethiopian birr	f	Br	\N	2017-10-29 17:02:33.239651	2017-10-29 17:02:33.239666
44	EUR	European Euro	f	€	\N	2017-10-29 17:02:33.247482	2017-10-29 17:02:33.2475
45	FJD	Fijian dollar	f	FJ$	\N	2017-10-29 17:02:33.256748	2017-10-29 17:02:33.256766
46	FKP	Falkland Islands pound	f	£	\N	2017-10-29 17:02:33.263984	2017-10-29 17:02:33.263994
47	GBP	British pound	f	£	\N	2017-10-29 17:02:33.273057	2017-10-29 17:02:33.273081
48	GEL	Georgian lari	f	GEL	\N	2017-10-29 17:02:33.282221	2017-10-29 17:02:33.282239
49	GHS	Ghanaian cedi	f	GH₵	\N	2017-10-29 17:02:33.289585	2017-10-29 17:02:33.28962
50	GIP	Gibraltar pound	f	£	\N	2017-10-29 17:02:33.301253	2017-10-29 17:02:33.301267
51	GMD	Gambian dalasi	f	D	\N	2017-10-29 17:02:33.30772	2017-10-29 17:02:33.307738
52	GNF	Guinean franc	f	FG	\N	2017-10-29 17:02:33.319775	2017-10-29 17:02:33.319793
53	GQE	Central African CFA franc	f	CFA	\N	2017-10-29 17:02:33.325546	2017-10-29 17:02:33.325556
54	GTQ	Guatemalan quetzal	f	Q	\N	2017-10-29 17:02:33.330558	2017-10-29 17:02:33.330568
55	GYD	Guyanese dollar	f	GY$	\N	2017-10-29 17:02:33.335717	2017-10-29 17:02:33.335734
56	HKD	Hong Kong dollar	f	HK$	\N	2017-10-29 17:02:33.340799	2017-10-29 17:02:33.340809
57	HNL	Honduran lempira	f	L	\N	2017-10-29 17:02:33.362952	2017-10-29 17:02:33.36297
58	HRK	Croatian kuna	f	kn	\N	2017-10-29 17:02:33.370703	2017-10-29 17:02:33.370712
59	HTG	Haitian gourde	f	G	\N	2017-10-29 17:02:33.375718	2017-10-29 17:02:33.375728
60	HUF	Hungarian forint	f	Ft	\N	2017-10-29 17:02:33.567093	2017-10-29 17:02:33.567108
61	IDR	Indonesian rupiah	f	Rp	\N	2017-10-29 17:02:33.573218	2017-10-29 17:02:33.573229
62	ILS	Israeli new sheqel	f	₪	\N	2017-10-29 17:02:33.578168	2017-10-29 17:02:33.578178
63	INR	Indian rupee	f	₉	\N	2017-10-29 17:02:33.583238	2017-10-29 17:02:33.583248
64	IQD	Iraqi dinar	f	د.ع	\N	2017-10-29 17:02:33.588812	2017-10-29 17:02:33.588824
65	IRR	Iranian rial	f	IRR	\N	2017-10-29 17:02:33.594737	2017-10-29 17:02:33.594749
66	ISK	Icelandic króna	f	kr	\N	2017-10-29 17:02:33.600534	2017-10-29 17:02:33.600545
67	JMD	Jamaican dollar	f	J$	\N	2017-10-29 17:02:33.614024	2017-10-29 17:02:33.614045
68	JOD	Jordanian dinar	f	JOD	\N	2017-10-29 17:02:33.622149	2017-10-29 17:02:33.62216
69	JPY	Japanese yen	f	¥	\N	2017-10-29 17:02:33.629519	2017-10-29 17:02:33.629535
70	KES	Kenyan shilling	f	KSh	\N	2017-10-29 17:02:33.636654	2017-10-29 17:02:33.636667
71	KGS	Kyrgyzstani som	f	сом	\N	2017-10-29 17:02:33.64335	2017-10-29 17:02:33.643362
72	KHR	Cambodian riel	f	៛	\N	2017-10-29 17:02:33.650653	2017-10-29 17:02:33.650666
73	KMF	Comorian franc	f	KMF	\N	2017-10-29 17:02:33.65802	2017-10-29 17:02:33.658032
74	KPW	North Korean won	f	W	\N	2017-10-29 17:02:33.665236	2017-10-29 17:02:33.665249
75	KRW	South Korean won	f	W	\N	2017-10-29 17:02:33.674159	2017-10-29 17:02:33.674176
76	KWD	Kuwaiti dinar	f	KWD	\N	2017-10-29 17:02:33.683393	2017-10-29 17:02:33.68341
77	KYD	Cayman Islands dollar	f	KY$	\N	2017-10-29 17:02:33.690127	2017-10-29 17:02:33.690139
78	KZT	Kazakhstani tenge	f	T	\N	2017-10-29 17:02:33.699192	2017-10-29 17:02:33.699209
79	LAK	Lao kip	f	KN	\N	2017-10-29 17:02:33.707053	2017-10-29 17:02:33.707066
80	LBP	Lebanese lira	f	£	\N	2017-10-29 17:02:33.71191	2017-10-29 17:02:33.711921
81	LKR	Sri Lankan rupee	f	Rs	\N	2017-10-29 17:02:33.717211	2017-10-29 17:02:33.717221
82	LRD	Liberian dollar	f	L$	\N	2017-10-29 17:02:33.723698	2017-10-29 17:02:33.723711
83	LSL	Lesotho loti	f	M	\N	2017-10-29 17:02:33.731526	2017-10-29 17:02:33.731539
84	LTL	Lithuanian litas	f	Lt	\N	2017-10-29 17:02:33.739009	2017-10-29 17:02:33.739023
85	LVL	Latvian lats	f	Ls	\N	2017-10-29 17:02:33.746566	2017-10-29 17:02:33.746583
86	LYD	Libyan dinar	f	LD	\N	2017-10-29 17:02:33.754172	2017-10-29 17:02:33.754184
87	MAD	Moroccan dirham	f	MAD	\N	2017-10-29 17:02:33.762115	2017-10-29 17:02:33.762131
88	MDL	Moldovan leu	f	MDL	\N	2017-10-29 17:02:33.768997	2017-10-29 17:02:33.769019
89	MGA	Malagasy ariary	f	FMG	\N	2017-10-29 17:02:33.777417	2017-10-29 17:02:33.777436
90	MKD	Macedonian denar	f	MKD	\N	2017-10-29 17:02:33.784359	2017-10-29 17:02:33.784371
91	MMK	Myanma kyat	f	K	\N	2017-10-29 17:02:33.791445	2017-10-29 17:02:33.791458
92	MNT	Mongolian tugrik	f	₮	\N	2017-10-29 17:02:33.800049	2017-10-29 17:02:33.800065
93	MOP	Macanese pataca	f	P	\N	2017-10-29 17:02:33.807884	2017-10-29 17:02:33.807897
94	MRO	Mauritanian ouguiya	f	UM	\N	2017-10-29 17:02:33.81514	2017-10-29 17:02:33.815155
95	MUR	Mauritian rupee	f	Rs	\N	2017-10-29 17:02:33.822052	2017-10-29 17:02:33.822067
96	MVR	Maldivian rufiyaa	f	Rf	\N	2017-10-29 17:02:33.828325	2017-10-29 17:02:33.828335
97	MWK	Malawian kwacha	f	MK	\N	2017-10-29 17:02:33.833913	2017-10-29 17:02:33.833924
98	MXN	Mexican peso	f	$	\N	2017-10-29 17:02:33.839951	2017-10-29 17:02:33.839961
99	MYR	Malaysian ringgit	f	RM	\N	2017-10-29 17:02:33.845268	2017-10-29 17:02:33.845278
100	MZM	Mozambican metical	f	MTn	\N	2017-10-29 17:02:33.850335	2017-10-29 17:02:33.850345
101	NAD	Namibian dollar	f	N$	\N	2017-10-29 17:02:33.855789	2017-10-29 17:02:33.855807
102	NGN	Nigerian naira	t	₦	566	2017-10-29 17:02:33.8609	2017-10-29 17:02:33.86091
103	NIO	Nicaraguan córdoba	f	C$	\N	2017-10-29 17:02:33.866166	2017-10-29 17:02:33.866176
104	NOK	Norwegian krone	f	kr	\N	2017-10-29 17:02:33.871605	2017-10-29 17:02:33.871615
105	NPR	Nepalese rupee	f	NRs	\N	2017-10-29 17:02:33.877175	2017-10-29 17:02:33.877185
106	NZD	New Zealand dollar	f	NZ$	\N	2017-10-29 17:02:33.882706	2017-10-29 17:02:33.882718
107	OMR	Omani rial	f	OMR	\N	2017-10-29 17:02:33.888101	2017-10-29 17:02:33.888112
108	PAB	Panamanian balboa	f	B./	\N	2017-10-29 17:02:33.893368	2017-10-29 17:02:33.893378
109	PEN	Peruvian nuevo sol	f	S/.	\N	2017-10-29 17:02:33.900699	2017-10-29 17:02:33.900717
110	PGK	Papua New Guinean kina	f	K	\N	2017-10-29 17:02:33.91017	2017-10-29 17:02:33.910183
111	PHP	Philippine peso	f	₱	\N	2017-10-29 17:02:33.917133	2017-10-29 17:02:33.917146
112	PKR	Pakistani rupee	f	Rs.	\N	2017-10-29 17:02:33.925863	2017-10-29 17:02:33.92588
113	PLN	Polish zloty	f	zł	\N	2017-10-29 17:02:33.933845	2017-10-29 17:02:33.933857
114	PYG	Paraguayan guarani	f	₲	\N	2017-10-29 17:02:33.940004	2017-10-29 17:02:33.940014
115	QAR	Qatari riyal	f	QR	\N	2017-10-29 17:02:33.946708	2017-10-29 17:02:33.946722
116	RON	Romanian leu	f	L	\N	2017-10-29 17:02:33.955702	2017-10-29 17:02:33.955719
117	RSD	Serbian dinar	f	din.	\N	2017-10-29 17:02:33.969338	2017-10-29 17:02:33.969356
118	RUB	Russian ruble	f	R	\N	2017-10-29 17:02:33.978569	2017-10-29 17:02:33.978593
119	SAR	Saudi riyal	f	SR	\N	2017-10-29 17:02:33.986935	2017-10-29 17:02:33.986948
120	SBD	Solomon Islands dollar	f	SI$	\N	2017-10-29 17:02:33.994236	2017-10-29 17:02:33.994254
121	SCR	Seychellois rupee	f	SR	\N	2017-10-29 17:02:34.001612	2017-10-29 17:02:34.001627
122	SDG	Sudanese pound	f	SDG	\N	2017-10-29 17:02:34.010132	2017-10-29 17:02:34.010148
123	SEK	Swedish krona	f	kr	\N	2017-10-29 17:02:34.017433	2017-10-29 17:02:34.017447
124	SGD	Singapore dollar	f	S$	\N	2017-10-29 17:02:34.025233	2017-10-29 17:02:34.025249
125	SHP	Saint Helena pound	f	£	\N	2017-10-29 17:02:34.033123	2017-10-29 17:02:34.033136
126	SLL	Sierra Leonean leone	f	Le	\N	2017-10-29 17:02:34.039628	2017-10-29 17:02:34.039641
127	SOS	Somali shilling	f	Sh.	\N	2017-10-29 17:02:34.04684	2017-10-29 17:02:34.046853
128	SRD	Surinamese dollar	f	$	\N	2017-10-29 17:02:34.056087	2017-10-29 17:02:34.056105
129	SYP	Syrian pound	f	LS	\N	2017-10-29 17:02:34.064346	2017-10-29 17:02:34.064361
130	SZL	Swazi lilangeni	f	E	\N	2017-10-29 17:02:34.071464	2017-10-29 17:02:34.071477
131	THB	Thai baht	f	฿	\N	2017-10-29 17:02:34.078624	2017-10-29 17:02:34.078638
132	TJS	Tajikistani somoni	f	TJS	\N	2017-10-29 17:02:34.087335	2017-10-29 17:02:34.087354
133	TMT	Turkmen manat	f	m	\N	2017-10-29 17:02:34.096268	2017-10-29 17:02:34.096284
134	TND	Tunisian dinar	f	DT	\N	2017-10-29 17:02:34.103935	2017-10-29 17:02:34.103947
135	TRY	Turkish new lira	f	TRY	\N	2017-10-29 17:02:34.110417	2017-10-29 17:02:34.11043
136	TTD	Trinidad and Tobago dollar	f	TT$	\N	2017-10-29 17:02:34.11726	2017-10-29 17:02:34.117274
137	TWD	New Taiwan dollar	f	NT$	\N	2017-10-29 17:02:34.125435	2017-10-29 17:02:34.125449
138	TZS	Tanzanian shilling	f	TZS	\N	2017-10-29 17:02:34.132859	2017-10-29 17:02:34.132872
139	UAH	Ukrainian hryvnia	f	UAH	\N	2017-10-29 17:02:34.139218	2017-10-29 17:02:34.139231
140	UGX	Ugandan shilling	f	USh	\N	2017-10-29 17:02:34.146715	2017-10-29 17:02:34.146733
141	USD	United States dollar	f	$	844	2017-10-29 17:02:34.154063	2017-10-29 17:02:34.154075
142	UYU	Uruguayan peso	f	$U	\N	2017-10-29 17:02:34.160425	2017-10-29 17:02:34.160438
143	UZS	Uzbekistani som	f	UZS	\N	2017-10-29 17:02:34.169094	2017-10-29 17:02:34.169111
144	VEB	Venezuelan bolivar	f	Bs	\N	2017-10-29 17:02:34.177372	2017-10-29 17:02:34.177385
145	VND	Vietnamese dong	f	₫	\N	2017-10-29 17:02:34.184524	2017-10-29 17:02:34.184537
146	VUV	Vanuatu vatu	f	VT	\N	2017-10-29 17:02:34.192895	2017-10-29 17:02:34.192908
147	WST	Samoan tala	f	WS$	\N	2017-10-29 17:02:34.200345	2017-10-29 17:02:34.200358
148	XAF	Central African CFA franc	f	CFA	\N	2017-10-29 17:02:34.207253	2017-10-29 17:02:34.207265
149	XCD	East Caribbean dollar	f	EC$	\N	2017-10-29 17:02:34.21494	2017-10-29 17:02:34.214955
150	XDR	Special Drawing Rights	f	SDR	\N	2017-10-29 17:02:34.222054	2017-10-29 17:02:34.222066
151	XOF	West African CFA franc	f	CFA	\N	2017-10-29 17:02:34.228553	2017-10-29 17:02:34.228565
152	XPF	CFP franc	f	F	\N	2017-10-29 17:02:34.236016	2017-10-29 17:02:34.236034
153	YER	Yemeni rial	f	YER	\N	2017-10-29 17:02:34.244395	2017-10-29 17:02:34.244412
154	ZAR	South African rand	f	R	\N	2017-10-29 17:02:34.251404	2017-10-29 17:02:34.251416
155	ZMK	Zambian kwacha	f	ZK	\N	2017-10-29 17:02:34.259437	2017-10-29 17:02:34.259451
156	ZWR	Zimbabwean dollar	f	Z$	\N	2017-10-29 17:02:34.266229	2017-10-29 17:02:34.266242
\.


--
-- Name: currency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('currency_id_seq', 156, true);


--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer (id, name, email, accept_marketing, user_id, notes, last_updated, date_created) FROM stdin;
\.


--
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer_id_seq', 1, false);


--
-- Data for Name: delivery_option; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY delivery_option (id, code, name, description, is_enabled, last_updated, date_created) FROM stdin;
1	delivery	Delivery	Deliver this package to the address specified	t	2017-10-29 17:02:34.351201	2017-10-29 17:02:34.35122
2	pick_up	Pick Up	Deliver this package to the nearest hub for pickup	t	2017-10-29 17:02:34.360069	2017-10-29 17:02:34.360082
\.


--
-- Name: delivery_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('delivery_option_id_seq', 2, true);


--
-- Data for Name: delivery_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY delivery_status (id, name, code, last_updated, date_created) FROM stdin;
1	Pending	pending	2017-10-29 17:02:34.446047	2017-10-29 17:02:34.446068
2	Completed	completed	2017-10-29 17:02:34.453512	2017-10-29 17:02:34.453533
3	Cancelled	cancelled	2017-10-29 17:02:34.460131	2017-10-29 17:02:34.460144
4	Fulfilled	fulfilled	2017-10-29 17:02:34.468385	2017-10-29 17:02:34.468403
5	Customer_cancelled	customer-cancelled	2017-10-29 17:02:34.475883	2017-10-29 17:02:34.475897
6	Returned	returned	2017-10-29 17:02:34.48354	2017-10-29 17:02:34.48356
7	Processing	processing	2017-10-29 17:02:34.49175	2017-10-29 17:02:34.491767
\.


--
-- Name: delivery_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('delivery_status_id_seq', 7, true);


--
-- Data for Name: earning; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY earning (id, amount, is_credit, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: earning_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('earning_id_seq', 1, false);


--
-- Data for Name: email; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY email (id, subject, html, text, send_at, sent_at, status, message_id, "to", from_, notification_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: email_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('email_id_seq', 1, false);


--
-- Data for Name: image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY image (id, name, height, width, url, slug, auction_id, product_id, category_id, is_cover, variant_id, last_updated, date_created) FROM stdin;
1	blv5.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509355746/SB2017-10-30%2009:29:05.613652.jpg	blv5-jpg	\N	1	\N	f	\N	2017-10-30 09:29:06.288199	2017-10-30 09:29:06.28823
2	blv4.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509355746/SB2017-10-30%2009:29:06.300362.jpg	blv4-jpg	\N	1	\N	f	\N	2017-10-30 09:29:06.677583	2017-10-30 09:29:06.677602
3	blv3.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509355746/SB2017-10-30%2009:29:06.687517.jpg	blv3-jpg	\N	1	\N	t	\N	2017-10-30 09:29:07.169671	2017-10-30 09:29:07.144145
4	Sexy-women-shoes-roman-shoes-peep-toe_3.png	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509383116/SB2017-10-30%2017:05:16.118218.png	sexy-women-shoes-roman-shoes-peep-toe-3-png	\N	2	\N	f	\N	2017-10-30 17:05:17.165165	2017-10-30 17:05:17.165185
5	Sexy-women-shoes-roman-shoes-peep-toe_2.png	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509383117/SB2017-10-30%2017:05:17.176765.png	sexy-women-shoes-roman-shoes-peep-toe-2-png	\N	2	\N	f	\N	2017-10-30 17:05:17.841125	2017-10-30 17:05:17.841148
6	Sexy-women-shoes-roman-shoes-peep-toe_1.png	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509383117/SB2017-10-30%2017:05:17.851731.png	sexy-women-shoes-roman-shoes-peep-toe-1-png	\N	2	\N	f	\N	2017-10-30 17:05:18.245588	2017-10-30 17:05:18.245605
7	Sexy-women-shoes-roman-shoes-peep-toe.png	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509383118/SB2017-10-30%2017:05:18.253717.png	sexy-women-shoes-roman-shoes-peep-toe-png	\N	2	\N	t	\N	2017-10-30 17:05:18.728631	2017-10-30 17:05:18.707795
8	-2059088897852472798.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509388841/SB2017-10-30%2018:40:40.482358.jpg	2059088897852472798-jpg	\N	3	\N	f	\N	2017-10-30 18:40:41.63241	2017-10-30 18:40:41.632432
9	-8123939121530048374.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509388841/SB2017-10-30%2018:40:41.646273.jpg	8123939121530048374-jpg	\N	3	\N	f	\N	2017-10-30 18:40:42.203964	2017-10-30 18:40:42.203986
10	-1064232348156630992.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509388842/SB2017-10-30%2018:40:42.212392.jpg	1064232348156630992-jpg	\N	3	\N	f	\N	2017-10-30 18:40:42.802704	2017-10-30 18:40:42.802728
11	-1164397192-438658860.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509388842/SB2017-10-30%2018:40:42.817151.jpg	1164397192-438658860-jpg	\N	3	\N	t	\N	2017-10-30 18:40:43.254284	2017-10-30 18:40:43.234287
12	New-designed-fashion-style-slim-woman-autumn.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509402794/SB2017-10-30%2022:33:13.785885.jpg	new-designed-fashion-style-slim-woman-autumn-jpg	\N	4	\N	f	\N	2017-10-30 22:33:14.442638	2017-10-30 22:33:14.442661
13	New-designed-fashion-style-slim-woman-autumn.jpg_350x350.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509402794/SB2017-10-30%2022:33:14.451979.jpg	new-designed-fashion-style-slim-woman-autumn-jpg-350x350-jpg	\N	4	\N	t	\N	2017-10-30 22:33:14.833382	2017-10-30 22:33:14.808701
14	Europe-and-America-winter-autumn-high-heel.png	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509403735/SB2017-10-30%2022:48:55.002087.png	europe-and-america-winter-autumn-high-heel-png	\N	5	\N	f	\N	2017-10-30 22:48:56.884082	2017-10-30 22:48:56.8841
15	Europe-and-America-winter-autumn-high-heel.png_350x350.png	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509403737/SB2017-10-30%2022:48:56.894657.png	europe-and-america-winter-autumn-high-heel-png-350x350-png	\N	5	\N	t	\N	2017-10-30 22:48:57.471366	2017-10-30 22:48:57.450187
16	New-products-pu-leather-shoe-woman-low_1.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509404419/SB2017-10-30%2023:00:18.641713.jpg	new-products-pu-leather-shoe-woman-low-1-jpg	\N	6	\N	f	\N	2017-10-30 23:00:19.561453	2017-10-30 23:00:19.561473
17	New-products-pu-leather-shoe-woman-low.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509404419/SB2017-10-30%2023:00:19.575920.jpg	new-products-pu-leather-shoe-woman-low-jpg	\N	6	\N	f	\N	2017-10-30 23:00:19.949565	2017-10-30 23:00:19.949584
18	New-products-pu-leather-shoe-woman-low.jpg_350x350.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509404420/SB2017-10-30%2023:00:19.956949.jpg	new-products-pu-leather-shoe-woman-low-jpg-350x350-jpg	\N	6	\N	t	\N	2017-10-30 23:00:20.329143	2017-10-30 23:00:20.310154
19	Women-Slash-Neck-Flare-Sleeve-Bodycon-Sexy-Body-Conscious-Dress-In-Black_1.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509405462/SB2017-10-30%2023:17:42.149651.jpg	women-slash-neck-flare-sleeve-bodycon-sexy-body-conscious-dress-in-black-1-jpg	\N	7	\N	f	\N	2017-10-30 23:17:43.068921	2017-10-30 23:17:43.068943
20	Women-Slash-Neck-Flare-Sleeve-Bodycon-Sexy-Body-Conscious-Dress-In-Black.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509405463/SB2017-10-30%2023:17:43.077425.jpg	women-slash-neck-flare-sleeve-bodycon-sexy-body-conscious-dress-in-black-jpg	\N	7	\N	f	\N	2017-10-30 23:17:43.552995	2017-10-30 23:17:43.553012
21	Women-Slash-Neck-Flare-Sleeve-Bodycon-Sexy-Body-Conscious-Dress-In-Black.jpg_640x640.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509405463/SB2017-10-30%2023:17:43.561441.jpg	women-slash-neck-flare-sleeve-bodycon-sexy-body-conscious-dress-in-black-jpg-640x640-jpg	\N	7	\N	t	\N	2017-10-30 23:17:43.85585	2017-10-30 23:17:43.83839
22	New-clubwear-women-clothes-sleeveless-v-neck.jpg_350x350.jpg	\N	\N	http://res.cloudinary.com/rusty-x/image/upload/v1509440123/SB2017-10-31%2008:55:23.317803.jpg	new-clubwear-women-clothes-sleeveless-v-neck-jpg-350x350-jpg	\N	8	\N	t	\N	2017-10-31 08:55:24.024966	2017-10-31 08:55:24.002937
\.


--
-- Name: image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('image_id_seq', 22, true);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY message (id, text, receiver_id, sender_id, conversation_id, product_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('message_id_seq', 1, false);


--
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY notification (id, name, email, sms, push, code, last_updated, date_created) FROM stdin;
\.


--
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('notification_id_seq', 1, false);


--
-- Data for Name: order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "order" (id, customer_id, first_name, last_name, discount, shipping_charge, code, coupon_code, message, verified_status, payment_option_id, payment_status_id, delivery_option_id, delivery_status_id, order_status_id, email, phone, address_line1, address_line2, city, user_id, state_id, country_id, auto_set_shipment, shipment_name, shipment_phone, shipment_address, shipment_city, shipment_description, shipment_state, shipment_country, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: order_entry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY order_entry (id, order_id, user_id, variant_id, product_id, quantity, price, order_status_id, date_cancelled, date_cust_cancelled, date_delivered, date_processed, date_returned, last_updated, date_created) FROM stdin;
\.


--
-- Name: order_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_entry_id_seq', 1, false);


--
-- Name: order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_id_seq', 1, false);


--
-- Data for Name: order_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY order_status (id, name, code, last_updated, date_created) FROM stdin;
1	Pending	pending	2017-10-29 17:02:34.537584	2017-10-29 17:02:34.537606
2	Completed	completed	2017-10-29 17:02:34.546024	2017-10-29 17:02:34.546038
3	Cancelled	cancelled	2017-10-29 17:02:34.553605	2017-10-29 17:02:34.553621
4	Fulfilled	fulfilled	2017-10-29 17:02:34.560938	2017-10-29 17:02:34.560953
5	Customer_cancelled	customer-cancelled	2017-10-29 17:02:34.5689	2017-10-29 17:02:34.568914
6	Returned	returned	2017-10-29 17:02:34.575574	2017-10-29 17:02:34.575587
7	Processing	processing	2017-10-29 17:02:34.58261	2017-10-29 17:02:34.582625
\.


--
-- Name: order_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_status_id_seq', 7, true);


--
-- Data for Name: payment_channel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY payment_channel (id, code, name, last_updated, date_created) FROM stdin;
1	cash	Cash	2017-10-29 17:02:34.516602	2017-10-29 17:02:34.516624
2	pos	POS	2017-10-29 17:02:34.524718	2017-10-29 17:02:34.524734
\.


--
-- Name: payment_channel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('payment_channel_id_seq', 2, true);


--
-- Data for Name: payment_option; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY payment_option (id, code, name, type, is_enabled, last_updated, date_created) FROM stdin;
1	pay_on_delivery	Pay On Delivery	postpaid	t	2017-10-29 17:02:34.322895	2017-10-29 17:02:34.322917
2	recurrent_card	Recurrent Card	prepaid	t	2017-10-29 17:02:34.332126	2017-10-29 17:02:34.332138
3	new_card	New Card	prepaid	t	2017-10-29 17:02:34.338872	2017-10-29 17:02:34.338883
\.


--
-- Name: payment_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('payment_option_id_seq', 3, true);


--
-- Data for Name: payment_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY payment_status (id, code, name, description, last_updated, date_created) FROM stdin;
1	pending	Pending	\N	2017-10-29 17:02:34.370057	2017-10-29 17:02:34.370072
2	approved	Approved	\N	2017-10-29 17:02:34.378715	2017-10-29 17:02:34.378729
3	review	Review	\N	2017-10-29 17:02:34.404254	2017-10-29 17:02:34.404279
4	aborted	Aborted	\N	2017-10-29 17:02:34.412386	2017-10-29 17:02:34.412406
5	paid	Paid	\N	2017-10-29 17:02:34.41987	2017-10-29 17:02:34.41991
6	queued	Queued	\N	2017-10-29 17:02:34.427377	2017-10-29 17:02:34.427394
7	disbursed	Disbursed	\N	2017-10-29 17:02:34.434922	2017-10-29 17:02:34.434936
\.


--
-- Name: payment_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('payment_status_id_seq', 7, true);


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product (id, name, description, sku, handle, quantity, regular_price, sale_price, weight, has_variants, on_sale, is_featured, variant_attributes, user_id, view_count, last_updated, date_created) FROM stdin;
2	Red Bottom Sexy Women Stilleto	Black sexy stilleto shoe with a clean finish and suede covering.	blac-stilleto	red-bottom-sexy-women-stilleto	5	8500	7999	\N	t	f	f	size: 37,38,39	1	2	2017-10-30 18:20:18.603617	2017-10-30 17:05:15.938688
3	Classy red bottom stilleto sandal	Available in black and white	Sand0001	classy-red-bottom-stilleto-sandal	7	12000	10999	\N	t	f	f	Colour: white, black|Size:37,38,39	1	2	2017-10-30 21:22:16.156896	2017-10-30 18:40:40.301375
5	Elegant Formal Stilleto (Red Bottom)	An elegant formal stilleto heel, designed with a polished finish. Close deals in this heels.\r\nAvailable in white and black	cream-formal	elegant-formal-stilleto-red-bottom	5	10000	8999	\N	t	f	f	colour:white,black|size:37,38,39	1	1	2017-10-30 23:01:41.850964	2017-10-30 22:48:54.837402
6	Patent Leather Formal Low Heel Shoe	Patent leather low heel formal shoe suitable for all occassions	patent-low	patent-leather-formal-low-heel-shoe	5	8300	6500	\N	t	f	f	colour:wine,black|size:36,37,38,39	1	1	2017-10-30 23:03:37.287944	2017-10-30 23:00:18.57387
4	Bodycon classy midi dress	White stripped bodycon midi dress. Available in sizes M-XXL	whit-midi	bodycon-classy-midi-dress	10	6500	4999	\N	t	f	f	Size: -M, -L, -XL, -XXL, -XXXL	1	1	2017-10-30 23:04:03.854211	2017-10-30 22:33:13.585169
7	Off-Shoulder Bodycon Midi Dress	Class and elegant in every way	black-dress-01	off-shoulder-bodycon-midi-dress	10	6800	5500	\N	t	f	f	size:-M,-L,-XL,-XXL	1	1	2017-10-30 23:24:56.661332	2017-10-30 23:17:42.074951
8	Sexy Evening Dress Club Wear	A stunning sexy evening dress suitable for the club or a night out with bae	club00001	sexy-evening-dress-club-wear	10	6500	5950	\N	t	f	f	colour:-black,-red,-blue|size:-M	1	1	2017-10-31 08:59:28.345025	2017-10-31 08:55:23.26506
1	Blackview R6 5.5" fingerprint 3GB/32GB	A solid smart phone guaranteed to delivery service	blv00001	blackview-r6-5-5-fingerprint-3gb-32gb	1	59000	49000	\N	f	t	f		1	22	2017-10-31 09:09:48.262383	2017-10-30 09:29:05.432155
\.


--
-- Data for Name: product_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_detail (id, name, content, type_id, "position", product_id, last_updated, date_created) FROM stdin;
3			1	2	1	2017-10-30 12:30:20.237164	2017-10-30 12:30:20.237186
4	Photo Show	<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src="http://res.cloudinary.com/rusty-x/image/upload/v1509266448/SB2017-10-29%2008:40:47.897692.jpg" alt="" width="425" height="425" /></p>\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img src="http://res.cloudinary.com/rusty-x/image/upload/v1509266476/SB2017-10-29%2008:41:16.641071.jpg" alt="" width="741" height="494" /></p>\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img src="http://res.cloudinary.com/rusty-x/image/upload/v1509266496/SB2017-10-29%2008:41:36.541935.webp" alt="" width="744" height="369" /></p>\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src="http://res.cloudinary.com/rusty-x/image/upload/v1509266588/SB2017-10-29%2008:43:08.081718.webp" alt="" width="745" height="369" /></p>	2	3	1	2017-10-30 12:40:06.858136	2017-10-30 12:30:20.24543
5	Video show	https://www.youtube.com/embed/ri24dZ9meRo?rel=0&amp;amp;showinfo=0	3	4	1	2017-10-30 13:00:47.193494	2017-10-30 12:30:20.257459
2	Quick Specs	Brand name:Blackview|Band mode:2 SIM, Dual-band|RAM:3GB|ROM:32GB|OS:Android 6 mashmallow|Screen Size:5.5"|Primary Camera:13MP|Secondary Camera:5MP|CPU:Quad Core|CPU Brand:MTK|Display Resolution:1920x1080|Battery type:Not Detachable|Battery Capacity:3000mAh|Cellular:GSM/WCDMA/LTE|2G:GSM 850/900/1800/1900MHz|3G:WCDMA 900/2100MHz|4G:LTE 800/900/1800/2100/2600Mhz|Recording Definition:1080p	4	1	1	2017-10-30 15:32:41.843128	2017-10-30 12:30:20.137717
\.


--
-- Name: product_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_detail_id_seq', 5, true);


--
-- Data for Name: product_detail_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_detail_type (id, code, name, description, last_updated, date_created) FROM stdin;
1	text	Text	\N	2017-10-29 17:02:35.098869	2017-10-29 17:02:35.098908
2	picture	Picture	\N	2017-10-29 17:02:35.108895	2017-10-29 17:02:35.108914
3	video	Video	\N	2017-10-29 17:02:35.116746	2017-10-29 17:02:35.116763
4	table	Table	\N	2017-10-29 17:02:35.124297	2017-10-29 17:02:35.12431
\.


--
-- Name: product_detail_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_detail_type_id_seq', 4, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_id_seq', 8, true);


--
-- Data for Name: product_view; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_view (id, view_count, product_id, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: product_view_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_view_id_seq', 1, false);


--
-- Data for Name: recurrent_card; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY recurrent_card (id, token, mask, brand, exp_month, exp_year, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: recurrent_card_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('recurrent_card_id_seq', 1, false);


--
-- Data for Name: referral; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY referral (id, name, email, is_verified, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: referral_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('referral_id_seq', 1, false);


--
-- Data for Name: sms; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY sms (id, text, "to", from_, send_at, status, message_id, notification_id, sent_at, last_updated, date_created) FROM stdin;
\.


--
-- Name: sms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sms_id_seq', 1, false);


--
-- Data for Name: state; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY state (id, code, name, slug, country_code, country_id, last_updated, date_created) FROM stdin;
1	KDA	Kaduna	kaduna	NG	1	2017-10-29 17:02:35.143672	2017-10-29 17:02:35.143698
2	KAN	Kano	kano	NG	1	2017-10-29 17:02:35.157439	2017-10-29 17:02:35.157468
3	BNE	Benue	benue	NG	1	2017-10-29 17:02:35.166572	2017-10-29 17:02:35.166582
4	GMB	Gombe	gombe	NG	1	2017-10-29 17:02:35.176606	2017-10-29 17:02:35.176628
5	EBN	Ebonyi	ebonyi	NG	1	2017-10-29 17:02:35.187584	2017-10-29 17:02:35.187598
6	DEL	Delta	delta	NG	1	2017-10-29 17:02:35.19762	2017-10-29 17:02:35.197632
7	YBE	Yobe	yobe	NG	1	2017-10-29 17:02:35.207132	2017-10-29 17:02:35.207144
8	ZFR	Zamfara	zamfara	NG	1	2017-10-29 17:02:35.217478	2017-10-29 17:02:35.217492
9	EKI	Ekiti	ekiti	NG	1	2017-10-29 17:02:35.227262	2017-10-29 17:02:35.227274
10	EDO	Edo	edo	NG	1	2017-10-29 17:02:35.240396	2017-10-29 17:02:35.240413
11	CRI	Cross River	cross-river	NG	1	2017-10-29 17:02:35.25098	2017-10-29 17:02:35.250999
12	JIG	Jigawa	jigawa	NG	1	2017-10-29 17:02:35.262414	2017-10-29 17:02:35.262426
13	BOR	Borno	borno	NG	1	2017-10-29 17:02:35.271949	2017-10-29 17:02:35.271962
14	RVS	Rivers	rivers	NG	1	2017-10-29 17:02:35.294921	2017-10-29 17:02:35.294944
15	PLT	Plateau	plateau	NG	1	2017-10-29 17:02:35.30577	2017-10-29 17:02:35.305782
16	SOK	Sokoto	sokoto	NG	1	2017-10-29 17:02:35.313295	2017-10-29 17:02:35.313306
17	ENU	Enugu	enugu	NG	1	2017-10-29 17:02:35.321159	2017-10-29 17:02:35.32117
18	ABV	Abuja	abuja	NG	1	2017-10-29 17:02:35.328493	2017-10-29 17:02:35.328504
19	IMO	Imo	imo	NG	1	2017-10-29 17:02:35.336304	2017-10-29 17:02:35.336315
20	LOS	Lagos	lagos	NG	1	2017-10-29 17:02:35.344262	2017-10-29 17:02:35.344273
21	KGI	Kogi	kogi	NG	1	2017-10-29 17:02:35.354047	2017-10-29 17:02:35.354062
22	KEB	Kebbi	kebbi	NG	1	2017-10-29 17:02:35.363295	2017-10-29 17:02:35.363311
23	KAT	Katsina	katsina	NG	1	2017-10-29 17:02:35.373809	2017-10-29 17:02:35.373824
24	ABR	Anambra	anambra	NG	1	2017-10-29 17:02:35.385409	2017-10-29 17:02:35.385429
25	KWA	Kwara	kwara	NG	1	2017-10-29 17:02:35.397858	2017-10-29 17:02:35.397869
26	ABI	Abia	abia	NG	1	2017-10-29 17:02:35.405053	2017-10-29 17:02:35.405063
27	ADA	Adamawa	adamawa	NG	1	2017-10-29 17:02:35.41239	2017-10-29 17:02:35.412401
28	BAY	Bayelsa	bayelsa	NG	1	2017-10-29 17:02:35.424384	2017-10-29 17:02:35.424399
29	BCH	Bauchi	bauchi	NG	1	2017-10-29 17:02:35.433449	2017-10-29 17:02:35.43346
30	AKI	Akwa Ibom	akwa-ibom	NG	1	2017-10-29 17:02:35.442027	2017-10-29 17:02:35.442044
31	TAR	Taraba	taraba	NG	1	2017-10-29 17:02:35.452344	2017-10-29 17:02:35.452358
32	OSU	Osun	osun	NG	1	2017-10-29 17:02:35.463579	2017-10-29 17:02:35.463592
33	OYO	Oyo	oyo	NG	1	2017-10-29 17:02:35.475081	2017-10-29 17:02:35.475094
34	OGN	Ogun	ogun	NG	1	2017-10-29 17:02:35.488058	2017-10-29 17:02:35.488078
35	NAS	Nasarawa	nasarawa	NG	1	2017-10-29 17:02:35.500587	2017-10-29 17:02:35.500602
36	OND	Ondo	ondo	NG	1	2017-10-29 17:02:35.510852	2017-10-29 17:02:35.510867
37	NIG	Niger	niger	NG	1	2017-10-29 17:02:35.521343	2017-10-29 17:02:35.521364
38	GH-0	Greater Accra	greater-accra	GH	56	2017-10-29 17:02:35.531784	2017-10-29 17:02:35.531797
39	GH-1	Ashanti	ashanti	GH	56	2017-10-29 17:02:35.556713	2017-10-29 17:02:35.556752
40	GH-2	Northern	northern	GH	56	2017-10-29 17:02:35.568063	2017-10-29 17:02:35.568074
41	GH-3	Western	western	GH	56	2017-10-29 17:02:35.578375	2017-10-29 17:02:35.578388
42	GH-4	Brong-Ahafo	brong-ahafo	GH	56	2017-10-29 17:02:35.588168	2017-10-29 17:02:35.588181
43	GH-5	Central	central	GH	56	2017-10-29 17:02:35.598212	2017-10-29 17:02:35.598225
44	GH-6	Eastern	eastern	GH	56	2017-10-29 17:02:35.608518	2017-10-29 17:02:35.608531
45	GH-7	Upper West	upper-west	GH	56	2017-10-29 17:02:35.618329	2017-10-29 17:02:35.618341
46	GH-8	Upper East	upper-east	GH	56	2017-10-29 17:02:35.627836	2017-10-29 17:02:35.627848
47	GH-9	Volta	volta	GH	56	2017-10-29 17:02:35.636563	2017-10-29 17:02:35.636574
48	KE-0	Coast	coast	KE	180	2017-10-29 17:02:35.644272	2017-10-29 17:02:35.644282
49	KE-1	North Eastern	north-eastern	KE	180	2017-10-29 17:02:35.65138	2017-10-29 17:02:35.65139
50	KE-2	Eastern	eastern	KE	180	2017-10-29 17:02:35.658582	2017-10-29 17:02:35.658593
51	KE-3	Central	central	KE	180	2017-10-29 17:02:35.667877	2017-10-29 17:02:35.66791
52	KE-4	Rift Valley	rift-valley	KE	180	2017-10-29 17:02:35.679407	2017-10-29 17:02:35.679421
53	KE-5	Western	western	KE	180	2017-10-29 17:02:35.699036	2017-10-29 17:02:35.699056
54	KE-6	Nyanza	nyanza	KE	180	2017-10-29 17:02:35.71337	2017-10-29 17:02:35.713385
55	KE-7	Nairobi	nairobi	KE	180	2017-10-29 17:02:35.725981	2017-10-29 17:02:35.726005
56	AB	Alberta	alberta	CA	164	2017-10-29 17:02:35.737728	2017-10-29 17:02:35.737743
57	BC	British Columbia	british-columbia	CA	164	2017-10-29 17:02:35.751078	2017-10-29 17:02:35.751101
58	MB	Manitoba	manitoba	CA	164	2017-10-29 17:02:35.763409	2017-10-29 17:02:35.763424
59	NB	New Brunswick	new-brunswick	CA	164	2017-10-29 17:02:35.778948	2017-10-29 17:02:35.778967
60	NL	Newfoundland & Labrador	newfoundland-labrador	CA	164	2017-10-29 17:02:35.796212	2017-10-29 17:02:35.796227
61	NT	Northwest Territories	northwest-territories	CA	164	2017-10-29 17:02:35.807755	2017-10-29 17:02:35.807769
62	NS	Nova Scotia	nova-scotia	CA	164	2017-10-29 17:02:35.820572	2017-10-29 17:02:35.820588
63	NU	Nunavut	nunavut	CA	164	2017-10-29 17:02:35.831507	2017-10-29 17:02:35.831521
64	ON	Ontario	ontario	CA	164	2017-10-29 17:02:35.841898	2017-10-29 17:02:35.841909
65	AL	Alabama	alabama	US	210	2017-10-29 17:02:35.852751	2017-10-29 17:02:35.852763
66	AK	Alaska	alaska	US	210	2017-10-29 17:02:35.862261	2017-10-29 17:02:35.862273
67	AR	Arkansas	arkansas	US	210	2017-10-29 17:02:35.872465	2017-10-29 17:02:35.872478
68	CA	California	california	US	210	2017-10-29 17:02:35.882867	2017-10-29 17:02:35.882878
69	CO	Colorado	colorado	US	210	2017-10-29 17:02:35.893215	2017-10-29 17:02:35.893229
70	CT	Connecticut	connecticut	US	210	2017-10-29 17:02:35.904128	2017-10-29 17:02:35.904138
71	DE	Delaware	delaware	US	210	2017-10-29 17:02:35.915066	2017-10-29 17:02:35.915079
72	FL	Florida	florida	US	210	2017-10-29 17:02:35.926235	2017-10-29 17:02:35.926247
73	GA	Georgia	georgia	US	210	2017-10-29 17:02:35.935595	2017-10-29 17:02:35.935607
74	HI	Hawaii	hawaii	US	210	2017-10-29 17:02:35.946755	2017-10-29 17:02:35.94677
75	ID	Idaho	idaho	US	210	2017-10-29 17:02:35.95642	2017-10-29 17:02:35.956432
76	IL	Illinois	illinois	US	210	2017-10-29 17:02:35.966226	2017-10-29 17:02:35.966239
77	IN	Indiana	indiana	US	210	2017-10-29 17:02:35.977507	2017-10-29 17:02:35.97752
78	IA	Iowa	iowa	US	210	2017-10-29 17:02:35.987727	2017-10-29 17:02:35.987748
79	KS	Kansas	kansas	US	210	2017-10-29 17:02:35.999455	2017-10-29 17:02:35.999469
80	KY	Kentucky	kentucky	US	210	2017-10-29 17:02:36.010541	2017-10-29 17:02:36.010563
81	ME	Maine	maine	US	210	2017-10-29 17:02:36.020227	2017-10-29 17:02:36.020239
82	MD	Maryland	maryland	US	210	2017-10-29 17:02:36.030464	2017-10-29 17:02:36.030478
83	MA	Massachusetts	massachusetts	US	210	2017-10-29 17:02:36.041268	2017-10-29 17:02:36.041281
84	MI	Michigan	michigan	US	210	2017-10-29 17:02:36.051381	2017-10-29 17:02:36.051394
85	MN	Minnesota	minnesota	US	210	2017-10-29 17:02:36.061298	2017-10-29 17:02:36.061309
86	MS	Mississippi	mississippi	US	210	2017-10-29 17:02:36.071118	2017-10-29 17:02:36.071132
87	MO	Missouri	missouri	US	210	2017-10-29 17:02:36.082058	2017-10-29 17:02:36.082072
88	MT	Montana	montana	US	210	2017-10-29 17:02:36.092509	2017-10-29 17:02:36.092522
89	NE	Nebraska	nebraska	US	210	2017-10-29 17:02:36.103761	2017-10-29 17:02:36.103775
90	NV	Nevada	nevada	US	210	2017-10-29 17:02:36.114868	2017-10-29 17:02:36.114881
91	NH	New Hampshire	new-hampshire	US	210	2017-10-29 17:02:36.128687	2017-10-29 17:02:36.128712
92	NJ	New Jersey	new-jersey	US	210	2017-10-29 17:02:36.143287	2017-10-29 17:02:36.143303
93	NM	New Mexico	new-mexico	US	210	2017-10-29 17:02:36.156867	2017-10-29 17:02:36.156887
94	NY	New York	new-york	US	210	2017-10-29 17:02:36.168814	2017-10-29 17:02:36.168827
95	NC	North Carolina	north-carolina	US	210	2017-10-29 17:02:36.182149	2017-10-29 17:02:36.182167
96	ND	North Dakota	north-dakota	US	210	2017-10-29 17:02:36.194276	2017-10-29 17:02:36.19429
97	OH	Ohio	ohio	US	210	2017-10-29 17:02:36.202993	2017-10-29 17:02:36.203004
98	OK	Oklahoma	oklahoma	US	210	2017-10-29 17:02:36.210199	2017-10-29 17:02:36.21021
99	OR	Oregon	oregon	US	210	2017-10-29 17:02:36.217978	2017-10-29 17:02:36.217989
100	RI	Rhode Island	rhode-island	US	210	2017-10-29 17:02:36.225609	2017-10-29 17:02:36.22562
101	SC	South Carolina	south-carolina	US	210	2017-10-29 17:02:36.233728	2017-10-29 17:02:36.233746
102	SD	South Dakota	south-dakota	US	210	2017-10-29 17:02:36.241227	2017-10-29 17:02:36.241238
103	TN	Tennessee	tennessee	US	210	2017-10-29 17:02:36.249001	2017-10-29 17:02:36.249012
104	TX	Texas	texas	US	210	2017-10-29 17:02:36.257294	2017-10-29 17:02:36.257304
105	UT	Utah	utah	US	210	2017-10-29 17:02:36.26546	2017-10-29 17:02:36.265472
106	VT	Vermont	vermont	US	210	2017-10-29 17:02:36.273771	2017-10-29 17:02:36.273786
107	VA	Virginia	virginia	US	210	2017-10-29 17:02:36.281679	2017-10-29 17:02:36.28169
108	WA	Washington	washington	US	210	2017-10-29 17:02:36.289453	2017-10-29 17:02:36.289464
109	WV	West Virginia	west-virginia	US	210	2017-10-29 17:02:36.297066	2017-10-29 17:02:36.297077
110	WI	Wisconsin	wisconsin	US	210	2017-10-29 17:02:36.304445	2017-10-29 17:02:36.304455
111	WY	Wyoming	wyoming	US	210	2017-10-29 17:02:36.312075	2017-10-29 17:02:36.312087
112	PE	Prince Edward Island	prince-edward-island	CA	164	2017-10-29 17:02:36.320193	2017-10-29 17:02:36.320204
113	QC	Quebec	quebec	CA	164	2017-10-29 17:02:36.328607	2017-10-29 17:02:36.328618
114	SK	Saskatchewan	saskatchewan	CA	164	2017-10-29 17:02:36.341022	2017-10-29 17:02:36.341037
115	YT	Yukon	yukon	CA	164	2017-10-29 17:02:36.349776	2017-10-29 17:02:36.349799
116	GB-BKM	Buckinghamshire	buckinghamshire	GB	50	2017-10-29 17:02:36.357429	2017-10-29 17:02:36.35744
117	GB-CAM	Cambridgeshire	cambridgeshire	GB	50	2017-10-29 17:02:36.36917	2017-10-29 17:02:36.369188
118	GB-CMA	Cumbria	cumbria	GB	50	2017-10-29 17:02:36.381168	2017-10-29 17:02:36.381193
119	GB-DBY	Derbyshire	derbyshire	GB	50	2017-10-29 17:02:36.395068	2017-10-29 17:02:36.395085
120	GB-DEV	Devon	devon	GB	50	2017-10-29 17:02:36.407104	2017-10-29 17:02:36.407115
121	GB-DOR	Dorset	dorset	GB	50	2017-10-29 17:02:36.414804	2017-10-29 17:02:36.414814
122	GB-ESX	East Sussex	east-sussex	GB	50	2017-10-29 17:02:36.423016	2017-10-29 17:02:36.423027
123	GB-ESS	Essex	essex	GB	50	2017-10-29 17:02:36.430826	2017-10-29 17:02:36.430836
124	GB-GLS	Gloucestershire	gloucestershire	GB	50	2017-10-29 17:02:36.438208	2017-10-29 17:02:36.438219
125	GB-HAM	Hampshire	hampshire	GB	50	2017-10-29 17:02:36.446839	2017-10-29 17:02:36.446879
126	GB-HRT	Hertfordshire	hertfordshire	GB	50	2017-10-29 17:02:36.457398	2017-10-29 17:02:36.457413
127	GB-KEN	Kent	kent	GB	50	2017-10-29 17:02:36.466148	2017-10-29 17:02:36.466162
128	GB-LAN	Lancashire	lancashire	GB	50	2017-10-29 17:02:36.478561	2017-10-29 17:02:36.478573
129	GB-LEC	Leicestershire	leicestershire	GB	50	2017-10-29 17:02:36.489009	2017-10-29 17:02:36.489042
130	GB-LIN	Lincolnshire	lincolnshire	GB	50	2017-10-29 17:02:36.49875	2017-10-29 17:02:36.498762
131	GB-NFK	Norfolk	norfolk	GB	50	2017-10-29 17:02:36.512614	2017-10-29 17:02:36.512634
132	GB-NYK	North Yorkshire	north-yorkshire	GB	50	2017-10-29 17:02:36.520401	2017-10-29 17:02:36.520411
133	GB-NTH	Northamptonshire	northamptonshire	GB	50	2017-10-29 17:02:36.528112	2017-10-29 17:02:36.528124
134	GB-NTT	Nottinghamshire	nottinghamshire	GB	50	2017-10-29 17:02:36.536275	2017-10-29 17:02:36.536289
135	GB-OXF	Oxfordshire	oxfordshire	GB	50	2017-10-29 17:02:36.544523	2017-10-29 17:02:36.544534
136	GB-SOM	Somerset	somerset	GB	50	2017-10-29 17:02:36.551949	2017-10-29 17:02:36.55196
137	GB-STS	Staffordshire	staffordshire	GB	50	2017-10-29 17:02:36.560217	2017-10-29 17:02:36.560228
138	GB-SFK	Suffolk	suffolk	GB	50	2017-10-29 17:02:36.567415	2017-10-29 17:02:36.567425
139	GB-SRY	Surrey	surrey	GB	50	2017-10-29 17:02:36.574556	2017-10-29 17:02:36.574567
140	GB-WAR	Warwickshire	warwickshire	GB	50	2017-10-29 17:02:36.581715	2017-10-29 17:02:36.581726
141	GB-WSX	West Sussex	west-sussex	GB	50	2017-10-29 17:02:36.589092	2017-10-29 17:02:36.589103
142	GB-WOR	Worcestershire	worcestershire	GB	50	2017-10-29 17:02:36.598475	2017-10-29 17:02:36.598485
143	GB-LND	London	london	GB	50	2017-10-29 17:02:36.606167	2017-10-29 17:02:36.606178
144	GB-BDG	Barking and Dagenham	barking-and-dagenham	GB	50	2017-10-29 17:02:36.61341	2017-10-29 17:02:36.613421
145	GB-BNE	Barnet	barnet	GB	50	2017-10-29 17:02:36.62369	2017-10-29 17:02:36.623701
146	GB-BEX	Bexley	bexley	GB	50	2017-10-29 17:02:36.630808	2017-10-29 17:02:36.630818
147	GB-BEN	Brent	brent	GB	50	2017-10-29 17:02:36.63783	2017-10-29 17:02:36.637841
148	GB-BRY	Bromley	bromley	GB	50	2017-10-29 17:02:36.645523	2017-10-29 17:02:36.645534
149	GB-CMD	Camden	camden	GB	50	2017-10-29 17:02:36.653182	2017-10-29 17:02:36.653192
150	GB-CRY	Croydon	croydon	GB	50	2017-10-29 17:02:36.660232	2017-10-29 17:02:36.660242
151	GB-EAL	Ealing	ealing	GB	50	2017-10-29 17:02:36.667367	2017-10-29 17:02:36.667377
152	GB-ENF	Enfield	enfield	GB	50	2017-10-29 17:02:36.678017	2017-10-29 17:02:36.678036
153	GB-GRE	Greenwich	greenwich	GB	50	2017-10-29 17:02:36.690411	2017-10-29 17:02:36.690428
154	GB-HCK	Hackney	hackney	GB	50	2017-10-29 17:02:36.699059	2017-10-29 17:02:36.69907
155	GB-HMF	Hammersmith and Fulham	hammersmith-and-fulham	GB	50	2017-10-29 17:02:36.709881	2017-10-29 17:02:36.709893
156	GB-HRY	Haringey	haringey	GB	50	2017-10-29 17:02:36.72177	2017-10-29 17:02:36.721784
157	GB-HRW	Harrow	harrow	GB	50	2017-10-29 17:02:36.732342	2017-10-29 17:02:36.732354
158	GB-HAV	Havering	havering	GB	50	2017-10-29 17:02:36.742666	2017-10-29 17:02:36.74268
159	GB-HIL	Hillingdon	hillingdon	GB	50	2017-10-29 17:02:36.753681	2017-10-29 17:02:36.753743
160	GB-HNS	Hounslow	hounslow	GB	50	2017-10-29 17:02:36.764139	2017-10-29 17:02:36.764153
161	GB-ISL	Islington	islington	GB	50	2017-10-29 17:02:36.777151	2017-10-29 17:02:36.777169
162	GB-KEC	Kensington and Chelsea	kensington-and-chelsea	GB	50	2017-10-29 17:02:36.788335	2017-10-29 17:02:36.788347
163	GB-KTT	Kingston upon Thames	kingston-upon-thames	GB	50	2017-10-29 17:02:36.798235	2017-10-29 17:02:36.798247
164	GB-LBH	Lambeth	lambeth	GB	50	2017-10-29 17:02:36.806168	2017-10-29 17:02:36.806179
165	GB-LEW	Lewisham	lewisham	GB	50	2017-10-29 17:02:36.815164	2017-10-29 17:02:36.815176
166	GB-MRT	Merton	merton	GB	50	2017-10-29 17:02:36.827579	2017-10-29 17:02:36.827594
167	GB-NWM	Newham	newham	GB	50	2017-10-29 17:02:36.839521	2017-10-29 17:02:36.839534
168	GB-RDB	Redbridge	redbridge	GB	50	2017-10-29 17:02:36.856826	2017-10-29 17:02:36.856844
169	GB-RIC	Richmond upon Thames	richmond-upon-thames	GB	50	2017-10-29 17:02:36.86775	2017-10-29 17:02:36.867764
170	GB-SWK	Southwark	southwark	GB	50	2017-10-29 17:02:36.879002	2017-10-29 17:02:36.879017
171	GB-STN	Sutton	sutton	GB	50	2017-10-29 17:02:36.889675	2017-10-29 17:02:36.889688
172	GB-TWH	Tower Hamlets	tower-hamlets	GB	50	2017-10-29 17:02:36.900059	2017-10-29 17:02:36.900073
173	GB-WFT	Waltham Forest	waltham-forest	GB	50	2017-10-29 17:02:36.914276	2017-10-29 17:02:36.914288
174	GB-WND	Wandsworth	wandsworth	GB	50	2017-10-29 17:02:36.925277	2017-10-29 17:02:36.925294
175	GB-WSM	Westminster	westminster	GB	50	2017-10-29 17:02:36.936797	2017-10-29 17:02:36.936809
176	GB-BNS	Barnsley	barnsley	GB	50	2017-10-29 17:02:36.946128	2017-10-29 17:02:36.94614
177	GB-BIR	Birmingham	birmingham	GB	50	2017-10-29 17:02:36.955149	2017-10-29 17:02:36.955161
178	GB-BOL	Bolton	bolton	GB	50	2017-10-29 17:02:36.965635	2017-10-29 17:02:36.965648
179	GB-BRD	Bradford	bradford	GB	50	2017-10-29 17:02:36.976853	2017-10-29 17:02:36.976866
180	GB-BUR	Bury	bury	GB	50	2017-10-29 17:02:36.987152	2017-10-29 17:02:36.987164
181	GB-CLD	Calderdale	calderdale	GB	50	2017-10-29 17:02:36.997473	2017-10-29 17:02:36.997486
182	GB-COV	Coventry	coventry	GB	50	2017-10-29 17:02:37.010073	2017-10-29 17:02:37.010092
183	GB-DNC	Doncaster	doncaster	GB	50	2017-10-29 17:02:37.022823	2017-10-29 17:02:37.022837
184	GB-DUD	Dudley	dudley	GB	50	2017-10-29 17:02:37.033906	2017-10-29 17:02:37.033919
185	GB-GAT	Gateshead	gateshead	GB	50	2017-10-29 17:02:37.044825	2017-10-29 17:02:37.044838
186	GB-KIR	Kirklees	kirklees	GB	50	2017-10-29 17:02:37.055809	2017-10-29 17:02:37.05582
187	GB-KWL	Knowsley	knowsley	GB	50	2017-10-29 17:02:37.063344	2017-10-29 17:02:37.063355
188	GB-LDS	Leeds	leeds	GB	50	2017-10-29 17:02:37.070785	2017-10-29 17:02:37.070796
189	GB-LIV	Liverpool	liverpool	GB	50	2017-10-29 17:02:37.078395	2017-10-29 17:02:37.078405
190	GB-MAN	Manchester	manchester	GB	50	2017-10-29 17:02:37.085502	2017-10-29 17:02:37.085513
191	GB-NET	Newcastle upon Tyne	newcastle-upon-tyne	GB	50	2017-10-29 17:02:37.093235	2017-10-29 17:02:37.093245
192	GB-NTY	North Tyneside	north-tyneside	GB	50	2017-10-29 17:02:37.10023	2017-10-29 17:02:37.100241
193	GB-OLD	Oldham	oldham	GB	50	2017-10-29 17:02:37.107755	2017-10-29 17:02:37.107765
194	GB-RCH	Rochdale	rochdale	GB	50	2017-10-29 17:02:37.119274	2017-10-29 17:02:37.119289
195	GB-ROT	Rotherham	rotherham	GB	50	2017-10-29 17:02:37.129456	2017-10-29 17:02:37.12947
196	GB-SLF	Salford	salford	GB	50	2017-10-29 17:02:37.139095	2017-10-29 17:02:37.13911
197	GB-SAW	Sandwell	sandwell	GB	50	2017-10-29 17:02:37.14935	2017-10-29 17:02:37.149365
198	GB-SFT	Sefton	sefton	GB	50	2017-10-29 17:02:37.158075	2017-10-29 17:02:37.158088
199	GB-SHF	Sheffield	sheffield	GB	50	2017-10-29 17:02:37.166575	2017-10-29 17:02:37.166598
200	GB-SOL	Solihull	solihull	GB	50	2017-10-29 17:02:37.177059	2017-10-29 17:02:37.177075
201	GB-STY	South Tyneside	south-tyneside	GB	50	2017-10-29 17:02:37.185805	2017-10-29 17:02:37.185818
202	GB-SKP	Stockport	stockport	GB	50	2017-10-29 17:02:37.194708	2017-10-29 17:02:37.19472
203	GB-SND	Sunderland	sunderland	GB	50	2017-10-29 17:02:37.209013	2017-10-29 17:02:37.209034
204	GB-TAM	Tameside	tameside	GB	50	2017-10-29 17:02:37.21677	2017-10-29 17:02:37.21678
205	GB-TRF	Trafford	trafford	GB	50	2017-10-29 17:02:37.22422	2017-10-29 17:02:37.224231
206	GB-WKF	Wakefield	wakefield	GB	50	2017-10-29 17:02:37.234964	2017-10-29 17:02:37.234975
207	GB-WLL	Walsall	walsall	GB	50	2017-10-29 17:02:37.242359	2017-10-29 17:02:37.24237
208	GB-WGN	Wigan	wigan	GB	50	2017-10-29 17:02:37.254309	2017-10-29 17:02:37.254327
209	GB-WRL	Wirral	wirral	GB	50	2017-10-29 17:02:37.265243	2017-10-29 17:02:37.265254
210	GB-WLV	Wolverhampton	wolverhampton	GB	50	2017-10-29 17:02:37.275895	2017-10-29 17:02:37.275913
211	GB-BAS	Bath and North East Somerset	bath-and-north-east-somerset	GB	50	2017-10-29 17:02:37.288683	2017-10-29 17:02:37.288699
212	GB-BDF	Bedford	bedford	GB	50	2017-10-29 17:02:37.298424	2017-10-29 17:02:37.298435
213	GB-BBD	Blackburn with Darwen	blackburn-with-darwen	GB	50	2017-10-29 17:02:37.306611	2017-10-29 17:02:37.306622
214	GB-BPL	Blackpool	blackpool	GB	50	2017-10-29 17:02:37.314011	2017-10-29 17:02:37.314021
215	GB-BMH	Bournemouth	bournemouth	GB	50	2017-10-29 17:02:37.322467	2017-10-29 17:02:37.322478
216	GB-BRC	Bracknell Forest	bracknell-forest	GB	50	2017-10-29 17:02:37.330386	2017-10-29 17:02:37.330396
217	GB-BNH	Brighton and Hove	brighton-and-hove	GB	50	2017-10-29 17:02:37.337876	2017-10-29 17:02:37.337886
218	GB-BST	Bristol	bristol	GB	50	2017-10-29 17:02:37.345653	2017-10-29 17:02:37.345663
219	GB-CBF	Central Bedfordshire	central-bedfordshire	GB	50	2017-10-29 17:02:37.353514	2017-10-29 17:02:37.353531
220	GB-CHE	Cheshire East	cheshire-east	GB	50	2017-10-29 17:02:37.363608	2017-10-29 17:02:37.363619
221	GB-CHW	Cheshire West and Chester	cheshire-west-and-chester	GB	50	2017-10-29 17:02:37.371799	2017-10-29 17:02:37.371818
222	GB-CON	Cornwall	cornwall	GB	50	2017-10-29 17:02:37.379461	2017-10-29 17:02:37.379471
223	GB-DAL	Darlington	darlington	GB	50	2017-10-29 17:02:37.389367	2017-10-29 17:02:37.389382
224	GB-DER	Derby	derby	GB	50	2017-10-29 17:02:37.398692	2017-10-29 17:02:37.398705
225	GB-DUR	Durham County	durham-county	GB	50	2017-10-29 17:02:37.411333	2017-10-29 17:02:37.411352
226	GB-ERY	East Riding of Yorkshire	east-riding-of-yorkshire	GB	50	2017-10-29 17:02:37.419563	2017-10-29 17:02:37.419574
227	GB-HAL	Halton	halton	GB	50	2017-10-29 17:02:37.427404	2017-10-29 17:02:37.427415
228	GB-HPL	Hartlepool	hartlepool	GB	50	2017-10-29 17:02:37.434762	2017-10-29 17:02:37.434773
229	GB-HEF	Herefordshire	herefordshire	GB	50	2017-10-29 17:02:37.443199	2017-10-29 17:02:37.443211
230	GB-IOW	Isle of Wight	isle-of-wight	GB	50	2017-10-29 17:02:37.451068	2017-10-29 17:02:37.451079
231	GB-IOS	Isles of Scilly	isles-of-scilly	GB	50	2017-10-29 17:02:37.458477	2017-10-29 17:02:37.458487
232	GB-KHL	Kingston upon Hull	kingston-upon-hull	GB	50	2017-10-29 17:02:37.466202	2017-10-29 17:02:37.466212
233	GB-LCE	Leicester	leicester	GB	50	2017-10-29 17:02:37.474233	2017-10-29 17:02:37.474243
234	GB-LUT	Luton	luton	GB	50	2017-10-29 17:02:37.481548	2017-10-29 17:02:37.481558
235	GB-MDW	Medway	medway	GB	50	2017-10-29 17:02:37.488933	2017-10-29 17:02:37.488944
236	GB-MDB	Middlesbrough	middlesbrough	GB	50	2017-10-29 17:02:37.496745	2017-10-29 17:02:37.496756
237	GB-MIK	Milton Keynes	milton-keynes	GB	50	2017-10-29 17:02:37.504633	2017-10-29 17:02:37.504646
238	GB-NEL	North East Lincolnshire	north-east-lincolnshire	GB	50	2017-10-29 17:02:37.512634	2017-10-29 17:02:37.512646
239	GB-NLN	North Lincolnshire	north-lincolnshire	GB	50	2017-10-29 17:02:37.520929	2017-10-29 17:02:37.520941
240	GB-NSM	North Somerset	north-somerset	GB	50	2017-10-29 17:02:37.529407	2017-10-29 17:02:37.529425
241	GB-NBL	Northumberland	northumberland	GB	50	2017-10-29 17:02:37.542092	2017-10-29 17:02:37.542108
242	GB-NGM	Nottingham	nottingham	GB	50	2017-10-29 17:02:37.551975	2017-10-29 17:02:37.551987
243	GB-PTE	Peterborough	peterborough	GB	50	2017-10-29 17:02:37.560729	2017-10-29 17:02:37.560746
244	GB-PLY	Plymouth	plymouth	GB	50	2017-10-29 17:02:37.570163	2017-10-29 17:02:37.570177
245	GB-POL	Poole	poole	GB	50	2017-10-29 17:02:37.581544	2017-10-29 17:02:37.581559
246	GB-POR	Portsmouth	portsmouth	GB	50	2017-10-29 17:02:37.590842	2017-10-29 17:02:37.590863
247	GB-RDG	Reading	reading	GB	50	2017-10-29 17:02:37.599159	2017-10-29 17:02:37.599172
248	GB-RCC	Redcar and Cleveland	redcar-and-cleveland	GB	50	2017-10-29 17:02:37.607245	2017-10-29 17:02:37.607258
249	GB-RUT	Rutland	rutland	GB	50	2017-10-29 17:02:37.614943	2017-10-29 17:02:37.614954
250	GB-SHR	Shropshire	shropshire	GB	50	2017-10-29 17:02:37.622541	2017-10-29 17:02:37.622554
251	GB-SLG	Slough	slough	GB	50	2017-10-29 17:02:37.632321	2017-10-29 17:02:37.632332
252	GB-SGC	South Gloucestershire	south-gloucestershire	GB	50	2017-10-29 17:02:37.64607	2017-10-29 17:02:37.646082
253	GB-STH	Southampton	southampton	GB	50	2017-10-29 17:02:37.655767	2017-10-29 17:02:37.655782
254	GB-SOS	Southend-on-Sea	southend-on-sea	GB	50	2017-10-29 17:02:37.664808	2017-10-29 17:02:37.664823
255	GB-STT	Stockton-on-Tees	stockton-on-tees	GB	50	2017-10-29 17:02:37.673178	2017-10-29 17:02:37.673193
256	GB-STE	Stoke-on-Trent	stoke-on-trent	GB	50	2017-10-29 17:02:37.681903	2017-10-29 17:02:37.681917
257	GB-SWD	Swindon	swindon	GB	50	2017-10-29 17:02:37.691156	2017-10-29 17:02:37.69117
258	GB-TFW	Telford and Wrekin	telford-and-wrekin	GB	50	2017-10-29 17:02:37.701331	2017-10-29 17:02:37.701346
259	GB-THR	Thurrock	thurrock	GB	50	2017-10-29 17:02:37.710528	2017-10-29 17:02:37.710545
260	GB-TOB	Torbay	torbay	GB	50	2017-10-29 17:02:37.720023	2017-10-29 17:02:37.720037
261	GB-WRT	Warrington	warrington	GB	50	2017-10-29 17:02:37.729768	2017-10-29 17:02:37.729781
262	GB-WBK	West Berkshire	west-berkshire	GB	50	2017-10-29 17:02:37.741453	2017-10-29 17:02:37.741465
263	GB-WIL	Wiltshire	wiltshire	GB	50	2017-10-29 17:02:37.752409	2017-10-29 17:02:37.752422
264	GB-WNM	Windsor and Maidenhead	windsor-and-maidenhead	GB	50	2017-10-29 17:02:37.762886	2017-10-29 17:02:37.762899
265	GB-WOK	Wokingham	wokingham	GB	50	2017-10-29 17:02:37.773093	2017-10-29 17:02:37.773106
266	GB-YOR	York	york	GB	50	2017-10-29 17:02:37.784305	2017-10-29 17:02:37.784318
267	GB-ANN	Antrim and Newtownabbey	antrim-and-newtownabbey	GB	50	2017-10-29 17:02:37.79644	2017-10-29 17:02:37.796456
268	GB-AND	Ards and North Down	ards-and-north-down	GB	50	2017-10-29 17:02:37.808121	2017-10-29 17:02:37.80814
269	GB-ABC	Armagh Banbridge and Craigavon	armagh-banbridge-and-craigavon	GB	50	2017-10-29 17:02:37.820301	2017-10-29 17:02:37.820316
270	GB-BFS	Belfast	belfast	GB	50	2017-10-29 17:02:37.82954	2017-10-29 17:02:37.829551
271	GB-CCG	Causeway Coast and Glens	causeway-coast-and-glens	GB	50	2017-10-29 17:02:37.837454	2017-10-29 17:02:37.837466
272	GB-DRS	Derry and Strabane	derry-and-strabane	GB	50	2017-10-29 17:02:37.846	2017-10-29 17:02:37.846011
273	GB-FMO	Fermanagh and Omagh	fermanagh-and-omagh	GB	50	2017-10-29 17:02:37.853425	2017-10-29 17:02:37.853435
274	GB-LBC	Lisburn and Castlereagh	lisburn-and-castlereagh	GB	50	2017-10-29 17:02:37.860981	2017-10-29 17:02:37.860991
275	GB-MEA	Mid and East Antrim	mid-and-east-antrim	GB	50	2017-10-29 17:02:37.869328	2017-10-29 17:02:37.869338
276	GB-MUL	Mid Ulster	mid-ulster	GB	50	2017-10-29 17:02:37.876623	2017-10-29 17:02:37.876632
277	GB-NMD	Newry Mourne and Down	newry-mourne-and-down	GB	50	2017-10-29 17:02:37.884009	2017-10-29 17:02:37.88402
278	GB-ABE	Aberdeen City	aberdeen-city	GB	50	2017-10-29 17:02:37.892123	2017-10-29 17:02:37.892135
279	GB-ABD	Aberdeenshire	aberdeenshire	GB	50	2017-10-29 17:02:37.900344	2017-10-29 17:02:37.900357
280	GB-ANS	Angus	angus	GB	50	2017-10-29 17:02:37.908987	2017-10-29 17:02:37.909
281	GB-AGB	Argyll and Bute	argyll-and-bute	GB	50	2017-10-29 17:02:37.918597	2017-10-29 17:02:37.918609
282	GB-CLK	Clackmannanshire	clackmannanshire	GB	50	2017-10-29 17:02:37.930343	2017-10-29 17:02:37.930356
283	GB-DGY	Dumfries and Galloway	dumfries-and-galloway	GB	50	2017-10-29 17:02:37.938241	2017-10-29 17:02:37.938253
284	GB-DND	Dundee City	dundee-city	GB	50	2017-10-29 17:02:37.946215	2017-10-29 17:02:37.946231
285	GB-EAY	East Ayrshire	east-ayrshire	GB	50	2017-10-29 17:02:37.956084	2017-10-29 17:02:37.956096
286	GB-EDU	East Dunbartonshire	east-dunbartonshire	GB	50	2017-10-29 17:02:37.967004	2017-10-29 17:02:37.967015
287	GB-ELN	East Lothian	east-lothian	GB	50	2017-10-29 17:02:37.976044	2017-10-29 17:02:37.976056
288	GB-ERW	East Renfrewshire	east-renfrewshire	GB	50	2017-10-29 17:02:37.984496	2017-10-29 17:02:37.984515
289	GB-EDH	Edinburgh	edinburgh	GB	50	2017-10-29 17:02:37.992572	2017-10-29 17:02:37.992583
290	GB-ELS	Eilean Siar	eilean-siar	GB	50	2017-10-29 17:02:38.008442	2017-10-29 17:02:38.008468
291	GB-FAL	Falkirk	falkirk	GB	50	2017-10-29 17:02:38.020877	2017-10-29 17:02:38.020903
292	GB-FIF	Fife	fife	GB	50	2017-10-29 17:02:38.03238	2017-10-29 17:02:38.032395
293	GB-GLG	Glasgow City	glasgow-city	GB	50	2017-10-29 17:02:38.043987	2017-10-29 17:02:38.044001
294	GB-HLD	Highland	highland	GB	50	2017-10-29 17:02:38.05547	2017-10-29 17:02:38.055485
295	GB-IVC	Inverclyde	inverclyde	GB	50	2017-10-29 17:02:38.067137	2017-10-29 17:02:38.067152
296	GB-MLN	Midlothian	midlothian	GB	50	2017-10-29 17:02:38.081137	2017-10-29 17:02:38.081157
297	GB-MRY	Moray	moray	GB	50	2017-10-29 17:02:38.094362	2017-10-29 17:02:38.094378
298	GB-NAY	North Ayrshire	north-ayrshire	GB	50	2017-10-29 17:02:38.105163	2017-10-29 17:02:38.105176
299	GB-NLK	North Lanarkshire	north-lanarkshire	GB	50	2017-10-29 17:02:38.119078	2017-10-29 17:02:38.119097
300	GB-ORK	Orkney Islands	orkney-islands	GB	50	2017-10-29 17:02:38.131852	2017-10-29 17:02:38.131888
301	GB-PKN	Perth and Kinross	perth-and-kinross	GB	50	2017-10-29 17:02:38.143758	2017-10-29 17:02:38.143775
302	GB-RFW	Renfrewshire	renfrewshire	GB	50	2017-10-29 17:02:38.156595	2017-10-29 17:02:38.156612
303	GB-SCB	Scottish Borders	scottish-borders	GB	50	2017-10-29 17:02:38.165722	2017-10-29 17:02:38.165735
304	GB-ZET	Shetland Islands	shetland-islands	GB	50	2017-10-29 17:02:38.174697	2017-10-29 17:02:38.17471
305	GB-SAY	South Ayrshire	south-ayrshire	GB	50	2017-10-29 17:02:38.185182	2017-10-29 17:02:38.185194
306	GB-SLK	South Lanarkshire	south-lanarkshire	GB	50	2017-10-29 17:02:38.196577	2017-10-29 17:02:38.196589
307	GB-STG	Stirling	stirling	GB	50	2017-10-29 17:02:38.207639	2017-10-29 17:02:38.207656
308	GB-WDU	West Dunbartonshire	west-dunbartonshire	GB	50	2017-10-29 17:02:38.217084	2017-10-29 17:02:38.217097
309	GB-WLN	West Lothian	west-lothian	GB	50	2017-10-29 17:02:38.23141	2017-10-29 17:02:38.23143
310	GB-BGW	Blaenau Gwent	blaenau-gwent	GB	50	2017-10-29 17:02:38.244492	2017-10-29 17:02:38.244506
311	GB-BGE	Bridgend	bridgend	GB	50	2017-10-29 17:02:38.256221	2017-10-29 17:02:38.256236
312	GB-CAY	Caerphilly	caerphilly	GB	50	2017-10-29 17:02:38.266955	2017-10-29 17:02:38.266966
313	GB-CRF	Cardiff	cardiff	GB	50	2017-10-29 17:02:38.277582	2017-10-29 17:02:38.277599
314	GB-CMN	Carmarthenshire	carmarthenshire	GB	50	2017-10-29 17:02:38.288811	2017-10-29 17:02:38.288823
315	GB-CGN	Ceredigion	ceredigion	GB	50	2017-10-29 17:02:38.302626	2017-10-29 17:02:38.30264
316	GB-CWY	Conwy	conwy	GB	50	2017-10-29 17:02:38.311516	2017-10-29 17:02:38.311529
317	GB-DEN	Denbighshire	denbighshire	GB	50	2017-10-29 17:02:38.324763	2017-10-29 17:02:38.324782
318	GB-FLN	Flintshire	flintshire	GB	50	2017-10-29 17:02:38.33648	2017-10-29 17:02:38.336492
319	GB-GWN	Gwynedd	gwynedd	GB	50	2017-10-29 17:02:38.349812	2017-10-29 17:02:38.349824
320	GB-AGY	Isle of Anglesey	isle-of-anglesey	GB	50	2017-10-29 17:02:38.367903	2017-10-29 17:02:38.367915
321	GB-MTY	Merthyr Tydfil	merthyr-tydfil	GB	50	2017-10-29 17:02:38.38031	2017-10-29 17:02:38.380322
322	GB-MON	Monmouthshire	monmouthshire	GB	50	2017-10-29 17:02:38.392276	2017-10-29 17:02:38.392294
323	GB-NTL	Neath Port Talbot	neath-port-talbot	GB	50	2017-10-29 17:02:38.402552	2017-10-29 17:02:38.402566
324	GB-NWP	Newport	newport	GB	50	2017-10-29 17:02:38.410315	2017-10-29 17:02:38.410331
325	GB-PEM	Pembrokeshire	pembrokeshire	GB	50	2017-10-29 17:02:38.417675	2017-10-29 17:02:38.417685
326	GB-POW	Powys	powys	GB	50	2017-10-29 17:02:38.425399	2017-10-29 17:02:38.42541
327	GB-RCT	Rhondda, Cynon, Taff	rhondda-cynon-taff	GB	50	2017-10-29 17:02:38.433109	2017-10-29 17:02:38.433119
328	GB-SWA	Swansea	swansea	GB	50	2017-10-29 17:02:38.440401	2017-10-29 17:02:38.440411
329	GB-TOF	Torfaen	torfaen	GB	50	2017-10-29 17:02:38.448712	2017-10-29 17:02:38.448722
330	GB-VGL	Vale of Glamorgan	vale-of-glamorgan	GB	50	2017-10-29 17:02:38.458872	2017-10-29 17:02:38.458887
331	GB-WRX	Wrexham	wrexham	GB	50	2017-10-29 17:02:38.469408	2017-10-29 17:02:38.469433
332	AZ	Arizona	arizona	US	210	2017-10-29 17:02:38.478201	2017-10-29 17:02:38.478214
333	LA	Louisiana	louisiana	US	210	2017-10-29 17:02:38.485976	2017-10-29 17:02:38.485987
334	PA	Pennsylvania	pennsylvania	US	210	2017-10-29 17:02:38.493275	2017-10-29 17:02:38.493286
\.


--
-- Name: state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('state_id_seq', 334, true);


--
-- Data for Name: timezone; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY timezone (id, name, code, "offset", last_updated, date_created) FROM stdin;
1	(GMT+0100) Africa/Lagos	Africa/Lagos	+0100	2017-10-29 17:02:34.275846	2017-10-29 17:02:34.275862
\.


--
-- Name: timezone_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('timezone_id_seq', 1, true);


--
-- Data for Name: transaction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY transaction (id, customer_id, cart_id, code, description, payment_option_id, transaction_status_id, order_id, amount, phone, email, address_line1, city, state, country, auto_set_shipment, shipment_name, shipment_phone, shipment_address, shipment_city, shipment_description, shipment_state, shipment_country, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Data for Name: transaction_entry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY transaction_entry (id, variant_id, transaction_id, quantity, price, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: transaction_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('transaction_entry_id_seq', 1, false);


--
-- Name: transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('transaction_id_seq', 1, false);


--
-- Data for Name: transaction_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY transaction_status (id, name, code, message, last_updated, date_created) FROM stdin;
1	Successful	successful	Your transaction completed successfully	2017-10-29 17:02:34.291969	2017-10-29 17:02:34.292001
2	Failed	failed	Your transaction failed	2017-10-29 17:02:34.300529	2017-10-29 17:02:34.300543
3	Pending	pending	Pending further action	2017-10-29 17:02:34.30767	2017-10-29 17:02:34.307683
\.


--
-- Name: transaction_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('transaction_status_id_seq', 3, true);


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "user" (id, username, email, alt_email, first_name, last_name, gender, phone, alt_phone, date_of_birth, password, is_enabled, is_verified, phone_verified, email_verified, is_staff, is_admin, login_count, last_login_at, current_login_at, last_login_ip, current_login_ip, awarded_credit, purchased_credit, user_earning, referral_code, profile_pic, verification_hash, otp, is_incognito, referral_count, last_updated, date_created) FROM stdin;
1	buyorbid	info@buyorbidng.com	\N	buyorbid	buyorbid	\N	\N	\N	\N	$2a$12$efWUtxhvgBbm.nSiADTaoO.XpwXJAnMur/qeEYa/vuMQ1simuoRIq	t	f	f	f	t	t	0	\N	\N	\N	\N	950	4000	2500	buy0001	http://res.cloudinary.com/rusty-x/image/upload/v1509194906/profile/BOB-PP_1.png	UE1vMEZ1LVJXMWE3a2wydWxJM1hPX2w2X05JeGpSRVA6aW5mb0BidXlvcmJpZG5nLmNvbQ	691414	f	0	2017-10-30 09:24:18.970375	2017-10-30 09:24:16.412743
\.


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_id_seq', 1, true);


--
-- Data for Name: variant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY variant (id, name, attribute_keys, description, height, width, regular_price, sale_price, weight, tax_class, sku, shipping_class, is_saved, on_sale, quantity, availability, view_count, user_id, product_id, last_updated, date_created, length) FROM stdin;
2	Red Bottom Sexy Women Stilleto 37	size	Black sexy stilleto shoe with a clean finish and suede covering.	\N	\N	8500	7999	\N	\N	blac-stilleto	\N	\N	f	0	f	0	1	2	2017-10-30 17:05:16.097507	2017-10-30 17:05:16.097539	\N
4	Red Bottom Sexy Women Stilleto 39	size	Black sexy stilleto shoe with a clean finish and suede covering.	\N	\N	8500	7999	\N	\N	blac-stilleto	\N	\N	f	0	f	0	1	2	2017-10-30 17:05:16.112364	2017-10-30 17:05:16.112375	\N
3	Red Bottom Sexy Women Stilleto 38	size		\N	\N	8500	7999	\N	\N	blac-stilleto	\N	\N	f	2	t	0	1	2	2017-10-30 17:58:09.580407	2017-10-30 17:05:16.10753	\N
5	Classy red bottom stilleto sandal white 37	Colour|Size	Available in black and white	\N	\N	12000	10999	\N	\N	Sand0001	\N	\N	f	0	f	0	1	3	2017-10-30 18:40:40.446342	2017-10-30 18:40:40.446369	\N
6	Classy red bottom stilleto sandal white 38	Colour|Size	Available in black and white	\N	\N	12000	10999	\N	\N	Sand0001	\N	\N	f	0	f	0	1	3	2017-10-30 18:40:40.457086	2017-10-30 18:40:40.457099	\N
7	Classy red bottom stilleto sandal white 39	Colour|Size	Available in black and white	\N	\N	12000	10999	\N	\N	Sand0001	\N	\N	f	0	f	0	1	3	2017-10-30 18:40:40.462057	2017-10-30 18:40:40.462068	\N
8	Classy red bottom stilleto sandal black 37	Colour|Size	Available in black and white	\N	\N	12000	10999	\N	\N	Sand0001	\N	\N	f	0	f	0	1	3	2017-10-30 18:40:40.466884	2017-10-30 18:40:40.466895	\N
10	Classy red bottom stilleto sandal black 39	Colour|Size	Available in black and white	\N	\N	12000	10999	\N	\N	Sand0001	\N	\N	f	0	f	0	1	3	2017-10-30 18:40:40.476359	2017-10-30 18:40:40.476371	\N
9	Classy red bottom stilleto sandal black 38	Colour|Size		\N	\N	12000	10999	\N	\N	Sand0001	\N	\N	f	2	t	0	1	3	2017-10-30 19:34:53.465017	2017-10-30 18:40:40.471543	\N
1	Blackview R6 5.5" fingerprint 3GB/32GB	\N	A solid smart phone guaranteed to delivery service	\N	\N	59000	49000	\N	\N	blv00001	\N	\N	t	1	f	0	1	1	2017-10-30 09:29:05.595764	2017-10-30 09:29:05.595823	\N
11	Bodycon classy midi dress -M	Size	White stripped bodycon midi dress. Available in sizes M-XXL	\N	\N	6500	4999	\N	\N	whit-midi	\N	\N	f	0	f	0	1	4	2017-10-30 22:33:13.748184	2017-10-30 22:33:13.748205	\N
13	Bodycon classy midi dress -XL	Size	White stripped bodycon midi dress. Available in sizes M-XXL	\N	\N	6500	4999	\N	\N	whit-midi	\N	\N	f	0	f	0	1	4	2017-10-30 22:33:13.764219	2017-10-30 22:33:13.764232	\N
14	Bodycon classy midi dress -XXL	Size	White stripped bodycon midi dress. Available in sizes M-XXL	\N	\N	6500	4999	\N	\N	whit-midi	\N	\N	f	0	f	0	1	4	2017-10-30 22:33:13.770462	2017-10-30 22:33:13.77048	\N
15	Bodycon classy midi dress -XXXL	Size	White stripped bodycon midi dress. Available in sizes M-XXL	\N	\N	6500	4999	\N	\N	whit-midi	\N	\N	f	0	f	0	1	4	2017-10-30 22:33:13.775614	2017-10-30 22:33:13.775624	\N
12	Bodycon classy midi dress -L	Size		\N	\N	6500	4999	\N	\N	whit-midi	\N	\N	f	2	t	0	1	4	2017-10-30 22:34:43.129747	2017-10-30 22:33:13.758919	\N
16	Elegant Formal Stilleto (Red Bottom) white 37	colour|size	An elegant formal stilleto heel, designed with a polished finish. Close deals in this heels.\r\nAvailable in white and black	\N	\N	10000	8999	\N	\N	cream-formal	\N	\N	f	0	f	0	1	5	2017-10-30 22:48:54.94763	2017-10-30 22:48:54.947853	\N
17	Elegant Formal Stilleto (Red Bottom) white 38	colour|size	An elegant formal stilleto heel, designed with a polished finish. Close deals in this heels.\r\nAvailable in white and black	\N	\N	10000	8999	\N	\N	cream-formal	\N	\N	f	0	f	0	1	5	2017-10-30 22:48:54.96672	2017-10-30 22:48:54.966738	\N
18	Elegant Formal Stilleto (Red Bottom) white 39	colour|size	An elegant formal stilleto heel, designed with a polished finish. Close deals in this heels.\r\nAvailable in white and black	\N	\N	10000	8999	\N	\N	cream-formal	\N	\N	f	0	f	0	1	5	2017-10-30 22:48:54.97351	2017-10-30 22:48:54.97352	\N
19	Elegant Formal Stilleto (Red Bottom) black 37	colour|size	An elegant formal stilleto heel, designed with a polished finish. Close deals in this heels.\r\nAvailable in white and black	\N	\N	10000	8999	\N	\N	cream-formal	\N	\N	f	0	f	0	1	5	2017-10-30 22:48:54.97981	2017-10-30 22:48:54.979824	\N
21	Elegant Formal Stilleto (Red Bottom) black 39	colour|size	An elegant formal stilleto heel, designed with a polished finish. Close deals in this heels.\r\nAvailable in white and black	\N	\N	10000	8999	\N	\N	cream-formal	\N	\N	f	0	f	0	1	5	2017-10-30 22:48:54.990719	2017-10-30 22:48:54.990739	\N
20	Elegant Formal Stilleto (Red Bottom) black 38	colour|size		\N	\N	10000	8999	\N	\N	cream-formal	\N	\N	f	1	t	0	1	5	2017-10-30 22:49:51.592436	2017-10-30 22:48:54.984989	\N
22	Patent Leather Formal Low Heel Shoe wine 36	colour|size	Patent leather low heel formal shoe suitable for all occassions	\N	\N	8300	6500	\N	\N	patent-low	\N	\N	f	0	f	0	1	6	2017-10-30 23:00:18.597534	2017-10-30 23:00:18.597561	\N
23	Patent Leather Formal Low Heel Shoe wine 37	colour|size	Patent leather low heel formal shoe suitable for all occassions	\N	\N	8300	6500	\N	\N	patent-low	\N	\N	f	0	f	0	1	6	2017-10-30 23:00:18.606737	2017-10-30 23:00:18.606754	\N
24	Patent Leather Formal Low Heel Shoe wine 38	colour|size	Patent leather low heel formal shoe suitable for all occassions	\N	\N	8300	6500	\N	\N	patent-low	\N	\N	f	0	f	0	1	6	2017-10-30 23:00:18.611786	2017-10-30 23:00:18.611798	\N
25	Patent Leather Formal Low Heel Shoe wine 39	colour|size	Patent leather low heel formal shoe suitable for all occassions	\N	\N	8300	6500	\N	\N	patent-low	\N	\N	f	0	f	0	1	6	2017-10-30 23:00:18.616841	2017-10-30 23:00:18.616852	\N
26	Patent Leather Formal Low Heel Shoe black 36	colour|size	Patent leather low heel formal shoe suitable for all occassions	\N	\N	8300	6500	\N	\N	patent-low	\N	\N	f	0	f	0	1	6	2017-10-30 23:00:18.621202	2017-10-30 23:00:18.621214	\N
28	Patent Leather Formal Low Heel Shoe black 38	colour|size	Patent leather low heel formal shoe suitable for all occassions	\N	\N	8300	6500	\N	\N	patent-low	\N	\N	f	0	f	0	1	6	2017-10-30 23:00:18.630854	2017-10-30 23:00:18.630866	\N
29	Patent Leather Formal Low Heel Shoe black 39	colour|size	Patent leather low heel formal shoe suitable for all occassions	\N	\N	8300	6500	\N	\N	patent-low	\N	\N	f	0	f	0	1	6	2017-10-30 23:00:18.635413	2017-10-30 23:00:18.635423	\N
27	Patent Leather Formal Low Heel Shoe black 37	colour|size		\N	\N	8300	6500	\N	\N	patent-low	\N	\N	f	1	t	0	1	6	2017-10-30 23:00:45.547229	2017-10-30 23:00:18.62624	\N
30	Off-Shoulder Bodycon Midi Dress -M	size	Class and elegant in every way	\N	\N	6800	5500	\N	\N	black-dress-01	\N	\N	f	0	f	0	1	7	2017-10-30 23:17:42.125866	2017-10-30 23:17:42.125897	\N
31	Off-Shoulder Bodycon Midi Dress -L	size	Class and elegant in every way	\N	\N	6800	5500	\N	\N	black-dress-01	\N	\N	f	0	f	0	1	7	2017-10-30 23:17:42.13308	2017-10-30 23:17:42.133095	\N
33	Off-Shoulder Bodycon Midi Dress -XXL	size	Class and elegant in every way	\N	\N	6800	5500	\N	\N	black-dress-01	\N	\N	f	0	f	0	1	7	2017-10-30 23:17:42.143458	2017-10-30 23:17:42.143471	\N
32	Off-Shoulder Bodycon Midi Dress -XL	size		\N	\N	6800	5500	\N	\N	black-dress-01	\N	\N	f	2	t	0	1	7	2017-10-30 23:18:10.540006	2017-10-30 23:17:42.138583	\N
35	Sexy Evening Dress Club Wear -red -M	colour|size	A stunning sexy evening dress suitable for the club or a night out with bae	\N	\N	6500	5950	\N	\N	club00001	\N	\N	f	0	f	0	1	8	2017-10-31 08:55:23.305382	2017-10-31 08:55:23.3054	\N
36	Sexy Evening Dress Club Wear -blue -M	colour|size	A stunning sexy evening dress suitable for the club or a night out with bae	\N	\N	6500	5950	\N	\N	club00001	\N	\N	f	0	f	0	1	8	2017-10-31 08:55:23.311702	2017-10-31 08:55:23.311717	\N
34	Sexy Evening Dress Club Wear -black -M	colour|size		\N	\N	6500	5950	\N	\N	club00001	\N	\N	f	8	t	0	1	8	2017-10-31 08:55:48.901089	2017-10-31 08:55:23.297625	\N
\.


--
-- Name: variant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('variant_id_seq', 36, true);


--
-- Data for Name: viewed_product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY viewed_product (id, view_count, product_id, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: viewed_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('viewed_product_id_seq', 1, false);


--
-- Data for Name: wishlist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY wishlist (id, name, description, slug, is_private, user_id, last_updated, date_created) FROM stdin;
1	General	\N	\N	f	1	2017-10-30 09:24:17.055122	2017-10-30 09:24:17.055138
\.


--
-- Data for Name: wishlist_entry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY wishlist_entry (id, product_id, wishlist_id, user_id, last_updated, date_created) FROM stdin;
\.


--
-- Name: wishlist_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('wishlist_entry_id_seq', 1, false);


--
-- Name: wishlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('wishlist_id_seq', 1, true);


--
-- Name: _user_code_uc; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT _user_code_uc UNIQUE (user_id, code);


--
-- Name: _user_order_uc; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT _user_order_uc UNIQUE (user_id, code);


--
-- Name: address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- Name: admin_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_message
    ADD CONSTRAINT admin_message_pkey PRIMARY KEY (id);


--
-- Name: admin_message_response_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_message_response
    ADD CONSTRAINT admin_message_response_pkey PRIMARY KEY (id);


--
-- Name: alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- Name: auction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auction
    ADD CONSTRAINT auction_pkey PRIMARY KEY (id);


--
-- Name: auction_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auction_request
    ADD CONSTRAINT auction_request_pkey PRIMARY KEY (id);


--
-- Name: bank_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank
    ADD CONSTRAINT bank_code_key UNIQUE (code);


--
-- Name: bank_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank
    ADD CONSTRAINT bank_pkey PRIMARY KEY (id);


--
-- Name: bid_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bid
    ADD CONSTRAINT bid_pkey PRIMARY KEY (id);


--
-- Name: cart_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart_item
    ADD CONSTRAINT cart_item_pkey PRIMARY KEY (id);


--
-- Name: cart_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart
    ADD CONSTRAINT cart_pkey PRIMARY KEY (id);


--
-- Name: category_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_code_key UNIQUE (code);


--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: city_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);


--
-- Name: conversation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY conversation
    ADD CONSTRAINT conversation_pkey PRIMARY KEY (id);


--
-- Name: conversee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY conversee
    ADD CONSTRAINT conversee_pkey PRIMARY KEY (id);


--
-- Name: country_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);


--
-- Name: coupon_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY coupon
    ADD CONSTRAINT coupon_pkey PRIMARY KEY (id);


--
-- Name: credit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY credit
    ADD CONSTRAINT credit_pkey PRIMARY KEY (id);


--
-- Name: credit_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY credit_type
    ADD CONSTRAINT credit_type_pkey PRIMARY KEY (id);


--
-- Name: currency_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY currency
    ADD CONSTRAINT currency_pkey PRIMARY KEY (id);


--
-- Name: customer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: delivery_option_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY delivery_option
    ADD CONSTRAINT delivery_option_pkey PRIMARY KEY (id);


--
-- Name: delivery_status_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY delivery_status
    ADD CONSTRAINT delivery_status_code_key UNIQUE (code);


--
-- Name: delivery_status_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY delivery_status
    ADD CONSTRAINT delivery_status_name_key UNIQUE (name);


--
-- Name: delivery_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY delivery_status
    ADD CONSTRAINT delivery_status_pkey PRIMARY KEY (id);


--
-- Name: earning_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY earning
    ADD CONSTRAINT earning_pkey PRIMARY KEY (id);


--
-- Name: email_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email
    ADD CONSTRAINT email_pkey PRIMARY KEY (id);


--
-- Name: image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_pkey PRIMARY KEY (id);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: notification_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_code_key UNIQUE (code);


--
-- Name: notification_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);


--
-- Name: order_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_entry
    ADD CONSTRAINT order_entry_pkey PRIMARY KEY (id);


--
-- Name: order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);


--
-- Name: order_status_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_status
    ADD CONSTRAINT order_status_code_key UNIQUE (code);


--
-- Name: order_status_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_status
    ADD CONSTRAINT order_status_name_key UNIQUE (name);


--
-- Name: order_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_status
    ADD CONSTRAINT order_status_pkey PRIMARY KEY (id);


--
-- Name: payment_channel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payment_channel
    ADD CONSTRAINT payment_channel_pkey PRIMARY KEY (id);


--
-- Name: payment_option_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payment_option
    ADD CONSTRAINT payment_option_pkey PRIMARY KEY (id);


--
-- Name: payment_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payment_status
    ADD CONSTRAINT payment_status_pkey PRIMARY KEY (id);


--
-- Name: product_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_detail
    ADD CONSTRAINT product_detail_pkey PRIMARY KEY (id);


--
-- Name: product_detail_type_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_detail_type
    ADD CONSTRAINT product_detail_type_code_key UNIQUE (code);


--
-- Name: product_detail_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_detail_type
    ADD CONSTRAINT product_detail_type_pkey PRIMARY KEY (id);


--
-- Name: product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: product_view_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_view
    ADD CONSTRAINT product_view_pkey PRIMARY KEY (id);


--
-- Name: recurrent_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recurrent_card
    ADD CONSTRAINT recurrent_card_pkey PRIMARY KEY (id);


--
-- Name: referral_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY referral
    ADD CONSTRAINT referral_pkey PRIMARY KEY (id);


--
-- Name: sms_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sms
    ADD CONSTRAINT sms_pkey PRIMARY KEY (id);


--
-- Name: state_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY state
    ADD CONSTRAINT state_pkey PRIMARY KEY (id);


--
-- Name: timezone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY timezone
    ADD CONSTRAINT timezone_pkey PRIMARY KEY (id);


--
-- Name: transaction_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction_entry
    ADD CONSTRAINT transaction_entry_pkey PRIMARY KEY (id);


--
-- Name: transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- Name: transaction_status_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction_status
    ADD CONSTRAINT transaction_status_code_key UNIQUE (code);


--
-- Name: transaction_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction_status
    ADD CONSTRAINT transaction_status_pkey PRIMARY KEY (id);


--
-- Name: user_alt_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_alt_email_key UNIQUE (alt_email);


--
-- Name: user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_username_key UNIQUE (username);


--
-- Name: variant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY variant
    ADD CONSTRAINT variant_pkey PRIMARY KEY (id);


--
-- Name: viewed_product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY viewed_product
    ADD CONSTRAINT viewed_product_pkey PRIMARY KEY (id);


--
-- Name: wishlist_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wishlist_entry
    ADD CONSTRAINT wishlist_entry_pkey PRIMARY KEY (id);


--
-- Name: wishlist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wishlist
    ADD CONSTRAINT wishlist_pkey PRIMARY KEY (id);


--
-- Name: ix_address_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_address_date_created ON address USING btree (date_created);


--
-- Name: ix_address_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_address_last_updated ON address USING btree (last_updated);


--
-- Name: ix_admin_message_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_admin_message_date_created ON admin_message USING btree (date_created);


--
-- Name: ix_admin_message_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_admin_message_last_updated ON admin_message USING btree (last_updated);


--
-- Name: ix_admin_message_response_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_admin_message_response_date_created ON admin_message_response USING btree (date_created);


--
-- Name: ix_admin_message_response_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_admin_message_response_last_updated ON admin_message_response USING btree (last_updated);


--
-- Name: ix_auction_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_auction_date_created ON auction USING btree (date_created);


--
-- Name: ix_auction_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_auction_last_updated ON auction USING btree (last_updated);


--
-- Name: ix_auction_request_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_auction_request_date_created ON auction_request USING btree (date_created);


--
-- Name: ix_auction_request_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_auction_request_last_updated ON auction_request USING btree (last_updated);


--
-- Name: ix_auction_request_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_auction_request_user_id ON auction_request USING btree (user_id);


--
-- Name: ix_bank_country_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_country_code ON bank USING btree (country_code);


--
-- Name: ix_bank_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_date_created ON bank USING btree (date_created);


--
-- Name: ix_bank_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bank_last_updated ON bank USING btree (last_updated);


--
-- Name: ix_bid_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bid_date_created ON bid USING btree (date_created);


--
-- Name: ix_bid_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bid_last_updated ON bid USING btree (last_updated);


--
-- Name: ix_bid_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_bid_user_id ON bid USING btree (user_id);


--
-- Name: ix_cart_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_cart_date_created ON cart USING btree (date_created);


--
-- Name: ix_cart_item_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_cart_item_date_created ON cart_item USING btree (date_created);


--
-- Name: ix_cart_item_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_cart_item_last_updated ON cart_item USING btree (last_updated);


--
-- Name: ix_cart_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_cart_last_updated ON cart USING btree (last_updated);


--
-- Name: ix_category_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_category_date_created ON category USING btree (date_created);


--
-- Name: ix_category_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_category_last_updated ON category USING btree (last_updated);


--
-- Name: ix_city_country_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_city_country_code ON city USING btree (country_code);


--
-- Name: ix_city_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_city_date_created ON city USING btree (date_created);


--
-- Name: ix_city_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_city_last_updated ON city USING btree (last_updated);


--
-- Name: ix_city_state_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_city_state_code ON city USING btree (state_code);


--
-- Name: ix_conversation_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_conversation_date_created ON conversation USING btree (date_created);


--
-- Name: ix_conversation_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_conversation_last_updated ON conversation USING btree (last_updated);


--
-- Name: ix_conversee_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_conversee_date_created ON conversee USING btree (date_created);


--
-- Name: ix_conversee_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_conversee_last_updated ON conversee USING btree (last_updated);


--
-- Name: ix_conversee_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_conversee_user_id ON conversee USING btree (user_id);


--
-- Name: ix_country_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_country_code ON country USING btree (code);


--
-- Name: ix_country_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_country_date_created ON country USING btree (date_created);


--
-- Name: ix_country_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_country_last_updated ON country USING btree (last_updated);


--
-- Name: ix_country_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_country_name ON country USING btree (name);


--
-- Name: ix_coupon_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_coupon_date_created ON coupon USING btree (date_created);


--
-- Name: ix_coupon_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_coupon_last_updated ON coupon USING btree (last_updated);


--
-- Name: ix_coupon_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_coupon_user_id ON coupon USING btree (user_id);


--
-- Name: ix_credit_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_credit_date_created ON credit USING btree (date_created);


--
-- Name: ix_credit_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_credit_last_updated ON credit USING btree (last_updated);


--
-- Name: ix_credit_type_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_credit_type_date_created ON credit_type USING btree (date_created);


--
-- Name: ix_credit_type_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_credit_type_last_updated ON credit_type USING btree (last_updated);


--
-- Name: ix_credit_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_credit_user_id ON credit USING btree (user_id);


--
-- Name: ix_currency_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_currency_code ON currency USING btree (code);


--
-- Name: ix_currency_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_currency_date_created ON currency USING btree (date_created);


--
-- Name: ix_currency_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_currency_last_updated ON currency USING btree (last_updated);


--
-- Name: ix_customer_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_date_created ON customer USING btree (date_created);


--
-- Name: ix_customer_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_customer_last_updated ON customer USING btree (last_updated);


--
-- Name: ix_delivery_option_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_delivery_option_date_created ON delivery_option USING btree (date_created);


--
-- Name: ix_delivery_option_is_enabled; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_delivery_option_is_enabled ON delivery_option USING btree (is_enabled);


--
-- Name: ix_delivery_option_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_delivery_option_last_updated ON delivery_option USING btree (last_updated);


--
-- Name: ix_delivery_status_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_delivery_status_date_created ON delivery_status USING btree (date_created);


--
-- Name: ix_delivery_status_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_delivery_status_last_updated ON delivery_status USING btree (last_updated);


--
-- Name: ix_earning_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_earning_date_created ON earning USING btree (date_created);


--
-- Name: ix_earning_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_earning_last_updated ON earning USING btree (last_updated);


--
-- Name: ix_earning_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_earning_user_id ON earning USING btree (user_id);


--
-- Name: ix_email_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_email_date_created ON email USING btree (date_created);


--
-- Name: ix_email_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_email_last_updated ON email USING btree (last_updated);


--
-- Name: ix_image_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_image_date_created ON image USING btree (date_created);


--
-- Name: ix_image_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_image_last_updated ON image USING btree (last_updated);


--
-- Name: ix_message_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_message_date_created ON message USING btree (date_created);


--
-- Name: ix_message_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_message_last_updated ON message USING btree (last_updated);


--
-- Name: ix_notification_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_notification_date_created ON notification USING btree (date_created);


--
-- Name: ix_notification_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_notification_last_updated ON notification USING btree (last_updated);


--
-- Name: ix_order_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_order_date_created ON "order" USING btree (date_created);


--
-- Name: ix_order_entry_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_order_entry_date_created ON order_entry USING btree (date_created);


--
-- Name: ix_order_entry_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_order_entry_last_updated ON order_entry USING btree (last_updated);


--
-- Name: ix_order_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_order_last_updated ON "order" USING btree (last_updated);


--
-- Name: ix_order_status_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_order_status_date_created ON order_status USING btree (date_created);


--
-- Name: ix_order_status_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_order_status_last_updated ON order_status USING btree (last_updated);


--
-- Name: ix_payment_channel_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_payment_channel_code ON payment_channel USING btree (code);


--
-- Name: ix_payment_channel_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_channel_date_created ON payment_channel USING btree (date_created);


--
-- Name: ix_payment_channel_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_channel_last_updated ON payment_channel USING btree (last_updated);


--
-- Name: ix_payment_channel_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_channel_name ON payment_channel USING btree (name);


--
-- Name: ix_payment_option_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_payment_option_code ON payment_option USING btree (code);


--
-- Name: ix_payment_option_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_option_date_created ON payment_option USING btree (date_created);


--
-- Name: ix_payment_option_is_enabled; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_option_is_enabled ON payment_option USING btree (is_enabled);


--
-- Name: ix_payment_option_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_option_last_updated ON payment_option USING btree (last_updated);


--
-- Name: ix_payment_option_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_option_name ON payment_option USING btree (name);


--
-- Name: ix_payment_option_type; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_option_type ON payment_option USING btree (type);


--
-- Name: ix_payment_status_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_payment_status_code ON payment_status USING btree (code);


--
-- Name: ix_payment_status_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_status_date_created ON payment_status USING btree (date_created);


--
-- Name: ix_payment_status_description; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_status_description ON payment_status USING btree (description);


--
-- Name: ix_payment_status_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_status_last_updated ON payment_status USING btree (last_updated);


--
-- Name: ix_payment_status_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_payment_status_name ON payment_status USING btree (name);


--
-- Name: ix_product_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_date_created ON product USING btree (date_created);


--
-- Name: ix_product_detail_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_detail_date_created ON product_detail USING btree (date_created);


--
-- Name: ix_product_detail_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_detail_last_updated ON product_detail USING btree (last_updated);


--
-- Name: ix_product_detail_type_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_detail_type_date_created ON product_detail_type USING btree (date_created);


--
-- Name: ix_product_detail_type_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_detail_type_last_updated ON product_detail_type USING btree (last_updated);


--
-- Name: ix_product_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_last_updated ON product USING btree (last_updated);


--
-- Name: ix_product_view_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_view_date_created ON product_view USING btree (date_created);


--
-- Name: ix_product_view_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_view_last_updated ON product_view USING btree (last_updated);


--
-- Name: ix_product_view_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_product_view_user_id ON product_view USING btree (user_id);


--
-- Name: ix_recurrent_card_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_recurrent_card_date_created ON recurrent_card USING btree (date_created);


--
-- Name: ix_recurrent_card_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_recurrent_card_last_updated ON recurrent_card USING btree (last_updated);


--
-- Name: ix_recurrent_card_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_recurrent_card_user_id ON recurrent_card USING btree (user_id);


--
-- Name: ix_referral_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_referral_date_created ON referral USING btree (date_created);


--
-- Name: ix_referral_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_referral_last_updated ON referral USING btree (last_updated);


--
-- Name: ix_referral_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_referral_user_id ON referral USING btree (user_id);


--
-- Name: ix_sms_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_sms_date_created ON sms USING btree (date_created);


--
-- Name: ix_sms_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_sms_last_updated ON sms USING btree (last_updated);


--
-- Name: ix_state_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_state_code ON state USING btree (code);


--
-- Name: ix_state_country_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_state_country_code ON state USING btree (country_code);


--
-- Name: ix_state_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_state_date_created ON state USING btree (date_created);


--
-- Name: ix_state_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_state_last_updated ON state USING btree (last_updated);


--
-- Name: ix_state_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_state_name ON state USING btree (name);


--
-- Name: ix_timezone_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_timezone_date_created ON timezone USING btree (date_created);


--
-- Name: ix_timezone_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_timezone_last_updated ON timezone USING btree (last_updated);


--
-- Name: ix_transaction_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_transaction_date_created ON transaction USING btree (date_created);


--
-- Name: ix_transaction_entry_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_transaction_entry_date_created ON transaction_entry USING btree (date_created);


--
-- Name: ix_transaction_entry_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_transaction_entry_last_updated ON transaction_entry USING btree (last_updated);


--
-- Name: ix_transaction_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_transaction_last_updated ON transaction USING btree (last_updated);


--
-- Name: ix_transaction_status_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_transaction_status_date_created ON transaction_status USING btree (date_created);


--
-- Name: ix_transaction_status_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_transaction_status_last_updated ON transaction_status USING btree (last_updated);


--
-- Name: ix_user_current_login_at; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_current_login_at ON "user" USING btree (current_login_at);


--
-- Name: ix_user_current_login_ip; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_current_login_ip ON "user" USING btree (current_login_ip);


--
-- Name: ix_user_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_date_created ON "user" USING btree (date_created);


--
-- Name: ix_user_last_login_at; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_last_login_at ON "user" USING btree (last_login_at);


--
-- Name: ix_user_last_login_ip; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_last_login_ip ON "user" USING btree (last_login_ip);


--
-- Name: ix_user_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_last_updated ON "user" USING btree (last_updated);


--
-- Name: ix_user_login_count; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_user_login_count ON "user" USING btree (login_count);


--
-- Name: ix_user_referral_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ix_user_referral_code ON "user" USING btree (referral_code);


--
-- Name: ix_variant_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_variant_date_created ON variant USING btree (date_created);


--
-- Name: ix_variant_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_variant_last_updated ON variant USING btree (last_updated);


--
-- Name: ix_viewed_product_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_viewed_product_date_created ON viewed_product USING btree (date_created);


--
-- Name: ix_viewed_product_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_viewed_product_last_updated ON viewed_product USING btree (last_updated);


--
-- Name: ix_viewed_product_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_viewed_product_user_id ON viewed_product USING btree (user_id);


--
-- Name: ix_wishlist_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_wishlist_date_created ON wishlist USING btree (date_created);


--
-- Name: ix_wishlist_entry_date_created; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_wishlist_entry_date_created ON wishlist_entry USING btree (date_created);


--
-- Name: ix_wishlist_entry_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_wishlist_entry_last_updated ON wishlist_entry USING btree (last_updated);


--
-- Name: ix_wishlist_entry_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_wishlist_entry_user_id ON wishlist_entry USING btree (user_id);


--
-- Name: ix_wishlist_last_updated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_wishlist_last_updated ON wishlist USING btree (last_updated);


--
-- Name: ix_wishlist_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_wishlist_user_id ON wishlist USING btree (user_id);


--
-- Name: address_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: address_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: address_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_state_id_fkey FOREIGN KEY (state_id) REFERENCES state(id);


--
-- Name: address_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: admin_message_response_admin_message_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_message_response
    ADD CONSTRAINT admin_message_response_admin_message_id_fkey FOREIGN KEY (admin_message_id) REFERENCES admin_message(id);


--
-- Name: admin_message_response_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_message_response
    ADD CONSTRAINT admin_message_response_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: admin_message_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_message
    ADD CONSTRAINT admin_message_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: auction_request_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auction_request
    ADD CONSTRAINT auction_request_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: auction_request_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auction_request
    ADD CONSTRAINT auction_request_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: auction_variant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auction
    ADD CONSTRAINT auction_variant_id_fkey FOREIGN KEY (variant_id) REFERENCES variant(id);


--
-- Name: bank_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank
    ADD CONSTRAINT bank_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: bid_auction_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bid
    ADD CONSTRAINT bid_auction_id_fkey FOREIGN KEY (auction_id) REFERENCES auction(id);


--
-- Name: bid_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bid
    ADD CONSTRAINT bid_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: cart_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart
    ADD CONSTRAINT cart_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: cart_delivery_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart
    ADD CONSTRAINT cart_delivery_option_id_fkey FOREIGN KEY (delivery_option_id) REFERENCES delivery_option(id);


--
-- Name: cart_item_cart_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart_item
    ADD CONSTRAINT cart_item_cart_id_fkey FOREIGN KEY (cart_id) REFERENCES cart(id);


--
-- Name: cart_item_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart_item
    ADD CONSTRAINT cart_item_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: cart_item_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart_item
    ADD CONSTRAINT cart_item_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: cart_item_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart_item
    ADD CONSTRAINT cart_item_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: cart_item_variant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart_item
    ADD CONSTRAINT cart_item_variant_id_fkey FOREIGN KEY (variant_id) REFERENCES variant(id);


--
-- Name: cart_payment_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart
    ADD CONSTRAINT cart_payment_option_id_fkey FOREIGN KEY (payment_option_id) REFERENCES payment_option(id);


--
-- Name: cart_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart
    ADD CONSTRAINT cart_state_id_fkey FOREIGN KEY (state_id) REFERENCES state(id);


--
-- Name: cart_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cart
    ADD CONSTRAINT cart_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: categories_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_category_id_fkey FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: categories_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: category_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES category(id);


--
-- Name: city_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: city_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_state_id_fkey FOREIGN KEY (state_id) REFERENCES state(id);


--
-- Name: conversee_conversation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY conversee
    ADD CONSTRAINT conversee_conversation_id_fkey FOREIGN KEY (conversation_id) REFERENCES conversation(id);


--
-- Name: conversee_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY conversee
    ADD CONSTRAINT conversee_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: coupon_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY coupon
    ADD CONSTRAINT coupon_category_id_fkey FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: coupon_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY coupon
    ADD CONSTRAINT coupon_order_id_fkey FOREIGN KEY (order_id) REFERENCES "order"(id);


--
-- Name: coupon_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY coupon
    ADD CONSTRAINT coupon_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: credit_credit_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY credit
    ADD CONSTRAINT credit_credit_type_id_fkey FOREIGN KEY (credit_type_id) REFERENCES credit_type(id);


--
-- Name: credit_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY credit
    ADD CONSTRAINT credit_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: customer_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: earning_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY earning
    ADD CONSTRAINT earning_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: email_notification_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email
    ADD CONSTRAINT email_notification_id_fkey FOREIGN KEY (notification_id) REFERENCES notification(id);


--
-- Name: image_auction_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_auction_id_fkey FOREIGN KEY (auction_id) REFERENCES auction(id);


--
-- Name: image_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_category_id_fkey FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: image_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: image_variant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_variant_id_fkey FOREIGN KEY (variant_id) REFERENCES variant(id);


--
-- Name: message_conversation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_conversation_id_fkey FOREIGN KEY (conversation_id) REFERENCES conversation(id);


--
-- Name: message_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE SET NULL;


--
-- Name: message_receiver_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_receiver_id_fkey FOREIGN KEY (receiver_id) REFERENCES "user"(id);


--
-- Name: message_sender_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_sender_id_fkey FOREIGN KEY (sender_id) REFERENCES "user"(id);


--
-- Name: order_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: order_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: order_delivery_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_delivery_option_id_fkey FOREIGN KEY (delivery_option_id) REFERENCES delivery_option(id);


--
-- Name: order_delivery_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_delivery_status_id_fkey FOREIGN KEY (delivery_status_id) REFERENCES delivery_status(id);


--
-- Name: order_entry_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_entry
    ADD CONSTRAINT order_entry_order_id_fkey FOREIGN KEY (order_id) REFERENCES "order"(id);


--
-- Name: order_entry_order_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_entry
    ADD CONSTRAINT order_entry_order_status_id_fkey FOREIGN KEY (order_status_id) REFERENCES order_status(id);


--
-- Name: order_entry_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_entry
    ADD CONSTRAINT order_entry_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: order_entry_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_entry
    ADD CONSTRAINT order_entry_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: order_entry_variant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_entry
    ADD CONSTRAINT order_entry_variant_id_fkey FOREIGN KEY (variant_id) REFERENCES variant(id);


--
-- Name: order_order_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_order_status_id_fkey FOREIGN KEY (order_status_id) REFERENCES order_status(id);


--
-- Name: order_payment_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_payment_option_id_fkey FOREIGN KEY (payment_option_id) REFERENCES payment_option(id);


--
-- Name: order_payment_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_payment_status_id_fkey FOREIGN KEY (payment_status_id) REFERENCES payment_status(id);


--
-- Name: order_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_state_id_fkey FOREIGN KEY (state_id) REFERENCES state(id);


--
-- Name: order_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: product_detail_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_detail
    ADD CONSTRAINT product_detail_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_detail_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_detail
    ADD CONSTRAINT product_detail_type_id_fkey FOREIGN KEY (type_id) REFERENCES product_detail_type(id);


--
-- Name: product_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: product_view_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_view
    ADD CONSTRAINT product_view_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_view_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_view
    ADD CONSTRAINT product_view_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: recurrent_card_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recurrent_card
    ADD CONSTRAINT recurrent_card_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: referral_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY referral
    ADD CONSTRAINT referral_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: sms_notification_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sms
    ADD CONSTRAINT sms_notification_id_fkey FOREIGN KEY (notification_id) REFERENCES notification(id);


--
-- Name: state_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY state
    ADD CONSTRAINT state_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: transaction_cart_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_cart_id_fkey FOREIGN KEY (cart_id) REFERENCES cart(id);


--
-- Name: transaction_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: transaction_entry_transaction_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction_entry
    ADD CONSTRAINT transaction_entry_transaction_id_fkey FOREIGN KEY (transaction_id) REFERENCES transaction(id);


--
-- Name: transaction_entry_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction_entry
    ADD CONSTRAINT transaction_entry_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: transaction_entry_variant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction_entry
    ADD CONSTRAINT transaction_entry_variant_id_fkey FOREIGN KEY (variant_id) REFERENCES variant(id);


--
-- Name: transaction_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_order_id_fkey FOREIGN KEY (order_id) REFERENCES "order"(id);


--
-- Name: transaction_payment_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_payment_option_id_fkey FOREIGN KEY (payment_option_id) REFERENCES payment_option(id);


--
-- Name: transaction_transaction_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_transaction_status_id_fkey FOREIGN KEY (transaction_status_id) REFERENCES transaction_status(id);


--
-- Name: transaction_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: variant_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY variant
    ADD CONSTRAINT variant_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: variant_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY variant
    ADD CONSTRAINT variant_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: viewed_product_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY viewed_product
    ADD CONSTRAINT viewed_product_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: viewed_product_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY viewed_product
    ADD CONSTRAINT viewed_product_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: wishlist_entry_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wishlist_entry
    ADD CONSTRAINT wishlist_entry_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: wishlist_entry_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wishlist_entry
    ADD CONSTRAINT wishlist_entry_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: wishlist_entry_wishlist_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wishlist_entry
    ADD CONSTRAINT wishlist_entry_wishlist_id_fkey FOREIGN KEY (wishlist_id) REFERENCES wishlist(id);


--
-- Name: wishlist_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wishlist
    ADD CONSTRAINT wishlist_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

