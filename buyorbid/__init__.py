# Imports
from flask import current_app as app
import os
import botocore
from elasticsearch_dsl import Search
from PIL import Image as PImage
# Retrieve the current application and expand it's properties


# Logger
logger = app.logger

# Bcrypt
bcrypt = app.bcrypt

# Database
db = app.db

# Alembic
alembic = app.alembic

# API
api = app.api

# CSS/JS Assets
assets = app.assets

# Login Manager
login_manager = app.login_manager

# Authorization Principal
principal = app.principal

# File Uploader
# uploads = app.uploads

# Celery
celery = app.celery

# Amazon s3
# s3 = app.s3

# Elasticsearch
es = app.es

elastic = Search(using=es, index=app.config.get('ES_INDEX'))

# search = app.search

# Redis
redis = app.redis

cache = app.cache

infobip = app.infobip
zohomail = app.zohomail
ses = app.ses

# sparkpost = app.sparkpost

notifier = app.notifier

# Flutterwave
flutterwave = app.flutterwave

# Uploaders
photos = app.photos
archives = app.archives

moment = app.moment

#Flask Oauth
oauth = app.oauth

# @event.listens_for(Pool, "checkout")
# def ping_connection(dbapi_connection, connection_record, connection_proxy):
#     cursor = dbapi_connection.cursor()
#     try:
#         cursor.execute("SELECT 1")
#     except:
#         # optional - dispose the whole pool
#         # instead of invalidating one at a time
#         # connection_proxy._pool.dispose()

#         # raise DisconnectionError - pool will try
#         # connecting again up to three times before raising.
#         raise exc.DisconnectionError()
#     cursor.close()


def check_image_size(src, dimensions=(200, 200), square=True):
    """
    Check's image dimensions. If square is true,
    check that the image is a square,
    else check that it matches the dimensions

    """

    img = PImage.open(src)
    width, height = img.size
    d_width, d_height = dimensions

    return True

    # if square:
    #     return int(width) == int(height)

    # else:
    #     return True


def handle_uploaded_photos(files, dimensions=(400, 400), square=True):
    """ Handles file uploads """

    uploaded_files = []
    errors = []

    for _f in files:
        try:
            filename = photos.save(_f)
            path = photos.path(filename)
            if check_image_size(path, dimensions, square):
                data = {"name": filename, "filepath": path}
                uploaded_files.append(data)
            else:
                errors.append("%s is not the right dimension" % filename)
        except Exception, e:
            logger.info(e)
            errors.append("Uploading %s is not allowed" % _f.filename)

    return uploaded_files, errors


def register_api(cls, *urls, **kwargs):
    """ A simple pass through class to add entities to the api registry """
    kwargs["endpoint"] = getattr(cls, 'resource_name', kwargs.get("endpoint", None))
    app.api_registry.append((cls, urls, kwargs))


@app.teardown_request
def shutdown_session(exception=None):
    if exception:
        db.session.rollback()
        db.session.close()
        db.session.remove()
    db.session.close()
    db.session.remove()


@app.teardown_appcontext
def teardown_db(exception=None):
    db.session.commit()
    db.session.close()
    db.session.remove()
