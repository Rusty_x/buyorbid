from buyorbid import infobip, redis, celery, app, zohomail, ses, notifier
from buyorbid.models import User
from notification.schemas import *
from notification.services.notifications import *
import ast


def check_debug(phone, email):
    """set recieving PHONE and EMAIL to dev settings when debug is true"""

    debug = app.config.get('DEBUG')
    phone = phone
    email = email

    if debug:
        phone = app.config.get('DEV_PHONE')
        email = app.config.get('DEV_EMAIL')

    return phone, email


def verify_and_send(user, code, email=None, sms=None):
    # if not user.email_verified:
    #     email = None
    if not user.phone_verified:
        sms = None

    return notifier.send(code, email, sms, email_service=EmailService, sms_service=SmsService, email_schema=EmailSchema,
                         sms_schema=SmsSchema)


@celery.task
def send_phone_verification(token, user_id, voice=False):
    # merchant = Merchant.query.get(merchant_id)
    if voice:
        token = ". ".join(token) + ". " + " " "the code again is" + ": " + ". ".join(token)

    user = User.query.get(user_id)
    phone, email = check_debug(user.phone, user.email)

    sms = dict(name=user.full_name, to=phone, voice=voice, payload=dict(token=token))
    res = verify_and_send(user, code='msv', sms=sms)

    print res


@celery.task
def send_user_notification(user_id, notification_code, subject=None, **kwargs):
    # merchant = Merchant.query.get(merchant_id)
    user = User.query.get(user_id)
    phone, email = check_debug(user.phone, user.email)

    email = dict(name=user.full_name, to=email, subject=subject, payload=dict(**kwargs))
    sms = dict(name=user.full_name, to=phone, payload=dict(**kwargs))
    res = verify_and_send(user, code=notification_code, sms=sms, email=email)

    print res


@celery.task
def send_notification(notification_code, subject=None, **kwargs):
    """send general notification"""

    to_email = kwargs.get('email', None)
    to_phone = kwargs.get('phone', None)
    name = kwargs.get('name', " ")

    email = dict(name=name, to=to_email, subject=subject, payload=dict(**kwargs)) if to_email else None
    sms = dict(name=name, to=to_phone, payload=dict(**kwargs)) if to_phone else None
    return notifier.send(notification_code, email, sms, email_service=EmailService, sms_service=SmsService, email_schema=EmailSchema,
                         sms_schema=SmsSchema)
