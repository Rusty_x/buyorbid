"""
forms.py

@Author: kunsam002 & rusty
@Date: jan 26, 2017
@Time: 3:24 PM

Form module for all wtforms used within the project

"""
from flask_wtf.form import FlaskForm
from wtforms.fields.core import RadioField
from wtforms.fields.simple import FileField

from ext.base.exceptions import ValidationFailed
from wtforms import StringField, PasswordField, BooleanField, ValidationError, SelectField, \
    FloatField, IntegerField, widgets, Field, HiddenField, FormField, SelectMultipleField, TextAreaField, RadioField

from flask_wtf import Form
from wtforms.ext.dateutil.fields import DateField, DateTimeField
from buyorbid.models import *
from wtforms.validators import Optional, DataRequired, Email, EqualTo, NumberRange
from sqlalchemy import or_, and_, func


def fetch_states():
    return State.query.filter(State.country_code == "NG").order_by(asc(State.name)).all()


def fetch_countries():
    return Country.query.filter(func.lower(Country.name) == "nigeria").order_by(asc(Country.name)).all()


class RequiredIf(DataRequired):
    # a validator which makes a field required if
    # another field is set and has a truthy value

    def __init__(self, other_field_name, *args, **kwargs):
        self.other_field_name = other_field_name
        super(RequiredIf, self).__init__(*args, **kwargs)

    def __call__(self, form, field):
        other_field = form._fields.get(self.other_field_name)
        if other_field is None:
            raise Exception('no field named "%s" in form' % self.other_field_name)
        if bool(other_field.data):
            super(RequiredIf, self).__call__(form, field)


class RequiredOption(DataRequired):
    # a validator which makes a field required if
    # another field is set and has a truthy value

    def __init__(self, other_field_name, option_value, *args, **kwargs):
        self.other_field_name = other_field_name
        self.option_value = option_value
        super(RequiredOption, self).__init__(*args, **kwargs)

    def __call__(self, form, field):
        other_field = form._fields.get(self.other_field_name)
        if other_field is None:
            raise Exception('no field named "%s" in form' % self.other_field_name)
        if bool(other_field.data == self.option_value):
            super(RequiredOption, self).__call__(form, field)


class IntegerListField(Field):
    """ Custom field to support sending in multiple integers separated by commas and returning the content as a list """

    widget = widgets.TextInput()

    def __init__(self, label='', validators=None, **kwargs):
        super(IntegerListField, self).__init__(label, validators, **kwargs)

    def _value(self):
        if self.data:
            string_data = [str(y) for y in self.data]
            return u','.join(string_data)

        else:
            return u''

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = [int(x.strip()) for x in valuelist[0].split(',')]
        else:
            self.data = []

    def process_data(self, valuelist):
        if valuelist:
            self.data = [int(x.strip()) for x in valuelist[0].split(',')]
        else:
            self.data = []


def check_chars(input):
    """ Checks if there's a special character in the text """

    chars = """ '"!#$%^&*()+=]}[{|\':;?/>,<\r\n\t """
    return any((c in chars) for c in input)

class ContactForm(FlaskForm):
    full_name = StringField('Full Name', validators=[DataRequired()])
    subject = StringField('Subject', validators=[DataRequired()])
    email = StringField('Email Address', validators=[DataRequired(), Email()])
    phone = StringField('Phone Number', validators=[DataRequired()])
    body = StringField('Message', validators=[DataRequired()], widget=widgets.TextArea())


class LoginForm(FlaskForm):
    username = StringField('Username / Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember me', validators=[Optional()], description="Remember me")

class ForgotPasswordForm(FlaskForm):
    username = StringField('Username or Email Address', validators=[DataRequired()])

class UserResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    verify_password = PasswordField('Verify Password',
                                    validators=[DataRequired(), EqualTo("password", "Passwords are not equal")])


class SignUpForm(FlaskForm):
    name = StringField('Courier Name', validators=[DataRequired()])
    subdomain = StringField('Sub domain', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Admin address', validators=[DataRequired(), Email()])
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    verify_password = PasswordField('Verify Password', validators=[DataRequired(), EqualTo('password')])

    def validate_username(self, field):
        if User.query.filter(User.username == field.data).count() != 0:
            raise ValidationError("This username is already in use")

        if check_chars(field.data):
            raise ValidationError("Only letters and numbers, no spaces or special characters allowed")

    def validate_email(self, field):
        if User.query.filter(User.email == field.data).count() != 0:
            raise ValidationError("This email address is already in use")


class AccountSignUpForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired()])
    referrer = StringField('Friend\'s Referral Code', validators=[Optional()])
    method_of_reach = SelectField('How Did We Fall On You?', validators=[Optional()], coerce=str, choices=[("","--- Select One ---"),("website","Direct Website"),("friend","Friend Referral"),("search_engine","Search Engine"),("social_media","Social Media")])
    phone = StringField('Phone Number', validators=[Optional()])
    email = StringField('Email address', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    verify_password = PasswordField('Verify Password', validators=[DataRequired(), EqualTo('password')])

    def validate_email(self, field):
        if User.query.filter(or_(func.lower(User.username) == field.data.lower(),
                                 func.lower(User.email) == field.data.lower())).count() != 0:
            raise ValidationError("This username is already in use")

        if check_chars(field.data):
            raise ValidationError("Only letters and numbers, no spaces or special characters allowed")

    def validate_username(self, field):
        if User.query.filter(or_(func.lower(User.username) == field.data.lower(),
                                 func.lower(User.email) == field.data.lower())).count() != 0:
            raise ValidationError("This username is already in use")

    def validate_phone(self, field):
        if User.query.filter(or_(func.lower(User.phone) == field.data.lower())).count() != 0:
            raise ValidationError("This phone number is already in use")


class ProductForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], description="The name of your product")
    # handle = StringField('Url handle', validators = [Required()])
    # title = StringField('Title', validators = [Optional()])
    regular_price = FloatField('Standard Price', validators=[Optional()], default=0.0,
                               description="Enter a numeric value. No commas allowed")
    sale_price = FloatField('Discount Price', default=0.0, validators=[Optional()],
                            description="Enter the new discounted price. No commas allowed")
    affiliate_price = FloatField('Affiliate Price', default=0.0, validators=[Optional()], description="Enter the affiliate price. No commas allowed")
    description = StringField('Product Details', validators=[Optional()], widget=widgets.TextArea(),
                              description="A detailed description of this product")
    has_variants = BooleanField("Does This Product Have Variants?", default=False,
                                description="Variants are based on attributes like colours, sizes, patterns etc.")
    variant_attributes = TextAreaField("Variant Attributes", validators=[Optional()],
                                       description="Enter variant attributes and values as shown")

    caption = StringField('Short Description', validators=[Optional()], widget=widgets.TextArea(),
                          description="A short description of your product")
    sku = StringField('SKU', validators=[Optional()], description="An internal identification number for this product")
    # group_id = SelectField('Category', coerce=int, validators=[DataRequired()])
    # track_stock_level = BooleanField('This product has variants (use this to specify different sizes, color etc.)',
    #                                  validators=[Optional()])
    quantity = FloatField('Quantity', validators=[Optional()], default=0,
                          description="Enter a numeric value. No commas allowed")
    weight = FloatField('Weight (kg)', validators=[Optional()])
    # require_shipping = BooleanField('Require Shipping', validators = [Optional()])
    is_featured = BooleanField('Feature this product on my home page', validators=[Optional()], default=True)
    is_in_house = BooleanField('Is In-House', validators=[Optional()], default=False, description="Is Product Sold by In-House Staff?")
    visibility = BooleanField('Display this product in my store', validators=[Optional()], default=True)
    category_id = SelectField('Directory Category', coerce=int, validators=[Optional()])
    categories = StringField('Product Categories', validators=[Optional()])
    # section_id = SelectField('Directory Section', coerce=int, validators=[Optional()])
    show_price = BooleanField('Display Product Price', default=True,
                              description="Choose to display the price of the product.")
    is_private = BooleanField('Make Product Private', default=False,
                              description="Choose to display the product to specific set of customers.")
    images = FileField("files[]")
    set_cover_image = BooleanField('Set as Cover', default=False,
                              description="Choose to set a new image as cover image.")

    def validate_sale_price(form, field):
        if field.data > form.regular_price.data:
            raise ValidationError("Discount cannot be more than price. Please adjust the value")

class ImageUploadForm(FlaskForm):
    images = FileField("files[]")

class ProductDetailForm(FlaskForm):
    t_name = StringField('Table Name', validators=[Optional()], default="Quick Specs", description="The name the detail")
    t_content = TextAreaField('Table Content', validators=[Optional()], description="The detail content")
    t_type_id = IntegerField('Table Type', validators=[DataRequired()], default=4, description="The detail type")
    t_position = IntegerField("Table Position", validators=[DataRequired()], default=1, description="In what order should this detail show")
    tx_name = StringField('Text Name', validators=[Optional()], description="The name the detail")
    tx_content = TextAreaField('Text Content', validators=[Optional()], description="The detail content")
    tx_type_id = IntegerField('Text Type', validators=[DataRequired()], default=1, description="The detail type")
    tx_position = IntegerField("Text Position", validators=[Optional()], default=2, description="In what order should this detail show")
    ps_name = StringField('Photos Name', validators=[Optional()], description="The name the detail")
    ps_content = TextAreaField('Photos Content', validators=[Optional()], description="The detail content")
    ps_type_id = IntegerField('Photos Type', validators=[DataRequired()], default=2, description="The detail type")
    ps_position = IntegerField("Photos Position", validators=[Optional()], default=3, description="In what order should this detail show")
    v_name = StringField('Video Name', validators=[Optional()], description="The name the detail")
    v_content = TextAreaField('Video Photos Content', validators=[Optional()], description="The detail content")
    v_type_id = IntegerField('Video Photos Type', validators=[DataRequired()], default=3, description="The detail type")
    v_position = IntegerField("Video Position", validators=[Optional()], default=4, description="In what order should this detail show")


class VariaintForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], description="The name of your product")
    # handle = StringField('Url handle', validators = [Required()])
    # title = StringField('Title', validators = [Optional()])
    regular_price = FloatField('Standard Price', validators=[Optional()], default=0.0,
                               description="Enter a numeric value. No commas allowed")
    sale_price = FloatField('Discount Price', default=0.0, validators=[Optional()], description="Enter the new discounted price. No commas allowed")
    affiliate_price = FloatField('Affiliate Price', default=0.0, validators=[Optional()], description="Enter the affiliate price. No commas allowed")
    description = TextAreaField('Product Details', validators=[Optional()],
                                description="A detailed description of this product")
    caption = StringField('Short Description', validators=[Optional()], widget=widgets.TextArea(),
                          description="A short description of your product")
    sku = StringField('SKU', validators=[Optional()], description="An internal identification number for this product")
    # group_id = SelectField('Category', coerce=int, validators=[DataRequired()])
    # track_stock_level = BooleanField('This product has variants (use this to specify different sizes, color etc.)',
    #                                  validators=[Optional()])
    quantity = FloatField('Quantity', validators=[Optional()], default=0,
                          description="Enter a numeric value. No commas allowed")
    weight = FloatField('Weight (kg)', validators=[Optional()])
    # require_shipping = BooleanField('Require Shipping', validators = [Optional()])
    is_featured = BooleanField('Feature this product on my home page', validators=[Optional()], default=True)
    availability = BooleanField('Available', validators=[Optional()], default=True,
                                description='Is this variant available?')
    visibility = BooleanField('Display this product in my store', validators=[Optional()], default=True)
    # category_id = SelectField('Directory Category', coerce=int, validators=[Optional()])
    show_price = BooleanField('Display Product Price', default=True,
                              description="Choose to display the price of the product.")
    is_private = BooleanField('Make Product Private', default=False,
                              description="Choose to display the product to specific set of customers.")

    def validate_sale_price(form, field):
        if field.data > form.regular_price.data:
            raise ValidationError("Discount cannot be more than price. Please adjust the value")


class CategoryForm(FlaskForm):
    name = StringField('Name', [DataRequired("Name your category")])
    description = TextAreaField('Description', validators=[Optional()],
                                description="A detailed description of this category")
    parent_id = SelectField('Parent', [Optional()], coerce=int)
    visibility = BooleanField('Visibility', [Optional()])
    images = FileField("files[]")


class CartForm(FlaskForm):
    variant_ids = RadioField('Product', [DataRequired("Please Select a Variant")], coerce=int)
    variant_id = SelectField('Product', [DataRequired("Please Select a Variant")], coerce=int)
    quantity = IntegerField('Quantity', [DataRequired()], default=1)

    def validate_quantity(form, field):
        variant = Variant.query.get(form.variant_id.data)

        if not variant:
            raise ValidationError("This item doesn't exist")  # ################################

        if field.data > variant.quantity:
            raise ValidationError("Sorry, there are only %d of this item available" % variant.quantity)

class AdminMessageReplyForm(Form):
    body = StringField('Reply Message', validators=[DataRequired()], widget=widgets.TextArea())


class CheckoutForm(FlaskForm):
    first_name = StringField('First Name', [DataRequired('Please Enter your First Name')])
    last_name = StringField('Last Name', [DataRequired('Please Enter your Last Name')])
    email = StringField('Email Address', [DataRequired('Provide a contact E-mail address'), Email()])
    phone = StringField('Phone', [DataRequired('Provide a Phone Number we can reach you on')])
    address_line1 = StringField('Address (Line 1)', [DataRequired('Tell us where to deliver your item(s)')])
    address_line2 = StringField('Address (Line 2)')
    house_number = StringField('House/Apt. Num', [DataRequired()])
    city = StringField('City')
    form_refresh = StringField('Form Refresh')
    state_code = SelectField('State', [DataRequired()], coerce=int)
    country_code = SelectField('Country', [DataRequired()], coerce=int)
    total = FloatField('total', [Optional()])
    prev_coupon = FloatField('pc', [Optional()], default=0)
    prev_delivery = FloatField('pd', [Optional()], default=0)
    payment_option_ids = RadioField('Payment Method', [Optional("Please Select a Payment Method")], coerce=int)
    payment_option_id = SelectField('Payment Method', [DataRequired("Please Select a Payment Method")], coerce=int)
    delivery_option_codes = RadioField('Delivery Method', [Optional("Please Select a Delivery Method")], coerce=int)
    delivery_option_code = SelectField('Delivery Method', [Optional("Please Select a Delivery Method")], coerce=int)
    bob_coupon = SelectField('BOB Coupon', [Optional("Please Select a coupon")], coerce=int)
    auto_set_shipment = BooleanField('Set Shipping Address as Billing Address', default=True)
    quick_signup_success = StringField('quick_signup_success')
    shipment_name = StringField('Name')
    shipment_phone = StringField('Shipping Phone')
    shipment_address = StringField('Address')
    shipment_city = StringField('Shipping City')
    shipment_description = TextAreaField('Additional Information')

    # card information
    card_number = StringField("Card Number", [Optional()])
    card_expiry = StringField("Card Expiry", [Optional()])
    expiry_month = IntegerField("Month", [Optional()])
    expiry_year = IntegerField("Year", [Optional()])
    card_ids = RadioField('Select Card', [Optional("Please Select a Card for Payment")], coerce=int)
    card_id = SelectField('Select Card', [Optional("Please Select a Card for Payment")], coerce=int)


    cvv = StringField("CVV")
    save_card = BooleanField("Save this card")

    password = PasswordField('Password', validators=[Optional()])
    verify_password = PasswordField('Verify Password', validators=[Optional(), EqualTo('password')])


    def validate_email(self, field):

        if check_chars(field.data):
            raise ValidationError("Only letters and numbers, no spaces or special characters allowed")


# shipment_state = SelectField('Shipment State', coerce=str)

    def validate_state_id(form, field):
        if field.data == 0:
            raise ValidationError("Please select a valid state")

    def validate_card_expiry(form, field):
        if field.data:
            exp = field.data
            exp = exp.replace(" ", "").split("/")
            try:
                form.expiry_month.data = exp[0]
                form.expiry_year.data = exp[1]
            except:
                raise ValidationError("Please provide proper card expiry")

class AuctionForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], description="The name of your product")
    auction_information = StringField('Auction Information', validators=[Optional()], widget=widgets.TextArea(),
                                      description="A detailed description of this product. Please ensure all Video Embeded codes are of width and height 100%.")
    other_information = StringField('Terms & Condition', validators=[Optional()], widget=widgets.TextArea(),
                                    description="A short description of your product. Please ensure all Video Embeded codes are of width and height 100%.")
    on_bid = BooleanField("Activate Bid", validators=[Optional()], default=False)
    bid_start = DateTimeField("Bid Start Date", format="%m/%d/%Y", validators=[DataRequired()],
                              default=datetime.today())
    bid_end = DateTimeField("Bid End Date", format="%m/%d/%Y", default=datetime.today() + timedelta(days=1),
                            validators=[DataRequired()])
    starting_bid = FloatField('Starting Bid', validators=[DataRequired()], default=0.0,
                              description="Enter Starting Bid")
    value = FloatField('Value', validators=[DataRequired()], default=0.0, description="Value")
    bid_cost = FloatField('Bid Cost', validators=[Optional()], description="Bid Cost")
    bid_increment = FloatField('Bid Increment', validators=[DataRequired()], default=0.0, description="Bid Increment")

    def validate_bid_end(form, field):
        if form.bid_start.data > field.data:
            raise ValidationError("End date cannot be older than the start date")


class BidForm(FlaskForm):
    user_id = IntegerField("User", validators=[Optional()], description="User")
    auction_id = IntegerField("Auction", validators=[Optional()], description="Auction")
    price = FloatField('Bid Price', validators=[Optional()], default=0.0, description="Enter Bid Amount")

    # def validate_price(form, field):
    #     auction = Auction.query.get(form.auction_id.data)
    #     min_price = auction.min_price
    #     if min_price > field.data:
    #         raise ValidationError("Minimum allowed Bid amount is %s" % min_price)


class UpdateAuctionForm(FlaskForm):
    removables = SelectMultipleField(validators=[Optional()], coerce=int)


class AuctionRequestForm(FlaskForm):
    user_id = IntegerField("User", validators=[Optional()], description="User")
    product_id = IntegerField("Product", validators=[Optional()], description="Product")


class CreditTypeForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()], description="The name of the Type of Credit")
    code = StringField('Code', validators=[DataRequired()], widget=widgets.TextArea(),
                       description="Unique code for the type of credit")


class CreditForm(FlaskForm):
    amount = FloatField('Amount', validators=[Optional()], description="Amount")
    price = FloatField('Price', validators=[Optional()], description="Price")
    is_awarded = BooleanField("Awarded Credit", validators=[Optional()], default=False)
    is_purchased = BooleanField("Purchased Credit", validators=[Optional()], default=False)
    is_credit = BooleanField("Account Credit", validators=[DataRequired()], default=False)
    credit_type_id = SelectField("Credit Type", validators=[DataRequired()], coerce=int,
                                 choices=[(0, "--- Select One ---")] + [(i.id, i.name) for i in CreditType.query.all()])

    def validate_state_id(form, field):
        if field.data == 0:
            raise ValidationError("Please select a valid state")


class NotificationForm(FlaskForm):
    name = StringField("Name", [DataRequired()])
    email = TextAreaField("Email Template")
    sms = TextAreaField("SMS Template")
    push = TextAreaField("Push Template")
    code = StringField("Code", [DataRequired("Provide a unique notification code")])


class AskForm(FlaskForm):
    product_id = IntegerField("Product", validators=[Optional()], description="Product")
    text = TextAreaField("Product Question", validators=[DataRequired()], description="The question you want to ask",
                         default="Hi there, could you kindly share your experience with this product")
    sender_id = IntegerField("Sender", validators=[DataRequired()], description="sender")
    receiver_id = IntegerField("Recipient", validators=[DataRequired()], description="recipient")

class ReplyForm(Form):
    text = StringField('Reply Message', validators=[DataRequired()], widget=widgets.TextArea())
    sender_id = IntegerField("Sender", validators=[DataRequired()], description="sender")
    receiver_id = IntegerField("Recipient", validators=[DataRequired()], description="recipient")

class ResponseForm(Form):
    body = StringField('Reply Message', validators=[DataRequired()], widget=widgets.TextArea())

class ProfileUpdateForm(Form):
    username = StringField('UserName', validators=[Optional()])
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    phone = StringField('Phone', validators=[DataRequired()])


class ChangePasswordForm(Form):
    old_password = PasswordField('Old Password', validators=[DataRequired()])
    new_password = PasswordField('New Password', validators=[DataRequired()])
    verify_password = PasswordField('Verify New Password', validators=[DataRequired(), EqualTo('new_password')],
                                    description="Verify your password")

    def validate_new_password(form, field):
        if field.data == form.old_password.data:
            raise ValidationError("Password cannot be changed to currently existing password.")

class OrderUpdateForm(FlaskForm):
    order_id = IntegerField('Order ID', validators=[DataRequired()], description="The id of the order")
    order_status_id = IntegerField('Order Status', validators=[DataRequired()], description="The status of the order currently")



class CartItemForm(FlaskForm):
    id = IntegerField('Item', validators=[DataRequired()])
    variant_id = IntegerField('Variant', validators=[DataRequired()])
    quantity = IntegerField('Quantity', default=0, validators=[Optional()])

    def validate_quantity(form, field):
        variant = Variant.query.get(form.variant_id.data)

        if not variant:
            raise ValidationError("This item doesn't exist")

        if field.data > variant.quantity:
            raise ValidationError("Sorry, there are only %d of this item available" % variant.quantity)

        if not variant.price:
            raise ValidationError("Sorry, item with invalid price cannot be added to cart")


class BlankForm(Form): pass