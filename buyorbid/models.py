"""
models.py

@Author: kunsam002 & rusty
@Date: 26 Jan, 2017
@Time: 3:24 PM

"""
import hashlib
import inspect as pyinspect
import json
import pprint
import sys
from datetime import datetime, timedelta

from flask.helpers import url_for
from sqlalchemy import func, event
from sqlalchemy import inspect, UniqueConstraint, desc, asc
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property, Comparator
from sqlalchemy.orm import dynamic
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.orm.collections import InstrumentedList
from ext.base.search import index_object, delete_indexed_object, generate_mapping
from ext.base.utils import slugify, convert_dict, id_generator, token_generator, whole_number_format
from socket import gethostname, gethostbyname
from buyorbid import db, logger, bcrypt, app


def get_model_from_table_name(tablename):
    """ return the Model class for a given __tablename__ """

    _models = [args[1] for args in globals().items() if pyinspect.isclass(args[
                                                                              1]) and issubclass(args[1], db.Model)]

    for _m in _models:
        try:
            if _m.__tablename__ == tablename:
                return _m
        except Exception, e:
            logger.info(e)
            raise

    return None


def slugify_from_name(context):
    """
    An sqlalchemy processor that works with default and on update
    field parameters to automatically slugify the name parameters in the model
    """
    return slugify(context.current_parameters['name'])


def generate_token_code(context):
    return hashlib.md5(
        "%s:%s:%s" % (
            context.current_parameters["user_id"], context.current_parameters["email"], datetime.utcnow())).hexdigest()


def generate_email_hash(context):
    return hashlib.md5("%s:%s" % (context.current_parameters["email"], datetime.utcnow())).hexdigest()


class AppMixin(object):
    """ Mixin class for general attributes and functions """

    @property
    def pk(self):
        """ generic way to retrieve the identity of a model object """
        pk_name = inspect(self.__class__).primary_key[0].name
        return getattr(self, pk_name)

    @classmethod
    def primary_key(cls):
        """ generic way to retrieve the identity of a model object """
        pk_name = inspect(cls).primary_key[0].name
        return getattr(cls, pk_name)

    @declared_attr
    def date_created(cls):
        return db.Column(db.DateTime, default=datetime.utcnow, index=True)

    @declared_attr
    def last_updated(cls):
        return db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow, index=True)

    def as_dict(self, include_only=None, exclude=["is_deleted"], extras=[], child=None, child_include=[]):
        """ Retrieve all values of this model as a dictionary """
        data = inspect(self)

        if include_only is None:
            include_only = data.attrs.keys() + extras

        else:
            include_only = include_only + extras + ["id", "last_updated", "date_created"]

        _dict = dict([(k, getattr(self, k)) for k in include_only if isinstance(getattr(self, k),
                                                                                (hybrid_property, InstrumentedAttribute,
                                                                                 InstrumentedList,
                                                                                 dynamic.AppenderMixin)) is False and k not in exclude])

        for key, obj in _dict.items():
            is_list_int = False
            if isinstance(obj, db.Model):
                _dict[key] = obj.as_dict()

            if isinstance(obj, datetime):
                _dict[key] = unicode(obj).replace(" ", "T")

            if isinstance(obj, (list, tuple)):
                items = []
                print key, obj, "the obj"
                for item in obj:
                    print item, "the item"
                    if type(item) in [int, str]:
                        items.append(item)
                        is_list_int = True
                    else:
                        inspect_item = inspect(item)
                        items.append(
                            dict([(k, getattr(item, k)) for k in inspect_item.attrs.keys() + extras if
                                  k not in exclude and hasattr(item, k)]))
                if is_list_int:
                    _dict[key] = items
                    continue
                for item in items:
                    obj = item.get(child)
                    if obj:
                        item[child] = obj.as_dict(extras=child_include)
        return _dict

    def level_dict(self, include_only=None, exclude=["is_deleted"], extras=[], child=None, child_include=[]):
        data = self.as_dict(include_only=include_only, exclude=exclude, extras=extras, child=child,
                            child_include=child_include)
        for key, value in data.items():
            if type(data.get(key)) == dict:
                data.pop(key)
        return data

    def as_json_dict(self, include_only=None, exclude=["is_deleted"], extras=[], child=None, child_include=[],
                     indent=None):
        data = self.level_dict(include_only=include_only, exclude=exclude, extras=extras, child=child,
                               child_include=child_include)
        data = convert_dict(data, indent=indent)
        return json.loads(data)

    def as_full_json_dict(self, include_only=None, exclude=["is_deleted"], extras=[], child=None, child_include=[],
                          indent=None):
        data = self.as_dict(include_only=include_only, exclude=exclude, extras=extras, child=child,
                            child_include=child_include)
        data = convert_dict(data, indent=indent)
        return json.loads(data)


class UserMixin(AppMixin):
    """ Mixin class for Shop related attributes and functions """

    @declared_attr
    def user_id(cls):
        return db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)

    @declared_attr
    def user(cls):
        return db.relationship("User", foreign_keys=cls.user_id)

class PasswordResetToken(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=True)
    email = db.Column(db.String(200), nullable=False)
    code = db.Column(db.String(200), unique=False, default=generate_token_code)
    is_expired = db.Column(db.Boolean, default=False)


class Bank(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), unique=True, nullable=False)
    name = db.Column(db.String(200), nullable=False)
    branch = db.Column(db.String(200), unique=False, nullable=False)
    country_code = db.Column(db.String(200), nullable=False, index=True)
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False)


class Country(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["code", "name", "slug", "phone_code", "enabled"]

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=False, index=True, unique=True)
    name = db.Column(db.String(200), nullable=False, index=True, unique=True)
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name)
    phone_code = db.Column(db.String)
    enabled = db.Column(db.Boolean, default=False)

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<Country %r>' % self.name


class State(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["code", "name", "slug", "country_id", "country"]
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=False, index=True, unique=True)
    name = db.Column(db.String(200), index=True)
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name)
    delivery_fee = db.Column(db.Float, default=0.0)
    country_code = db.Column(db.String(200), nullable=False, index=True)
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False)
    country = db.relationship("Country", foreign_keys="State.country_id")

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<State %r>' % self.name


class City(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "zipcode", "slug", "state_id", "state", "country_id", "country"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    zipcode = db.Column(db.String(200))
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name)
    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=False)
    state = db.relationship("State", foreign_keys="City.state_id")
    state_code = db.Column(db.String(200), nullable=False, index=True)
    country_code = db.Column(db.String(200), nullable=False, index=True)
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False)
    country = db.relationship("Country", foreign_keys="City.country_id")

    # __table_args__ = (
    #     UniqueConstraint("slug", "state_code", "country_code"),
    # )

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<State %r>' % self.name


class Currency(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["code", "name", "enabled", "symbol", "payment_code"]
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=False, index=True, unique=True)
    name = db.Column(db.String(200))
    enabled = db.Column(db.Boolean, default=False)
    symbol = db.Column(db.String(200))
    payment_code = db.Column(db.String(200))

    def __unicode__(self):
        return "%s (%s)" % (self.name.title(), self.code)

    def __repr__(self):
        return '<Currency %r>' % self.name


class Address(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    phone = db.Column(db.String(200), nullable=True)
    street = db.Column(db.Text, nullable=True)
    city = db.Column(db.String(200), nullable=True)
    lat = db.Column(db.String(200), nullable=True)
    lng = db.Column(db.String(200), nullable=True)
    zipcode = db.Column(db.Text, nullable=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)

    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=False)
    state = db.relationship("State", foreign_keys="Address.state_id")

    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False)
    country = db.relationship("Country", foreign_keys="Address.country_id")


class Timezone(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    code = db.Column(db.String(200))
    offset = db.Column(db.String(200))  # UTC time

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<Timezone %r>' % self.name


class PaymentChannel(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code"]

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=False, index=True, unique=True)
    name = db.Column(db.String(200), index=True)

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<PaymentChannel %r>' % self.name


class PaymentOption(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code", "type"]
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=False, index=True, unique=True)
    name = db.Column(db.String(200), index=True)
    type = db.Column(db.String(50), index=True)
    is_enabled = db.Column(db.Boolean, default=True, index=True)
    carts = db.relationship("Cart", backref="payment_option", lazy="dynamic")

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<PaymentOption %r>' % self.name


class PaymentStatus(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code", "description"]
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=False, index=True, unique=True)
    name = db.Column(db.String(200), index=True)
    description = db.Column(db.String(200), index=True, nullable=True)

    def __unicode__(self):
        return self.name


class DeliveryOption(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code", "description"]

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(50))
    name = db.Column(db.String(200))
    description = db.Column(db.String(200), nullable=True)
    is_enabled = db.Column(db.Boolean, default=True, index=True)
    carts = db.relationship('Cart', backref="delivery_option", lazy='dynamic', cascade="all,delete-orphan")
    orders = db.relationship('Order', backref="delivery_option", lazy='dynamic')
    carts = db.relationship("Cart", backref="delivery_option", lazy="dynamic")

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<DeliveryOption %r>' % self.name


# class BankAccount(AppMixin, db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(200), nullable=False)
#     account_number = db.Column(db.String(200), nullable=False)
#     bvn = db.Column(db.String(200), nullable=True)
#     token = db.Column(db.String(200), nullable=True)
#
#     bank_code = db.Column(db.Integer, db.ForeignKey('bank.code'), nullable=False)
#     bank = db.relationship("Bank", foreign_keys="BankAccount.bank_code")
#
#     courier_id = db.Column(db.Integer, db.ForeignKey('courier.id', ondelete="cascade"), nullable=True)
#     merchant_id = db.Column(db.Integer, db.ForeignKey('merchant.id', ondelete="cascade"), nullable=True)


class RecurrentCard(UserMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["token", "mask", "brand", "exp_month", "exp_year"]

    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(200), nullable=False)
    mask = db.Column(db.String(300), nullable=True)
    brand = db.Column(db.String(300), nullable=True)
    exp_month = db.Column(db.Integer, nullable=True)
    exp_year = db.Column(db.Integer, nullable=True)


class User(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["username", "email", "alt_email", "first_name", "last_name", "gender", "phone", "alt_phone",
                            "date_of_birth", "is_enabled", "is_verified", "is_staff", "is_admin", "login_count",
                            "last_login_at", "current_login_at", "last_login_ip", "current_login_ip", "awarded_credit",
                            "purchased_credit", "user_earning", "referral_code", "profile_pic", "full_name",
                            "user_credit", "wishlist_product_ids", "referral_count", "available_coupon_count",
                            "available_coupon_worth"]

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), unique=True)
    email = db.Column(db.String(200), unique=True)
    alt_email = db.Column(db.String(200), unique=True)
    first_name = db.Column(db.String(200), unique=False, nullable=False)
    last_name = db.Column(db.String(200), unique=False)
    gender = db.Column(db.String(200), nullable=True)  # Male or Female
    phone = db.Column(db.String(200), nullable=True)
    alt_phone = db.Column(db.String(200), nullable=True)
    date_of_birth = db.Column(db.Date, nullable=True)
    password = db.Column(db.Text, unique=False)
    is_enabled = db.Column(db.Boolean, default=False)
    is_verified = db.Column(db.Boolean, default=False)
    phone_verified = db.Column(db.Boolean, default=False)
    email_verified = db.Column(db.Boolean, default=False)
    is_staff = db.Column(db.Boolean, default=False)
    is_admin = db.Column(db.Boolean, default=False)
    login_count = db.Column(db.Integer, default=0, index=True)
    last_login_at = db.Column(db.DateTime, index=True)
    current_login_at = db.Column(db.DateTime, index=True)
    last_login_ip = db.Column(db.String(200), index=True)
    current_login_ip = db.Column(db.String(200), index=True)
    awarded_credit = db.Column(db.Integer, default=0)
    purchased_credit = db.Column(db.Integer, default=0)
    user_earning = db.Column(db.Float, default=0.0)
    wishlists = db.relationship("Wishlist", cascade="all,delete-orphan", backref="_user", lazy="dynamic")
    wishlist_entries = db.relationship("WishlistEntry", cascade="all,delete-orphan", backref="_user", lazy="dynamic")
    credits = db.relationship("Credit", cascade="all,delete-orphan", backref="_user", lazy="dynamic")
    earnings = db.relationship("Earning", cascade="all,delete-orphan", backref="_user", lazy="dynamic")
    cards = db.relationship("RecurrentCard", cascade="all,delete-orphan", backref="_user", lazy="dynamic")
    coupons = db.relationship("Coupon", cascade="all,delete-orphan", backref="_user", lazy="dynamic")
    referral_code = db.Column(db.String(200), unique=True, index=True)
    auction_requests = db.relationship("AuctionRequest", cascade="all,delete-orphan", backref="_user", lazy="dynamic")
    conversees = db.relationship("Conversee", cascade="all,delete-orphan", backref="_user", lazy="dynamic")
    admin_messages = db.relationship("AdminMessage", cascade="all,delete-orphan", backref="user", lazy="dynamic")
    message_responses = db.relationship("AdminMessageResponse", cascade="all,delete-orphan", backref="user", lazy="dynamic")
    orders = db.relationship("Order", cascade="all,delete-orphan", backref="user", lazy="dynamic")
    profile_pic = db.Column(db.Text, unique=False)
    method_of_reach = db.Column(db.String(200), nullable=True)
    verification_hash = db.Column(db.String(200), nullable=True)
    otp = db.Column(db.String(50), nullable=True)
    is_incognito = db.Column(db.Boolean, default=False)
    referral_count = db.Column(db.Integer, default=0)

    def update_last_login(self):
        if self.current_login_at is None and self.last_login_at is None:
            self.current_login_at = self.last_login_at = datetime.now()
            self.current_login_ip = self.last_login_ip = gethostbyname(gethostname())

        if self.current_login_at != self.last_login_at:
            self.last_login_at = self.current_login_at
            self.last_login_ip = self.current_login_ip
            self.current_login_at = datetime.now()
            self.current_login_ip = gethostbyname(gethostname())

        if self.last_login_at == self.current_login_at:
            self.current_login_at = datetime.now()
            self.current_login_ip = gethostbyname(gethostname())

        self.login_count += 1
        db.session.add(self)
        db.session.commit()

    @property
    def approx_name(self):
        _first = self.first_name
        _last = " %s." % self.last_name[0].upper() if self.last_name else ""

        return "%s%s" % (_first, _last)

    @property
    def available_coupon_count(self):
        return self.coupons.filter(Coupon.is_expired != True, Coupon.is_used != True).count()

    @property
    def available_coupon_worth(self):
        return db.session.query(func.sum(Coupon.amount)).filter(Coupon.user_id == self.id, Coupon.is_expired != True,
                                                                Coupon.is_used != True).scalar()

    @property
    def full_name(self):
        _first = self.first_name
        _last = " %s" % self.last_name if self.last_name else ""

        return "%s%s" % (_first, _last)

    @property
    def name(self):
        return self.full_name

    @property
    def user_credit(self):
        return int(self.awarded_credit or 0) + int(self.purchased_credit or 0)

    @property
    def conversations(self):
        return [c.conversation for c in self.conversees]

    def get_id(self):
        """ For login manager """
        return self.id

    def is_authenticated(self):
        """ For login manager """
        return True

    def is_anonymous(self):
        """ For login manager """
        return True

    def get_auth_token(self):
        """ Returns the user's authentication token """
        return hashlib.md5("%s:%s" % (self.username, self.password)).hexdigest()

    def is_active(self):
        """ Returns if the user is active or not. Overridden from UserMixin """
        return self.is_enabled

    def encrypt_password(self, password):
        """
        Generates a password from the plain string

        :param password: plain password string
        """

        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        """
        Checks the given password against the saved password

        :param password: password to check
        :type password: string

        :returns True or False
        :rtype: bool

        """
        return bcrypt.check_password_hash(str(self.password), password)

    def set_password(self, new_password):
        """
        Sets a new password for the user

        :param new_password: the new password
        :type new_password: string
        """

        self.encrypt_password(new_password)
        db.session.add(self)
        db.session.commit()

    @property
    def wishlist_product_ids(self):
        """ Returns list of products the user added to wishlist """
        return [i.product_id for i in self.wishlist_entries]


class Customer(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    email = db.Column(db.String(200), nullable=False)
    accept_marketing = db.Column(db.Boolean, default=True)
    # cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    cart_items = db.relationship('CartItem', backref='customer', lazy='dynamic')
    orders = db.relationship('Order', backref='customer', lazy='dynamic')
    addresses = db.relationship('Address', cascade="all,delete-orphan", backref='customer', lazy='dynamic')
    transactions = db.relationship('Transaction', cascade="all,delete-orphan", backref='customer', lazy='dynamic')
    notes = db.Column(db.Text, nullable=True)
    is_incognito = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<Customer %s >' % self.name


class ProductDetail(AppMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    content = db.Column(db.Text)
    type_id = db.Column(db.Integer, db.ForeignKey('product_detail_type.id'))
    type = db.relationship('ProductDetailType', foreign_keys='ProductDetail.type_id')
    position = db.Column(db.Integer, default=0)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))


class ProductDetailType(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code", "description"]

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=False, unique=True)
    name = db.Column(db.String(200), nullable=False)
    description = db.Column(db.Text, nullable=True)


class ProductView(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    view_count = db.Column(db.Integer, default=1)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    product = db.relationship('Product', foreign_keys='ProductView.product_id')


class Product(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "description", "sku", "handle", "quantity", "regular_price", "sale_price", "weight",
                            "has_variants", "on_sale", "categories", "user_id", "min_price", "auction_requests",
                            "view_count", "variants","percentage_off","is_in_house",
                            "max_price", "is_discounted", "cover_image_url", "total_quantity", "price",
                            "product_details", "category_names", "category_ids", "category_codes"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    description = db.Column(db.Text, nullable=True)  # basic desription for a product
    sku = db.Column(db.String(200), nullable=False)
    handle = db.Column(db.String(200), default=slugify_from_name)
    quantity = db.Column(db.Integer)
    regular_price = db.Column(db.Float, nullable=False)
    sale_price = db.Column(db.Float)
    affiliate_price = db.Column(db.Float)
    weight = db.Column(db.Float)
    has_variants = db.Column(db.Boolean)
    on_sale = db.Column(db.Boolean, default=False)
    is_featured = db.Column(db.Boolean, default=False)
    is_in_house = db.Column(db.Boolean, default=False)
    variant_attributes = db.Column(db.Text)
    auction_requests = db.relationship("AuctionRequest", cascade="all,delete-orphan", backref="product", lazy="dynamic")

    #
    cart_items = db.relationship('CartItem', backref='product_', lazy='dynamic', cascade='all,delete-orphan')
    # relationship with variant
    variants = db.relationship('Variant', backref='product_', lazy='dynamic', cascade='all,delete-orphan')

    # related to image
    images = db.relationship('Image', backref='product_', lazy='dynamic', cascade='all,delete-orphan')

    # related to category
    # category_id = db.Column(db.Integer, db.ForeignKey('category.id', ondelete='SET NULL'), nullable=True)
    # channel_id = db.Column(db.Integer, db.ForeignKey('channel.id', ondelete='SET NULL'), nullable=True)
    # location_id = db.Column(db.Integer, db.ForeignKey('location.id', ondelete='SET NULL'), nullable=True)
    categories = db.relationship('Category', secondary="categories", backref=db.backref('categories_', lazy='dynamic'))
    product_details = db.relationship("ProductDetail", cascade="all,delete-orphan", backref="product", lazy="dynamic")
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'), nullable=True)
    view_count = db.Column(db.Integer, default=0)
    wishlist_entries = db.relationship('WishlistEntry', backref='product', lazy='dynamic', cascade='all')

    # def cart_choices(self):
    #     """ Returns the variants for the shopping cart, based on wether it is tracked or untracked """
    #
    #     return Variant.query.filter(Variant.product == self)

    @hybrid_property
    def total_quantity(self):
        """Get the total number of product by summing all available variant quantities
        :returns : product.total
        """
        total_quantity = 0

        for variant in self.variants.all():
            quantity = variant.quantity
            total_quantity = total_quantity + quantity

        return total_quantity

    @property
    def cover_image(self):
        return Image.query.filter(Image.product_id == self.id, Image.is_cover == True).first()

    @property
    def cover_image_url(self):
        if self.cover_image:
            return self.cover_image.url
        else:
            return None

    @property
    def price(self):
        if self.on_sale:
            price = self.sale_price
        else:
            price = self.regular_price

        return price

    @property
    def min_price(self):
        m = min([i.price for i in self.variants]) if self.variants.count()>0 else 0
        return m

    @property
    def max_price(self):
        m = max([i.regular_price for i in self.variants]) if self.variants.count()>0 else 0
        return m

    @property
    def is_discounted(self):
        if self.max_price > self.min_price:
            return True
        else:
            return False

    @property
    def percentage_off(self):
        if self.is_discounted and self.max_price > 0.0 and self.min_price > 0:
            """ Gets the discount price """
            dp = self.min_price
            """ Gets the standard price """
            sp = self.max_price

            """ Calculates the percentage off """
            po = int(((sp - dp) / sp) * 100)
            return str(po) + "%"
        else:
            return False



    @property
    def category_names(self):
        return ", ".join([getattr(x, "name", "") for x in self.categories])

    @property
    def category_codes(self):
        return ", ".join([getattr(x, "code", "") for x in self.categories])

    @property
    def category_ids(self):
        return ", ".join([str(getattr(x, "id", "")) for x in self.categories])

    @hybrid_property
    def get_quantity(self):
        var = self.variants
        q = sum([v.quantity for v in var])
        return q


class Variant(AppMixin, db.Model):
    __searchable__ = True

    __listeners_for_index__ = ["product"]

    __include_in_index__ = ["name", "attribute_keys", "description", "height", "width", "length", "regular_price",
                            "sale_price", "weight", "tax_class", "sku", "shipping_class", "is_saved", "on_sale",
                            "quantity", "availability", "view_count", "user_id", "product_id", "product", "cover_image",
                            "price"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    attribute_keys = db.Column(db.String(200), nullable=True)
    description = db.Column(db.Text, nullable=True)
    height = db.Column(db.Float)
    width = db.Column(db.Float)
    length = db.Column(db.Float)
    regular_price = db.Column(db.Float, nullable=True)
    sale_price = db.Column(db.Float)
    affiliate_price = db.Column(db.Float)
    weight = db.Column(db.Float)
    tax_class = db.Column(db.String(200))
    sku = db.Column(db.String(200))
    shipping_class = db.Column(db.String(200))
    is_saved = db.Column(db.Boolean)
    on_sale = db.Column(db.Boolean, default=False)
    quantity = db.Column(db.Integer, default=0)
    availability = db.Column(db.Boolean, default=False)
    view_count = db.Column(db.Integer, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'), nullable=True)
    cart_items = db.relationship('CartItem', backref='variant_', lazy='dynamic')
    auctions = db.relationship('Auction', backref='variant_', lazy='dynamic')
    order_entries = db.relationship('OrderEntry', backref='variant', lazy='dynamic')
    # relationship with product
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    product = db.relationship('Product', foreign_keys='Variant.product_id')

    # related to image
    images = db.relationship('Image', backref='variant', lazy='dynamic', cascade='all,delete-orphan')

    # allocations = db.relationship("Allocation", cascade="all,delete-orphan", backref="_variant", lazy="dynamic")
    #
    # @hybrid_property
    # def total_allocation(self):
    #     """Get the total number of particular variant already assigned to location/channel
    #     :returns : allocation.total
    #     """
    #     total_quantity = 0
    #
    #     for alloc in self.allocations.all():
    #         quantity = alloc.quantity
    #         total_quantity = total_quantity + quantity
    #
    #     return total_quantity


    @property
    def cover_image(self):
        return Image.query.filter(Image.variant_id == self.id, Image.is_cover == True).first()

    @property
    def price(self):
        if self.product.on_sale:
            price = self.sale_price
        else:
            price = self.regular_price

        return price

    @property
    def w_price(self):
        if self.on_sale:
            price = self.sale_price
        else:
            price = self.regular_price

        return whole_number_format(price)


class Image(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    height = db.Column(db.Float)
    width = db.Column(db.Float)
    url = db.Column(db.String(200), nullable=False)
    slug = db.Column(db.String(200), nullable=True, default=slugify_from_name)
    auction_id = db.Column(
        db.Integer, db.ForeignKey('auction.id'), nullable=True)
    # relationship with product
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    # collection_id = db.Column(db.Integer, db.ForeignKey('collection.id'))
    is_cover = db.Column(db.Boolean, default=False)

    # relationship with variant
    variant_id = db.Column(db.Integer, db.ForeignKey('variant.id'))


# __table_args__ = (
#     UniqueConstraint('user_id', 'slug', name='_user_image_uc'),
# )


class Category(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code", "description", "visibility", "permanent", "cover_image_id", "level",
                            "parent_id", "cover_image_url", "categories", "total_products"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    code = db.Column(db.String(200), unique=True, nullable=False)
    description = db.Column(db.String(200))
    visibility = db.Column(db.Boolean, default=False)
    permanent = db.Column(db.Boolean)
    view_count = db.Column(db.Integer, default=0)
    cover_image_id = db.Column(db.Integer)
    level = db.Column(db.Integer, default=0, nullable=False)
    images = db.relationship('Image', backref='category', lazy='dynamic', cascade="all,delete-orphan")

    # possible relationship to a parent category for subcategories
    parent_id = db.Column(db.Integer, db.ForeignKey('category.id'), nullable=True)
    categories = db.relationship('Category', backref=db.backref('sub_category', remote_side='Category.id'))

    # related to product
    products = db.relationship('Product', secondary="categories",
                               backref=db.backref('products', lazy='dynamic'))

    detail_tag = db.Column(db.String(500))  # used to record keys needed for the spec sheet

    @property
    def total_products(self):
        """ Total number of products within the category """
        # count = 0
        # for cat in self.categories:
        #     count = count + len(cat.products)
        count = sum([len(cat.products) for cat in self.categories])
        total_count = count + len(self.products)
        # return len(self.products)
        return total_count or 0

    @property
    def cover_image(self):
        try:
            return Image.query.filter(Image.category_id == self.id, Image.is_cover == True).first()
        except:
            return None

    @property
    def cover_image_url(self):
        if self.cover_image:
            return self.cover_image.url
        else:
            return None


# Association table for Category and Product
categories = db.Table('categories',
                      db.Column('category_id', db.Integer, db.ForeignKey('category.id')),
                      db.Column('product_id', db.Integer, db.ForeignKey('product.id'))
                      )


# Shopping carts belong to customers, not to users
class CartItem(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)
    product = db.relationship("Product", foreign_keys="CartItem.product_id")
    variant_id = db.Column(db.Integer, db.ForeignKey('variant.id'), nullable=True)
    variant = db.relationship("Variant", foreign_keys="CartItem.variant_id")
    quantity = db.Column(db.Integer)
    cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    price = db.Column(db.Float)
    total = db.Column(db.Float)

    def calculate_total(self):
        """Calculates the total charge of this item """
        self.total = float(self.quantity or 0) * float(self.price or 0.0)
        db.session.add(self)
        db.session.commit()
        return self.total

    @property
    def variant_price(self):
        return self.variant.price

    def __repr__(self):
        return '<CartItem %r>' % self.variant


class Cart(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(200), nullable=True)
    last_name = db.Column(db.String(200), nullable=True)
    total = db.Column(db.Float, default=0.0)
    delivery_charge = db.Column(db.Float, default=0.0)
    phone = db.Column(db.String(200), nullable=True)
    email = db.Column(db.String(200), nullable=True)
    address_line1 = db.Column(db.String(200), nullable=True)
    address_line2 = db.Column(db.String(200), nullable=True)
    city = db.Column(db.String(200), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=True)
    state = db.relationship("State", foreign_keys="Cart.state_id")
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=True)
    country = db.relationship("Country", foreign_keys="Cart.country_id")
    payment_option_id = db.Column(db.Integer, db.ForeignKey('payment_option.id'), nullable=True)
    delivery_option_id = db.Column(db.Integer, db.ForeignKey('delivery_option.id'), nullable=True)
    # auto_set_shipment = db.Column(db.Boolean, default=False)
    shipment_name = db.Column(db.String(200), nullable=True)
    shipment_phone = db.Column(db.String(200), nullable=True)
    shipment_address = db.Column(db.String(300), nullable=True)
    shipment_city = db.Column(db.String(200))
    shipment_description = db.Column(db.String(200))
    shipment_state = db.Column(db.String(200), nullable=True)
    shipment_country = db.Column(db.String(200), nullable=True)
    purchase_session_token = db.Column(db.String(100), nullable=True)  # used to id a purchase session on checkout
    transactions = db.relationship('Transaction', backref='cart', lazy='dynamic')
    cart_items = db.relationship('CartItem', cascade="all,delete-orphan", backref='cart', lazy='dynamic')
    quantity = db.Column(db.Integer, default=0)
    checkout_type = db.Column(db.String(200), nullable=True)

    # customer = db.relationship('Customer', backref='cart', uselist=False)

    # @property
    # def delivery_charge(self):
    #     return self.user.fetch_delivery_charge(self.state_id) or 0.0

    # @property
    # def payment_instruction(self):
    #     p = self.user.fetch_payment_method(self.payment_type_id)
    #     if p:
    #         return p.instructions

    @property
    def token(self):
        s = "BoBcRgBn" +"_"+ token_generator(size=4)+"_"+ id_generator(size=6) + "_" + str(self.id)
        return s.lower()

    def item(self, variant_id):
        return CartItem.query.filter(CartItem.cart == self, CartItem.variant_id == variant_id).first()

    def items(self):
        return CartItem.query.filter(CartItem.cart == self).order_by(asc(CartItem.variant_id)).all()

    def items_count(self):
        return CartItem.query.filter(CartItem.cart == self).order_by(asc(CartItem.variant_id)).count()

    @property
    def cart_total(self):
        return db.session.query(func.sum(CartItem.total)).join(Variant).filter(CartItem.cart_id == self.id, CartItem.variant_id==Variant.id).filter(Variant.quantity>=1).scalar()

    @property
    def total(self):
        dc = self.delivery_charge if self.delivery_charge else 0.0
        return dc + self.cart_total

    @property
    def total_quantity(self):
        q = db.session.query(func.sum(CartItem.quantity)).filter(CartItem.cart_id == self.id).scalar()
        self.quantity = q
        db.session.add(self)
        db.session.commit()
        return q


class Order(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["user", "delivery_status", "delivery_option", "state", "order_status", "entries","customer_id",
                            "total", "checkout_type", "verified_status", "amount", "discount", "first_name","last_name",
                            "shipment_state","shipment_city","city","payment_option","email","bob_commission"]

    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=True)
    first_name = db.Column(db.String(200), nullable=True)
    last_name = db.Column(db.String(200), nullable=True)
    discount = db.Column(db.Float, default=0.0)
    amount = db.Column(db.Float, default=0.0)
    shipping_charge = db.Column(db.Float, default=0.0)
    code = db.Column(db.String(200), nullable=False)
    coupon_code = db.Column(db.String(200), nullable=True)
    message = db.Column(db.Text, nullable=True)
    verified_status = db.Column(db.Boolean, default=False)  # considering either verified or not
    payment_option_id = db.Column(db.Integer, db.ForeignKey('payment_option.id'), nullable=True)
    payment_status_id = db.Column(db.Integer, db.ForeignKey('payment_status.id'), nullable=True)
    delivery_option_id = db.Column(db.Integer, db.ForeignKey('delivery_option.id'), nullable=True)
    delivery_status_id = db.Column(db.Integer, db.ForeignKey('delivery_status.id'), nullable=True)
    order_status_id = db.Column(db.Integer, db.ForeignKey('order_status.id'), nullable=True)
    entries = db.relationship("OrderEntry", cascade="all,delete-orphan", backref="order", lazy="dynamic")
    coupons = db.relationship("Coupon", cascade="all,delete-orphan", backref="order", lazy="dynamic")
    transactions = db.relationship('Transaction', backref="order_", lazy='dynamic')
    # Adding address information to the order
    email = db.Column(db.String(200), nullable=False)
    phone = db.Column(db.String(200), nullable=False)
    address_line1 = db.Column(db.String(200), nullable=False)
    address_line2 = db.Column(db.String(200), nullable=True)
    city = db.Column(db.String(200), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=False)
    state = db.relationship("State", foreign_keys="Order.state_id")
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False)
    country = db.relationship("Country", foreign_keys="Order.country_id")
    auto_set_shipment = db.Column(db.Boolean, default=False)
    shipment_name = db.Column(db.String(200), nullable=True)
    shipment_phone = db.Column(db.String(200), nullable=True)
    shipment_address = db.Column(db.String(300), nullable=True)
    shipment_city = db.Column(db.String(200))
    shipment_description = db.Column(db.String(200))
    shipment_state = db.Column(db.String(200), nullable=True)
    shipment_country = db.Column(db.String(200), nullable=True)
    # channel_id = db.Column(db.Integer, db.ForeignKey('channel.id'), nullable=False)
    payment_option = db.relationship("PaymentOption", foreign_keys="Order.payment_option_id")
    payment_status = db.relationship("PaymentStatus", foreign_keys="Order.payment_status_id")
    checkout_type = db.Column(db.String(200), nullable=True)
    bob_commission = db.Column(db.Float, default=0.0)
    # delivery_option = db.relationship("DeliveryOption", foreign_keys="Order.delivery_option_id")
    # delivery_status = db.relationship("DeliveryStatus", foreign_keys="Order.delivery_status_id")


    __table_args__ = (
        UniqueConstraint('user_id', 'code', name='_user_order_uc'),
    )

    @property
    def full_name(self):
        _first = self.first_name
        _last = " %s" % self.last_name if self.last_name else ""

        return "%s%s" % (_first, _last)

    @property
    def total(self):
        return self.amount if self.amount else 0.0

    @property
    def w_total(self):
        return whole_number_format(self.amount)

    @property
    def shipping_fee(self):
        return whole_number_format(self.shipping_charge)

    @property
    def total_quantity(self):
        return db.session.query(func.sum(OrderEntry.quantity)).filter(OrderEntry.order_id == self.id).scalar()

    @property
    def total_entries(self):
        return self.entries.count()

    def cal_bob_commission(self):
        commission = db.session.query(func.sum(OrderEntry.bob_commission)).filter(OrderEntry.order_id == self.id).scalar()
        if not commission:
            commission=0
        self.bob_commission=commission
        db.session.add(self)
        db.session.commit()
        return commission


class OrderEntry(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'), nullable=False)
    # customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    variant_id = db.Column(db.Integer, db.ForeignKey('variant.id'), nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)
    quantity = db.Column(db.Integer)
    price = db.Column(db.Float)
    # payment_status_id = db.Column(db.Integer, db.ForeignKey('payment_status.code'), nullable=True)
    # delivery_option_id = db.Column(db.Integer, db.ForeignKey('delivery_option.code'), nullable=True)
    # delivery_status_id = db.Column(db.Integer, db.ForeignKey('delivery_status.code'), nullable=True)
    order_status_id = db.Column(db.Integer, db.ForeignKey('order_status.id'), nullable=True)
    date_cancelled = db.Column(db.Date, nullable=True)
    date_cust_cancelled = db.Column(db.Date, nullable=True)
    date_delivered = db.Column(db.Date, nullable=True)
    date_processed = db.Column(db.Date, nullable=True)
    date_returned = db.Column(db.Date, nullable=True)
    is_in_house = db.Column(db.Boolean, default=False)
    affiliate_price = db.Column(db.Float, default=0.0)
    bob_commission = db.Column(db.Float, default=0.0)

    @property
    def total(self):
        """Calculates the total charge of this item """
        qty = self.quantity
        tot= qty * self.price
        if self.is_in_house:
            self.bob_commission = 0.1*tot
            db.session.add(self)
            db.session.commit()
        elif self.affiliate_price and self.affiliate_price>0:
            aff_tot = qty*self.affiliate_price
            self.bob_commission= tot- aff_tot
            db.session.add(self)
            db.session.commit()
        return tot

    @property
    def w_total(self):
        """Calculates the total charge of this item """
        return whole_number_format(self.quantity * self.price)

    @property
    def w_price(self):
        """Calculates the total charge of this item """
        return whole_number_format(self.price)

    @property
    def product(self):
        return self.variant.product

    @property
    def user(self):
        return User.query.get(self.user_id)

    @property
    def cover_image(self):
        return self.variant.product.cover_image

    @property
    def name(self):
        return self.variant.product.name

    @property
    def var(self):
        return self.variant.name

    @property
    def image_url(self):
        return self.variant.product.cover_image.url

    @property
    def qty(self):
        return self.quantity


class TransactionStatus(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code", "message"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    code = db.Column(db.String(200), nullable=False, unique=True)
    message = db.Column(db.String(200))
    transactions = db.relationship("Transaction", cascade="all,delete-orphan", backref="transaction_status",
                                   lazy="dynamic")

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<TransactionStatus %r>' % self.name

        # # table arguments to extend the behaviour of our classes
        # __table_args__ = (
        #     UniqueConstraint('gateway', 'code', name='_transaction_status_code'),
        # )


class Transaction(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=True)
    cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'), nullable=True)
    code = db.Column(db.String(200), nullable=False)
    description = db.Column(db.Text)
    payment_option_id = db.Column(db.Integer, db.ForeignKey('payment_option.id'), nullable=False)
    # payment_type_code = db.Column(db.Integer, db.ForeignKey('payment_type.code'), nullable=False)
    transaction_status_id = db.Column(db.Integer, db.ForeignKey('transaction_status.id'),
                                      nullable=True)  # update this to not make a reference of edit the refernced table
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'), nullable=True)
    order = db.relationship('Order', foreign_keys='Transaction.order_id')
    amount = db.Column(db.Float)
    entries = db.relationship("TransactionEntry", cascade="all,delete-orphan", backref="transaction", lazy="dynamic")
    phone = db.Column(db.String(200), nullable=True)
    email = db.Column(db.String(200), nullable=True)
    address_line1 = db.Column(db.String(200), nullable=True)
    city = db.Column(db.String(200), nullable=True)
    state = db.Column(db.String(200), nullable=True)
    country = db.Column(db.String(200), nullable=True)
    auto_set_shipment = db.Column(db.Boolean, default=False)
    shipment_name = db.Column(db.String(200), nullable=True)
    shipment_phone = db.Column(db.String(200), nullable=True)
    shipment_address = db.Column(db.String(300), nullable=True)
    shipment_city = db.Column(db.String(200))
    shipment_description = db.Column(db.String(200))
    shipment_state = db.Column(db.String(200), nullable=True)
    shipment_country = db.Column(db.String(200), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    checkout_type = db.Column(db.String(200), nullable=True)

    # channel_id = db.Column(db.Integer, db.ForeignKey('channel.id'), nullable=False)

    @property
    def amount_in_string(self):
        return str(int(self.amount))

    # @property
    # def notification_url(self):
    #     return "%s%s" % (self.user.url, url_for('.transaction_notification', id=self.id))

    # @property
    # def transaction_hash(self):
    #     """ perform a sha512 hash of [gtpay_tranx_id + gtpay_tranx_amt + gtpay_tranx_noti_url + hashkey] (in that order) """
    #     _key = "%s%s%s%s" % (self.code, self.amount_in_string, self.notification_url, self.payment_method.merchant_hash)
    #
    #     # logger.info(_key)
    #
    #     return hashlib.sha512(_key).hexdigest()
    @property
    def response_url(self):
        url = "%s%s" % (app.config.get("CANONICAL_URL"), url_for('frontend_bp.vbv_confirmation', code=self.code))
        return url

    __table_args__ = (
        UniqueConstraint('user_id', 'code', name='_user_code_uc'),
    )


class TransactionEntry(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    variant_id = db.Column(db.Integer, db.ForeignKey('variant.id'), nullable=False)
    transaction_id = db.Column(db.Integer, db.ForeignKey('transaction.id'), nullable=False)
    quantity = db.Column(db.Integer)
    price = db.Column(db.Float)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)

    @property
    def product(self):
        return self.variant.product

    @property
    def cover_image(self):
        return self.variant.product.cover_image

    @hybrid_property
    def total(self):
        """Calculates the total charge of this item """
        return self.quantity * self.price


class OrderStatus(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False, unique=True)
    code = db.Column(db.String(200), nullable=False, unique=True)
    orders = db.relationship("Order", cascade="all,delete-orphan", backref="order_status", lazy="dynamic")
    order_entries = db.relationship("OrderEntry", cascade="all,delete-orphan", backref="order_status", lazy="dynamic")


class DeliveryStatus(AppMixin, db.Model):
    __searchable__ = True
    __include_in_index__ = ["name", "code"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False, unique=True)
    code = db.Column(db.String(200), nullable=False, unique=True)
    orders = db.relationship("Order", cascade="all,delete-orphan", backref="delivery_status", lazy="dynamic")


class AuctionRequest(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)


class Auction(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "description", "other_information", "title", "auction_information", "value",
                            "starting_bid", "max_bid", "bid_cost", "cover_image_url", "price", "min_price", "bid_count",
                            "bid_increment", "variant_id", "variant", "cover_image_id", "current_bidder",
                            "on_bid", "bid_start", "bid_end"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)
    description = db.Column(db.Text, nullable=True)
    other_information = db.Column(db.Text, unique=False)
    title = db.Column(db.String(200), unique=False)
    auction_information = db.Column(db.Text, unique=False)
    value = db.Column(db.Float, unique=False, default=0)
    starting_bid = db.Column(db.Float, unique=False, default=0)
    max_bid = db.Column(db.Float, unique=False, default=0)
    bid_cost = db.Column(db.Float, unique=False, default=0)  # bid credit cost
    bid_increment = db.Column(db.Float, unique=False, default=0)
    variant_id = db.Column(db.Integer, db.ForeignKey('variant.id'), nullable=True)
    variant = db.relationship("Variant", foreign_keys="Auction.variant_id")
    images = db.relationship('Image', backref='auction',
                             lazy='dynamic', cascade="all,delete-orphan")
    cover_image_id = db.Column(db.Integer)  # over image among all images
    on_bid = db.Column(db.Boolean, default=False)
    bid_start = db.Column(db.DateTime)
    bid_end = db.Column(db.DateTime)
    bids = db.relationship("Bid", cascade="all,delete-orphan", backref="auction", lazy="dynamic")

    @property
    def cover_image(self):
        """ Retrieves the cover image from the list of images """
        if self.cover_image_id:
            return Image.query.get(self.cover_image_id)
        else:
            cover_image = self.images.filter().first()
            if cover_image:
                self.cover_image_id = cover_image.id

            # On first call, set the cover image id to prevent subsequent
            # searching
            try:
                db.session.add(self)
                db.session.commit()
            except:
                db.session.rollback()
                raise

            return cover_image

    @property
    def cover_image_url(self):
        if self.cover_image:
            return self.cover_image.url
        else:
            return None

    @property
    def bid_end_timestamp(self):
        if self.on_bid and self.bid_end > self.bid_start:
            _full_day = self.bid_end + timedelta(days=1)
            return int(_full_day.strftime('%s')) * 1000
        else:
            return None

    @property
    def price(self):
        bid = Bid.query.filter(Bid.auction_id == self.id).order_by(desc(Bid.id)).first()
        if bid:
            return bid.price
        else:
            return self.starting_bid

    @property
    def min_price(self):
        return self.price + self.bid_increment

    @property
    def current_bidder(self):
        bid = Bid.query.filter(Bid.auction_id == self.id).order_by(desc(Bid.id)).first()
        return bid.user if bid else None

    @property
    def bid_count(self):
        return self.bids.count()


class Bid(UserMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["auction_id", "auction", "price", "user_id", "user", "bid_time"]

    __listeners_for_index__ = ["auction"]

    id = db.Column(db.Integer, primary_key=True)
    auction_id = db.Column(db.Integer, db.ForeignKey('auction.id'), nullable=False)
    price = db.Column(db.Float, unique=False, default=0.0)
    bid_time = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)


class Credit(UserMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["user_id", "user", "amount", "price", "is_awarded", "is_purchased", "is_credit",
                            "credit_type_id", "credit_type"]

    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Float, unique=False, default=0.0)  # amount in value of credit purchased
    price = db.Column(db.Float, unique=False, default=0.0)  # amount spent acquiring the credit
    is_awarded = db.Column(db.Boolean, default=False)
    is_purchased = db.Column(db.Boolean, default=False)
    is_credit = db.Column(db.Boolean, default=False)
    credit_type_id = db.Column(db.Integer, db.ForeignKey('credit_type.id'),
                               nullable=True)  # type of credit awarded e.g referral, promo, signup


class CreditType(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    code = db.Column(db.String(200), nullable=False)
    credits = db.relationship("Credit", cascade="all", backref="credit_type", lazy="dynamic")


class Earning(UserMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["user_id", "user", "amount", "is_credit"]

    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Integer, unique=False, default=0)
    is_credit = db.Column(db.Boolean, default=False)


class Referral(UserMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "email", "is_verified"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    email = db.Column(db.String(200), nullable=False)
    is_verified = db.Column(db.Boolean, default=False)


class Coupon(UserMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["order_id", "order", "type", "amount", "min_spend", "category_id", "category", "is_used",
                            "is_expired", "expiry_date"]

    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'), nullable=True)
    type = db.Column(db.String(200), nullable=False)  # category/bob
    amount = db.Column(db.Float, default=False)  # value in naira of these coupon
    min_spend = db.Column(db.Float, default=False)  # minimum amount spent to make this coupon applicable
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'), nullable=True)
    category = db.relationship("Category", foreign_keys="Coupon.category_id")
    is_used = db.Column(db.Boolean, default=False)
    is_expired = db.Column(db.Boolean, default=False)
    expiry_date = db.Column(db.DateTime, nullable=False)  # nullable false


# ============================================#
# ==this models are for notifications=========#

class AdminMessage(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["full_name", "email", "phone", "subject", "is_read", "has_parent",
                            "user_read", "is_replied", "date_replied", "body", "responses", "user_id"]

    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(200), nullable=False)
    email = db.Column(db.String(200), nullable=False)
    phone = db.Column(db.String(200), nullable=False)
    subject = db.Column(db.Text)
    is_read = db.Column(db.Boolean, default=False)
    user_read = db.Column(db.Boolean, default=False)
    has_parent = db.Column(db.Boolean, default=False)
    is_replied = db.Column(db.Boolean, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    date_replied = db.Column(db.DateTime, nullable=True)
    body = db.Column(db.Text)  # for plain text messages
    responses = db.relationship(
        'AdminMessageResponse', backref='admin_message', lazy='dynamic', cascade="all,delete-orphan")


class AdminMessageResponse(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    admin_message_id = db.Column(db.Integer, db.ForeignKey(
        'admin_message.id'), nullable=False)
    body = db.Column(db.Text)  # for plain text messages
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)


class Notification(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    email = db.Column(db.Text, nullable=True)
    sms = db.Column(db.Text, nullable=True)
    push = db.Column(db.Text, nullable=True)
    code = db.Column(db.String(200), nullable=False, unique=True)

    # relationship to compiled email
    emails = db.relationship('Email', backref='notification', lazy='dynamic', cascade='all,delete-orphan')
    # relationship to compiled sms
    sms_ = db.relationship('Sms', backref='notification', lazy='dynamic', cascade='all,delete-orphan')


# __table_args__ = (
# 	UniqueConstraint("name", "code"),
# )


class Email(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    subject = db.Column(db.String(400), nullable=False)
    html = db.Column(db.Text)
    text = db.Column(db.Text)
    send_at = db.Column(db.DateTime)
    sent_at = db.Column(db.DateTime)
    status = db.Column(db.String(200))
    message_id = db.Column(db.String(200))
    to = db.Column(db.String(200), nullable=False)
    from_ = db.Column(db.String(200), nullable=False)
    # notification_id = db.Column(db.Integer, db.ForeignKey('notification.id'), nullable=False)
    notification_id = db.Column(db.Integer, db.ForeignKey('notification.id'), nullable=False)


class Sms(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text)
    to = db.Column(db.String(200), nullable=False)
    from_ = db.Column(db.String(200), nullable=False)
    send_at = db.Column(db.DateTime)
    status = db.Column(db.String(200), default='pending')
    message_id = db.Column(db.String(200))
    notification_id = db.Column(db.Integer, db.ForeignKey('notification.id'), nullable=False)
    # notification_code = db.Column(db.String(200), nullable=False)
    sent_at = db.Column(db.DateTime)


# ======this ends notification models=========#
# ============================================#


# ========= chat models===============#
# ====================================#

class Conversation(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    conversees = db.relationship('Conversee', backref='conversation', lazy='dynamic', cascade='all,delete-orphan')
    messages = db.relationship('Message', backref='conversation', lazy='dynamic', cascade='all,delete-orphan')

    # receiver = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    # sender = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def get_receiver_id(self, sender_id):
        msg = self.messages.first()
        if msg.receiver_id == sender_id:
            return msg.sender_id
        else:
            return msg.receiver_id

    @property
    def receiver(self):
        return User.query.get(self.receiver_id)


class Conversee(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    conversation_id = db.Column(db.Integer, db.ForeignKey('conversation.id'), nullable=False)


class Message(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text)
    receiver_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    conversation_id = db.Column(db.Integer, db.ForeignKey('conversation.id'), nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id', ondelete='SET NULL'), nullable=True)
    product = db.relationship("Product", foreign_keys="Message.product_id")

    @property
    def receiver(self):
        return User.query.get(self.receiver_id)

    @property
    def sender(self):
        return User.query.get(self.sender_id)


# ========= chat models  end==========#
# ====================================#
class Wishlist(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    description = db.Column(db.Text)
    slug = db.Column(db.String(200), nullable=True)
    is_private = db.Column(db.Boolean, default=False)
    entries = db.relationship(
        'WishlistEntry', cascade="all,delete-orphan", backref='wishlist', lazy='dynamic')


class WishlistEntry(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey(
        'product.id'), nullable=False)
    wishlist_id = db.Column(db.Integer, db.ForeignKey(
        'wishlist.id'), nullable=False)


class ViewedProduct(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    view_count = db.Column(db.Integer, default=0)

    # relationship with product
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)
    product = db.relationship('Product', foreign_keys='ViewedProduct.product_id')
