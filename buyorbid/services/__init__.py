from ext.base.exceptions import ObjectNotFoundException, ActionDeniedException, ValidationFailed, \
    FurtherActionException

from ext.services.orm import ServiceFactory
from ext.base.utils import DateJSONEncoder, generate_key, token_generator, number_format, id_generator, slugify
from sqlalchemy.sql.expression import or_
from ext.integrations import pycard
from sqlalchemy import func, or_, and_
from ext.base import utils
from buyorbid import models, logger, es
from buyorbid.models import *
from buyorbid.services import search
# from buyorbid import logger, celery, billing, notifier, bcrypt
import requests
from cloudinary.uploader import upload
from datetime import date, datetime, timedelta
from flask import render_template_string, url_for
from flask_restful import abort
from bs4 import BeautifulSoup
import cloudinary
import itertools
from itertools import groupby
from elasticsearch_dsl import Q, A
import pprint
import os
import base64
import time
import json

cloudinary.config(
    cloud_name='rusty-x',
    api_key='629579476381886',
    api_secret='OjwKGxaV4e-oyo7RrEE5ulqr-3g'
)

ES_INDEX = app.config.get("ES_INDEX")

def power_search_object_index(obj):
    logger.info("in power search")
    # logger.info(models)
    # logger.info(doc_type)
    # logger.info(obj_id)
    # model_class = getattr(models, doc_type)
    # logger.info(model_class)
    # obj = model_class.get(obj_id)
    includes = getattr(obj, "__include_in_index__", [])
    excludes = getattr(obj, "__exclude_in_index__", [])
    listeners = getattr(obj, "__listeners_for_index__", [])
    logger.info(includes)
    logger.info(excludes)
    logger.info(listeners)
    data = search.convert_to_dict(obj, includes=includes, excludes=excludes)
    logger.info(data)
    es.index(index=ES_INDEX, doc_type=obj.__tablename__, id=obj.id, body=data)
    search.index_listeners(obj, listeners=listeners)
    logger.info(obj)
    logger.info(obj.id)
    logger.info(obj.__tablename__)
    db.session.add(obj)
    logger.info("just added")
    db.session.commit()
    logger.info("after expunge")
    db.session.flush()
    logger.info("after flush")
    db.session.refresh
    logger.info("about to search")
    search.index(obj)

def power_delete_search_object(doc_type, obj_id):
    model_class = getattr(models, doc_type)
    obj = model_class.get(obj_id)
    search.delete(obj)