from buyorbid.async import send_user_notification, send_notification
from buyorbid.services import *
from buyorbid.services import product
from buyorbid.services.core import *
from buyorbid.services import search
from random import randint
from werkzeug.datastructures import FileStorage

from ext.base.utils import whole_number_format

BaseUserService = ServiceFactory.create_service(User, db)
BaseCustomerService = ServiceFactory.create_service(Customer, db)
PasswordResetTokenService = ServiceFactory.create_service(PasswordResetToken, db)

class UserService(BaseUserService):
    """ Create and manage user accounts """

    @classmethod
    def create(cls, ignored_args=None, include_address=True, **kwargs):
        """ Overriding the parent class method to create a service which registers users """

        print "I got to the create"
        logger.info(kwargs)
        password = kwargs.pop("password", None)

        referral_code = kwargs.get("referrer", None)
        signup_coupon = app.config.get("SIGNUP_COUPON")
        signup_bid = app.config.get("SIGNUP_BID_CREDIT")
        amounts = app.config.get("SIGNUP_COUPON_AMOUNT")

        logger.info(kwargs)
        user = BaseUserService.create(**kwargs)
        logger.info(user)
        pad = "000" + str(user.id)
        n_referral_code = user.first_name[0:3] + pad[-4:]
        user.set_password(password)
        user = cls.update(user.id, referral_code=n_referral_code.lower())

        if referral_code:
            referrer = User.query.filter(User.referral_code == referral_code.lower()).first()
            if referrer:
                ReferralService.create(user_id=referrer.id, name=user.full_name, email=user.email)

        if signup_coupon:
            exp = datetime.now() + timedelta(days=45)
            for amount in amounts:
                min_spend = amount * 5
                CouponService.create(user_id=user.id, type="bob", amount=amount, min_spend=min_spend, expiry_date=exp)

        if signup_bid:
            amount = app.config.get("SIGNUP_BID_AMOUNT")
            cls.update(user.id, awarded_credit=amount)

        data = {"user_id": user.id, "name": "General"}
        product.WishlistService.create(**data)
        hash_ = generate_key(bytes=24, safe=True)
        s = "%s:%s" % (hash_, user.email)
        url_key = base64.urlsafe_b64encode(s).replace("=", "")
        email_data = {}
        try:
            url = "%s%s" % (
                app.config.get("P_CANONICAL_URL"), url_for('public_bp.signup_verification', hash=url_key))

            email_data = dict(amount=whole_number_format(sum(amounts)), link=url)
            print url
        except:
            pass

        token = token_generator(6)

        num = randint(1, 3)
        path = os.path.join(app.config.get("STATIC_FOLDER"), "profile_pattern%sB.png" % num)
        filepath = open(path, 'r+')
        file = FileStorage(filepath)
        files = [file]
        uploaded_files, errors = handle_uploaded_photos(files, (160, 160))
        uploaded_file = uploaded_files[0]
        path = uploaded_file.get("filepath")

        upload_result = upload(path, public_id="profile/BOB-PP%s" % "_"+str(user.id))
        url = upload_result["url"]

        user = UserService.update(user.id, otp=token, verification_hash=url_key, profile_pic=url)

        send_user_notification.delay(user.id, 'msv', subject='Welcome to BuyOrBid!', **email_data)
        send_notification.delay('msv', subject='New User SignUp', email="info@buyorbidng.com", name=user.full_name, **email_data)

        return user

    @classmethod
    def update(cls, obj_id, ignored_args=None, **kwargs):
        ignored_args = ["id", "date_created", "email", "username", "password", "last_updated", "username"]

        user = BaseUserService.get(obj_id)

        user = BaseUserService.update(user.id, ignored_args=ignored_args, **kwargs)

        # user account update notification
        message_data = {'user': user}
        return user

    @classmethod
    def check_user(cls, email):
        user = User.query.filter_by(email=email).first()
        if user:
            return True
        return False

    @classmethod
    def reset_password(cls, obj_id, password, **kwargs):
        """ Method to reset a user's password to a new password """

        obj = BaseUserService.get(obj_id)

        obj = db.session.merge(obj)

        obj.set_password(password)

        # reset password notification
        message_data = {'user': obj}
        # StandardNotificationService.send('password.reset', email_addresses=[obj.email], user_ids=[obj.id], **message_data)

        return obj

    @classmethod
    def request_new_user_password(cls, obj_id, **kwargs):
        """
        Generates a password change token for the requested user
        """
        obj = BaseUserService.get(obj_id)
        token = PasswordResetTokenService.create(email=obj.email, user_id=obj_id)
        url = app.config.get("P_CANONICAL_URL")+url_for("public_bp.recover_password", code=token.code)
        email_data=dict(name=obj.full_name, link=url)
        send_user_notification.delay(obj.id, 'pr', subject='Password Reset Token', **email_data)

        return token


    @classmethod
    def change_password(cls, obj_id, old_password, new_password, **kwargs):
        """ Change the password of an existing user """

        obj = BaseUserService.get(obj_id)
        logger.info(old_password)
        logger.info(new_password)

        if obj.check_password(old_password):
            obj.set_password(new_password)
            #
            # # reset password notification
            # message_data = {'user': obj}
            # # StandardNotificationService.send('password.reset', email_addresses=[obj.email], user_ids=[obj.id], **message_data)

            # p = kwargs.get("password", "")
            data = {"password": bcrypt.generate_password_hash(new_password)}
            logger.info(data)
            obj = cls.update(obj.id, **data)
            return obj

        else:
            raise Exception("Invalid password given. Password cannot be reset.")

    @classmethod
    def enable_user(cls, obj_id):
        """ Activate the user account """

        obj = BaseUserService.get(obj_id)

        # obj.is_enabled = True
        #
        # try:
        #     db.session.add(obj)
        #     db.session.commit()
        #
        #     # user account activated
        #     message_data = {'user': obj}
        #     # notification.send('user.activated', email_addresses=[obj.email], user_ids=[obj.id], **message_data)
        #
        #     return obj
        #
        # except:
        #     db.session.rollback()
        #     raise
        data = {"is_enabled": True}
        obj = cls.update(obj.id, **data)
        return obj

    @classmethod
    def add_to_wishlist(cls, obj_id, **kwargs):
        """ Activate the user account """

        obj = BaseUserService.get(obj_id)

        wishlist = obj.wishlists.first()
        kwargs["wishlist_id"] = wishlist.id
        kwargs["user_id"] = obj_id
        entry = WishlistEntry.query.filter(WishlistEntry.user_id == obj_id, WishlistEntry.wishlist_id == wishlist.id,
                                           WishlistEntry.product_id == kwargs.get("product_id", 0)).first()
        if not entry:
            product.WishlistEntryService.create(**kwargs)
        obj = cls.update(obj.id)
        return obj

    @classmethod
    def disable_user(cls, obj_id):
        """ Activate the user account """

        obj = BaseUserService.get(obj_id)

        # obj.is_enabled = False
        #
        # try:
        #     db.session.add(obj)
        #     db.session.commit()
        #
        #     # user account deactivated
        #     message_data = {'user': obj}
        #     # notification.send('user.deactivated', email_addresses=[obj.email], user_ids=[obj.id], **message_data)
        #
        #     return obj
        # except:
        #     db.session.rollback()
        #     raise

        data = {"is_enabled": False}
        obj = cls.update(obj.id, **data)
        return obj

    @classmethod
    def send_phone_token(cls, obj_id, **kwargs):
        """send a verification code to merchant phone"""

        user = User.query.get(obj_id)
        token = token_generator(6)
        user = UserService.update(user.id, otp=token)

        voice = kwargs.get('voice', False)

        # send_phone_verification.delay(token, obj_id, voice=voice)

    @classmethod
    def verify_phone(cls, obj_id, **kwargs):
        user = User.query.get(obj_id)
        otp = kwargs.get('otp', None)
        referral_coupon = app.config.get("REFERRAL_COUPON")
        referral_bid = app.config.get("REFERRAL_BID_CREDIT")

        if user.otp == otp:
            user = UserService.update(user.id, phone_verified=True)

        referral = 0
        if referral_bid or referral_coupon:
            referral = Referral.query.filter_by(email=user.email).first()

        # Once OTP is verified on the first user
        if referral_coupon and referral:
            exp = datetime.now() + timedelta(days=45)
            amounts = app.config.get("REFERRAL_COUPON_AMOUNT")
            for amount in amounts:
                min_spend = amount * 5
                CouponService.create(user_id=referral.user_id, type="bob", amount=amount, min_spend=min_spend,
                                     expiry_date=exp)

        if referral_bid and referral:
            amount = app.config.get("REFERRAL_BID_AMOUNT")
            UserService.update(referral.user_id, awarded_credit=amount)

        return user

    @classmethod
    def verify_email(cls, obj_id):
        user = User.query.get(obj_id)
        referral_coupon = app.config.get("REFERRAL_COUPON")
        referral_bid = app.config.get("REFERRAL_BID_CREDIT")

        user = UserService.update(user.id, email_verified=True, is_verified=True)

        referral = 0
        if referral_bid or referral_coupon:
            referral = Referral.query.filter_by(email=user.email).first()

        if referral:
            ReferralService.update(referral.id, is_verified=True)
        # Once OTP is verified on the first user
        if referral_coupon and referral:
            exp = datetime.now() + timedelta(days=45)
            amounts = app.config.get("REFERRAL_COUPON_AMOUNT")
            for amount in amounts:
                min_spend = amount * 5
                CouponService.create(user_id=referral.user_id, type="bob", amount=amount, min_spend=min_spend,
                                     expiry_date=exp)

        if referral_bid and referral:
            amount = app.config.get("REFERRAL_BID_AMOUNT")
            UserService.update(referral.user_id, awarded_credit=amount)

        return user
        #
        #
        # @classmethod
        # def verify_email(cls, obj_id, **kwargs):
        #
        #     user = User.query.get(obj_id)
        #     user = UserService.update(user.id, email_verified=True)
        #
        #     # Once OTP is verified on the first user
        #     merchant = user.merchant
        #     if merchant and merchant.email is None:
        #         MerchantService.update(merchant.id, email=user.email)
        #
        #     courier = user.courier
        #     if courier and courier.email is None:
        #         CourierService.update(courier.id, email=user.email)
        #
        #     return user

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        logger.info("in user index search")
        obj = BaseUserService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseUserService.get(obj_id)
        search.delete(obj)


class CustomerService(BaseCustomerService):
    """service used to create a new customer"""

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseCustomerService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseCustomerService.get(obj_id)
        search.delete(obj)

    @classmethod
    def migrate_user(cls, obj_id, **kwargs):
        obj = BaseCustomerService.get(obj_id)
        logger.info(obj)
        password = kwargs.get("pass","123@Abc")
        data=obj.as_dict()
        data["first_name"]=obj.name

        data["password"]=password
        data["verify_password"]=password
        logger.info(data)

        username=""
        try:
            username = data.get("email").split("@")[0]
        except:
            username = slugify(data.get("name","profile"))

        username = "%s%s"%(username,obj.id).strip()

        data["username"]=username
        data["last_name"] = " "
        # data.pop("is_incognito")
        data.pop("id")
        data.pop("user_id")
        data.pop("last_updated")
        data.pop("date_created")
        data.pop("notes")
        data.pop("accept_marketing")
        data.pop("name")
        user = UserService.create(**data)
        logger.info("user----------")
        logger.info(user)
        obj = BaseCustomerService.update(obj_id, user_id=user.id, is_incognito=False)
        return obj
