"""
auction.py
@Author: rusty & kunsam002
Search service responsible for indexing and querying the search engine
"""

from buyorbid.services import *
from buyorbid.services import search

BaseAuctionService = ServiceFactory.create_service(Auction, db)
AuctionRequestService = ServiceFactory.create_service(AuctionRequest, db)
BaseBidService = ServiceFactory.create_service(Bid, db)
BaseCreditService = ServiceFactory.create_service(Credit, db)
BaseCreditTypeService = ServiceFactory.create_service(CreditType, db)
BaseEarningService = ServiceFactory.create_service(Earning, db)


class AuctionService(BaseAuctionService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        variant = Variant.query.get(kwargs.get("variant_id"))
        product = variant.product
        data = {"name": "%s - %s" % (product.name, variant.name), "variant_id": variant.id,
                "description": product.description,
                "title": product.name, "value": variant.price, "cover_image_id": product.cover_image.id}
        obj = BaseAuctionService.create(**data)
        for i in product.images:
            i.auction_id = obj.id
            db.session.add(i)
            db.session.commit()
        return obj

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseAuctionService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseAuctionService.get(obj_id)
        search.delete(obj)


class BidService(BaseBidService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseBidService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseBidService.get(obj_id)
        search.delete(obj)


class CreditService(BaseCreditService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseCreditService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseCreditService.get(obj_id)
        search.delete(obj)


class CreditTypeService(BaseCreditTypeService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseCreditTypeService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseCreditTypeService.get(obj_id)
        search.delete(obj)


class EarningService(BaseEarningService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseEarningService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseEarningService.get(obj_id)
        search.delete(obj)
