# from ext.base.exceptions import ObjectNotFoundException, ActionDeniedException, ValidationFailed, \
#     FurtherActionException
#
# from ext.services.orm import ServiceFactory
# from ext.base.utils import DateJSONEncoder, generate_key, token_generator, number_format, id_generator
# from sqlalchemy.sql.expression import or_

# from buyorbid.models import *
# # from buyorbid import logger, celery, billing, notifier, bcrypt
# from buyorbid.services import product
# import requests
# from datetime import date, datetime, timedelta
# from flask import render_template_string, url_for
#
# from bs4 import BeautifulSoup
# from itertools import groupby
# from elasticsearch_dsl import Q, A
# import pprint
from buyorbid.services import *
from buyorbid.services import search
from buyorbid import handle_uploaded_photos
from PIL import Image as PImage

BaseAddressService = ServiceFactory.create_service(Address, db)
BaseCountryService = ServiceFactory.create_service(Country, db)
BaseStateService = ServiceFactory.create_service(State, db)
BaseCityService = ServiceFactory.create_service(City, db)
BaseReferralService = ServiceFactory.create_service(Referral, db)
BaseCouponService = ServiceFactory.create_service(Coupon, db)
BasePaymentChannelService = ServiceFactory.create_service(PaymentChannel, db)
BasePaymentOptionService = ServiceFactory.create_service(PaymentOption, db)
BasePaymentStatusService = ServiceFactory.create_service(PaymentStatus, db)
BaseDeliveryOptionService = ServiceFactory.create_service(DeliveryOption, db)
BaseDeliveryStatusService = ServiceFactory.create_service(DeliveryStatus, db)
BaseOrderStatusService = ServiceFactory.create_service(OrderStatus, db)
BaseTransactionStatusService = ServiceFactory.create_service(TransactionStatus, db)
AdminMessageService = ServiceFactory.create_service(AdminMessage, db)
AdminMessageResponseService = ServiceFactory.create_service(AdminMessageResponse, db)


def update_user_referrer_count(obj_id):
    user = User.query.get(obj_id)
    if user:
        user.referral_count = user.referral_count + 1
        db.session.add(user)
        db.session.commit()
        return True
    else:
        return False


class ReferralService(BaseReferralService):
    """service used to create a new customer"""

    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        user_id = kwargs.get("user_id", None)
        if user_id:
            user = update_user_referrer_count(user_id)
        obj = BaseReferralService.create(**kwargs)
        return obj

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseReferralService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseReferralService.get(obj_id)
        search.delete(obj)


class AddressService(BaseAddressService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseAddressService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseAddressService.get(obj_id)
        search.delete(obj)


class CountryService(BaseCountryService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseCountryService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseCountryService.get(obj_id)
        search.delete(obj)


class CityService(BaseCityService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseCityService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseCityService.get(obj_id)
        search.delete(obj)


class StateService(BaseStateService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseStateService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseStateService.get(obj_id)
        search.delete(obj)


class CouponService(BaseCouponService):
    """
    create : signature - type, amount, expiry_date, category_id(o)
    """

    @classmethod
    def apply_coupon(cls, coupon_ids, base_amount, **kwargs):
        """apply all coupons uses for an order
            :param coupon_ids: list. a list of coupon ids
            :param base_amount: float. amount to deduct from
        """

        coupons = Coupon.query.filter(Coupon.id.in_(coupon_ids)).all()

        total_val = sum([c.amount for c in coupons])
        new_amount = base_amount - total_val

        for coupon in coupons:
            cls.update(coupon.id, is_used=True, **kwargs)

        return new_amount

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseCouponService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseCouponService.get(obj_id)
        search.delete(obj)


class PaymentChannelService(BasePaymentChannelService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BasePaymentChannelService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BasePaymentChannelService.get(obj_id)
        search.delete(obj)


class PaymentOptionService(BasePaymentOptionService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BasePaymentOptionService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BasePaymentOptionService.get(obj_id)
        search.delete(obj)


class PaymentStatusService(BasePaymentStatusService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BasePaymentStatusService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BasePaymentStatusService.get(obj_id)
        search.delete(obj)


class DeliveryOptionService(BaseDeliveryOptionService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseDeliveryOptionService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseDeliveryOptionService.get(obj_id)
        search.delete(obj)


class DeliveryStatusService(BaseDeliveryStatusService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseDeliveryStatusService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseDeliveryStatusService.get(obj_id)
        search.delete(obj)


class OrderStatusService(BaseOrderStatusService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseOrderStatusService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseOrderStatusService.get(obj_id)
        search.delete(obj)


class TransactionStatusService(BaseTransactionStatusService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseTransactionStatusService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseTransactionStatusService.get(obj_id)
        search.delete(obj)
