from buyorbid import app, logger, db, oauth
from buyorbid.models import *
import requests
from base64 import b64encode
from ext.base.utils import id_generator
import imaplib, email, re


class Oauth:
    def __init__(self, name, base_url, request_url, access_url, authorize_url, key, secret, token_params=None):
        self.auth = oauth.remote_app(name,
                                     base_url=base_url,
                                     request_token_url=request_url,
                                     access_token_url=access_url,
                                     authorize_url=authorize_url,
                                     consumer_key=key,
                                     consumer_secret=secret,
                                     request_token_params=token_params
                                     )

    def authorize(self, url, next_url=None):
        return self.auth.authorize(callback=url)

    def post(self, url, data):
        response = self.auth.post(url, data=data)
        return response


twitter = Oauth(name='twitter',
                base_url='https://api.twitter.com/1.1',
                request_url='https://api.twitter.com/oauth/request_token',
                access_url='https://api.twitter.com/oauth/access_token',
                authorize_url='https://api.twitter.com/oauth/authorize',
                key=app.config.get('TWITTER_CLIENT_ID'),
                secret=app.config.get('TWITTER_CLIENT_SECRET')
                )

facebook = Oauth(name='facebook',
                 base_url='https://graph.facebook.com/',
                 request_url=None,
                 access_url='/oauth/access_token',
                 authorize_url='https://www.facebook.com/dialog/oauth',
                 key=app.config.get('FACEBOOK_APP_ID'),
                 secret=app.config.get('FACEBOOK_APP_SECRET'),
                 token_params={'scope': 'email'}
                 )
