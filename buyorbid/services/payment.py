from buyorbid.services import *
# import base64
# import pprint
# from datetime import timedelta
#
# import time

from buyorbid.services.product import TransactionService
# from ext.base.exceptions import ValidationFailed, FurtherActionException, ObjectNotFoundException
# from ext.base.utils import decrypt_data, code_generator, generate_key
# from ext.integrations import pycard
# from sqlalchemy import func, or_, and_
# from sendbox_payments.async import *
# from buyorbid.models import *
# from ext.services.orm import ServiceFactory
from buyorbid import flutterwave
import bcrypt as byc
import json
from buyorbid.services import search

BaseCurrencyService = ServiceFactory.create_service(Currency, db)
BaseCardService = ServiceFactory.create_service(RecurrentCard, db)
# BaseWalletService = ServiceFactory.create_service(Wallet, db)
# BaseBankAccountService = ServiceFactory.create_service(BankAccount, db)
# BaseBankService = ServiceFactory.create_service(BankAccount, db)
BaseTransactionService = ServiceFactory.create_service(Transaction, db)


class CurrencyService(BaseCurrencyService):
    """service used to create a new customer"""

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseCurrencyService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseCurrencyService.get(obj_id)
        search.delete(obj)



# class TransactionService(BaseTransactionService):
#     @classmethod
#     def confirm_vbv(cls, trans_code, **kwargs):
#         """ Confirm transaction that is authorized via VBVSecure Code """
#
#         logger.info('Printing inside confirm_vbv')
#         pprint.pprint(kwargs)
#
#         # Failsafe mechanism for when the response structure changes
#         resp = kwargs.get("resp")
#         logger.info("printing response data")
#         pprint.pprint(resp[0])
#
#         data = json.loads(str(resp[0]))
#         logger.info("printing response data from json")
#         pprint.pprint(data)
#
#         trans = Transaction.query.filter(Transaction.code == str(trans_code.upper())).first()
#
#         if not trans:
#             raise ObjectNotFoundException(Transaction, trans_code)
#
#         res_code = data.get("responsecode", None)
#         response_message = data.get("responsemessage", None)
#         response_token = data.get("responsetoken", None)
#
#         status_code = trans.status_code
#
#         if res_code and res_code in ["0", "00"]:
#             data['response_status'] = 'success'
#             data['response_code'] = res_code
#             data['response_message'] = response_message
#             status_code = 'successful'
#
#         else:
#             data['response_status'] = 'failed'
#             data['response_code'] = res_code
#             data['response_message'] = response_message
#             status_code = 'failed'
#
#         trans = TransactionService.update(trans.id, status_code=status_code, **data)
#
#         # Expire the request here
#         # RequestService.update(request_obj.id, is_expired=True)
#
#         return trans

    # @classmethod
    # def confirm_ib(cls, trans_code, **kwargs):
    #     """ Confirm transaction that is authorized via VBVSecure Code """
    #
    #     logger.info('Printing inside confirm_vbv')
    #     pprint.pprint(kwargs)
    #
    #     # Failsafe mechanism for when the response structure changes
    #     resp = kwargs.get("resp")
    #     logger.info("printing response data")
    #     pprint.pprint(resp[0])
    #
    #     data = json.loads(str(resp[0]))
    #     logger.info("printing response data from json")
    #     pprint.pprint(data)
    #
    #     trans = Transaction.query.filter(Transaction.code == str(trans_code.upper())).first()
    #
    #     if not trans:
    #         raise ObjectNotFoundException(Transaction, trans_code)
    #
    #     res_code = data.get("responsecode", None)
    #     response_message = data.get("responsemessage", None)
    #     response_token = data.get("responsetoken", None)
    #
    #     status_code = trans.status_code
    #
    #     if res_code and res_code in ["0", "00"]:
    #         data['response_status'] = 'success'
    #         data['response_code'] = res_code
    #         data['response_message'] = response_message
    #         status_code = 'success'
    #
    #     else:
    #         data['response_status'] = 'failed'
    #         data['response_code'] = res_code
    #         data['response_message'] = response_message
    #         status_code = 'failed'
    #
    #     trans = TransactionService.update(trans.id, status_code=status_code, **data)
    #
    #     # Expire the request here
    #     # RequestService.update(request_obj.id, is_expired=True)
    #
    #     return trans

    # @classmethod
    # def capture(cls, trans_code, **kwargs):
    #     """ charge a card for a preauthorized transaction"""
    #
    #     trans = Transaction.query.filter(func.upper(Transaction.code) == str(trans_code.upper())).first()
    #
    #     if not trans:
    #         raise ObjectNotFoundException(Transaction, trans_code)
    #
    #     kwargs['trxreference'] = trans.transactionreference
    #     kwargs['trxauthorizeid'] = trans.authorizeid
    #     kwargs['currency'] = trans.currency
    #     kwargs['amount'] = trans.amount
    #
    #     res = flutterwave.card.capture(**kwargs)
    #
    #     data = res.get('data')
    #     status = res.get('status')
    #     res_code = data.get('responsecode', None)
    #
    #     status_code = trans.status_code
    #
    #     if status == "success" and res_code in ["0", "00"]:
    #         kwargs['response_status'] = status
    #         kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
    #         kwargs['response_code'] = res_code
    #         status_code = 'captured'
    #         # notifier.send()  # to.do: send out a notification here............
    #
    #     else:
    #         kwargs['response_status'] = status
    #         kwargs['response_message'] = res.get('responsemessage')  # get response message from res
    #         kwargs['response_code'] = res_code
    #         status_code = 'failed'
    #         # notifier.send()  # to.do: send out a notification here............
    #
    #     trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)
    #
    #     return trans
    #
    # @classmethod
    # def refund(cls, trans_code, **kwargs):
    #     """ refund a captured transaction """
    #     amount = kwargs.get('amount', None)
    #
    #     trans = Transaction.query.filter(func.upper(Transaction.code) == str(trans_code.upper())).first()
    #     trans_data = trans.as_dict()
    #     print "===data==="
    #     pprint.pprint(trans_data)
    #
    #     if not trans:
    #         raise ObjectNotFoundException(Transaction, trans_code)
    #
    #     trans_data['trxreference'] = trans.transactionreference
    #     trans_data['trxauthorizeid'] = trans.authorizeid
    #     trans_data['currency'] = trans.currency
    #     trans_data['amount'] = trans.amount
    #     if amount:
    #         trans_data['amount'] = amount
    #
    #     res = flutterwave.card.refund(**trans_data)
    #
    #     data = res.get('data')
    #     status = res.get('status')
    #     res_code = data.get('responsecode', None)
    #
    #     status_code = trans.status_code
    #
    #     if status == "success" and res_code in ["0", "00"]:
    #         trans_data['response_status'] = status
    #         trans_data['response_message'] = data.get('responsemessage', None)  # get response message from res
    #         trans_data['response_code'] = res_code
    #         status_code = 'refunded'
    #         # notifier.send()  # to.do: send out a notification here............
    #         trans_data.pop('status_code')
    #         trans_data.pop('code')
    #         trans = TransactionService.create(status_code=status_code, code=code_generator(), **trans_data)
    #     else:
    #         trans_data['response_status'] = status
    #         trans_data['response_message'] = res.get('responsemessage')  # get response message from res
    #         trans_data['response_code'] = res_code
    #         status_code = 'failed'
    #         # notifier.send()  # to.do: send out a notification here............
    #         trans_data.pop('status_code')
    #         trans_data.pop('code')
    #         trans = TransactionService.create(status_code=status_code, code=code_generator(), **trans_data)
    #
    #     # trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)
    #
    #     return trans
    #
    # @classmethod
    # def fl_refund(cls, trans_code, **kwargs):
    #     """ refund a captured transaction """
    #
    #     trans = Transaction.query.filter(Transaction.code == str(trans_code.upper())).first()
    #
    #     if not trans:
    #         raise ObjectNotFoundException(Transaction, trans_code)
    #
    #     kwargs['paymentreference'] = trans.transactionreference
    #     kwargs['refundamount'] = 500
    #
    #     res = flutterwave.card.fl_refund(**kwargs)
    #
    #     data = res.get('data')
    #     status = res.get('status')
    #     res_code = data.get('responsecode', None)
    #
    #     status_code = trans.status_code
    #
    #     if status == "success" and res_code in ["0", "00"]:
    #         kwargs['response_status'] = status
    #         kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
    #         kwargs['response_code'] = res_code
    #         status_code = 'fl_refunded'
    #         # notifier.send()  # to.do: send out a notification here............
    #         trans = TransactionService.create(status_code=status_code, **kwargs)
    #     else:
    #         kwargs['response_status'] = status
    #         kwargs['response_message'] = res.get('responsemessage')  # get response message from res
    #         kwargs['response_code'] = res_code
    #         status_code = 'failed'
    #         # notifier.send()  # to.do: send out a notification here............
    #
    #         trans = TransactionService.create(status_code=status_code, **kwargs)
    #
    #     return trans
    #
    # @classmethod
    # def void(cls, trans_code, **kwargs):
    #     """cancel preauthorizated charge of a transaction"""
    #
    #     trans = Transaction.query.filter(Transaction.code == str(trans_code.upper())).first()
    #
    #     if not trans:
    #         raise ObjectNotFoundException(Transaction, trans_code)
    #
    #     kwargs['trxreference'] = trans.transactionreference
    #     kwargs['trxauthorizeid'] = trans.authorizeid
    #     kwargs['currency'] = trans.currency
    #     kwargs['amount'] = trans.amount
    #
    #     res = flutterwave.card.void(**kwargs)
    #
    #     data = res.get('data')
    #     status = res.get('status')
    #     res_code = data.get('responsecode', None)
    #
    #     status_code = trans.status_code
    #
    #     if status == "success" and res_code in ["0", "00"]:
    #         kwargs['response_status'] = status
    #         kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
    #         kwargs['response_code'] = res_code
    #         status_code = 'voided'
    #     else:
    #         kwargs['response_status'] = status
    #         kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
    #         kwargs['response_code'] = res_code
    #         status_code = 'failed'
    #
    #     trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)
    #
    #     return trans


class CardService(BaseCardService):
    """"""

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseCardService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseCardService.get(obj_id)
        search.delete(obj)


    @classmethod
    def charge(cls, card_id, **kwargs):
        """method used for a payment by a saved card"""


        card = cls.get(card_id)

        if not card:
            raise ObjectNotFoundException(RecurrentCard, card_id)

        kwargs['chargetoken'] = card.token
        kwargs['currency'] = "NGN"
        kwargs['custid'] = "ijektngkdl"
        print '=======kwargs==============='
        pprint.pprint(kwargs)
        # trans = TransactionService.create(status_code='pending', type_="debit", mode='card', **kwargs)

        res = flutterwave.card.charge(**kwargs)

        data = res.get('data')
        status = res.get('status')
        res_code = data.get('responsecode', None)

        # status_code = trans.status_code

        if status == "success" and res_code in ["0", "00"]:
            kwargs['response_status'] = status
            kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
            kwargs['response_code'] = res_code
            kwargs['transaction_reference'] = data.get('transactionreference', None)
            kwargs['status'] = 'successful'
            # trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)
            # return trans
        else:
            kwargs['response_status'] = status
            kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
            kwargs['response_code'] = res_code
            kwargs['status'] = 'failed'
            raise Exception
            # notifier.send()  # to.do: send out a notification here............

            # trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)

            # return trans
        return kwargs

    # @classmethod
    # def preauthorize(cls, card_key, **kwargs):
    #     """"""
    #     card = RecurrentCard.query.filter(func.lower(RecurrentCard.key) == str(card_key.lower())).first()
    #
    #     if not card:
    #         raise ObjectNotFoundException(RecurrentCard, card_key)
    #
    #     kwargs['chargetoken'] = card.token
    #     kwargs['code'] = code_generator()
    #     kwargs['mask'] = card.mask
    #     kwargs['brand'] = card.brand
    #     kwargs['method'] = "card"
    #     kwargs['card_key'] = card.key
    #     kwargs['authmodel'] = flutterwave.AUTHMODEL.NOAUTH
    #     kwargs['account_token'] = card.token
    #     kwargs['customer_key'] = card.customer.key
    #     kwargs['merchant_key'] = card.merchant.key
    #     customer = Customer.query.get(card.customer_id)
    #
    #     status_code = 'pre_authorized'
    #     trans = TransactionService.create(status_code=status_code, **kwargs)
    #
    #     res = flutterwave.card.preauthorize(**kwargs)
    #
    #     data = res.get('data')
    #     status = res.get('status')
    #     res_code = data.get('responsecode', None)
    #
    #     if status == "success" and res_code in ["0", "00"]:
    #         kwargs['response_status'] = status
    #         kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
    #         kwargs['response_code'] = res_code
    #         kwargs['transactionreference'] = data.get('transactionreference', None)
    #         kwargs['authorizeid'] = data.get('authorizeId', None)
    #         status_code = 'pre_authorized'
    #         # notifier.send()  # to.do: send out a notification here............
    #     else:
    #         kwargs['response_status'] = status
    #         kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
    #         kwargs['response_code'] = res_code
    #         status_code = 'failed'
    #         # notifier.send()  # to.do: send out a notification here............
    #
    #     trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)
    #
    #     trans_data = trans.level_dict()
    #     trans_data['merchant_key'] = kwargs.get('merchant_key')
    #
    #     if trans.status_code == 'pre_authorized':
    #         send_preauthorise_notification.delay(customer.id, 'prc', 'Pre-authorized Payment Successful', **trans_data)
    #     else:
    #         send_preauthorise_notification.delay(customer.id, 'prf', 'Pre-authorized Payment Failed', **trans_data)
    #
    #     return trans

    @classmethod
    def pay(cls, **kwargs):
        """method used for a payment by an unsaved card"""

        logger.info('before we start')
        pprint.pprint(kwargs)

        card_data = cls.check_card(**kwargs)
        brand = card_data.get('brand')
        cvv = card_data.get('cvv')
        pin = kwargs.get("pin", '1234')
        # pin = kwargs.get("pin", None)

        kwargs.update(card_data)

        authmodel = flutterwave.AUTHMODEL.choice(brand)
        validateoption = flutterwave.VALIDATEOPTION.SMS

        kwargs['code'] = code_generator()
        kwargs['method'] = "card"
        kwargs['mask'] = card_data.get('mask')
        kwargs['expiryyear'] = card_data.get('expiryyear')
        kwargs['exp_year'] = card_data.get('expiryyear')
        kwargs['expirymonth'] = card_data.get('expirymonth')
        kwargs['exp_month'] = card_data.get('expirymonth')
        kwargs['brand'] = brand
        kwargs['cvv'] = str(cvv)
        kwargs['pin'] = str(pin)
        # kwargs['pin'] = pin
        kwargs['currency'] = "NGN"
        kwargs['authmodel'] = authmodel
        kwargs['validateoption'] = validateoption

        # kwargs['narration'] = kwargs.get('narration', "payment for product")
        kwargs['customerId'] = 'ad4578'
        kwargs['custid'] = 'ad4578'

        pprint.pprint("Just before creating transaction")
        pprint.pprint(card_data)

        kwargs['authmodel'] = authmodel

        trans = TransactionService.get(kwargs.get("trans_id"))

        if authmodel == flutterwave.AUTHMODEL.VBVSECURECODE:
            ""
            kwargs['responseurl'] = trans.response_url


        # check that the pin is required. This action will happen only when the authmodel is not vbvsecurecode
        # if authmodel != flutterwave.AUTHMODEL.VBVSECURECODE and not pin:
        #     # take in the pin number and start again
        #     raise FurtherActionException(kwargs)

        pprint.pprint("card data for FLW")
        pprint.pprint(kwargs)
        try:
            res = flutterwave.card.pay(**kwargs)
            print "===res==="
            pprint.pprint(res)

            data = res.get('data')
            status = res.get('status')
            res_code = data.get('responsecode', None)
        except Exception, e:
            print e
            raise ValidationFailed({"data":e})

        if status == "success" and res_code in ["02"]:
            kwargs['response_status'] = status
            kwargs['response_message'] = data.get('responsemessage', None) # get response message from res
            kwargs['otptransactionidentifier'] = data.get('otptransactionidentifier')
            kwargs['response_code'] = res_code
            kwargs['transaction_reference'] = data.get('transactionreference', None)
            # trans = TransactionService.update(trans.id, status_code='validation_required', **kwargs)
            # data["code"] = trans.code
            raise FurtherActionException(data)

        elif status == "success" and res_code in ["00", "0"]:
            kwargs['response_status'] = status
            kwargs['status'] = "successful"
            kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
            kwargs['token'] = data.get('responsetoken', None)
            kwargs['response_code'] = res_code
            kwargs['transaction_reference'] = data.get('transactionreference', None)

        else:
            kwargs['response_status'] = status
            kwargs['status'] = "failed"
            kwargs['response_message'] = data.get('responsemessage', None)
            kwargs['response_code'] = res_code
            kwargs['transaction_reference'] = data.get('transactionreference', None)
            raise ValidationFailed({"cardno": ["Invalid or expired card"]})

        # return trans
        return kwargs

    @classmethod
    def validate(cls, code, **kwargs):

        # trans = Transaction.query.filter(Transaction.code == str(code.upper())).first()

        # if not trans:
        #     raise ObjectNotFoundException(Transaction, code)
        #
        # kwargs['trxreference'] = trans.transactionreference
        # kwargs['otptransactionidentifier'] = trans.otptransactionidentifier
        # kwargs['currency'] = trans.currency
        # kwargs['amount'] = trans.amount

        res = flutterwave.card.validate(**kwargs)

        data = res.get('data')
        status = res.get('status')
        res_code = data.get('responsecode', None)

        # status_code = trans.status_code
        status_code = ""
        # request_obj = Request.query.filter_by(request_key=trans.request_key).first()

        if status == "success" and res_code in ["0", "00", "200"]:
            kwargs['response_status'] = status
            kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
            kwargs['response_code'] = res_code
            kwargs['token'] = data.get("responsetoken", None)
            status_code = 'successful'
        else:
            kwargs['response_status'] = status
            kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
            kwargs['response_code'] = res_code
            status_code = 'failed'

        # trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)
        # trans_data = trans.level_dict()
        # trans_data['merchant_key'] = request_obj.merchant_key

        # if trans.status_code == 'successful':
        #     send_payment_notification.delay(request_obj.id, 'pc', 'Payment Successful', **trans_data)
        # else:
        #     send_payment_notification.delay(request_obj.id, 'pf', 'Payment Failed', **trans_data)

        # return trans

    @classmethod
    def check_card(cls, **kwargs):
        """ Validate the card and return the required values for storage """

        cardno = kwargs.get("card_number")
        expiry_year = int(kwargs.get("expiry_year"))
        expiry_month = int(kwargs.get("expiry_month"))
        cvv = kwargs.get("cvv")
        pin = kwargs.get("pin")

        # clean the parameters of the card
        # expiry_month, expiry_year = expiry.split("/")
        # expiry_month = int(expiry_month)
        # expiry_year = int(expiry_year)
        cvv = cvv

        card = pycard.Card(number=cardno, month=expiry_month, year=expiry_year, cvc=cvv)

        if card.is_valid and not card.is_expired:
            return dict(cardno=card.number, expirymonth=card.exp_date.mm, expiryyear=card.exp_date.yy,
                        friendly_brand=card.friendly_brand,
                        cvv=card.cvc, brand=card.brand, mask=card.mask, is_expired=card.is_expired, pin=pin)
        else:
            raise ValidationFailed({"cardno": ["Invalid card supplied"]})


    @classmethod
    def register(cls, **kwargs):

        card_data = cls.check_card(**kwargs)
        request_key = kwargs.get('request_key')

        print card_data, "=====card_data======"

        authmodel = flutterwave.AUTHMODEL.NOAUTH
        kwargs['authmodel'] = authmodel
        kwargs['currency'] = flutterwave.CURRENCYMODEL.NGN
        kwargs['validateoption'] = flutterwave.VALIDATEOPTION.SMS

        kwargs.update(card_data)

        res = flutterwave.card.tokenize(**kwargs)
        print  res, "=====tokenize======="

        data = res.get('data')
        status = res.get('status')
        res_code = data.get('responsecode', None)


        if status == "success" and res_code in ["0", "00", "200"]:

            kwargs['token'] = data.get('responsetoken')
            kwargs['exp_month'] = kwargs.get('expirymonth', None)
            kwargs['exp_year'] = kwargs.get('expiryyear', None)
            kwargs['mask'] = card_data.get('mask')
            kwargs['brand'] = card_data.get('brand')
            card = cls.create(**kwargs)
            return card_data

        else:
            kwargs['response_status'] = status
            kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
            kwargs['response_code'] = res_code

            raise ValidationFailed({"cardno": ["Invalid or expired card"]})



# class BankAccountService(BaseBankAccountService):
#
#     @classmethod
#     def create(cls, **kwargs):
#
#         res = flutterwave.bank.account_enquiry
#
#         data = res.get('data')
#
#         merchant_key = kwargs.get('merchant_key', None)
#
#         if merchant_key:
#             merchant = Merchant.query.filter(Merchant.key == merchant_key).first()
#             account = cls.create(merchant_id=merchant.id, **kwargs)
#         else:
#             merchant = MerchantService.create(**kwargs)
#             account = cls.create(merchant_id=merchant.id, **kwargs)
#
#         return account
#
#
# class BankService(BaseBankService):
#
#     @classmethod
#     def check_account(cls, **kwargs):
#         flw = dict()
#
#         bank_code = kwargs.get('bank_code')
#         state_code = kwargs.get('state_code')
#         country_code = kwargs.get('country_code')
#         request_key = kwargs.get('request_key')
#
#         request_obj = Request.query.filter(Request.request_key == request_key).first()
#
#         bank = Bank.query.get(bank_code)
#         state = State.query.get(state_code)
#         country = Country.query.get(country_code)
#
#         flw["destbankcode"] = kwargs.get('bank_code')
#         flw["recipientaccount"] = kwargs.get('bank_account_no')
#         res = flutterwave.bank.account_enquiry(**flw)
#
#         data = res.get('data')
#         status = res.get('status')
#         res_code = data.get('responsecode', None)
#
#         if status == "success" and res_code in ["0", "00"]:
#             li = list(data.get('accountnumber'))
#             mask = ''.join(['*' for i in li[:-4]] + [str(i) for i in li[-4:]])
#             res['response_status'] = status
#             res['response_message'] = data.get('responsemessage', None)  # get response message from res
#             res['response_code'] = res_code
#             res['account_name'] = data.get('accountname', None)
#             res['account_number'] = data.get('accountnumber', None)
#             res['mask'] = mask
#             # res['bank'] = bank.as_dict()
#             # res['country'] = country.as_dict()
#             # res['state'] = state.as_dict()
#             res['application_id'] = request_obj.application_id
#             res['status'] = 'successful'
#         else:
#             res['response_status'] = status
#             res['response_message'] = data.get('responsemessage', None)  # get response message from res
#             res['response_code'] = res_code
#
#             raise ValidationFailed(
#                 {"bank_account_no": ["Invalid bank account"], "bank_code": ["Invalid bank selected"]})
#
#         kwargs.update(**res)
#         return kwargs
#
#     @classmethod
#     def pay(cls, **kwargs):
#         """method used for charging bank accounts"""
#
#         logger.info('before we start')
#         pprint.pprint(kwargs)
#
#         acct_data = cls.check_account(**kwargs)
#         bank_code = acct_data.get('bank_code')
#         # bank_code = '058'
#         pin = kwargs.get("pin", None)
#
#         kwargs.update(acct_data)
#         kwargs.pop('data', '')
#         kwargs.pop('status', '')
#         kwargs.pop('response_message', '')
#         kwargs.pop('response_status', '')
#
#         authmodel = flutterwave.AUTHMODEL.choice(bank_code, authmodel=flutterwave.AUTHMODEL.NOAUTH)
#         validateoption = flutterwave.VALIDATEOPTION.PHONE_OTP
#
#         kwargs['code'] = code_generator()
#         kwargs['method'] = "bank"
#         kwargs['mask'] = acct_data.get('mask')
#         kwargs['accountnumber'] = acct_data.get('account_number')
#         # kwargs['accountnumber'] = '0156611412'
#         kwargs['authmodel'] = authmodel
#         kwargs['validateoption'] = validateoption
#
#         request_key = kwargs.get("request_key", None)
#
#         if not request_key:
#             raise ObjectNotFoundException(Request, request_key)
#
#         request_obj = Request.query.filter(Request.request_key == request_key).first()
#         name = request_obj.name
#         bits = name.split(" ")
#         if len(bits ) >=2:
#             kwargs['firstname'], kwargs['lastname'] = bits[0], " ".join(bits[1:])
#
#         kwargs['callback'] = request_obj.callback
#         kwargs['merchant_key'] = request_obj.merchant_key
#         kwargs['narration'] = request_obj.narration
#         kwargs['amount'] = request_obj.amount
#         kwargs['currency'] = request_obj.currency
#         kwargs['phonenumber'] = request_obj.phone
#         kwargs['email'] = request_obj.email
#         kwargs['bank_code'] = bank_code
#         kwargs['custid'] = request_obj.client_id
#
#         pprint.pprint("Just before creating transaction")
#         # pprint.pprint(acct_data)
#         pprint.pprint(kwargs)
#
#         trans = TransactionService.create(**kwargs)
#
#         kwargs['authmodel'] = authmodel
#         kwargs['transactionreference'] = trans.code
#
#         if authmodel == flutterwave.AUTHMODEL.INTERNET_BANKING:
#             kwargs['responseurl'] = trans.ib_response_url
#             kwargs['validateoption'] = trans.validateoption
#
#
#         pprint.pprint("bank data for FLW")
#         pprint.pprint(kwargs)
#         res = flutterwave.bank.account_pay(**kwargs)
#         print '======flutter res======'
#         pprint.pprint(res)
#
#         data = res.get('data')
#         status = res.get('status')
#         res_code = data.get('responsecode', None)
#
#         if status == "success" and res_code in ["02"]:
#             kwargs['response_status'] = status
#             kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
#             # kwargs['otptransactionidentifier'] = data.get('otptransactionidentifier')
#             kwargs['response_code'] = res_code
#             kwargs['transaction_reference'] = data.get('transactionreference', None)
#             trans = TransactionService.update(trans.id, status_code='validation_required', **kwargs)
#             data["code"] = trans.code
#             raise FurtherActionException(data)
#
#         elif status == "success" and res_code in ["00", "0"]:
#             kwargs['response_status'] = status
#             kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
#             kwargs['account_token'] = data.get('responsetoken', None)
#             kwargs['response_code'] = res_code
#             kwargs['transaction_reference'] = data.get('transactionreference', None)
#             trans = TransactionService.update(trans.id, status_code='successful', **kwargs)
#
#             # send notification
#             send_payment_notification.delay(request_obj.id, 'pc', 'Payment Successful', **trans.level_dict())
#
#         else:
#             kwargs['response_status'] = status
#             kwargs['response_message'] = data.get('responsemessage', None)
#             kwargs['response_code'] = res_code
#             kwargs['transaction_reference'] = data.get('transactionreference', None)
#             transaction = TransactionService.update(trans.id, status_code='failed', **kwargs)
#
#             # send notification
#             send_payment_notification.delay(request_obj.id, 'pf', 'Payment Failed', **trans.level_dict())
#             raise ValidationFailed({"Bank": ["Payment could not be completed"]})
#
#         return trans
