from buyorbid.async import send_notification
from buyorbid.services import *
from buyorbid.services.core import *
from buyorbid.services import search
from buyorbid import celery

# import json
# import os
# from cloudinary.uploader import upload
# import itertools
# from flask_restful import abort
# import cloudinary
# from marshmallow.utils import pprint
# from ext.base import utils
# from buyorbid.models import *
# from buyorbid.services.core import *
# from buyorbid.services import auction
# from ext.services.orm import ServiceFactory
# from ext.base.utils import slugify, id_generator
#
# cloudinary.config(
#     cloud_name='rusty-x',
#     api_key='629579476381886',
#     api_secret='OjwKGxaV4e-oyo7RrEE5ulqr-3g'
# )

BaseProductService = ServiceFactory.create_service(Product, db)
BaseProductViewService = ServiceFactory.create_service(ProductView, db)
BaseProductDescriptionService = ServiceFactory.create_service(ProductDetail, db)
BaseProductDescriptionTypeService = ServiceFactory.create_service(ProductDetailType, db)
BaseVariantService = ServiceFactory.create_service(Variant, db)
BaseCartService = ServiceFactory.create_service(Cart, db)
BaseCartItemService = ServiceFactory.create_service(CartItem, db)
BaseOrderService = ServiceFactory.create_service(Order, db)
BaseOrderEntryService = ServiceFactory.create_service(OrderEntry, db)
# BaseCollectionService = ServiceFactory.create_service(Collection, db)
BaseCategoryService = ServiceFactory.create_service(Category, db)
BaseImageService = ServiceFactory.create_service(Image, db)
BaseTransactionService = ServiceFactory.create_service(Transaction, db)
BaseTransactionEntryService = ServiceFactory.create_service(TransactionEntry, db)
WishlistService = ServiceFactory.create_service(Wishlist, db)
WishlistEntryService = ServiceFactory.create_service(WishlistEntry, db)


@celery.task
def load_categories(product_id, cat_ids):
    """load categories to products outside of the service factory"""
    product = Product.query.get(product_id)
    categories = Category.query.filter(Category.id.in_(cat_ids)).all()
    product.categories = categories
    db.session.add(product)
    db.session.commit()
    search.index(product)


@celery.task
def index_parent_category(obj_id):
    obj = Category.query.get(obj_id)
    if obj:
        search.index(obj)


class ProductService(BaseProductService):
    """ Service class to handle product creation """

    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        """ Custom create method support """
        images = kwargs.pop('image_ids', [])
        # variants = kwargs.pop('variants', [])
        # attributes = kwargs.pop('attributes', [])
        # product_details = kwargs.pop('product_details', [])
        print kwargs, '====kwargs 1====='

        print kwargs, '========im the kwargs======'
        obj = BaseProductService.create(ignored_args=ignored_args, **kwargs)
        kwargs['product_id'] = obj.id
        # ProductDescriptionService.create(product_details=product_details)

        # for image in images:
        #     pic = ImageService.update(image, product_id=obj.id)
        #
        # kwargs.pop('name')
        # kwargs.pop('regular_price')
        # kwargs.pop('sku')
        var_obj = cls.generate_variants(obj.id, **kwargs)

        # obj = BaseProductService.create(ignored_args=ignored_args, **kwargs)
        # kwargs['product_id'] = obj.id
        # ProductDescriptionService.create(product_details=product_details)

        # for image in images:
        #     pic = ImageService.update(image, product_id=obj.id)

        return obj

    #
    # @classmethod
    # def update(cls, obj_id, **kwargs):
    #     image_ids = kwargs.pop('image_ids', [])
    #     variants = kwargs.pop('variants', [])
    #     attributes = kwargs.pop('attributes', [])
    #     product_details = kwargs.pop('product_details', [])
    #
    #     kwargs['variant_attributes'] = attributes
    #     product = cls.update(obj_id, **kwargs)
    #
    #     for item in variants:
    #         item_id = item.pop('id')
    #         variant = VariantService.update(item_id, **item)
    #
    #     for item in product_details:
    #         item_id = item.pop('id')
    #         detail = ProductDescriptionService.update(item_id, **item)
    #
    #     for id in image_ids:
    #         pic = ImageService.update(id, product_id=product.id)
    #
    #     return product

    @classmethod
    def set_cover_image(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        cover = obj.cover_image

        if cover:
            cover.is_cover = False
            ImageService.update(cover.id)

        image_id = kwargs.get('image_id')
        ImageService.update(image_id, is_cover=True)

    @classmethod
    def string_to_dict(cls, string):
        """ Get product attributes from input as string and parse
        it into a dictionary"""

        result = dict()
        d = string.split('|')

        e = [i.split(':') for i in d]
        for i in e:
            if len(i) > 1:
                key = i[0].strip()
                v = i[1].strip()
                result[key] = v
        return result

    @classmethod
    def get_attributes(cls, variant_attributes):
        """ Get product attributes from input as string and parse
        it into a dictionary"""

        string = variant_attributes
        d = string.split('|')

        e = [i.split(':') for i in d]
        attributes = []
        values = []
        for i in e:
            key = i[0].strip()
            v = i[1]
            value = v.split(',')
            value = [v.strip() for v in value]

            attributes.append(key)
            values.append(value)

        return [attributes, values]

    @classmethod
    def generate_variants(cls, obj_id, **kwargs):
        """ Spool all possible product variants from product attributes """
        kwargs['product_id'] = obj_id
        obj = cls.get(obj_id)
        if not obj.has_variants:
            variant = VariantService.create(**kwargs)
            return [variant]

        variant_attributes = obj.variant_attributes
        t = cls.get_attributes(variant_attributes)

        keys = '|'.join(t[0])

        values = t[1]
        variant_list = []
        variants = itertools.product(*values)
        kwargs.pop('name', None)
        for i in variants:
            name = ' '.join(i)
            # name = obj.name + ' ' + "<span style=\"color:red\">"+name+"</span>"
            kwargs.pop('regular_price', 0.0)
            kwargs.pop('quantity', 0)
            variant = VariantService.create(name=name, attribute_keys=keys,
                                            regular_price=obj.regular_price, **kwargs)
            variant_list.append(variant)
        return variant_list

    @classmethod
    def set_cover_image(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        cover = obj.cover_image

        if cover:
            cover.is_cover = False
            ImageService.update(cover.id)

        image_id = kwargs.get('image_id')
        ImageService.update(image_id, is_cover=True)

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseProductService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseProductService.get(obj_id)
        search.delete(obj)


class VariantService(BaseVariantService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        obj = BaseVariantService.create(**kwargs)
        return obj

    @classmethod
    def add(cls, obj_id, **kwargs):
        variant = VariantService.get(obj_id)
        if variant:
            variant.quantity += kwargs.get('quantity')
            quantity = variant.quantity

            variant = VariantService.update(obj_id, quantity=quantity)

        return variant

    # @classmethod
    # def create_auction(cls, obj_id, **kwargs):
    #     variant = VariantService.get(obj_id)
    #     data = {"variant_id": obj_id}
    #     auction.AuctionService.create(**data)
    #
    #     return variant

    @classmethod
    def set(cls, obj_id, **kwargs):
        variant = VariantService.update(obj_id, quantity=kwargs.get('quantity'))
        return variant

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseVariantService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseVariantService.get(obj_id)
        search.delete(obj)

    @classmethod
    def update(cls, obj_id, **kwargs):
        v = BaseVariantService.update(obj_id, **kwargs)
        ProductService.update(v.product_id)
        return v


class ProductDescriptionService(BaseProductDescriptionService):
    @classmethod
    def create(cls, **kwargs):
        des = kwargs.get('product_details', [])
        product_id = kwargs.get('product_id')

        if type(des) is list:
            product_descriptions = []
            for d in des:
                if not d.get('type_id'):
                    pass
                else:
                    d['product_id'] = kwargs.get('product_id', None)
                    obj = BaseProductDescriptionService.create(**d)
                    product_descriptions.append(obj)
            return product_descriptions
        else:
            des['product_id'] = kwargs.get('product_id', None)
            obj = BaseProductDescriptionService.create(**des)
            return obj

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseProductDescriptionService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseProductDescriptionService.get(obj_id)
        search.delete(obj)


class ProductDescriptionTypeService(BaseProductDescriptionTypeService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseProductDescriptionTypeService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseProductDescriptionTypeService.get(obj_id)
        search.delete(obj)


class CartService(BaseCartService):
    @classmethod
    def attach_user(cls, cart_id, user_id):
        user = User.query.get(user_id)
        if not user:
            raise Exception("User not found")
        CartItem.query.filter(CartItem.cart_id == cart_id).update({CartItem.user_id: user_id}, synchronize_session=False)
        db.session.commit()
        obj = cls.update(cart_id, user_id=user_id)
        return obj

    @classmethod
    def clear_cart(cls, cart_id):
        """ Clears the cart by deleting all items inside it """
        CartItem.query.filter(CartItem.cart_id == cart_id).delete()
        cls.update(cart_id, delivery_charge=0)

    @classmethod
    def delete(cls, cart_id):
        """ Deletes Cart """
        CartItem.query.filter(CartItem.cart_id == cart_id).delete()
        BaseCartService.delete(cart_id)
        return True

    @classmethod
    def add_item(cls, cart_id, **kwargs):
        """ Append a session cart item to the cart """
        cart_item = CartItem.query.filter(CartItem.cart_id == cart_id,
                                          CartItem.variant_id == kwargs.get('variant_id')).first()
        kwargs["quantity"] = kwargs.get("quantity", 1)
        if cart_item:
            cart_item.quantity += kwargs.get('quantity')
            quantity = cart_item.quantity
            cart_item.calculate_total()
            CartItemService.update(cart_item.id, quantity=quantity)
        else:
            cart_item = CartItemService.create(cart_id=cart_id, **kwargs)
            cart_item.calculate_total()
            CartItemService.update(cart_item.id)

        return cart_item

    @classmethod
    def merge_cart(cls, old_cart_id, new_cart_id):
        """"""

        old_cart = Cart.query.get(old_cart_id)
        new_cart = Cart.query.get(new_cart_id)

        for item in old_cart.cart_items.all():
            new_cart.cart_items.append(item)
        # d = item.as_dict()
        # d.pop('cart_id', None)
        # CartItemService.create(cart_id=new_cart_id, **d)
        db.session.add(new_cart)
        db.session.commit()
        db.session.delete(old_cart)
        db.session.commit()
        return new_cart


class CartItemService(BaseCartItemService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        variant = Variant.query.get(kwargs.get("variant_id"))
        kwargs["product_id"] = variant.product_id
        kwargs["price"] = variant.price
        obj = BaseCartItemService.create(**kwargs)

        return obj


class ImageService(BaseImageService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        if kwargs.get("filepath") is None:
            raise Exception('You have to upload at least one image')

        image = kwargs.get("filepath")
        upload_result = upload(image, public_id="SB" + str(datetime.today()))
        os.remove(image)
        kwargs["url"] = upload_result["url"]
        obj = BaseImageService.create(**kwargs)

        return obj


class OrderService(BaseOrderService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        order_entries = kwargs.pop('entries')
        country = kwargs.pop('country')
        state = kwargs.pop('state')
        obj = BaseOrderService.create(ignored_args=ignored_args, **kwargs)

        entry_list = []
        for item in order_entries:
            variant = VariantService.get(item.get('variant_id'))
            item['order_id'] = obj.id
            item.pop("product", None)
            item.pop("product_", None)
            item.pop('customer', None)
            cart = item.pop('cart', None)
            item['user_id'] = cart.get('user_id', None)
            item.pop('variant', None)
            item.pop('variant_', None)
            product = variant.product
            qty = item.get('quantity')
            price = variant.price
            total_p = qty * variant.price
            affiliate_price = variant.affiliate_price if variant.affiliate_price else product.affiliate_price
            item["is_in_house"] = product.is_in_house
            item["affiliate_price"] = product.affiliate_price
            entry_data = dict(name=product.name, var=variant.name, image=product.cover_image_url, price=price, qty=qty,
                              total=total_p, is_in_house=product.is_in_house, affiliate_price=affiliate_price)
            OrderEntryService.create(**item)
            if variant:
                quantity = variant.quantity - item.get('quantity')
                VariantService.update(variant.id, quantity=quantity)
                new_q = product.get_quantity
                ProductService.update(product.id, quantity=new_q)
            else:
                pass

        # build notification here
        order = cls.get(obj.id)
        #order.cal_bob_commission()
        order_data = order.as_dict(
            include_only=['shipment_name', 'shipment_address', 'shipment_state', 'shipment_phone',
                          'shipment_country', 'code', 'shipping_fee', 'w_total', 'discount'])
        order_data.update(
            order_entry=[e.as_dict(include_only=['name', 'image_url', 'qty', 'w_price', 'w_total', "var"]) for e in
                         order.entries])

        send_notification.delay('order_successful', subject='Your Order Was Successful', email=order.email,
                                **order_data)
        send_notification.delay('order_successful', subject='New order', email="info@buyorbidng.com", **order_data)
        return order

    @classmethod
    def update(cls, obj_id, ignored_args=None, **kwargs):
        obj = cls.get(obj_id)
        entries = obj.entries
        status_id = kwargs.get("order_status_id")

        if obj is None:
            raise Exception('Order does not exist, Ooops!')

        obj = BaseOrderService.update(obj_id, ignored_args, **kwargs)
        if entries:
            for entry in entries:
                OrderEntryService.update(entry.id, **kwargs)

        if status_id in [3, 6]:
            for entry in entries:
                var = VariantService.get(entry.variant_id)
                VariantService.update(entry.variant_id, quantity=var.quantity + entry.quantity)
        return obj

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseOrderService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseOrderService.get(obj_id)
        search.delete(obj)


class OrderEntryService(BaseOrderEntryService):
    @classmethod
    def update(cls, obj_id, ignored_args=None, **kwargs):
        obj = cls.get(obj_id)
        status = kwargs.get('order_status_code')
        if status == 'processing':
            kwargs.update(date_processed=datetime.today())
        if status == 'cancelled':
            kwargs.update(date_cancelled=datetime.today())
        if status == 'fulfilled':
            kwargs.update(date_fulfilled=datetime.today())
        if status == 'customer_cancelled':
            kwargs.update(date_customer_cancelled=datetime.today())
        if status == 'returned':
            kwargs.update(date_returned=datetime.today())

        BaseOrderEntryService.update(obj_id, ignored_args, **kwargs)

    pass


class CategoryService(BaseCategoryService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        images = kwargs.pop('images', [])
        kwargs['code'] = slugify(kwargs.get('name'))

        if kwargs.get('parent_id') not in [None, 0, "0", ""]:
            parent = kwargs.get('parent_id')
            obj = cls.get(parent)
            level = obj.level + 1
            if level > 2:
                return abort(405, status="Not Allowed",
                             message="You cannot create categories more than 3 levels deep")
            kwargs['level'] = level

            obj = BaseCategoryService.create(ignored_args, **kwargs)

            if images != []:
                for image in images:
                    pic = ImageService.update(image, category_id=obj.id)
            index_parent_category(parent)
            return obj
        else:
            kwargs["parent_id"] = None
            obj = BaseCategoryService.create(ignored_args, **kwargs)

            if images != []:
                for image in images:
                    pic = ImageService.update(image, category_id=obj.id)

            return obj

    @classmethod
    def set_cover_image(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        cover = obj.cover_image

        if cover:
            cover.is_cover = False
            ImageService.update(cover.id)

        image_id = kwargs.get('image_id')
        ImageService.update(image_id, is_cover=True)

    @classmethod
    def add_products(cls, cat_ids, product_id):
        cats = cls.query.filter(Category.id.in_(cat_ids)).all()
        parents = []

        # drill up into the parents to attach the product to them also
        for c in cats:
            if c.parent_id:
                c_p = cls.get(c.parent_id)
                parents.append(c.parent_id)
                if c_p.parent_id:
                    parents.append(c_p.parent_id)

        cat_ids = cat_ids + parents
        # cat_ids = [c.id for c in cats]
        # prod = Product.query.get(product_id)
        # prod.categories = cats
        # ProductService.update(prod.id)
        load_categories(product_id, cat_ids)
        # for c in cats:
        #     c.products.append(prod)
        #     CategoryService.update(c.id)
        #     db.session.commit()
        #     db.session.()

        return cats

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        logger.info("in service class index object")
        obj = BaseCategoryService.get(obj_id)
        # db.session.merge(obj)
        # db.session.commit()
        # db.session.flush()
        # db.session.refresh
        #
        # logger.info(obj)
        # search.index(obj)
        # doc_type = str(cls.model_class.__tablename__)
        # logger.info(doc_type)
        # logger.info(obj_id)
        power_search_object_index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        # obj = BaseCategoryService.get(obj_id)
        # search.delete(obj)
        doc_type = str(cls.model_class.__tablename__)
        logger.info(doc_type)
        logger.info(obj_id)
        power_delete_search_object(doc_type, obj_id)


class TransactionService(BaseTransactionService):
    @classmethod
    def create(cls, **kwargs):
        from buyorbid.services.accounts import CustomerService

        # business = cls.get(business_id)
        # user_id = kwargs.get('user_id_', None)

        cart = CartService.get(kwargs.get("cart_id", None))
        # cart.user_id = user_id
        _cart = cart.level_dict()
        # _cart['user_id'] = user_id

        if cart.user_id:
            customer = Customer.query.filter(Customer.email == cart.email).first()
            if not customer:
                # Generate customer information if customer doesn't already exist
                # _cart['user_id'] = cart.user_id
                customer = CustomerService.create(name=_cart.get('first_name', " ") + " " + _cart.get('last_name', " "),
                                                  **_cart)
                address = AddressService.create(customer_id=customer.id, name="default", **_cart)
        else:
            customer = Customer.query.filter(Customer.email == cart.email).first()
            if not customer:
                # Generate customer information if customer doesn't already exist
                full_name = cart.shipment_name
                customer = CustomerService.create(is_incognito=True, name=full_name, **_cart)
                address = AddressService.create(customer_id=customer.id,
                                                name=_cart.get('first_name', " ") + " " + _cart.get('last_name', " "),
                                                **_cart)

        country = CountryService.get(_cart.get("country_id"))
        state = StateService.get(_cart.get("state_id"))
        _cart["customer_id"] = customer.id
        _cart["payment_status_id"] = PaymentStatus.query.filter_by(code=kwargs.get("payment_status", None)).first().id
        _cart["payment_option_id"] = kwargs.get("payment_option_id", None)
        # _cart["channel_id"] = kwargs.get("channel_id")
        _cart["delivery_charge"] = cart.delivery_charge
        _cart["shipping_charge"] = cart.delivery_charge
        _cart["country"] = country.name
        _cart["state"] = state.name
        _cart["cart_id"] = cart.id
        _cart["user_id"] = cart.user_id
        _cart["amount"] = kwargs.get('amount')
        _cart["discount"] = kwargs.get('discount')
        _cart["code"] = id_generator()
        _cart["checkout_type"] = cart.checkout_type
        # _cart["cart_name"] = cart.name

        transaction = BaseTransactionService.create(**_cart)

        entries = []
        entry_ids = []
        for item in cart.cart_items.all():
            entry = item.as_dict()
            _entry = TransactionEntryService.create(transaction_id=transaction.id, **entry)
            entries.append(entry)
            entry_ids.append(item.id)

        _cart["customer_id"] = transaction.customer_id
        _cart["payment_status_code"] = 'pending'
        _cart["delivery_charge"] = cart.delivery_charge
        _cart["country"] = transaction.country
        _cart["state"] = transaction.state
        _cart["cart_id"] = cart.id
        _cart["user_id"] = cart.user_id
        # _cart["amount"] = cart.total
        _cart['entries'] = entries
        _cart['order_status_id'] = OrderStatus.query.filter_by(code="pending").first().id
        _cart["code"] = id_generator()
        _cart["checkout_type"] = cart.checkout_type

        order = OrderService.create(**_cart)
        transaction = cls.update(transaction.id, order_id=order.id)
        for c in kwargs.get('coupon_ids'):
            if c:
                CouponService.update(c, order_id=order.id)
        # CartItemService.delete_by_ids(entry_ids)
        CartService.clear_cart(cart.id)

        return transaction

    @classmethod
    def review_transaction(cls, **kwargs):
        status_code = kwargs.get('status_code')
        transaction_id = kwargs.get('transaction_id')
        transaction = cls.get(transaction_id)
        order = None
        payment_status_code = None
        cart = CartService.get(transaction.cart_id)

        if status_code in ['successful']:
            if cart.payment_option.type == "postpaid":
                payment_status_code = "pending"
            elif cart.payment_option.type == "prepaid":
                payment_status_code = "paid"

            # order = OrderService.update(order.id, order_status_code='processing', payment_status_code='paid')
            # kwargs['transaction_status_code'] = 'complete'
            # _cart = cart.level_dict()
            # _cart["customer_id"] = transaction.customer_id
            # _cart["payment_status_code"] = 'paid'
            # # _cart["channel_id"] = transaction.channel_id
            # _cart["delivery_charge"] = cart.delivery_charge
            # _cart["country"] = transaction.country
            # _cart["state"] = transaction.state
            # _cart["cart_id"] = cart.id
            # _cart["amount"] = cart.total
            # _cart["code"] = id_generator()
            # _cart["entries"] = [c.as_dict() for c in cart.cart_items.all()]
            # _cart['order_status_code'] = "processing"
            # order = OrderService.create(**_cart)
            order = OrderService.update(transaction.order_id, order_status_code="processing",
                                        payment_status_code=payment_status_code)

            if cart.checkout_type == "BuyNow":
                CartService.delete(cart.id)
            elif cart.user_id == None:
                CartService.delete(cart.id)

        # CartService.clear_cart(cart.id)
        # kwargs['order_id'] = order.id
        else:
            # kwargs['transaction_status_code'] = 'failed'  # add the model first
            pass
        kwargs['transaction_status_code'] = status_code
        cls.update(transaction_id, **kwargs)

        return order

    @classmethod
    def confirm_vbv(cls, trans_code, **kwargs):
        """ Confirm transaction that is authorized via VBVSecure Code """

        # Failsafe mechanism for when the response structure changes
        resp = kwargs.get("resp")

        data = json.loads(str(resp[0]))

        trans = Transaction.query.filter(Transaction.code == str(trans_code.upper())).first()

        if not trans:
            raise ObjectNotFoundException(Transaction, trans_code)

        res_code = data.get("responsecode", None)
        response_message = data.get("responsemessage", None)
        response_token = data.get("responsetoken", None)

        status_code = trans.status_code

        if res_code and res_code in ["0", "00"]:
            data['response_status'] = 'success'
            data['response_code'] = res_code
            data['response_message'] = response_message
            status_code = 'successful'

        else:
            data['response_status'] = 'failed'
            data['response_code'] = res_code
            data['response_message'] = response_message
            status_code = 'failed'

        trans = TransactionService.update(trans.id, status_code=status_code, **data)

        # Expire the request here
        # RequestService.update(request_obj.id, is_expired=True)

        return trans


class TransactionEntryService(BaseTransactionEntryService):
    pass


class ProductViewService(BaseProductViewService):
    @classmethod
    def add(cls, product_id, user_id):
        view = ProductView.query.filter_by(product_id=product_id, user_id=user_id).first()
        if view:
            view = cls.update(view.id, view_count=view.view_count + 1)
        else:
            view = ProductViewService.create(user_id=user_id, product_id=product_id)

        return view
