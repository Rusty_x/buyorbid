from buyorbid.services import *
from buyorbid.services import search
# import json
# import os
# from cloudinary.uploader import upload
# import itertools
# from flask_restful import abort
# import cloudinary
# from marshmallow.utils import pprint
# from ext.base import utils
# from buyorbid.models import *
# from buyorbid.services.core import *
# from buyorbid.services import auction
# from ext.services.orm import ServiceFactory
# from ext.base.utils import slugify, id_generator

BaseConversationService = ServiceFactory.create_service(Conversation, db)
BaseConverseeService = ServiceFactory.create_service(Conversee, db)
BaseMessageService = ServiceFactory.create_service(Message, db)



class ConversationService(BaseConversationService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseConversationService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseConversationService.get(obj_id)
        search.delete(obj)



class ConverseeService(BaseConverseeService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseConverseeService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseConverseeService.get(obj_id)
        search.delete(obj)



class MessageService(BaseMessageService):
    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseMessageService.get(obj_id)
        search.index(obj)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseMessageService.get(obj_id)
        search.delete(obj)

