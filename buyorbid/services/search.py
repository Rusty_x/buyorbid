"""
search.py
@Author: kunsam002 & rusty
Search service responsible for indexing and querying the search engine
"""

from buyorbid.models import *
from buyorbid import models
from buyorbid import db, app, logger, es, celery
from ext.base.utils import DateJSONEncoder
import inspect
import sys
import urllib
import json
from sqlalchemy.orm.collections import InstrumentedList
from sqlalchemy.orm import dynamic
import requests

ES_INDEX = app.config.get("ES_INDEX")

if ES_INDEX is None:
    raise Exception("Elasticsearch Index not configured. Please specify index in the config file")


class Struct(dict):
    """ Temporary struct class """

    def __init__(self, **kwargs):
        self.update(kwargs)

    def __getattr__(self, k):
        return self.get(k)

    def __setattr__(self, k, v):
        self[k] = v

    def convert_fields(self):
        """ Convert the field from the native python format based on the elasticsearch index mapping """

        __tablename__ = getattr(self, "__tablename__", None)

        if __tablename__:
            for k, v in self.__dict__.items():

                cls_ = get_model_from_table_name(__tablename__)

                if cls_:
                    _field = cls_.__table__.columns.get(k)

                    if _field is not None and _field.type.__class__ in [DateTime, Date] and v is not None:
                        setattr(self, str(k), dateutil.parser.parse(v))
                else:
                    setattr(self, str(k), v)

    def as_dict(self):
        _dict = {}

        for key, obj in self.__dict__.items():

            if isinstance(obj, Struct):
                _dict[key] = obj.__dict__

            elif isinstance(obj, (list, tuple)):
                _dict[key] = [s.__dict__ for s in obj]
            else:
                _dict[key] = obj

        return _dict


def group_dir_categories(objects):
    """ this function will group the page content objects and return an updated grouping of the results """

    keys = []

    for i in objects:
        if i.dir_section not in keys:
            keys.append(i.dir_section)

    keys = sorted(keys)

    result = []

    for i in range(len(keys)):
        obj = keys[i]
        s = get("parent", obj.parent_id)
        obj.cover_image_url = s.cover_image_url
        obj.categories = []
        for i in objects:
            if i.section_id == obj.id:
                i.pop('parent', None)
                obj.categories.append(i)

        obj.categories.sort(key=lambda x: x.date_created, reverse=False)

        result.append(obj)

    result.sort(key=lambda x: x.id)

    return result


def group_objects(objects):
    """ this function will group the page content objects and return an updated grouping of the results """

    keys = []

    for i in objects:
        if i.page_content_container not in keys:
            keys.append(i.page_content_container)

    keys = sorted(keys)

    result = []

    for i in range(len(keys)):
        obj = keys[i]
        page_contents = []
        for i in objects:
            if i.content_container_id == obj.id:
                i.pop('container', None)
                page_contents.append(i)

        page_contents = sorted(page_contents, key=lambda x: x.position, reverse=False)

        obj.page_contents = page_contents

        result.append(obj)

    result = sorted(result, key=lambda x: x.position)

    return result


def group_products(objects):
    """ this function will group the products and return an updated grouping of the products by shop """

    keys = []

    for i in objects:
        if i.shop not in keys:
            keys.append(i.shop)

    keys = sorted(keys)

    result = []

    for i in range(len(keys)):
        obj = keys[i]
        obj.products = []
        for i in objects:
            if i.shop_id == obj.id:
                i.pop('shop', None)
                obj.products.append(i)

        obj.products.sort(key=lambda x: x.date_created, reverse=False)
        obj.total = sum(i.cart_price for i in obj.products)
        obj.currency = get("currency", obj.currency_id)
        obj.categories = obj.products[0].shop_categories
        obj.product_ids = ', '.join(str(i.id) for i in obj.products)

        result.append(obj)

    result.sort(key=lambda x: x.position)

    return result


def custom_index_setup(_delete=False, follow=False, skip=False):
    delete_index()

    request_body = {
        "settings": {
            "number_of_shards": 1,
            "analysis": {
                "filter": {
                    "autocomplete_filter": {
                        "type": "edge_ngram",
                        "min_gram": 1,
                        "max_gram": 20
                    }
                },
                "analyzer": {
                    "autocomplete": {
                        "type": "custom",
                        "tokenizer": "standard",
                        "filter": [
                            "lowercase",
                            "autocomplete_filter"
                        ]
                    }
                }
            }
        }
    }
    res = es.indices.create(index=ES_INDEX, body=request_body)


def install_mapping():
    category_mapping = {
        "category": {
            "properties": {
                "id": {
                    "type": "integer"
                },
                "code": {
                    "type": "string",
                    "analyzer": "autocomplete",
                    "search_analyzer": "standard"
                },
                "name": {
                    "type": "string",
                    "analyzer": "autocomplete",
                    "search_analyzer": "standard",
                    "fields": {
                        "raw": {
                            "type": "string",
                            "index": "not_analyzed",
                        },
                        "normal": {
                            "type": "string",
                            "analyzer": "standard",
                            "search_analyzer": "standard"
                        }
                    }
                },
                "description": {
                    "type": "string",
                    "analyzer": "autocomplete",
                    "search_analyzer": "standard"
                },
                "detail_tag": {
                    "type": "string"
                },
                "cover_image_id": {
                    "type": "integer"
                },
                "view_count": {
                    "type": "integer"
                },
                "level": {
                    "type": "integer"
                },
                "permanent": {
                    "type": "boolean"
                },
                "visibility": {
                    "type": "boolean"
                }
            }
        }
    }
    product_mapping = {
        "product": {
            "properties": {
                "name": {
                    "type": "string",
                    "analyzer": "autocomplete",
                    "search_analyzer": "standard",
                    "fields": {
                        "raw": {
                            "type": "string",
                            "index": "not_analyzed",
                        },
                        "normal": {
                            "type": "string",
                            "analyzer": "standard",
                            "search_analyzer": "standard"
                        }
                    }
                },
                "description": {
                    "type": "string",
                    "analyzer": "autocomplete",
                    "search_analyzer": "standard"
                },
                # "categories": {
                #     "type": "nested",
                #     "properties": {
                #         "code": {
                #             "type": "nested",
                #             "properties": {
                #                 "model": {
                #                     "type": "string"
                #                 },
                #                 "make": {
                #                     "type": "string"
                #                 }
                #             }
                #         },
                #         "last_name": {
                #             "type": "string"
                #         },
                #         "first_name": {
                #             "type": "string"
                #         }
                #     }
                # }
                "categories": {
                    "type": "nested",
                    "properties": {
                        "id": {
                            "type": "integer"
                        },
                        "code": {
                            "type": "string"
                        },
                        "name": {
                            "type": "string",
                            "analyzer": "autocomplete",
                            "search_analyzer": "standard",
                            "fields": {
                                "raw": {
                                    "type": "string",
                                    "index": "not_analyzed",
                                },
                                "normal": {
                                    "type": "string",
                                    "analyzer": "standard",
                                    "search_analyzer": "standard"
                                }
                            }
                        },
                        "description": {
                            "type": "string",
                            "analyzer": "autocomplete",
                            "search_analyzer": "standard"
                        },
                        "cover_image_id": {
                            "type": "integer"
                        }
                    }
                }

            }
        }
    }
    es.indices.put_mapping(index=ES_INDEX, doc_type="category", body=category_mapping)
    es.indices.put_mapping(index=ES_INDEX, doc_type="product", body=product_mapping)

    # _cls = [PaymentStatus, Category, Country, State, City, OrderStatus, PaymentChannel, PaymentOption, Timezone,
    #         TransactionStatus, Bank, DeliveryOption, DeliveryStatus]
    # for i in _cls:
    #     logger.info(i)
    #     rebuild_model_index(i)


def rebuild_index(_delete=False, follow=False, skip=False):
    """ rebuild the search index. Useful for when search is integrated separately or when the index needs to be rebuilt """

    # load all models and determine the ones that are available for indexing by determining if '__searchable__ = True'

    _searchables_ = []
    # _ngrams_ = []

    for name, cls in inspect.getmembers(sys.modules[models.__name__], inspect.isclass):
        if issubclass(cls, db.Model) and hasattr(cls, '__searchable__'):
            _searchables_.append(cls)

    if _delete:
        delete_index()
    custom_index_setup()
    for cls in _searchables_:
        rebuild_model_index(cls, follow=follow, skip=skip)


def delete_index():
    es.indices.delete(index=ES_INDEX, ignore=[400, 404])


def rebuild_model_index(cls, follow=True, skip=False):
    """ rebuilds the index of a searchable model"""

    logger.info("Re-indexing model class %s" % cls)
    print "-------- Re-indexing model class %s -----------" % cls
    # for obj in db.session.query(model_class.id).distinct().all()
    for _obj in db.session.query(cls.id).distinct().all():

        _search_obj = get(cls.__tablename__, _obj.id)

        if skip is True and _search_obj != None:
            # print "<%s - %s> Already exists, Skipping..." % (cls.__tablename__, _obj.id)
            pass
        else:
            obj = cls.query.get(_obj.id)

            # print "Indexing <%s - %s>" % (cls.__tablename__, _obj.id)
            index(obj, follow=follow)

    logger.info("Indexing Model Class %s completed successfully" % cls)
    print "-------- Indexing Model Class %s completed successfully --------" % cls
    es.indices.refresh(index=ES_INDEX)


def get(doc_type, id):
    """ queries the search engine for a particular document """
    try:

        res = es.get(index=ES_INDEX, doc_type=doc_type, id=id, ignore=[400, 404])

        data = res.get('_source', None)
        if data:
            data["__tablename__"] = res.get("_type")
            return convert_dict(data, doc_type)

    except Exception, e:
        logger.info(e)
        raise


def convert_to_subdict(obj):
    __tablename__ = obj.__tablename__
    _prop = obj.as_full_json_dict()
    _prop["__tablename__"] = __tablename__
    return _prop


def convert_to_dict(obj, includes=[], excludes=[]):
    """ Converts a model into dict for indexing """

    data = obj.as_full_json_dict(include_only=includes, exclude=excludes)
    # include extras
    for prop in includes:
        _prop = getattr(obj, prop, None)

        if _prop and isinstance(_prop, AppMixin):
            _prop = convert_to_subdict(_prop)

        if _prop and isinstance(_prop, (list, dynamic.AppenderMixin, InstrumentedList, db.Query)):
            res = []
            for p in _prop:
                if type(p) in [int, str]:
                    res.append(p)
                else:
                    _p = convert_to_subdict(p)
                    res.append(_p)
            _prop = res

        data[prop] = _prop

    # # exclude fields to hide
    for prop in excludes:
        data.pop(prop, None)

    return data


def convert_from_subdict(data):
    __tablename__ = data.pop("__tablename__")
    _sub_cls = models.get_model_from_table_name(__tablename__)
    value = _sub_cls(**data)

    return value


def convert_from_dict(data, doc_type):
    """ converts data from a dictionary into the data model """

    cls = models.get_model_from_table_name(doc_type)

    obj = cls.query.get(data["id"])

    return obj

    return obj


def convert_dict(data, doc_type):
    _data = json.dumps(data, default=DateJSONEncoder)
    return json.loads(_data, object_hook=lambda d: Struct(**d))


def index_object(obj, action):
    if action in ["insert", "update"]:
        index(obj)

    if action in ["delete"]:
        delete(obj)


@celery.task(queue='index')
def index_class_object(tablename, obj_id, action):
    obj = fetch_object(tablename, obj_id)
    db.session.merge(obj)
    if obj:
        if action in ["insert", "update"]:
            index(obj)

        if action in ["delete"]:
            delete(obj)
    else:
        logger.info("Attempting to index an unknown doc_type: %s" % tablename)
        print "------Attempting to index an unknown doc_type: %s-------" % tablename


def fetch_object(tablename, obj_id):
    cls = models.get_model_from_table_name(tablename)
    if cls:
        obj = cls.query.get(obj_id)
        db.session.add(obj)
        db.session.merge(obj)
        db.session.commit()
        return obj


def index(obj, follow=True, **kwargs):
    """ Push data to the search. """
    # Fetch data to additionally include or exclude from search information
    includes = getattr(obj, "__include_in_index__", [])
    excludes = getattr(obj, "__exclude_in_index__", [])
    listeners = getattr(obj, "__listeners_for_index__", [])
    # ngrams = getattr(obj, "__ngram_fields__", [])

    data = convert_to_dict(obj, includes=includes, excludes=excludes)
    if obj.__tablename__ in ["category"]:
        data = {"nested": data}
    es.index(index=ES_INDEX, doc_type=obj.__tablename__, id=obj.id, body=data)
    if follow:
        index_listeners(obj, listeners=listeners)


def delete(obj):
    """ Deletes an object from the index """
    try:
        es.delete(index=ES_INDEX, doc_type=obj.__tablename__, id=obj.id)
    except:
        pass


def delete_by_query(doc_type=None, body={}):
    try:
        es.delete_by_query(index=ES_INDEX, doc_type=doc_type, q=body)
    except:
        pass


def delete_by_requests(doc_type, body={}):
    try:
        domain = "http://" + app.config.get("ES_CONFIG")[0]["host"]
        url = domain + ":9200" + "/sme/" + doc_type + "/_query"
        body = json.dumps(body)
        requests.delete(url=url, data=body)
    except:
        pass


def query(doc_type=None, body={}, page=1, size=20, source=True, from_db=False, request_args=None):
    """ executes the search query """
    if page < 1:
        page = 1
    if size < 1:
        size = 1
    from_ = int(size) * (int(page) - 1)

    res = es.search(index=ES_INDEX, doc_type=doc_type, body=body, from_=from_, size=size, _source=source,
                    ignore=[400, 404])

    hits = res.get("hits", {})
    total = int(hits.get("total", 0))

    items = []

    for hit in hits.get("hits", {}):
        data = hit.get("_source")
        doc_type = hit.get("_type")

        data["__tablename__"] = doc_type

        if from_db:
            obj = convert_from_dict(data, doc_type)
            if obj:
                items.append(obj)
        else:
            obj = convert_dict(data, doc_type)
            if obj:
                items.append(obj)

    response = {"total": total, "objects": items, "page": abs(page), "limit": abs(size)}

    paging = build_pagination(page, size, total)

    response.update(paging)

    return response


def build_pagination(page, limit, total):
    """ Generate the paging element of the results """
    pages = (total / limit) + min(1, total % limit)
    prev = max(1, page - 1) if page > 1 else None
    next = min(pages, page + 1) if page < pages else None

    return locals()


def index_listeners(obj, listeners=[]):
    '''
    This method indexes model listeners
    '''

    for l in listeners:
        _prop = getattr(obj, l)

        if _prop and isinstance(_prop, AppMixin):
            index(_prop, follow=False)

        if _prop and isinstance(_prop,
                                (list, dynamic.AppenderMixin, InstrumentedList, db.Query, dynamic.AppenderQuery)):
            res = []
            for p in _prop:
                index(p, follow=False)
