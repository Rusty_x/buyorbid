from buyorbid.models import *
from sqlalchemy.exc import IntegrityError
from buyorbid import db, app, logger, cache, redis
from buyorbid.services import search
from itertools import groupby
from flask import jsonify, render_template
import json
import htmlmin


# Site Services for public.py blueprints
def fetch_product_categories():
    _res = redis.get("side_product_categories")

    if _res:
        return json.loads(_res)

    product_categories = search.query(doc_type='category', body={"sort": {"nested.name": {"order": "asc"}}}, size=1000,
                                      from_db=False)

    _res = product_categories.get('objects')
    _res = jsonify(_res).data
    redis.set("side_product_categories", _res)
    resp = json.loads(_res)
    return resp


def render_domain_template(name, **kwargs):
    template_name = name

    _html = render_template(template_name, **kwargs)

    return htmlmin.minify(_html, remove_empty_space=True, remove_comments=True)
