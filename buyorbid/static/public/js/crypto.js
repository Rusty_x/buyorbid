
export default class Crypto {

    constructor(key) {
        // des_key: The private encryption key required to build an encryption
        this.key = key
    }


    encryptText(text) {
        let hashKey = md5(this.key)
        let cipher = new AES.ModeOfOperation.ctr(hashKey)

        let encryptedText = cipher.encrypt(text)
        return encryptedText.toString("base64")
    }

    decryptText(base64String) {
        let hashKey = md5(this.key)
        let cipher = new AES.ModeOfOperation.ctr(hashKey)

        let cipherText = new Buffer(base64String, "base64")
        let text = cipher.decrypt(cipherText)

        return text.toString("ascii")
    }

    encryptData(data) {

        let entries = Object.keys(data)
        let encryptedData = {}

        for(const entry of entries) {
            let text = data[entry]

            encryptedData[entry] = this.encryptText(text.toString())
        }

        return encryptedData
    }

    decryptData(encryptedData) {
        let entries = Object.keys(encryptedData)
        let data = {}

        for(const entry of entries) {
            let text = encryptedData[entry]
            data[entry] = this.decryptText(text)
        }

        return data
    }
 }
