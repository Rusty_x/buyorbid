"""
search.py
@Author: rusty & kunsam002
Subscribers that power search engine manipulation

"""

from flask.ext.sqlalchemy import models_committed, before_models_committed
from buyorbid.services import search
# from buyorbid.services.search import index_class_object
from sqlalchemy import event
from buyorbid.models import *
from buyorbid import app, logger, db, es, celery
import requests
import json
from buyorbid.services.core import *
from buyorbid.services.accounts import *
from buyorbid.services.profile import *
from buyorbid.services.payment import *
from buyorbid.services.product import *
from buyorbid.services.auction import *


@celery.task
def index_class_object(handle, obj_id, action):

    SERVICE_CLASSES = {
        "user": UserService,
        "country": CountryService,
        "state": StateService,
        "city": CityService,
        "currency": CurrencyService,
        "payment_channel": PaymentChannelService,
        "payment_status": PaymentStatusService,
        "payment_option": PaymentOptionService,
        "delivery_option": DeliveryOptionService,
        "delivery_status": DeliveryStatusService,
        "recurrent_card": CardService,
        "product": ProductService,
        "variant": VariantService,
        "product_detail": ProductDescriptionService,
        "product_detail_type": ProductDescriptionTypeService,
        "category": CategoryService,
        "order": OrderService,
        "order_status": OrderStatusService,
        "transaction_status": TransactionStatusService,
        "auction": AuctionService,
        "bid": BidService,
        "credit": CreditService,
        "credit_type": CreditTypeService,
        "earning": EarningService,
        "referral": ReferralService
    }

    _func = SERVICE_CLASSES.get(handle, None)

    if _func:
        if action in ["insert", "update"]:
            _func.index_search_object(obj_id)

        if action in ["delete"]:
            _func.delete_search_object(obj_id)


@models_committed.connect
def _index_model(app, changes):
    """ signal to study search """
    for obj, action in changes:
        if hasattr(obj, "__searchable__"):
            # index_class_object.delay(obj.__tablename__, obj.id, action)
            handle = obj.__tablename__
            index_class_object.delay(handle, obj.id, action)
