import os
from buyorbid.views import public

from sqlalchemy.sql.elements import or_
from xlsxwriter import Workbook
from buyorbid.models import *
from datetime import datetime, timedelta
import tablib


class ReportGen:
    def __init__(self):
        pass

    # For multiple worksheets pass the datasets to DataBook like tablib.Databook((dataset1, dataset2))
    @classmethod
    def prepare_data(cls, headers, *raw_data):
        # headers = ('first_name', 'last_name')
        # data = [('John', 'Adams'), ('George', 'Washington')]
        if len(raw_data) > 0 and type(raw_data[0]) is dict:
            data = tablib.Dataset()
            data.dict = raw_data
        elif len(raw_data) > 0 and type(raw_data[0]) is tuple:
            data = tablib.Dataset(*raw_data, headers=headers)
        else:
            data = tablib.Dataset()

        return data

    @classmethod
    def prepare_excel_data(cls, path):

        basepath = os.getcwd()
        fullpath = basepath + path
        imported_data = tablib.Dataset().load(open(fullpath).read())
        json_string = imported_data.export('json')
        json_data = json.loads(json_string)
        return json_data

    @classmethod
    def download_csv(cls, filename, headers, *data):

        data = cls.prepare_data(headers, *data)
        fullpath = "%s.csv" % filename
        with open(fullpath, 'wb') as f:
            f.write(data.csv)

        return data.csv

    @classmethod
    def download_html(cls, filename, headers, *data):
        data = cls.prepare_data(headers, *data)

        # fullpath = "%s/%s.html" % (app.config.get("REPORTS_DIR"), filename)
        # with open(fullpath, 'wb') as f:
        #     f.write(data.html)
        return data.html

    @classmethod
    def download_json(cls, filename, headers, *data):
        data = cls.prepare_data(headers, *data)
        fullpath = "%s/%s" % (app.config.get("REPORTS_DIR"), "report-generation.json")
        with open(fullpath, 'wb') as f:
            f.write(data.json)
        return data.json

    @classmethod
    def download_xlsx(cls, filename, headers, *data):
        data = cls.prepare_data(headers, *data)

        fullpath = "%s.xlsx" % (filename)
        with open(fullpath, 'wb') as f:
            f.write(data.xlsx)

        return data.xlsx

    @classmethod
    def download_xls(cls, filename, headers, *data):
        data = cls.prepare_data(headers, *data)

        fullpath = "%s/%s.xls" % (app.config.get("REPORTS_DIR"), filename)
        with open(fullpath, 'wb') as f:
            f.write(data.xls)

        return data.xls

    @classmethod
    def download_workbook(cls, filename, data):
        book = tablib.Databook(data)
        fullpath = "%s.xlsx" % (filename)
        with open(fullpath, 'wb') as f:
            f.write(book.xlsx)

        return book.xlsx

#id	title	description	link	image_link	condition	availability	price	sale_price	sale_price_effective_date	brand

def create_catalog(is_sale=False, sale_period=None):
    prods = Product.query.all()
    data = []
    for p in prods:
        av = "in stock" if p.quantity >0 else "out of stock"
        url = app.config.get("P_CANONICAL_URL")+url_for("public_bp.product", id=p.id, handle=p.handle)
        sub = (p.id, p.name, p.description, url, p.cover_image_url, "new", av,
               whole_number_format(p.regular_price)+" NGN", whole_number_format(p.sale_price)+" NGN", "bob")
        if is_sale:
            sub = sub + tuple([sale_period])
            print sub

        data.append(sub)

    headers = ["id","title","description", "link", "image_link", "condition", "availability", "price", "sale_price",
               "brand"]
    if is_sale:
        headers.append("sale_price_effective_date")
    print headers, "headers=========="
    generated = ReportGen.download_csv("catalog", headers, *data)

    return generated