from buyorbid.services.product import *
from buyorbid.services.core import *
from buyorbid.services.accounts import *
from buyorbid.services import search
from buyorbid.models import *


def index_users():
    u = User.query.all()

    for i in u:
        search.index(i)
        print "indexed user ", i.id


def resend_signup_email(u_ids):
    base = "http://buyorbidng.com/signup/verification/"
    for i in u_ids:
        user = User.query.get(i)
        if user:
            hash_ = user.verification_hash
            url = base + hash_
            email_data = dict(link=url)
            print url
            send_user_notification.delay(user.id, 'msv', subject='Welcome to BuyOrBid', **email_data)


def clear_unused_carts():
    used_carts = db.session.query(CartItem.cart_id, func.count(CartItem.id)).group_by(CartItem.cart_id).all()
    used_cart_ids = [i[0] for i in used_carts]

    unused_carts = db.session.query(Cart).filter(Cart.user_id == None, ~Cart.id.in_(used_cart_ids)).all()
    for un_c in unused_carts:
        db.session.delete(un_c)
        db.session.commit()
    return True

def calc_bob_commissions():
    entries = OrderEntry.query.all()
    for entry in entries:
        affiliate_price = entry.variant.affiliate_price if entry.variant.affiliate_price else entry.product.affiliate_price
        entry.affiliate_price = affiliate_price
        entry.is_in_house=entry.product.is_in_house
        db.session.add(entry)
        db.session.commit()

    for entry in OrderEntry.query.all():
        print entry.total, entry.id

    for o in Order.query.all():
        o.cal_bob_commission()
