from datetime import date, time
import time
import base64
import urllib
from elasticsearch_dsl import Q
from flask import Blueprint, render_template, session, url_for, redirect, request, flash, g, _request_ctx_stack, abort
from flask_login import current_user, logout_user, _get_remote_addr, login_required, login_user
from sqlalchemy import desc, func
from ext.base import utils
from buyorbid.forms import *
# from future.search import mappings
from buyorbid.services.authentication import authenticate_user, authenticate_admin
from buyorbid.services.accounts import UserService
from buyorbid.services.auction import *
from buyorbid.services.product import *
from buyorbid.services import printing
from buyorbid.services.site_services import render_domain_template
from buyorbid import app, logger, db, handle_uploaded_photos
from notification.services.notifications import NotificationService

admin_bp = Blueprint('admin_bp', __name__, template_folder='../templates')

def check_unread_messages():
    """ This returns the number of unread messages """
    unread = AdminMessage.query.filter(Message.is_read == False).count()

    if unread == 0:
        return ""
    else:
        return unread


@admin_bp.context_processor
def admin_context():
    """ All variables and functions that need to be included in the theme template """
    DEBUG = app.config.get('DEBUG')
    today = date.today()
    timestamp = str(time.time())
    minimum = min
    number_format = utils.number_format
    length = len
    paging_url_build = utils.build_page_url
    return locals()


# login page
@admin_bp.route('/login/', methods=["GET", "POST"])
def login():
    # next_url = request.args.get("next") or url_for(".index")

    form = LoginForm()
    if form.validate_on_submit():
        data = form.data
        print data, '=====data===='
        username = data["username"]
        password = data["password"]
        remember = data["remember"]
        user = authenticate_admin(username, password)
        print user, '==user===='

        if user is not None:
            login_user(user, remember=remember, force=True)  # This is necessary to remember the user

            # include user roles into scope
            # identity_changed.send(app, identity=Identity(user.id))

            next_url = request.args.get("next", url_for(".index"))
            # logger.info(next_url)
            resp = redirect(next_url)

            # Transfer auth token to the frontend for use with api requests
            __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))

            # return set_cookies(resp, user_id=user.id, remember=remember, credentials=__xcred)

            resp.set_cookie("__xcred", __xcred)

            return resp

        else:
            flash("The username or password is invalid", "login")

    return render_domain_template("admin/login.html", **locals())


@admin_bp.route('/logout/')
@login_required
def logout():
    # if not current_user.is_authenticated():
    #     return redirect(url_for('.signup'))
    logout_user()

    resp = redirect(url_for('.login'))
    resp.set_cookie("__xcred", '', expires=0)
    return resp


@admin_bp.route('/')
@login_required
def index():
    page_title = "Dashboard"
    # Protect agains malicous paging parameters

    # pages = request.args.get("pages")
    # page = int(request.args.get("page", 1))
    # size = request.args.get("size", 20)

    # results = engineer_query(request)

    # products = inventory.ProductService.query.paginate(page, size, False)

    conversation_count = Conversation.query.count()
    user_count = User.query.count()
    order_count = Order.query.count()
    admin_message_count = AdminMessage.query.count()
    product_count = Product.query.count()

    return render_domain_template("admin/index.html", **locals())


@admin_bp.route('/auctions/', methods=['GET', 'POST'])
@login_required
def auctions():
    pageTitle = "Auctions"

    auctions = Auction.query.filter().all()

    return render_domain_template("admin/auctions.html", **locals())


@admin_bp.route('/products/', methods=['GET', 'POST'])
@login_required
def products():
    pageTitle = "Products"

    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        search_q = request.args.get("q", None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Product.query.order_by(desc(Product.id))

    if search_q:
        queries = [Product.name.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template("admin/products.html", **locals())

@admin_bp.route('/products/create/', methods=['GET', 'POST'])
@login_required
# @check_inventory_size('/admin/subscriptions/inventory_size/')
def add_product():
    pageTitle = "Create Product"

    form = ProductForm(csrf_enabled=False)
    form.category_id.choices = [(c.id, c.name) for c in Category.query.all()]
    categories = Category.query.all()

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if not form.validate_on_submit():
        print form.errors, '======errorss======'

    if form.validate_on_submit() and empty_files is False:

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            data['user_id'] = current_user.id
            print '===from validated===='
            cats = data.pop('categories')
            print data
            product = ProductService.create(**data)
            product_id = product.id

            images =[]
            for upload_ in uploaded_files:
                images = ImageService.create(product_id=product.id, **upload_)

            ProductService.set_cover_image(product.id, image_id=images.id)
            cats = cats.split(',')
            cats.pop(-1)
            CategoryService.add_products(cats, product.id)
            return redirect(url_for(".variants", id=product_id))

        else:
            flash("Error: Image not found!")
            form.errors["images"] = errors

    return render_domain_template("admin/product_form.html", **locals())

@admin_bp.route('/products/<int:id>/edit/', methods=['GET', 'POST'])
@login_required
def edit_product(id):
    pageTitle = "Edit Product"

    product = ProductService.get(id)
    form = ProductForm(csrf_enabled=False, obj=product)
    form.category_id.choices = [(c.id, c.name) for c in Category.query.all()]
    categories = Category.query.all()

    # files = request.files.getlist("images")
    # # Test for the first entry
    # empty_files = False
    # for _f in files:
    #     if _f.filename == '' or utils.check_extension(_f.filename) is False:
    #         empty_files = True
    #         break

    if not form.validate_on_submit():
        print form.errors, '======errorss======'

    # if form.validate_on_submit() and empty_files is False:
    if form.validate_on_submit():
        data = form.data
        set_cover = data.get("set_cover_image", False)
        data['user_id'] = current_user.id
        print '===from validated===='
        cats = data.pop('categories')
        print data
        product = ProductService.update(id, **data)
        cats = cats.split(',')
        cats.pop(-1)
        CategoryService.add_products(cats, product.id)
        return redirect(url_for(".variants", id=product.id))

    return render_domain_template("admin/product_form.html", **locals())


@admin_bp.route('/uploads/', methods=['GET', 'POST'])
@login_required
def upload_image():
    form = ImageUploadForm(csrf_enabled=False)

    files = request.files.getlist("image")
    print files, "files====="
    # Test for the first entry
    empty_files = False

    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break
    if form.validate_on_submit():
        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)
        images = None
        if uploaded_files:
            data = form.data
            for upload_ in uploaded_files:
                images = ImageService.create(**upload_)
                # flash("<script>top.$('.mce-btn.mce-open').parent().find('.mce-textbox').val('%s').closest('.mce-window').find('.mce-primary').click();</script>" % images.url)
        if images:
            return images.url
        else: return "None"

    return "None"


@admin_bp.route('/products/<int:id>/variants/', methods=['GET', 'POST'])
@login_required
def variants(id):
    pageTitle = "Variants"

    variants = Variant.query.filter_by(product_id=id).all()
    product = Product.query.get(id)

    return render_domain_template("admin/variants.html", **locals())

@admin_bp.route('/products/<int:id>/update_details/', methods=['GET', 'POST'])
@login_required
def add_detail(id):
    pageTitle = "Product Detail"

    product = Product.query.get(id)
    form = ProductDetailForm(csrf_enabled=False)
    dets = ProductDetail.query.filter_by(product_id=id)
    if dets.all():
        tab = dets.filter_by(type_id=4).first()
        tex = dets.filter_by(type_id=1).first()
        pic = dets.filter_by(type_id=2).first()
        vid = dets.filter_by(type_id=3).first()
        tab_ = None
        tex_ = None
        pic_ = None
        vid_ = None
        if tab:
            tab_ = tab.content
        if tex:
            tex_ = tex.content
        if pic:
            pic_ = pic.content
        if vid:
            vid_ = vid.content
        form = ProductDetailForm(csrf_enabled=False, t_content=tab_, tx_content=tex_, ps_content=pic_, v_content=vid_)
    else:
        tab = None
        pic = None
        tex = None
        vid = None
    categories = Category.query.filter(Category.detail_tag!=None).all()

    if not form.validate_on_submit():
        print form.errors, "errors==="
        print form.data, "data==="

    if form.validate_on_submit():
        data = form.data
        table = dict(name=data.get('t_name'), content=data.get('t_content'), type_id=data.get('t_type_id'), position=data.get('t_position'))
        text = dict(name=data.get('tx_name'), content=data.get('tx_content'), type_id=data.get('tx_type_id'), position=data.get('tx_position'))
        photoshow = dict(name=data.get('ps_name'), content=data.get('ps_content'), type_id=data.get('ps_type_id'), position=data.get('ps_position'))
        video = dict(name=data.get('v_name'), content=data.get('v_content'), type_id=data.get('v_type_id'), position=data.get('v_position'))
        if dets.all():
            if tab and type(tab) not in [unicode, str]:
                ProductDescriptionService.update(tab.id, **table)
            if tex and type(tex) not in [unicode, str]:
                ProductDescriptionService.update(tex.id, **text)
            if pic and type(pic) not in [unicode, str]:
                ProductDescriptionService.update(pic.id, **photoshow)
            if vid and type(vid) not in [unicode, str]:
                ProductDescriptionService.update(vid.id, **video)
        else:
            dets = [table, text, photoshow, video]
            kwargs = dict()
            kwargs["product_details"] = dets
            kwargs["product_id"] = id

            ProductDescriptionService.create(**kwargs)

    return render_domain_template("admin/details/product_detail.html", **locals())


@admin_bp.route('/products/<int:id>/variants/<int:variant_id>/edit/', methods=['GET', 'POST'])
@login_required
def edit_variant(id, variant_id):
    pageTitle = "Variant Edit"

    variant = Variant.query.get(variant_id)
    product = Product.query.get(variant.product_id)

    form = VariaintForm(csrf_enabled=False)

    if form.validate_on_submit():
        data = form.data
        VariantService.update(variant_id, **data)
        print "i worked==============="

    return render_domain_template("admin/variant_form.html", **locals())


@admin_bp.route('/products/<int:id>/variants/<int:variant_id>/create_auction/', methods=['GET', 'POST'])
@login_required
def create_auction(id,variant_id):
    pageTitle = "Create Auction"
    variant = Variant.query.get(variant_id)
    data = {"variant_id":variant.id}
    AuctionService.create(**data)

    return redirect(url_for('.variants', id=variant.product_id))

@admin_bp.route('/notifications/', methods=['GET', 'POST'])
@login_required
def notifications():
    notification = Notification.query.all()
    return render_domain_template("admin/notifications.html", **locals())


@admin_bp.route('/notifications/new/', methods=['GET', 'POST'])
@login_required
def new_notification():
    """add the template for a new notification"""
    form = NotificationForm()
    print '===b4 validate==='
    if form.validate_on_submit():
        print '===validated==='
        data = form.data
        noti = NotificationService.create(**data)
    else:
        print form.errors, '====errors===='
    return render_domain_template("admin/notification_form.html", **locals())

@admin_bp.route('/notifications/<int:id>/edit/', methods=['GET', 'POST'])
@login_required
def edit_notification(id):
    """add the template for a new notification"""
    notification = Notification.query.get(id)
    form = NotificationForm(obj=notification)
    print '===b4 validate==='
    if form.validate_on_submit():
        print '===validated==='
        data = form.data
        noti = NotificationService.update(id, **data)
    else:
        print form.errors, '====errors===='
    return render_domain_template("admin/notification_form.html", **locals())


@admin_bp.route('/categories/', methods=['GET', 'POST'])
@login_required
def categories():
    categories = Category.query.filter().all()
    l = len
    return render_domain_template("admin/categories.html", **locals())


@admin_bp.route('/categories/create/', methods=['GET', 'POST'])
@login_required
def add_category():
    pageTitle = "Create Category"

    form = CategoryForm()
    categories = Category.query.filter().all()
    form.parent_id.choices = [(0, "----select one----")] + [(c.id, c.name) for c in categories]
    print form.parent_id.choices, "=====sle"

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if not form.validate_on_submit():
        print form.errors, '======errorss======'

    if form.validate_on_submit() and empty_files is False:
        print "form validated"

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            data['user_id'] = current_user.id
            print '===from validated===='
            print data
            category = CategoryService.create(**data)

            images = []
            for upload_ in uploaded_files:
                images = ImageService.create(category_id=category.id, **upload_)

            CategoryService.set_cover_image(category.id, image_id=images.id)
            return redirect(url_for(".categories", id=category.id))

        else:
            flash("Error: Image not found!")
            form.errors["images"] = errors

    return render_domain_template("admin/category_form.html", **locals())

@admin_bp.route('/orders/', methods=['GET', 'POST'])
@login_required
def orders():
    pageTitle="Orders"

    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        status_code = request.args.get("status", None)
        search_q = request.args.get("q", None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    order_statuses = OrderStatus.query.all()
    form = OrderUpdateForm()

    query = Order.query.order_by(desc(Order.id))

    if status_code:
        status = OrderStatus.query.filter(OrderStatus.code == status_code).first()

    if status_code and status:
        query = Order.query.filter(Order.order_status_id == status.id).order_by(desc(Order.id))


    if search_q:
        queries = [Order.code.ilike("%%%s%%" % search_q), Order.first_name.ilike("%%%s%%" % search_q), Order.last_name.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))


    return render_domain_template("admin/orders.html", **locals())

@admin_bp.route('/orders/<int:id>/<string:code>/', methods=['GET', 'POST'])
@login_required
def order_invoice(id, code):
    pageTitle = "Order Invoice"

    order = Order.query.get(id)

    if not order:
        abort(404)

    transaction = Transaction.query.filter(Transaction.order_id == order.id).order_by(desc(Transaction.id)).first()
    return printing.return_pdf("prints/order_invoice.html", **locals())


@admin_bp.route('/orders/<int:id>/', methods=['GET', 'POST'])
@login_required
def order(id):
    order_ = Order.query.get(id)
    form = OrderUpdateForm()
    if not form.validate_on_submit():
        print form.errors, "errors"
    if form.validate_on_submit():
        print "in teht validate"
        data = form.data
        OrderService.update(id, **data)
        print "updated"
        order_.cal_bob_commission()
        return redirect(url_for(".orders"))

    return render_domain_template("admin/order_detail.html", **locals())



@admin_bp.route('/messages/', methods=['GET', 'POST'])
@login_required
def admin_messages():
    pageTitle = "Admin Messages"

    # Protect agains malicous paging parameters
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = AdminMessage.query.filter(AdminMessage.is_replied == False).order_by(desc(AdminMessage.date_created))

    results = query.paginate(page, 24, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_domain_template("admin/admin_messages.html", **locals())


@admin_bp.route('/messages/<int:id>/', methods=['GET', 'POST'])
@login_required
def view_admin_message(id):
    pageTitle = "Message"

    prev_url = url_for(".admin_messages")
    obj = AdminMessage.query.get(id)

    if not obj:
        abort(404)

    obj.is_read = True
    db.session.add(obj)
    db.session.commit()

    form = AdminMessageReplyForm()

    if form.validate_on_submit():
        data = form.data
        res_obj = AdminMessageResponseService.create(admin_message_id=obj.id,user_read=False, user_id=current_user.id, **data)
        if res_obj:
            flash("message successfully replied")

            # sending user notification of reply
            message_data = res_obj.as_json_dict()
            message_data['sender'] = 'Buyorbid'
            message_data['link'] = '%s/%s' % (app.config.get('P_CANONICAL_URL'), 'profile/messages') if obj.user_id else False
            message_data['user'] = True if obj.user_id else False
            message_data['full_name'] = obj.full_name
            pprint.pprint(message_data)
            send_notification.delay('new_message', subject='You have a new message', email=obj.email,
                                    **message_data)
            return redirect(url_for(".view_admin_message",id=id))
    return render_domain_template("admin/details/admin_message.html", **locals())



app.register_blueprint(admin_bp)
