from datetime import date, time, datetime, timedelta
import time
import base64
from elasticsearch_dsl import Q
from flask import Blueprint, render_template, session, url_for, redirect, request, flash, g, _request_ctx_stack, abort, \
    jsonify, make_response
from flask_login import current_user, logout_user, _get_remote_addr, login_required, login_user
from sqlalchemy import desc, func
from buyorbid.services import printing
from buyorbid.services.profile import ConversationService, ConverseeService, MessageService
from ext.base import utils
from buyorbid.forms import *
# from future.search import mappings
from buyorbid.services.authentication import authenticate_user, authenticate_forgot_password
from buyorbid.services.accounts import *
from buyorbid.services.core import *
from buyorbid.services.product import *
from buyorbid.services.payment import *
from buyorbid.services.auction import *
from buyorbid.services.site_services import fetch_product_categories, render_domain_template
from buyorbid.services.oauth import twitter, facebook
from buyorbid import app, logger, db, handle_uploaded_photos
import pprint
import random
import urllib

from ext.base.utils import encrypt_data_pyaes, dict_update, encrypt_form

public_bp = Blueprint('public_bp', __name__, template_folder='../templates')


# def fetch_cart():
# 	print "fetch cart is called"
# 	if current_user.is_authenticated():
# 		user = current_user
# 		cart = Cart.query.filter(Cart.user_id == user.id).first()
# 		if not cart:
# 			data = user.as_dict()
# 			cart = CartService.create(checkout_type="Regular",user_id=user.id, **data)
# 		session["cart_id"] = cart.id
#
# 		return cart
#
# 	cart_id = session.get("cart_id", None)
# 	logger.info("My cart ID: %s" % cart_id)
#
# 	if cart_id and Cart.query.get(cart_id) is not None:
# 		cart = Cart.query.get(cart_id)
# 	else:
# 		cart = CartService.create(checkout_type="Regular")
# 		print("new cart", cart)
#
# 	session["cart_id"] = cart.id
# 	print("session cart", session["cart_id"])
# 	session.modified = True
# 	return cart

def _card_label_design(id):
    card = RecurrentCard.query.get(id)
    if not card:
        return ""

    data = '<p style="text-align:center;"><img style="width:25px;margin-right: 10px;" src="/static/public/images/ico/%s.png"><strong>%s</strong><br><small> Expires <i>%s/%s</i>  *Added on <i>%s</i></small> </p>' % (
        card.brand, card.mask, card.exp_month, card.exp_year, card.date_created.strftime("%b/%d/%Y"))
    return data


def fetch_cart():
    if current_user.is_authenticated():
        user = current_user
        cart = Cart.query.filter(Cart.user_id == user.id).first()
        if not cart:
            data = user.as_dict()
            cart = CartService.create(user_id=user.id, checkout_type="Regular", **data)
        session["cart_id"] = cart.id

        return cart

    cart_id = session.get("cart_id", None)

    if cart_id and Cart.query.get(cart_id) is not None:
        cart = Cart.query.get(cart_id)
    else:
        cart = CartService.create(checkout_type="Regular")

    session["cart_id"] = cart.id
    session.modified = True
    return cart


# def fetch_cart():
#     _anon_cart = _fetch_anonymous_cart()
#     if current_user.is_authenticated():
#         user_id = current_user.id
#         session_cart = Cart.query.filter(Cart.user_id == user_id).first()
#
#         if not session_cart:
#             data = current_user.as_dict()
#             session_cart = CartService.create(user_id=user.id, **data)
#
#         session['cart_id'] = session_cart.id
#         session.modified = True
#
#         return session_cart
#
#     else:
#         _session_cart = _anon_cart
#
#     return _session_cart
#
#
# def _fetch_anonymous_cart():
#     cart_id = session.get("cart_id", None)
#     if cart_id:
#         session_cart = Cart.query.get(cart_id)
#
#         if not session_cart:
#             session_cart = CartService.create()
#             session['cart_id'] = session_cart.id
#             session.modified = True
#
#     cart = _fetch_cart()
#     return cart
#
#
# def _fetch_cart():
#     cart_id = session.get("cart_id", None)
#
#     if cart_id and Cart.query.get(cart_id) is not None:
#         session_cart = Cart.query.get(cart_id)
#
#         session["cart_id"] = session_cart.id
#         session.modified = True
#         return session_cart
# #
#
def clear_cart(id):
    """ Clears the cart by deleting all items inside it """
    _cart = Cart.query.get(id)

    if _cart:
        CartService.clear_cart(_cart.id)

    return _cart


def update_session_cart(**data):
    """ Updates data in the session cart. """

    cart_id = session.get("cart_id")

    session_cart = Cart.query.get(cart_id)

    CartService.add_item(session_cart.id, **data)


def update_session_cart_item(id, variant_id, quantity):
    """ Adjusts the cart item data """
    _item = CartItem.query.get(id)

    if _item:
        _item.quantity = quantity
        db.session.add(_item)
        db.session.commit()

    _item.calculate_total()

    return _item


def delete_session_cart_item(id, variant_id, quantity):
    CartItemService.delete(id)


def object_to_dict(obj):
    return obj.as_dict()


def group_cart_data():
    cart = fetch_cart()

    # items = my_uni_cart.uni_cart_items.order_by(UnifiedCartItem.shop_id).all()
    items = CartItem.query.filter(CartItem.cart_id == cart.id).all()

    data = {"cart_id": cart.id, "delivery_charge": cart.delivery_charge, "sub_total": cart.cart_total,
            "total_quantity": cart.total_quantity}
    items_list = []
    for i in items:
        d = {"cover_image_url": i.product.cover_image.url if i.product.cover_image else None,
             "product_name": i.product.name,
             "variant_name": i.variant.name,
             "variant_id": i.variant_id,
             "id": i.id,
             "product_url": url_for('.product', id=i.product_id, handle=i.product.handle),
             "max_quantity": i.variant.quantity,
             "quantity": i.quantity, "price": i.price, "total": i.total}
        items_list.append(d)

    data["cart_items"] = items_list

    return data


def sorter(lis):
    sor = lis.order_by(lis[0].__class__.date_created)
    return sor


def list_filter(iterable, key, val):
    keys = key.split('.')
    res = []
    if len(keys) == 1:
        res = [x for x in iterable if getattr(x, key) == val]
    if len(keys) == 2:
        k1 = keys[0]
        k2 = keys[1]
        res = [x for x in iterable if getattr(getattr(x, k1), k2).lower() == val.lower()]
    return res


@public_bp.context_processor
def public_context():
    """ All variables and functions that need to be included in the theme template """
    DEBUG = app.config.get('DEBUG')
    today = datetime.today()
    timestamp = str(time.time())
    minimum = min
    number_format = utils.number_format
    whole_number_format = utils.whole_number_format
    # theme = theme_processor
    # snippet = theme_snippet
    length = len
    to_dict = object_to_dict
    product_categories = fetch_product_categories()
    login_form = LoginForm()
    # if session.get("cart_id", None):
    sorting = sorter
    lister = list_filter
    cart = fetch_cart()
    encryptor = encrypt_data_pyaes
    dic_updator = dict_update
    js = json
    slugify = utils.slugify
    slugify = utils.slugify
    paging_url_build = utils.build_page_url
    # user = current_customer()
    if current_user.is_authenticated():
        wishlist_product_ids = session.get("wishlist_product_ids", [])
    else:
        wishlist_product_ids = []

    return locals()


@public_bp.route('/email_checker/', methods=['GET', 'POST'])
def email_checker():
    if request.method == "POST":
        _data = request.data

        try:
            email = _data
            cust = Customer.query.filter(Customer.email == email, Customer.is_incognito == True).first()
            if cust:
                if not User.query.filter(User.email == email).first():
                    data = {"exist": True, "cust_id": cust.id,
                            "name": "%s" % cust.name}
                else:
                    data = {"exist": False, "msg": "Customer Does not exist as Incognito"}
            else:
                data = {"exist": False, "msg": "Customer Does not exist as Incognito"}

            refine = json.dumps(data)
            response = make_response(refine)

            return response
        except Exception as e:
            print "---------Error: %s-----------" % str(e)
            logger.info("---------Error: %s-----------" % str(e))
            msg = "Failed with Error " + str(e)
            data = {"exist": False, "msg": msg}
            refine = json.dumps(data)
            response = make_response(refine)
            return response

    else:
        response = make_response("Request Method not Allowed")
        return response


@public_bp.route('/quick_user_reg/', methods=['GET', 'POST'])
def quick_user_reg():
    if request.method == "POST":
        _data = request.data
        _data = json.loads(_data)
        logger.info(_data)
        try:
            cust_id = int(_data.get("cust_id"))
            customer = CustomerService.migrate_user(cust_id, **_data)
            data = {"status": "success", "quick_signup_success": True}
            refine = json.dumps(data)
            response = make_response(refine)
            return response
        except:
            data = {"status": "error", "msg": "Unable to perform quick signup", "quick_signup_success": False}
            refine = json.dumps(data)
            response = make_response(refine)
            return response
    else:
        data = {"status": "error", "msg": "Request Method not Allowed", "quick_signup_success": False}
        refine = json.dumps(data)
        response = make_response(refine)
        return response


@public_bp.route('/cart_notification/')
def cart_notification():
    cart = fetch_cart()
    cart_data = group_cart_data()
    content_type = request.headers.get("Content-Type")
    if content_type == "application/json":
        data = dict(cart_data)

        # All the math needed will be computed here. Total by shop, shop_ids, etc

        return jsonify(data)

    return jsonify({})


@public_bp.route('/check_delivery/<int:id>')
def check_delivery(id):
    data = dict(fee=0)
    if id != 0:
        state = StateService.get(id)
        data = dict(fee=state.delivery_fee)
    return jsonify(data)


@public_bp.route('/header_data/')
def header_data():
    data = {}
    cart = fetch_cart()
    user_earning = user_credit = available_coupon_worth = None
    total_wishlist = 0
    content_type = request.headers.get("Content-Type")
    if content_type == "application/json":
        if current_user.is_authenticated():
            user_earning = current_user.user_earning
            user_credit = current_user.user_credit
            available_coupon_worth = current_user.available_coupon_worth
            total_wishlist = current_user.wishlist_entries.count()

        data = {"user_earning": user_earning, "user_credit": user_credit, "total_quantity": cart.total_quantity,
                "available_coupon_worth": available_coupon_worth,
                "total_wishlist": total_wishlist}

        return jsonify(data)

    return jsonify({})


@public_bp.route('/clear_cart_items/', methods=['POST'])
def clear_cart_items():
    _cart = fetch_cart()
    cart = clear_cart(_cart.id)
    content_type = request.headers.get("Content-Type")
    data = {"delivery_charge": cart.delivery_charge, "sub_total": cart.cart_total,
            "total_quantity": cart.total_quantity}
    return jsonify(data)


@public_bp.route('/')
def index():
    page_title = "Home"
    # Protect agains malicous paging parameters


    pages = request.args.get("pages")
    page = int(request.args.get("page", 1))
    size = request.args.get("size", 36)
    request_args = utils.copy_dict(request.args, {})

    # results = engineer_query(request)
    asc_desc = "desc"
    search_filters = {
        "bool": {
            "must": [
                {"exists": {"field": "cover_image_url"}},
                # {"range": {"date_created": {"lte": datetime.now() - timedelta(hours=12)}}},
                {"range": {"quantity": {"gt": 0}}}
            ],
            "must_not": [
            ]
        }
    }

    search_query = {
        "match_all": {}
    }

    sorting = dict([("date_created", {"order": asc_desc})])

    _body = {"query": {"filtered": {"query": search_query, "filter": search_filters}}, "sort": sorting}

    results = search.query(doc_type="product", body=_body, page=int(abs(page)), size=int(abs(size)),
                           request_args=request_args)

    if results.get("next", None):
        # build next page query parameters
        request_args["page"] = results["next"]
        results["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.get("prev", None):
        # build previous page query parameters
        request_args["page"] = results["prev"]
        results["previous_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    # all_objects = results.get("objects", [])
    # featured_object_lists = []
    # rounds = int(results.get("total", 0) / 4)
    # for i in range(0, rounds + 1):
    #     batch = all_objects[0 + i:4 + i]
    #     featured_object_lists.append(batch)

    batch_results = _batch_results

    return render_domain_template("public/index.html", **locals())


def _batch_results(results, size=4):
    total_len = len(results)
    featured_object_lists = []
    rounds = int(total_len / size)
    for i in range(0, rounds + 1):
        batch = results[0 + i:size + i]
        featured_object_lists.append(batch)

    return featured_object_lists


@public_bp.route('/auctions/')
def auctions():
    page_title = "All Auctions"
    # Protect agains malicous paging parameters

    pages = request.args.get("pages")
    page = int(request.args.get("page", 1))
    size = request.args.get("size", 20)

    # results = engineer_query(request)

    auctions = AuctionService.query.filter().order_by(desc(Auction.last_updated)).all()

    return render_domain_template("public/auctions.html", **locals())


@public_bp.route("/add_to_wishlist/", methods=["POST"])
@public_bp.route("/add_to_wishlist/<int:id>/", methods=["POST"])
def add_to_wishlist(id=None):
    data = request.json
    logger.info(data)
    product = ProductService.get(id)
    if not product:
        return jsonify({})

    data = {"product_id": id}
    UserService.add_to_wishlist(current_user.id, **data)
    list_ids = current_user.wishlist_product_ids
    session["wishlist_product_ids"] = list_ids

    return jsonify({"status": "success"})


@public_bp.route('/update_cart_item/', methods=["POST"])
def update_cart_item():
    cart = fetch_cart()
    data = request.json
    logger.info(data)
    sb_t = 0
    cart_item_form = CartItemForm(**data)
    cart_item_form.csrf_enabled = False
    if cart_item_form.validate_on_submit():
        data = cart_item_form.data
        logger.info(data)
        quantity = data.get("quantity", 1)
        variant_id = data.get("variant_id", 0)
        if quantity == 0:
            delete_session_cart_item(**data)
            # data = {}
            # d_item = uni_cart.uni_cart_items.filter(UnifiedCartItem.product_id == donation_item_id).first()
            # if d_item:
            #     data["making_donation"] = True
            # else:
            #     data["making_donation"] = False

            resp = jsonify(data)

            return resp

        variant = Variant.query.get(variant_id)
        _item = update_session_cart_item(**data)
        resp = jsonify(data)

        return resp
    else:
        data = cart_item_form.data
        logger.info(data)
        logger.info(cart_item_form.errors)

    return jsonify(cart_item_form.errors)


@public_bp.route('/sales/', methods=['GET', 'POST'])
@public_bp.route('/sales/<string:sec_slug>/', methods=['GET', 'POST'])
@public_bp.route('/sales/<string:sec_slug>/<string:cat_slug>/', methods=['GET', 'POST'])
def sales(sec_slug=None, cat_slug=None):
    page_title = "X-Mas Sales"
    flash("Sorry Sale Has Ended...Kindly Check back later.")
    return redirect(url_for('.index'))

    # Protect agains malicous paging parameters
    pages = request.args.get("pages")
    search_q = request.args.get("q", None)
    page = int(request.args.get("page", 1))
    size = request.args.get("size", 30)
    request_args = utils.copy_dict(request.args, {})
    doc_type = "product"

    asc_desc = "desc"

    end_date = datetime(2018,1,1)
    sale_end_time = int(end_date.strftime('%s')) * 1000

    search_filters = {
        "bool": {
            "must": [
                {"exists": {"field": "cover_image_url"}},
                # {"range": {"date_created": {"lte": datetime.now() - timedelta(hours=12)}}},
                {"range": {"quantity": {"gt": 0}}},
                {"term": {"on_sale": True}},
                {"range": {"sale_price": {"gte": 1}}}
            ],
            "must_not": [
                {"term": {"percentage_off": False}}
            ]
        }
    }

    search_query = {
        "match_all": {}
    }

    if search_q:
        search_query = {
            "multi_match": {
                "query": search_q,
                "type": "phrase",
                "fields": ["name", "category_names", "categories.name^5", "description", "variants.name^5"],
                "operator": "or",
                "analyzer": "standard",
                "minimum_should_match": "30%"
            }
        }

    overwrite_body = False
    sorting = dict([("last_updated", {"order": asc_desc})])

    if sec_slug:

        _sections_ = [s for s in fetch_product_categories() if s.get("nested", {}).get("code", "") == sec_slug.lower()]
        if len(_sections_) <= 0:
            return redirect('.sales')

        _section_ = _sections_[0]

        section = search.get("category", _section_.get("nested", {}).get("id", ""))
        if section:
            logger.info("in here")
            # search_filters["bool"]["must"].append({"term": {"category_codes": section.code}})
            search_query = {
                "query": {
                    "nested": {
                        "path": "categories",
                        "query": {
                            "match": {
                                "categories.code": sec_slug
                            }
                        },
                        "inner_hits": {}
                    }
                },
                "sort": sorting
            }

            overwrite_body = True

    if cat_slug:

        _categories = [s for s in fetch_product_categories() if s.get("nested", {}).get("code", "") == cat_slug.lower()]
        if len(_categories) <= 0:
            return redirect('.sales')

        _cat = _categories[0]

        category = search.get("category", _cat.get("nested", {}).get("id", ""))
        if category:
            # search_filters["bool"]["must"].append({"term": {"category_codes": category.code}})
            search_query = {
                "query": {
                    "nested": {
                        "path": "categories",
                        "query": {
                            "match": {
                                "categories.code": cat_slug
                            }
                        },
                        "inner_hits": {}
                    }
                },
                "sort": sorting
            }

            overwrite_body = True

    cart = fetch_cart()

    # results = engineer_query(request)

    _body = {"query": {"filtered": {"query": search_query, "filter": search_filters}}, "sort": sorting}
    if overwrite_body:
        _body = search_query

    # results = category
    results = search.query(doc_type=doc_type, body=_body, page=int(abs(page)), size=int(abs(size)),
                           request_args=request_args)
    if results.get("next", None):
        # build next page query parameters
        request_args["page"] = results["next"]
        results["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.get("prev", None):
        # build previous page query parameters
        request_args["page"] = results["prev"]
        results["previous_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    return render_domain_template("public/sales.html", **locals())

@public_bp.route('/products/', methods=['GET', 'POST'])
@public_bp.route('/products/<string:sec_slug>/', methods=['GET', 'POST'])
@public_bp.route('/products/<string:sec_slug>/<string:cat_slug>/', methods=['GET', 'POST'])
def products(sec_slug=None, cat_slug=None):
    # category = CategoryService.get(id)
    # print category.products
    # if not category:
    #     abort(404)
    page_title = "Products"

    # Protect agains malicous paging parameters
    pages = request.args.get("pages")
    search_q = request.args.get("q", None)
    page = int(request.args.get("page", 1))
    size = request.args.get("size", 18)
    request_args = utils.copy_dict(request.args, {})
    doc_type = "product"

    asc_desc = "desc"

    # if search_q == None and sec_slug == None and cat_slug == None:
    #     i = random.randint(1, 2)
    #     if i == 1:
    #         asc_desc = "asc"
    #     else:
    #         asc_desc = "desc"

    search_filters = {
        "bool": {
            "must": [
                {"exists": {"field": "cover_image_url"}},
                # {"range": {"date_created": {"lte": datetime.now() - timedelta(hours=12)}}},
                {"range": {"quantity": {"gt": 0}}}
            ],
            "must_not": [
            ]
        }
    }

    search_query = {
        "match_all": {}
    }

    if search_q:
        search_query = {
            "multi_match": {
                "query": search_q,
                "type": "phrase",
                # "slop": 1,
                "fields": ["name", "category_names", "categories.name^5", "description", "variants.name^5"],
                "operator": "or",
                "analyzer": "standard",
                "minimum_should_match": "30%"
            }
        }

    overwrite_body = False
    sorting = dict([("last_updated", {"order": asc_desc})])

    if sec_slug:

        _sections_ = [s for s in fetch_product_categories() if s.get("nested", {}).get("code", "") == sec_slug.lower()]
        if len(_sections_) <= 0:
            return redirect('.products')

        _section_ = _sections_[0]

        section = search.get("category", _section_.get("nested", {}).get("id", ""))
        if section:
            logger.info("in here")
            # search_filters["bool"]["must"].append({"term": {"category_codes": section.code}})
            search_query = {
                "query": {
                    "nested": {
                        "path": "categories",
                        "query": {
                            "match": {
                                "categories.code": sec_slug
                            }
                        },
                        "inner_hits": {}
                    }
                },
                "sort": sorting
            }

            overwrite_body = True

            page_title = "%s Products" % section.get("name")

    if cat_slug:

        _categories = [s for s in fetch_product_categories() if s.get("nested", {}).get("code", "") == cat_slug.lower()]
        if len(_categories) <= 0:
            return redirect('.products')

        _cat = _categories[0]

        category = search.get("category", _cat.get("nested", {}).get("id", ""))
        if category:
            # search_filters["bool"]["must"].append({"term": {"category_codes": category.code}})
            search_query = {
                "query": {
                    "nested": {
                        "path": "categories",
                        "query": {
                            "match": {
                                "categories.code": cat_slug
                            }
                        },
                        "inner_hits": {}
                    }
                },
                "sort": sorting
            }

            overwrite_body = True

            page_title = "%s Products" % category.get("name")

    cart = fetch_cart()

    # results = engineer_query(request)

    _body = {"query": {"filtered": {"query": search_query, "filter": search_filters}}, "sort": sorting}
    if overwrite_body:
        _body = search_query

    # results = category
    results = search.query(doc_type=doc_type, body=_body, page=int(abs(page)), size=int(abs(size)),
                           request_args=request_args)
    if results.get("next", None):
        # build next page query parameters
        request_args["page"] = results["next"]
        results["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.get("prev", None):
        # build previous page query parameters
        request_args["page"] = results["prev"]
        results["previous_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    return render_domain_template("public/products.html", **locals())


@public_bp.route('/products/<int:id>/', methods=["GET", "POST"])
@public_bp.route('/products/<int:id>/<string:handle>/', methods=["GET", "POST"])
def product(id, handle=None):
    product = Product.query.get(int(id))

    # allocations = Allocation.query.filter(Allocation.channel_id == channel.id,
    #                                       Allocation.product_id == id).all()
    # ran = [c.variant.price for c in allocations if c.variant.quantity > 0]
    # ran = sorted(ran)
    #
    if not product:
        print "there no such product: " + str(id)
        abort(404)

    page_title = "Product - %s" % product.name
    page_tags = dict(url=request.path, description=product.description, image=product.cover_image_url, title=page_title)

    user = current_user

    bid_end = datetime.today() + timedelta(days=2)

    specs = {}
    tex = None
    pic = None
    vid = None
    dets = ProductDetail.query.filter(ProductDetail.product_id == id)
    if dets.all():
        tab = dets.filter_by(type_id=4).first()
        specs = ProductService.string_to_dict(tab.content)
        tex = dets.filter(ProductDetail.type_id == 1).first()
        pic = dets.filter(ProductDetail.type_id == 2).first()
        vid = dets.filter(~ProductDetail.content.in_([""]), ProductDetail.type_id == 3).first()
    # if user and not user
    #     customer_id = user.customers.first().id
    # else:
    variant_prices = {}
    for i in product.variants:
        variant_prices[str(i.id)] = (utils.whole_number_format(i.price),utils.whole_number_format(i.regular_price) if i.on_sale else None)

    variant_prices = json.dumps(variant_prices)

    customer_id = None
    # print product.variants
    ask_form = AskForm(csrf_enabled=False)
    form = CartForm(csrf_token=False)
    prev_orders = OrderEntry.query.filter(OrderEntry.product_id == product.id, OrderEntry.user_id != None, OrderEntry.order_status_id.in_([2,4])). \
        distinct(OrderEntry.user_id)
    if user.is_authenticated():
        prev_orders = prev_orders.filter(OrderEntry.user_id != user.id)

    doc_type = "product"

    asc_desc = "desc"
    sorting = dict([("last_updated", {"order": asc_desc})])
    try:
        cat = product.categories[1]
    except:
        cat = None

    if cat:
        request_args = utils.copy_dict(request.args, {})
        search_query = {
            "query": {
                "nested": {
                    "path": "categories",
                    "query": {
                        "match": {
                            "categories.code": cat.code
                        }
                    },
                    "inner_hits": {}
                }
            },
            "sort": sorting
        }
        _body = search_query

        also_likes = search.query(doc_type=doc_type, body=_body, page=1, size=6,
                               request_args=request_args)
        if also_likes.get("next", None):
            # build next page query parameters
            request_args["page"] = also_likes["next"]
            also_likes["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

        if also_likes.get("prev", None):
            # build previous page query parameters
            request_args["page"] = also_likes["prev"]
            also_likes["previous_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    form.variant_ids.choices = [(c.id, "%s <span style=\'color:#ff570a;\'>(%s Available)</span>" % (c.name, c.quantity))
                                for c in product.variants if
                                c.quantity > 0]  # ##########################
    form.variant_id.choices = [(c.id, "%s (%s)" % (c.name, c.quantity)) for c in
                               product.variants if
                               c.quantity > 0]  # ##########################
    # cats = Category.query.filter(categories.product_id==product.id).all()
    if form.validate_on_submit():
        data = form.data
        variant = VariantService.get(data.get("variant_id"))
        data.update(product_id=product.id, price=variant.price, customer_id=customer_id)
        fetch_cart()
        cart_id = session.get("cart_id")
        CartService.add_item(cart_id, **data)
        # update_session_cart(**data)
        flash("Product has been successfully added to your cart!".upper())
        return redirect(url_for('.product', id=id, handle=handle))
    else:
        if form.is_submitted() and form.variant_ids.data in ["",None]:
            flash("Please Select a Product Variant to Add to Cart".upper())
        errors = [form.variant_id.errors, form.quantity.errors, form.errors]
        print errors

    cart = fetch_cart()

    if request.method == "GET":
        ProductService.update(product.id, view_count=product.view_count + 1)
        if getattr(user, "id", None) != None:
            ProductViewService.add(product.id, user.id)

    return render_domain_template("public/product_details.html", **locals())


@public_bp.route('/checkout/', methods=["GET", "POST"])
def checkout():
    page_title = "Contact Information & Payment"

    cart = fetch_cart()
    user = current_user
    if cart.cart_total <= 0:
        return redirect(url_for('.products'))

    form = CheckoutForm(obj=cart, csrf_enabled=False)

    card_label_design = _card_label_design

    form.country_code.choices = [(c.id, c.name) for c in fetch_countries()]
    # form.shipment_state.choices = [(c.id,c.name) for c in fetch_states()]
    # print form.shipment_state.choices
    # form.shipment_country.choices = [(c.id, c.name) for c in fetch_countries()]
    # print form.shipment_country.choices
    delivery_options = DeliveryOption.query.filter().all()
    payment_options = PaymentOption.query.filter(PaymentOption.is_enabled == True).all()
    form.state_code.choices = [(0, "Select a state")] + [(c.id, c.name) for c in fetch_states()]
    form.payment_option_id.choices = [(c.id, c.name) for c in payment_options]
    form.payment_option_ids.choices = [(c.id, c.name) for c in payment_options]
    # print form.payment_option_id.choices
    form.delivery_option_code.choices = [(c.id, c.name) for c in delivery_options]
    form.delivery_option_codes.choices = [(c.id, c.name) for c in delivery_options]
    ct = locals().get('cart')
    items = ct.cart_items

    cat_coupons = []
    coupons = []
    cat_coupon_total = 0

    form.card_id.choices = [(0, "none")]
    form.card_ids.choices = [(0, "none")]
    form.bob_coupon.choices = [(0, "none")]
    if current_user.is_authenticated():
        user = current_user
        cards = user.cards
        cat_ids = []

        # find all coupons for a particular product
        for i in items.all():
            li = i.product.categories
            li_ids = [c.id for c in li]
            cat = user.coupons.filter(Coupon.category_id.in_(li_ids), Coupon.min_spend <= i.product.price).first()
            if cat:
                cat_ids.append(cat.id)

        cat_coupons = user.coupons.filter(Coupon.category_id.in_(cat_ids)).all()
        cat_coupon_total = sum([c.amount for c in cat_coupons])
        total = cart.cart_total
        add = sum
        # coupons = user.coupons.filter(Coupon.type == 'bob', Coupon.is_used==False,
        #                               Coupon.is_expired==False).distinct(Coupon.amount).all()
        coupons = user.coupons.filter(Coupon.type == 'bob', Coupon.is_used == False,
                                      Coupon.is_expired == False).order_by(asc(Coupon.amount)).all()
        for c in coupons:
            if c.min_spend > total:
                c.disable = True

        form.bob_coupon.choices = [(0, "=====select one=====")] + [(c.id, "BOB N %s" % number_format(c.amount)) for c in
                                                                   coupons]
        # if len(form.bob_coupon.choices) < 2:
        #     # form.bob_coupon.choices = form.bob_coupon.choices + [(0," coupons")]
        #     form.bob_coupon.choices = [(0, "you don't have any coupons")]
        form.card_ids.choices = [(c.id, c.mask) for c in cards]
        form.card_id.choices = [(c.id, c.mask) for c in cards]

    # print delivery_options

    # form_refresh = form.data.pop('form_refresh')

    # if form_refresh == 'true':
    #     return render_domain_template("frontend/checkout.html", **locals())
    if not form.validate_on_submit():
        print("failed:", form.errors, "|||", form.data)

    if form.validate_on_submit():
        data = form.data
        logger.info(data)
        quick_signup_success = data.get("quick_signup_success", "failure")
        data["address_line1"] = data.get("house_number", "") + ", " + data.get("address_line1", "")
        country = Country.query.filter(Country.id == data.get("country_code", "")).first()
        state = State.query.filter(State.id == data.get("state_code", "")).first()
        delivery_option = DeliveryOption.query.filter(
            DeliveryOption.id == data.get("delivery_option_code", 1)).first()
        data["country_id"] = country.id
        data["state_id"] = state.id
        data["delivery_option_id"] = 1  # delivery_option.id
        data["delivery_charge"] = state.delivery_fee
        # pprint.pprint(**data)
        form.auto_set_shipment.data = True
        p_op = PaymentOption.query.get(form.payment_option_id.data)
        data["payment_option_code"] = p_op.code
        if form.auto_set_shipment.data:
            print "auto"
            data["shipment_name"] = form.first_name.data + " " + form.last_name.data
            data["shipment_phone"] = form.phone.data
            data["shipment_address"] = form.address_line1.data + form.address_line2.data
            data["shipment_city"] = form.city.data
            data["shipment_state"] = state.name
            # data["shipment_state"] = _search.get("state", form.state_code.data).name
            data["shipment_country"] = country.name
        # data["shipment_country"] = _search.get("country", form.country_code.data).name
        else:
            data["shipment_name"] = form.shipment_name.data
            data["shipment_phone"] = form.shipment_phone.data
            data["shipment_address"] = form.shipment_address.data
            data["shipment_city"] = form.shipment_city.data
        # data["shipment_state"] = form.shipment_state.data
        # data["shipment_country"] = form.shipment_country.data
        # cart = utils.populate_obj(cart, data)
        all_coupons = [c.id for c in cat_coupons] + [data.get('bob_coupon')]
        data['coupon_ids'] = all_coupons
        data.pop('total')
        cart = CartService.update(cart.id, **data)
        data['amount'] = CouponService.apply_coupon(all_coupons, cart.total, **{})
        data["discount"] = cart.total - data.get("amount")
        # data['user_id_'] = cart.user_id
        # save_card = data.get("save_card", False)
        # if save_card:
        #     ""
        #     CardService.register(**data)
        # if not UserService.check_user(email=data.get('email')):
        #     u = UserService.create(password='default', **data)
        #     data['user_id_'] = u.id
        if quick_signup_success == "success":
            quick_signup_success = True
            data["quick_signup_success"] = quick_signup_success
        return payment(code=p_op.code, pay_id=p_op.id, **data)
    # print cart.as_dict()

    # if current_user.is_authenticated():
    #     return redirect(url_for(".review"))
    # else:
    #     cart = fetch_cart()
    #     return redirect(url_for('.checkout', continue_as="option"))

    return render_domain_template("public/checkout.html", **locals())


@public_bp.route('/checkout/<int:id>/buy_now/prepare/', methods=["GET", "POST"])
def prepare_buy_now_checkout(id):
    obj = Variant.query.get(id)
    if not obj:
        abort(404)
    user_id = None
    if current_user.is_authenticated():
        user_id = current_user.id
    cart = CartService.create(checkout_type="BuyNow", user_id=user_id)
    data = {"variant_id": id, "user_id": user_id}
    CartService.add_item(cart.id, **data)
    return redirect(url_for('.buy_now_checkout', token=cart.token))


@public_bp.route('/checkout/<string:token>/buy_now/', methods=["GET", "POST"])
def buy_now_checkout(token):
    page_title = "Contact Information & Payment"
    if token in ["", " ", None, "None"]:
        abort(404)
    id = int(token.split("_")[-1])
    cart = Cart.query.get(id)
    if not cart:
        abort(404)
    is_quick_checkout = True
    user = current_user

    form = CheckoutForm(obj=cart, csrf_enabled=False)

    card_label_design = _card_label_design

    form.country_code.choices = [(c.id, c.name) for c in fetch_countries()]
    # form.shipment_state.choices = [(c.id,c.name) for c in fetch_states()]
    # print form.shipment_state.choices
    # form.shipment_country.choices = [(c.id, c.name) for c in fetch_countries()]
    # print form.shipment_country.choices
    delivery_options = DeliveryOption.query.filter().all()
    payment_options = PaymentOption.query.filter(PaymentOption.is_enabled == True).all()
    form.state_code.choices = [(0, "Select a state")] + [(c.id, c.name) for c in fetch_states()]
    form.payment_option_id.choices = [(c.id, c.name) for c in payment_options]
    form.payment_option_ids.choices = [(c.id, c.name) for c in payment_options]
    # print form.payment_option_id.choices
    form.delivery_option_code.choices = [(c.id, c.name) for c in delivery_options]
    form.delivery_option_codes.choices = [(c.id, c.name) for c in delivery_options]
    ct = locals().get('cart')
    items = ct.cart_items

    cat_coupons = []
    coupons = []
    cat_coupon_total = 0

    form.card_id.choices = [(0, "none")]
    form.card_ids.choices = [(0, "none")]
    form.bob_coupon.choices = [(0, "none")]
    if current_user.is_authenticated():
        user = current_user
        cards = user.cards
        cat_ids = []

        # find all coupons for a particular product
        for i in items.all():
            li = i.product.categories
            li_ids = [c.id for c in li]
            cat = user.coupons.filter(Coupon.category_id.in_(li_ids), Coupon.min_spend <= i.product.price).first()
            if cat:
                cat_ids.append(cat.id)

        cat_coupons = user.coupons.filter(Coupon.category_id.in_(cat_ids)).all()
        cat_coupon_total = sum([c.amount for c in cat_coupons])
        total = cart.cart_total
        add = sum
        # coupons = user.coupons.filter(Coupon.type == 'bob', Coupon.min_spend <= total, Coupon.is_used == False,
        #                               Coupon.is_expired == False).distinct(Coupon.amount).all()
        coupons = user.coupons.filter(Coupon.type == 'bob', Coupon.is_used == False,
                                      Coupon.is_expired == False).order_by(asc(Coupon.amount)).all()
        for c in coupons:
            if c.min_spend > total:
                c.disable = True
        form.bob_coupon.choices = [(0, "=====select one=====")] + [(c.id, "BOB N %s" % number_format(c.amount)) for c in
                                                                   coupons]
        # if len(form.bob_coupon.choices) < 2:
        #     # form.bob_coupon.choices = form.bob_coupon.choices + [(0," coupons")]
        #     form.bob_coupon.choices = [(0, "you don't have any coupons")]
        form.card_ids.choices = [(c.id, c.mask) for c in cards]
        form.card_id.choices = [(c.id, c.mask) for c in cards]

    # print delivery_options

    # form_refresh = form.data.pop('form_refresh')

    # if form_refresh == 'true':
    #     return render_domain_template("frontend/checkout.html", **locals())
    if not form.validate_on_submit():
        print("failed:", form.errors, "|||", form.data)

    if form.validate_on_submit():
        data = form.data
        data["address_line1"] = data.get("house_number", "") + ", " + data.get("address_line1", "")
        country = Country.query.filter(Country.id == data.get("country_code", "")).first()
        state = State.query.filter(State.id == data.get("state_code", "")).first()
        delivery_option = DeliveryOption.query.filter(
            DeliveryOption.id == data.get("delivery_option_code", 1)).first()
        data["country_id"] = country.id
        data["state_id"] = state.id
        data["delivery_option_id"] = 1  # delivery_option.id
        data["delivery_charge"] = state.delivery_fee
        # pprint.pprint(**data)
        form.auto_set_shipment.data = True
        p_op = PaymentOption.query.get(form.payment_option_id.data)
        data["payment_option_code"] = p_op.code
        if form.auto_set_shipment.data:
            print "auto"
            data["shipment_name"] = form.first_name.data + " " + form.last_name.data
            data["shipment_phone"] = form.phone.data
            data["shipment_address"] = form.address_line1.data + form.address_line2.data
            data["shipment_city"] = form.city.data
            data["shipment_state"] = state.name
            # data["shipment_state"] = _search.get("state", form.state_code.data).name
            data["shipment_country"] = country.name
        # data["shipment_country"] = _search.get("country", form.country_code.data).name
        else:
            data["shipment_name"] = form.shipment_name.data
            data["shipment_phone"] = form.shipment_phone.data
            data["shipment_address"] = form.shipment_address.data
            data["shipment_city"] = form.shipment_city.data
        # data["shipment_state"] = form.shipment_state.data
        # data["shipment_country"] = form.shipment_country.data
        # cart = utils.populate_obj(cart, data)
        all_coupons = [c.id for c in cat_coupons] + [data.get('bob_coupon')]
        data['coupon_ids'] = all_coupons
        data.pop('total')
        cart = CartService.update(cart.id, **data)
        data['amount'] = CouponService.apply_coupon(all_coupons, cart.total, **{})
        data["discount"] = cart.total - data.get("amount")
        # data['user_id_'] = cart.user_id
        # save_card = data.get("save_card", False)
        # if save_card:
        #     ""
        #     CardService.register(**data)
        # if not UserService.check_user(email=data.get('email')):
        #     u = UserService.create(password='default', **data)
        #     data['user_id_'] = u.id
        data["token"] = cart.token
        return payment(code=p_op.code, pay_id=p_op.id, **data)
    # print cart.as_dict()

    # if current_user.is_authenticated():
    #     return redirect(url_for(".review"))
    # else:
    #     cart = fetch_cart()
    #     return redirect(url_for('.checkout', continue_as="option"))

    return render_domain_template("public/checkout.html", **locals())


@public_bp.route('/checkemail/', methods=["GET"])
def check_email():
    resp = "false"
    email = request.args.get('email')
    user = User.query.filter_by(email=email).first()
    if user:
        resp = "true"
    return resp


@public_bp.route('/checkout/payment/<string:code>/', methods=["GET", "POST"])  # method here was post
def payment(code, **kwargs):
    page_title = "Payment"

    token = kwargs.get("token", None)
    if token and token in ["", " ", None, "None"]:
        abort(404)

    if token:
        id = int(token.split("_")[-1])
        cart = Cart.query.get(id)
        if not cart:
            abort(404)
    else:
        cart = fetch_cart()

    if cart.total <= 0:
        return redirect(url_for('.products'))

    for item in cart.cart_items:
        if item.variant.quantity == 0:
            db.session.delete(item)
            db.session.commit()
    # cart = fetch_cart()

    remember = True
    quick_signup_success = kwargs.get("quick_signup_success", False)

    # payment_option = PaymentType.query.filter(PaymentType.code == code.lower()).first()
    # print payment_option.code
    #
    # if not payment_option:
    # 	abort(404)
    # code = "new_card"
    # Construct a transaction object
    data = dict(payment_option_code=code, payment_option_id=kwargs.get('pay_id'),
                cart_id=cart.id, transaction_status_code="pending", payment_status="pending",
                checkout_type=cart.checkout_type)
    kwargs.update(**data)
    t = TransactionService.create(**kwargs)
    kwargs['narration'] = "payment for order with transaction code %s" % t.code

    if code == "pay_on_delivery":
        TransactionService.review_transaction(status_code='successful', transaction_id=t.id)

        if quick_signup_success:
            user = User.query.filter(User.email == cart.email).first()

            if user:
                current_cart = cart
                login_user(user, remember=remember, force=True)  # This is necessary to remember the user

                # Transfer auth token to the frontend for use with api requests
                __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))

                wishlist_product_ids = user.wishlist_product_ids
                session["wishlist_product_ids"] = wishlist_product_ids
                user.update_last_login()

                if current_cart:
                    cart = Cart.query.filter(Cart.user_id == user.id).first()
                    if not cart:
                        cart = fetch_cart()
                    for item in current_cart.cart_items.all():
                        cart.cart_items.append(item)

                    db.session.add(cart)
                    db.session.commit()
                    session["cart_id"] = cart.id
                session.permanent = True

                resp = redirect(url_for('.transaction_complete', transaction_id=t.id))
                resp.set_cookie("__xcred", __xcred)
                return resp

        return redirect(url_for('.transaction_complete', transaction_id=t.id))

    if code not in ['new_card']:
        # try:
        res = CardService.charge(**kwargs)
        status = res.get("status")
        kwargs['trans_id'] = t.id
        t = TransactionService.review_transaction(status_code=status,
                                                  transaction_id=kwargs.get('trans_id'))
        if quick_signup_success:
            user = User.query.filter(User.email == cart.email).first()

            if user:
                current_cart = cart
                login_user(user, remember=remember, force=True)  # This is necessary to remember the user

                # Transfer auth token to the frontend for use with api requests
                __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))

                wishlist_product_ids = user.wishlist_product_ids
                session["wishlist_product_ids"] = wishlist_product_ids
                user.update_last_login()

                if current_cart:
                    cart = Cart.query.filter(Cart.user_id == user.id).first()
                    if not cart:
                        cart = fetch_cart()
                    for item in current_cart.cart_items.all():
                        cart.cart_items.append(item)

                    db.session.add(cart)
                    db.session.commit()
                    session["cart_id"] = cart.id
                session.permanent = True

                resp = redirect(url_for('.transaction_complete', transaction_id=kwargs.get('trans_id')))
                resp.set_cookie("__xcred", __xcred)
                return resp
        return redirect(url_for('.transaction_complete', transaction_id=kwargs.get('trans_id')))
    # except e:
    #     print e.data
    #     t = TransactionService.review_transaction(status_code='failed', transaction_id=t.id)


    else:
        try:
            print '===tryin===='
            kwargs['trans_id'] = t.id
            res = CardService.pay(**kwargs)
            status = res.get("status")
            t = TransactionService.review_transaction(status_code=status,
                                                      transaction_id=kwargs.get('trans_id'))
            save_card = kwargs.get("save_card", False)
            if save_card:
                ""
                CardService.register(user_id=cart.user_id, **kwargs)

            if quick_signup_success:
                user = User.query.filter(User.email == cart.email).first()

                if user:
                    current_cart = cart
                    login_user(user, remember=remember, force=True)  # This is necessary to remember the user

                    # Transfer auth token to the frontend for use with api requests
                    __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))

                    wishlist_product_ids = user.wishlist_product_ids
                    session["wishlist_product_ids"] = wishlist_product_ids
                    user.update_last_login()

                    if current_cart:
                        cart = Cart.query.filter(Cart.user_id == user.id).first()
                        if not cart:
                            cart = fetch_cart()
                        for item in current_cart.cart_items.all():
                            cart.cart_items.append(item)

                        db.session.add(cart)
                        db.session.commit()
                        session["cart_id"] = cart.id
                    session.permanent = True

                    resp = redirect(url_for('.transaction_complete', transaction_id=kwargs.get('trans_id')))
                    resp.set_cookie("__xcred", __xcred)
                    return resp

            return redirect(url_for('.transaction_complete', transaction_id=kwargs.get('trans_id')))
        except FurtherActionException, e:
            print e.data
        except ValidationFailed, e:
            print e.data
            t = TransactionService.review_transaction(status_code='failed', transaction_id=t.id)


@public_bp.route('/vbv/<string:code>/confirmation/', methods=['GET', 'POST'])
def vbv_confirmation(code):
    """ Transaction code for vbv confirmation. This will route into the service to fetch and update the necessary view """
    data = request.args
    logger.info('Response message from vbv view')
    logger.info('Transaction object from vbv')
    trans = TransactionService.confirm_vbv(code, **data)
    resp = {"id": trans.code}

    return render_domain_template('frontend/vbv.html', **locals())


@public_bp.route('/transactions/<int:transaction_id>/transaction_complete/')
def transaction_complete(transaction_id):
    transaction = TransactionService.get(transaction_id)
    return render_domain_template("public/transaction_complete.html", **locals())


@public_bp.route('/signup/', methods=["GET", "POST"])
def signup():
    form = AccountSignUpForm(csrf_enabled=False)
    if not form.validate_on_submit():
        print 'failed===========', form.errors
    if form.validate_on_submit():
        data = form.data
        # data["username"] = data["email"]
        user = UserService.create(**data)

        if user is not None:
            login_user(user, force=True)  # This is necessary to remember the user

            next_url = request.args.get("next", url_for(".index"))
            # logger.info(next_url)
            resp = redirect(next_url)
            # Transfer auth token to the frontend for use with api requests
            __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))

            # return set_cookies(resp, user_id=user.id, remember=remember, credentials=__xcred)
            resp.set_cookie("__xcred", __xcred)

            wishlist_product_ids = user.wishlist_product_ids
            session["wishlist_product_ids"] = wishlist_product_ids
            user.update_last_login()

        return redirect(url_for('.signup_successful'))

    return render_domain_template("public/signup.html", **locals())


# return render_domain_template('public/signup.html', **locals())


# login page
@public_bp.route('/login/', methods=["GET", "POST"])
def login():
    next_url = request.args.get("next") or url_for(".index")
    if current_user.is_authenticated():
        flash("Please Logout to Login or Register a New Account".upper())
        return redirect(next_url)
    if request.method == 'GET':
        session['next_url'] = next_url
    r_code = request.args.get("code", None)
    action = request.args.get("action", None)

    form = LoginForm()
    if form.validate_on_submit():
        data = form.data
        current_cart = fetch_cart()
        username = data["username"]
        password = data["password"]
        remember = data.get("remember", True)
        user = authenticate_user(username, password)

        if user is not None:
            login_user(user, remember=True, force=True)  # This is necessary to remember the user

            # include user roles into scope
            # identity_changed.send(app, identity=Identity(user.id))
            # next_url = request.args.get("next", url_for(".index"))
            next_url = session.get('next_url', url_for(".index"))
            logger.info(next_url)
            resp = redirect(next_url)
            # Transfer auth token to the frontend for use with api requests
            __xcred = base64.b64encode("%s:%s" % (user.email, user.get_auth_token()))

            # return set_cookies(resp, user_id=user.id, remember=remember, credentials=__xcred)
            resp.set_cookie("__xcred", __xcred)
            resp.set_cookie("show_something_light", "true")

            wishlist_product_ids = user.wishlist_product_ids
            session["wishlist_product_ids"] = wishlist_product_ids
            user.update_last_login()

            if current_cart:
                cart = Cart.query.filter(Cart.user_id == user.id).first()
                if not cart:
                    cart = fetch_cart()
                for item in current_cart.cart_items.all():
                    cart.cart_items.append(item)

                db.session.add(cart)
                db.session.commit()
                session["cart_id"] = cart.id
            session.permanent = True

            return resp
        else:
            flash("The username or password is invalid", "login")
            return redirect(url_for('.index'))

    s_form = AccountSignUpForm(csrf_enabled=False, referrer=r_code)
    if s_form.is_submitted() and s_form.validate_on_submit() !=True:
        msg = None
        for error in s_form.errors:
            try:
                msg = s_form.errors[error]
                flash("Error: %s"%msg[0])
            except:
                pass
        if not msg:
            flash("Error in Signup information provided....Please check and Try again.")
        return redirect(url_for('.login', action='reg'))
    if s_form.validate_on_submit():
        data = s_form.data
        data['is_incognito'] = False
        logger.info(data)
        current_cart = fetch_cart()
        # data["username"] = data["email"]
        user = UserService.create(**data)
        login_user(user, remember=True, force=True)

        next_url = request.args.get("next", url_for(".signup_successful"))
        # logger.info(next_url)
        resp = redirect(next_url)
        # Transfer auth token to the frontend for use with api requests
        __xcred = base64.b64encode("%s:%s" % (user.email, user.get_auth_token()))

        # return set_cookies(resp, user_id=user.id, remember=remember, credentials=__xcred)
        resp.set_cookie("__xcred", __xcred)
        resp.set_cookie("show_something_light", "true")

        wishlist_product_ids = user.wishlist_product_ids
        session["wishlist_product_ids"] = wishlist_product_ids
        user.update_last_login()

        if current_cart:
            cart=CartService.attach_user(current_cart.id, user.id)
            session["cart_id"] = cart.id
        session.permanent = True

        return resp


    return render_domain_template("public/login.html", **locals())

@public_bp.route('/forgot_password/', methods=["GET", "POST"])
def forgot_password():
    hide_imp_flash=True
    next_url_ = request.args.get("next_url") or url_for(".index")
    form = ForgotPasswordForm()
    if form.validate_on_submit():
        data = form.data
        username = data["username"]
        user = authenticate_forgot_password(username)

        if user is not None:
            token = UserService.request_new_user_password(user.id)
            resp= redirect(url_for(".reset_request_successful"))
            resp.set_cookie("show_something_light", "false")
            return resp
        else:
            login_error = "This email address or username does not exist"

    return render_domain_template("public/forgot_password.html", **locals())


# recover password page
@public_bp.route('/reset_request_successful/')
def reset_request_successful():
    hide_imp_flash = True
    return render_domain_template("public/reset_request_successful.html", **locals())


# recover password page
@public_bp.route('/recover_password/<string:code>/', methods=["GET", "POST"])
def recover_password(code):
    reset_token = PasswordResetToken.query.filter(PasswordResetToken.code == code).first()
    user = User.query.get(reset_token.user_id)

    if not reset_token:
        flash("Invalid Password Reset Token... Please request for another token.")
        return redirect(url_for('.index'))

    if reset_token.is_expired:
        flash("Password Reset Token Expired... Please request for another token.")
        return redirect(url_for('.index'))

    if not user:
        abort(404)

    form = UserResetPasswordForm()

    if form.validate_on_submit():
        password = form.data["password"]
        current_cart = fetch_cart()
        user = UserService.reset_password(user.id, password)
        reset_token.is_expired = True
        db.session.add(reset_token)
        db.session.commit()

        if user is not None:
            login_user(user, remember=True, force=True)  # This is necessary to remember the user

            # include user roles into scope
            # identity_changed.send(app, identity=Identity(user.id))
            # next_url = request.args.get("next", url_for(".index"))
            next_url = url_for(".reset_success")
            resp = redirect(next_url)
            # Transfer auth token to the frontend for use with api requests
            __xcred = base64.b64encode("%s:%s" % (user.email, user.get_auth_token()))

            # return set_cookies(resp, user_id=user.id, remember=remember, credentials=__xcred)
            resp.set_cookie("__xcred", __xcred)
            resp.set_cookie("show_something_light", "false")

            wishlist_product_ids = user.wishlist_product_ids
            session["wishlist_product_ids"] = wishlist_product_ids
            user.update_last_login()

            if current_cart:
                cart = Cart.query.filter(Cart.user_id == user.id).first()
                if not cart:
                    cart = fetch_cart()
                for item in current_cart.cart_items.all():
                    cart.cart_items.append(item)

                db.session.add(cart)
                db.session.commit()
                session["cart_id"] = cart.id
            session.permanent = True
            email_data = dict(name=user.full_name)
            send_user_notification.delay(user.id, 'prs', subject='Password Reset Success', **email_data)
            return resp
        return redirect(url_for(".reset_success"))

    return render_domain_template("public/reset_password.html", **locals())


# recover password page
@public_bp.route('/reset_success/')
def reset_success():
    return render_domain_template("public/reset_successful.html", **locals())



@public_bp.route('/logout/')
@login_required
def logout():
    # if not current_user.is_authenticated():
    #     return redirect(url_for('.signup'))
    logout_user()
    session.pop("cart_id")
    resp = redirect(url_for('.index'))
    resp.set_cookie("__xcred", '', expires=0)
    resp.set_cookie("show_something_light", "true")
    return resp


@public_bp.route('/signup/successful/')
def signup_successful():
    """ The signup workflow for a sendbox merchant """

    return render_domain_template('public/signup_successful.html', **locals())


@public_bp.route('/signup/verification/<string:hash>/')
def signup_verification(hash):
    """ The signup verification for a sendbox merchant """

    user = User.query.filter_by(verification_hash=hash).first()

    # This should come first. If the user is already verified, then redirect to login instead
    if user and user.email_verified:
        return redirect(url_for('.login'))

    # The verification online for the first time needs to happen here
    if user:
        # user = UserService.verify_email(user.id)
        user = UserService.verify_email(user.id)

        login_user(user, remember=True, force=True)  # This is necessary to remember the user
        flash("Your Email Verification was successful...Welcome to BuyOrBidng!!!".upper())

        return redirect(url_for('.index'))

    return render_domain_template('public/invalid_verification.html')


@public_bp.route('/phone_verification')
def phone_verification():
    """ The phone verification"""

    user = User.query.filter_by(verification_hash=hash).first()

    # This should come first. If the user is already verified, then redirect to login instead
    if user and user.phone_verified:
        return redirect(url_for('.login'))

    # The verification online for the first time needs to happen here
    if user:
        # user = UserService.verify_email(user.id)
        user = UserService.update(user.id, email_verified=True, is_verified=True)

        login_user(user, remember=True, force=True)  # This is necessary to remember the user

        return redirect(url_for('.index'))

    return render_domain_template('public/invalid_verification.html')


@public_bp.route('/verify/<int:id>/')
def verify(id):
    """ request verification """

    user = User.query.get(id)
    next_url = request.args.get('next_url')
    # This should come first. If the user is already verified, then redirect to login instead
    if user and user.email_verified:
        return redirect(url_for('.login'))

    email_data = {}
    try:
        url = "%s%s" % (
            app.config.get("P_CANONICAL_URL"), url_for('public_bp.signup_verification', hash=user.verification_hash))

        email_data = dict(link=url)
        send_user_notification.delay(user.id, 'msv', subject='Welcome to BuyOrBid', **email_data)
    except:
        pass

    return redirect(next_url)


@public_bp.route('/contact-us/', methods=["GET", "POST"])
def contact_us():
    page_title = "Contact Us"
    form = ContactForm()

    if current_user:
        form = ContactForm(obj=current_user)

    if form.validate_on_submit():
        data = form.data
        if current_user:
            data["user_id"] = current_user.id
        else:
            ref_user = User.query.filter(User.email == data.get("email", "")).first()
            if ref_user:
                data["user_id"] = ref_user.id

        obj = AdminMessageService.create(**data)
        message_data = obj.as_json_dict()
        message_data.pop('email')
        message_data['sender'] = current_user.full_name
        message_data['user'] = False
        message_data['full_name'] = "Buyorbid"
        send_notification.delay('new_message', email="info@buyorbidng.com",
                                **message_data)

        flash("Your message has been sent! Thank you".upper())
        return redirect(url_for(".contact_us"))

    return render_domain_template("public/contact-us.html", **locals())


@public_bp.route('/auctions/1/auction/link/', methods=["GET", "POST"])
def auction():
    next_url = request.args.get("next") or url_for(".index")
    bid_end = datetime.today() + timedelta(days=3)
    return render_domain_template("public/auction.html", **locals())


@public_bp.route('/profile/', methods=["GET", "POST"])
@login_required
def profile():
    is_profile_index = True
    next_url = request.args.get("next") or url_for(".index")
    bid_end = datetime.today() + timedelta(days=3)

    pages = request.args.get("pages")
    page = int(request.args.get("page", 1))
    size = request.args.get("size", 20)

    request_args = utils.copy_dict(request.args, {})

    # results = engineer_query(request)
    asc_desc = "desc"
    search_filters = {
        "bool": {
            "must": [
                {"exists": {"field": "cover_image_url"}},
                # {"range": {"date_created": {"lte": datetime.now() - timedelta(hours=12)}}},
                {"range": {"quantity": {"gt": 0}}}
            ],
            "must_not": [
            ]
        }
    }

    search_query = {
        "match_all": {}
    }

    sorting = dict([("last_updated", {"order": asc_desc})])

    _body = {"query": {"filtered": {"query": search_query, "filter": search_filters}}, "sort": sorting}

    results = search.query(doc_type="product", body=_body, page=int(abs(page)), size=int(abs(size)),
                           request_args=request_args)

    if results.get("next", None):
        # build next page query parameters
        request_args["page"] = results["next"]
        results["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.get("prev", None):
        # build previous page query parameters
        request_args["page"] = results["prev"]
        results["previous_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

    all_objects = results.get("objects", [])
    featured_object_lists = []
    rounds = int(results.get("total", 0) / 4)
    for i in range(0, rounds + 1):
        batch = all_objects[0 + i:4 + i]
        featured_object_lists.append(batch)

    return render_domain_template("public/profile/index.html", **locals())


@public_bp.route('/profile/affiliates/', methods=['GET', 'POST'])
@login_required
def profile_affiliates():
    return render_domain_template("public/profile/affiliates.html")


@public_bp.route('/facebook/post/')
@login_required
def facebook_post():
    _user = current_user

    if not _user.referral_code:
        _user = users.create_referral_code(_user.id)

    url = 'https://www.facebook.com/dialog/feed'

    params = {
        'app_id': app.config.get('FACEBOOK_APP_ID'),
        'display': 'page',
        'caption': 'Traclist',
        'description': 'Get a N500 coupon each time you refer a friend to BuyOrBid. The more friends you invite, the more discount coupons you get. Get yours Now!!! ',
        'link': app.config.get('CANONICAL_URL') + url_for('.refer_address', code=_user.referral_code),
        'picture': app.config.get('CANONICAL_URL') + '/static/images/icon.png',
        'redirect_uri': app.config.get('CANONICAL_URL') + url_for('.profile_affiliates')
    }

    response = requests.post(url, data=params)

    return redirect(response.url)


@public_bp.route('/r/<string:code>/', methods=['GET'])
def refer_address(code):
    page_title = 'Refer User'

    referee = User.query.filter(func.lower(User.referral_code) == code.lower()).first()

    if not referee:
        return redirect(url_for('.index'))

    url = url_for('.login') + '?code=' + referee.referral_code

    return redirect(url)


@public_bp.route('/profile/referrals/', methods=['GET', 'POST'])
@login_required
def referrals():
    page_title = 'Referrals'
    _user = current_user

    if not _user.referral_token:
        _user = users.create_referral_code(_user.id)

    referral_link = app.config.get('CANONICAL_URL') + url_for('.refer_address', code=_user.referral_token)

    post_id = request.args.get('post_id')

    if post_id:
        flash('Referral Code successfully shared on your facebook timeline', 'share_success')

    referrals = Referral.query.filter(Referral.user_id == _user.id).order_by(asc('date_created'))

    verified_count = len(filter(lambda x: x.is_verified, referrals))

    pending_count = len(filter(lambda x: not x.is_verified, referrals))

    form = EmailListForm()

    if form.validate_on_submit():
        emails = form.emails.data
        for email in emails:
            users.refer_address(_user.id, email)
        return redirect(url_for('.referrals'))

    return render_domain_template('referrals.html', **locals())


@public_bp.route('/twitter/oauth/')
@login_required
def twitter_oauth():
    redirect_url = app.config.get('CANONICAL_URL') + url_for('.twitter_authorized')

    if session.has_key('twitter_token'):
        del session['twitter_token']

    return twitter.authorize(url=redirect_url)


@public_bp.route('/twitter/authorized/')
@login_required
@twitter.auth.authorized_handler
def twitter_authorized(resp):
    if resp is None:
        flash(u'You denied the request to sign in.')
        return redirect(url_for('.referrals'))

    session['twitter_token'] = (
        resp['oauth_token'],
        resp['oauth_token_secret']
    )

    return redirect(url_for('.twitter_share'))


@public_bp.route('/twitter/share/')
@login_required
def twitter_share():
    _user = current_user

    if not _user.referral_code:
        _user = users.create_referral_code(_user.id)

    post = 'Get a N1000 discount voucher each time you invite a friend to register on traclist.com ' + app.config.get(
        'CANONICAL_URL') + url_for('.refer_address', code=_user.referral_code)

    response = twitter.post(url='https://api.twitter.com/1.1/statuses/update.json', data={'status': post})
    if response.status == 403:
        flash("Couldn't post tweet!", 'share_error')
    else:
        flash('successfully shared on your Twitter timeline', 'share_success')

    return redirect(url_for('.referrals'))


@public_bp.route('/linkedin/oauth/')
@login_required
def linkedin_oauth():
    redirect_url = app.config.get('CANONICAL_URL') + url_for('.linkedin_oauth')

    oauth = LinkedInOauth()

    code = request.args.get('code', None)

    error = request.args.get('error', None)

    if code:
        response = oauth.exchange_token(code=code, redirect_uri=redirect_url)
        credentials = json.loads(response.content)
        return linkedin_share(credentials)

    elif error:
        flash('You did not grant the required permission', 'share_error')
        return redirect(url_for('.referrals'))

    else:
        resp = oauth.get_authorization_url(redirect_uri=redirect_url)
        return redirect(resp.url)


@public_bp.route('/profile/auctions/', methods=['GET', 'POST'])
@login_required
def profile_auctions(obj_id=None):
    return render_domain_template('public/profile/auctions.html')


@public_bp.route('/profile/addresses/', methods=['GET', 'POST'])
@login_required
def addresses(obj_id=None):
    return render_domain_template('public/profile/addresses.html')


@public_bp.route('/profile/messages/', methods=['GET', 'POST'])
@public_bp.route('/profile/messages/<int:obj_id>/', methods=['GET', 'POST'])
@login_required
def messages(obj_id=None):
    obj = None
    if obj_id:
        message = AdminMessage.query.get(obj_id)
        if not message:
            return abort(404, conversation="Message does not exist")

    messages = AdminMessage.query.filter(AdminMessage.user_id == current_user.id).all()
    form = ResponseForm(csrf_enabled=False)
    next_url = request.args.get("next")
    if form.validate_on_submit():
        data = form.data
        data["admin_message_id"] = obj_id
        data["user_id"] = current_user.id
        message = AdminMessageResponseService.create(**data)
        message_data = message.as_json_dict()
        message_data['sender'] = current_user.full_name
        message_data['user'] = False
        message_data['full_name'] = "Buyorbid"
        send_notification.delay('new_message', subject='You have a new message', email="info@buyorbidng.com",
                                **message_data)

    return render_domain_template("public/profile/messages.html", **locals())


@public_bp.route('/profile/conversations/', methods=['GET', 'POST'])
@public_bp.route('/profile/conversations/<int:conversation_id>/', methods=['GET', 'POST'])
@login_required
def conversations(conversation_id=None):
    is_profile_msg = True
    conversation = None
    if conversation_id:
        conversation = Conversation.query.get(conversation_id)
        if not conversation:
            return abort(404, conversation="this conversation does not exist")

    user = current_user
    m = Message.query.filter(or_(Message.receiver_id == user.id, Message.sender_id == user.id)).distinct(
        Message.conversation_id).all()
    c_ids = [c.conversation_id for c in m]
    conversations = Conversation.query.filter(Conversation.id.in_(c_ids))

    form = ReplyForm(csrf_enabled=False)
    next_url = request.args.get("next")
    if form.validate_on_submit():
        data = form.data
        data["conversation_id"] = conversation.id
        MessageService.create(**data)
        return redirect(url_for('.conversations', conversation_id=conversation_id))

    return render_domain_template("public/profile/conversations.html", **locals())


@public_bp.route('/profile/orders/', methods=['GET', 'POST'])
@login_required
def orders():
    is_profile_orders = True
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        search_q = request.args.get("q", None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Order.query.filter(Order.user_id == current_user.id).order_by(desc(Order.date_created))

    results = query.paginate(page, 20, False)

    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))
    return render_domain_template("public/profile/orders.html", **locals())


@public_bp.route('/profile/orders/<int:id>/<string:code>/', methods=['GET', 'POST'])
@login_required
def order_invoice(id, code):
    pageTitle = "Order Invoice"

    order = Order.query.get(id)

    if not order:
        abort(404)

    transaction = Transaction.query.filter(Transaction.order_id == order.id).order_by(desc(Transaction.id)).first()
    return printing.return_pdf("prints/order_invoice.html", **locals())


@public_bp.route('/profile/wishlists/', methods=['GET', 'POST'])
@login_required
def wishlists():
    is_profile_wishlists = True
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        search_q = request.args.get("q", None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = WishlistEntry.query.filter(WishlistEntry.user_id == current_user.id).order_by(
        desc(WishlistEntry.date_created))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))
    return render_domain_template("public/profile/wishlists.html", **locals())


@public_bp.route('/profile/settings/', methods=['GET', 'POST'])
@login_required
def profile_settings():
    page_title = "Settings"
    is_profile_settings = True
    update_profile_form = ProfileUpdateForm(obj=current_user)
    password_reset_form = ChangePasswordForm()
    profile_pic_form = BlankForm()
    user = current_user

    return render_domain_template("public/profile/settings.html", **locals())


@public_bp.route('/profile/upload_profile_pic/', methods=['GET', 'POST'])
@login_required
def upload_profile_pic():
    is_profile_settings = True
    files = request.files.getlist("images")

    uploaded_files, errors = handle_uploaded_photos(files, (160, 160))

    uploaded_file = uploaded_files[0]
    path = uploaded_file.get("filepath")

    upload_result = upload(path, public_id="profile/BOB-PP%s" % "_" + str(current_user.id))
    os.remove(path)
    url = upload_result["url"]
    data = {"profile_pic": url}
    UserService.update(current_user.id, **data)

    return redirect(url_for('.profile'))


@public_bp.route('/profile/settings/update/', methods=['GET', 'POST'])
@login_required
def profile_update():
    update_profile_form = ProfileUpdateForm()
    is_profile_settings = True

    if update_profile_form.validate_on_submit():
        data = update_profile_form.data
        user = UserService.update(current_user.id, **data)
        flash("Profile Update Successful")
        return redirect(url_for('.profile_settings'))

    return redirect(url_for('.profile_settings'))


@public_bp.route('/profile/settings/password_reset/', methods=['GET', 'POST'])
@login_required
def profile_password_reset():
    password_reset_form = ChangePasswordForm()
    flash("info|message")
    is_profile_settings = True

    if password_reset_form.validate_on_submit():
        data = password_reset_form.data
        user = UserService.change_password(current_user.id, **data)
        flash("Password Reset Successful")
        return redirect(url_for('.profile_settings'))

    return redirect(url_for('.profile_settings'))


@public_bp.route('/profile/settings/deactivate/', methods=['GET', 'POST'])
@login_required
def profile_deactivate():
    prev_url = url_for(".profile")
    is_profile_settings = True

    if request.method == "POST":
        obj = UserService.disable_user(current_user.id)
        flash("Account Successfully Deactivated!")
        return redirect(url_for('.logout'))

    return redirect(prev_url)


@public_bp.route('/ask_customer/', methods=['GET', 'POST'])
@login_required
def ask_customer():
    form = AskForm(csrf_enabled=False)
    next_url = request.args.get("next") or url_for(".index")
    if form.validate_on_submit():
        data = form.data
        r_id = data.get('receiver_id')
        s_id = data.get('sender_id')
        conversation = None
        message = Message.query.filter(Message.receiver_id.in_([r_id, s_id]),
                                       Message.sender_id.in_([r_id, s_id])).first()
        if message:
            conversation = Conversation.query.get(message.conversation_id)
        if not conversation:
            conversation = ConversationService.create()
            conversee = ConverseeService.create(user_id=data.get('sender_id'), conversation_id=conversation.id)
            conversee1 = ConverseeService.create(user_id=data.get('receiver_id'), conversation_id=conversation.id)
        message = MessageService.create(sender_id=data.get('sender_id'), receiver_id=data.get('receiver_id'),
                                        conversation_id=conversation.id, text=data.get('text'),
                                        product_id=data.get('product_id', None))

        message_data = message.as_json_dict()
        message_data['sender'] = message.sender.full_name
        message_data['link'] = '%s/%s' % (
        app.config.get('P_CANONICAL_URL'), 'profile/conversations')
        message_data['user'] = True
        message_data['full_name'] = current_user.full_name
        message_data['body'] = message.text
        send_notification.delay('new_message', subject='You have a new message', email=current_user.email,
                                **message_data)

    return redirect(next_url)


@public_bp.route('/place_bid/', methods=["GET", "POST"])
def place_bid():
    if not current_user.is_authenticated():
        return jsonify({})

    number_format = utils.no_decimal_number_format
    if request.method == "POST":
        data = request.json
        data["user_id"] = current_user.id
        form = BidForm(**data)
        auction = Auction.query.get(form.auction_id.data)

        if auction.bid_end < datetime.today():
            return make_response(jsonify({"status": "error", "message": "Auction bid time has expired."}))

        try:
            form.validate()
            data = form.data
            bid = BidService.create(**data)
            bid_created.send(bid.id)
            data = {"status": "success",
                    "payload": {"price": bid.price, "min_price": auction.min_price, "bid_count": auction.bid_count}}
            data = jsonify(data)
            response = make_response(data)
            return response
        except Exception as e:
            raise
    else:
        response = make_response("Request Method not Allowed")
        return response

#
# @public_bp.route('/emails/')
# def emails():
#     return render_domain_template("services/emails/layout.html", **locals())


@public_bp.route('/resources/')
def resources():
    args = request.args
    page = args.get('page')
    request_args = utils.copy_dict(request.args, {})

    if page == "faq":
        return render_domain_template('public/resources/faq.html')
    if page == "terms":
        return render_domain_template('public/resources/terms.html')
    if page == "privacy":
        return render_domain_template('public/resources/privacy.html')
    if page == "return":
        return render_domain_template('public/resources/return.html')
    if page == "membership":
        return render_domain_template('public/resources/membership.html')
    if page == "auction":
        return render_domain_template('public/resources/auction.html')

    return render_domain_template('public/resources/terms.html')


app.register_blueprint(public_bp)
