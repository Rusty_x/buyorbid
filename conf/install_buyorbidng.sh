#!/bin/sh
# @Author: kunsam002
# @Date:   2017-10-01
# @Last Modified by:   Olukunle Ogunmokun
# @Last Modified time: 2017-10-01

# Create application account
sudo useradd -M -s /bin/bash -U buyorbid
sudo addgroup buyorbid

sudo chgrp buyorbid /opt/buyorbid
sudo chmod g+s /opt/buyorbid

sudo find /opt/buyorbid -type d -exec chgrp buyorbid {} +
sudo find /opt/buyorbid -type d -exec chmod g+s {} +


# copy uwsgi.conf to upstart
sudo cp uwsgi.conf /etc/init/
sudo ln -s /lib/init/upstart-job /etc/init.d/uwsgi
#sudo ln -s /opt/buyorbid/conf/celery.service /etc/systemd/system/celery.service

# copy celery.conf to upstart
sudo cp celery.conf /etc/init/
sudo ln -s /lib/init/upstart-job /etc/init.d/celery

# create necessary uwsgi folders
sudo mkdir -p /var/log/uwsgi
sudo mkdir -p /etc/uwsgi/apps-enabled
sudo mkdir -p /etc/uwsgi/apps-available
sudo mkdir -p /var/log/celery
#sudo mkdir -p /var/run/celery

# necessary log folder for logging
sudo mkdir -p /var/log/buyorbid
sudo mkdir -p /var/log/nginx/buyorbid
sudo chown -R buyorbid.buyorbid /var/log/buyorbid
sudo chown -R buyorbid.buyorbid /var/log/nginx
sudo chown -R buyorbid.buyorbid /var/log/celery

#sudo chown -R buyorbid.buyorbid /var/log/celery
#sudo chown -R buyorbid.buyorbid /var/run/celery

# create necessary symbolic links for uwsgi
sudo ln -s /opt/buyorbid/conf/uwsgi/testmain.ini /etc/uwsgi/apps-enabled
sudo ln -s /opt/buyorbid/conf/uwsgi/testadmin.ini /etc/uwsgi/apps-enabled

# create necessary symbolic links for nginx
sudo ln -s /opt/buyorbid/conf/nginx/buyorbidng.com /etc/nginx/sites-enabled/
sudo rm /etc/nginx/sites-enabled/default


# Restart nginx server
#sudo service uwsgi restart
sudo service nginx restart
sudo service celery restart
#sudo systemctl restart celery.service
