#!/bin/sh
# @Author: kunsam002
# @Date:   2017-10-01
# @Last Modified by:   Olukunle Ogunmokun
# @Last Modified time: 2017-10-01

# Create application account
sudo useradd -M -s /bin/bash -U buyorbid
sudo addgroup buyorbid

sudo chgrp buyorbid /opt/buyorbid
sudo chmod g+s /opt/buyorbid

sudo find /opt/buyorbid -type d -exec chgrp buyorbid {} +
sudo find /opt/buyorbid -type d -exec chmod g+s {} +


# setup uwsgi service
sudo ln -s /opt/buyorbid/conf/uwsgi.service /etc/systemd/system/uwsgi.service
sudo ln -s /opt/buyorbid/conf/celery.service /etc/systemd/system/celery.service

# necessary log folder for logging
sudo mkdir -p /var/log/buyorbid
sudo mkdir -p /var/log/nginx/buyorbid
sudo mkdir -p /var/log/uwsgi

sudo mkdir -p /var/log/celery
sudo mkdir -p /var/run/celery

# set necessary permissions
sudo chown -R buyorbid.buyorbid /var/log/buyorbid
sudo chown -R buyorbid.buyorbid /var/log/nginx
sudo chown -R buyorbid.buyorbid /var/log/uwsgi

sudo chown -R buyorbid.buyorbid /var/log/celery
sudo chown -R buyorbid.buyorbid /var/run/celery

# create necessary symbolic links for nginx
sudo ln -s /opt/buyorbid/conf/nginx/buyorbidng.com /etc/nginx/sites-enabled/
sudo rm /etc/nginx/sites-enabled/default
sudo mkdir -p /etc/uwsgi
sudo ln -s /opt/buyorbid/conf/uwsgi/*.ini /etc/uwsgi/
## create necessary symbolic links for uwsgi
#sudo ln -s /opt/buyorbid/conf/uwsgi/main.ini /etc/uwsgi/apps-enabled
#sudo ln -s /opt/buyorbid/conf/uwsgi/admin.ini /etc/uwsgi/apps-enabled

# Restart nginx server
sudo systemctl daemon-reload
sudo systemctl restart uwsgi.service
sudo systemctl restart celery.service
sudo systemctl restart nginx.service

