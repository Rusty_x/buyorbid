upstream web_cluster {

  #least_conn;
  server  188.166.58.57;
}

server {
        listen 80;
        server_name *.buyorbidng.com www.buyorbidng.com m.buyorbidng.com buyorbidng.com;

  location ~ ^/(img|js|css|fonts)/ {  # |pi||ext|theme
        root                    /opt/buyorbid/buyorbid/static;
        add_header              Cache-Control public;
        expires                 30d;
        #access_log              off;
        access_log                            /var/log/nginx/buyorbid/main.access.log;
        error_log                             /var/log/nginx/buyorbid/main.error.log;
  }

  location / { try_files $uri @yourapplication; }

  location /static {
    alias                    /opt/buyorbid/buyorbid/static;
    add_header              Cache-Control public;
    expires                 30d;
  }

  location /robots.txt {
		alias                    /opt/buyorbid/buyorbid/robots.txt;
		add_header              Cache-Control public;
		expires                 30d;
	}

  location /favicon.ico {
    deny        all;
  }

  location @yourapplication {
    include uwsgi_params;
    uwsgi_pass unix:///tmp/main.sock;
  }


}



server {
	listen 80;
    server_name admin.buyorbidng.com;

  location ~ ^/(img|js|css|fonts)/ {  # |pi||ext|theme
    root                    /opt/buyorbid/buyorbid/static;
    add_header              Cache-Control public;
    expires                 30d;
    #access_log              off;
    access_log                            /var/log/nginx/buyorbid/admin.access.log;
    error_log                             /var/log/nginx/buyorbid/admin.error.log;
  }

  location / { try_files $uri @yourapplication; }

  location /static {
    alias                    /opt/buyorbid/buyorbid/static;
    add_header              Cache-Control public;
    expires                 30d;
  }

  location /robots.txt {
		alias                    /opt/buyorbid/buyorbid/robots.txt;
		add_header              Cache-Control public;
		expires                 30d;
	}

  location /favicon.ico {
    deny        all;
  }

  location @yourapplication {
    include uwsgi_params;
    uwsgi_pass unix:///tmp/admin.sock;
  }

}