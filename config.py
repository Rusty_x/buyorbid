"""
config.py

The file that will be used to specify each flask application's config
"""
import os
import socket
from celery.schedules import crontab
from kombu import Queue, Exchange


"""
config.py

Flask configuration script

"""

import os

class Config(object):
    """
    Base configuration class. Subclasses should include configurations for
    testing, development and production environments
    """

    DEBUG = True
    SECRET_KEY = '\x91c~\xc0-\xe3\'fB\xe19P0E\x93\xe8\x91`uu"\xd0\xb6\x01/\x0c\xed\\\xbd]HB\x99k\xf8'
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    DOMAIN = 'buyorbid'
    PROTOCOL = "http://"
    CANONICAL_URL = "%s%s" % (PROTOCOL, DOMAIN)

    P_CANONICAL_URL = "%s:%s" % (CANONICAL_URL, "5400")

    SYSTEM_USERNAME = 'admin'
    # Setup directory

    SETUP_DIR = os.path.join(os.path.dirname(os.path.abspath(__name__)), "setup")

    # Upload settings
    UPLOADS_DEFAULT_DEST = os.path.join(os.path.dirname(os.path.abspath(__name__)), "uploads")
    UPLOADS_ARCHIVE_DEFAULT_DEST = os.path.join(os.path.dirname(os.path.abspath(__name__)), "uploads/archive")

    # redis
    BASE_DIRECTORY = os.path.dirname(os.path.abspath(__name__))
    STATIC_FOLDER = os.path.join(BASE_DIRECTORY, 'buyorbid/static') # where all the webpack urls will recide

    # Integrating webpack configuration
    WEBPACK_MANIFEST_PATH = os.path.join(STATIC_FOLDER, 'manifest.json')
    WEBPACK_ASSETS_URL = '/static/'

    # Integrating file uploads
    UPLOADS_PATH = os.path.join(BASE_DIRECTORY, 'uploads')
    ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
    DEFAULT_UPLOAD_NAME = 'file' # default file name to use for all uploaded files
    MAX_CONTENT_LENGTH = 100 * 1024 * 1024 # 100MB max.

    # Celery setup
    CELERY_BROKER_URL = "redis://localhost:6379"
    CELERY_RESULT_BACKEND = "redis://localhost:6379"
    CELERY_DEFAULT_QUEUE = "buyorbid.app"

    # AWS Integration
    AWS_ACCESS_KEY = 'AKIAI6HSK7DZX4P7KYAA'
    AWS_ACCESS_SECRET = "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"
    AWS_REGION = 'us-east-1'
    # AWS_BUCKET = "sendbox.website.docs"

    # DEV_PHONE = "08126466955"
    # DEV_PHONE = "07046399814"
    # DEV_PHONE = "09059378437"
    # DEV_EMAIL = "rotola@buyorbidng.com"
    DEV_EMAIL = "rotolaakinsowon@yahoo.com, akins.rotola@gmail.com, kunle@officemotion.net"
    # DEV_EMAIL = "kunle@buyorbidng.com"

    # Flutterwave Test API
    FLUTTERWAVE_ENDPOINT = "http://staging1flutterwave.co:8080/pwc/rest/"
    FLUTTERWAVE_KEY = "tk_3jvt5rMpxg1odSeXqmBG"
    FLUTTERWAVE_MERCHANT_ID = "tk_I3N9pwQBfe"

    # elasticsearch
    ES_CONFIG = [
        {"host": "127.0.0.1"},
        {"timeout": "60"}
    ]

    ES_INDEX = "buyorbid"
    ES_HOST = "localhost:9200"

    INFOBIP_DEFAULT_SENDER = 'BuyOrBid'
    INFOBIP_SMS_URL = "https://api.infobip.com"
    INFOBIP_USERNAME = "BuyorBid"
    INFOBIP_PASSWORD = "forB0b!."

    DEV_SMS = ["2348126466955"]

    # # notifier configuration
    # NOTIFIER_API_KEY = "32095a0b0bbb89f05cf82bd023390dcb"
    # NOTIFIER_APP_ID = 1
    # NOTIFIER_USERNAME = "rusty_x"
    # NOTIFIER_SENDER = {
    #     "name": "Bhadguy",
    #     "email": "no-reply@buyorbidng.com"
    # }
    # NOTIFIER_SENDER = "BuyOrBid1"
    # NOTIFIER_BASE_URI = "http://0.0.0.0:5050/api/v1"

    # # Sparkpost
    # SPARKPOST_SENDER = {
    #     "name": "Sendbox",
    #     "email": "no-reply@email.sendbox.ng"
    # }
    # SPARKPOST_API_KEY = "e3e2cc4012616c0503cdc88215db13b442671d47"

    REDIS_URL = 'redis://localhost:6379'
    CACHE_TYPE = 'redis'
    CACHE_REDIS_URL = 'redis://localhost:6379'

    QUEUES = ['high', 'medium', 'low']

    GOOGLE_API_KEY = 'AIzaSyAqhs8ZmrtgPn_7luJb-NTFSMBBXspPRlM'
    # Billing Integration
    BILLING_PUBLIC_KEY = "sb_tEvcLneptnhgDS7R5g44jw"
    BILLING_PRIVATE_SBKEY = "sb_ud+8tRZFft7UXocOdOuZWQ"
    BILLING_USERNAME = "rusty"
    BILLING_BASE_URI = "http://0.0.0.0:5300/api/v1"
    BILLING_MERCHANT_KEY = "cln_a3C/KkZnOd9YF0zcr87/8Q"

    ZOHO_AUTH_TOKEN = "Zoho-authtoken 621a5b39b99be515b323ba8f1301f6fd"
    ZOHO_ACC_ID = "3650728000000008001"
    ZOHO_SENDER = "BuyOrBid<info@buyorbidng.com>"

    REFERRAL_COUPON = True
    REFERRAL_COUPON_AMOUNT = [500]

    REFERRAL_BID_CREDIT = True
    REFERRAL_BID_AMOUNT = 50

    SIGNUP_COUPON = True
    SIGNUP_COUPON_AMOUNT = [500,500,500,500,1000,2000,5000]

    SIGNUP_BID_CREDIT = True
    SIGNUP_BID_AMOUNT = 50

    # Configuring sentry logging
    SENTRY_DSN = "https://57e3c8f118af4c29b9032e7cf8f10eec:43c615ff7eaa45c9a54584314b63115c@sentry.io/230631"
    SENTRY_INCLUDE_PATHS = ['buyorbid']
    SENTRY_USER_ATTRS = ['first_name', 'last_name', 'full_name', 'email']

    FACEBOOK_APP_ID = '869553939886897'
    FACEBOOK_APP_SECRET = '37f4f7cd2cf2aaa3d3b9c53f1916b1c8'

    TWITTER_CLIENT_ID = '924310715355357185'
    TWITTER_CLIENT_SECRET = '9N3CERZ8VHigkxb2bH06EYDrepmW1cEQyHCytxY8izGCtjl5nG'

    INSTAGRAM_CLIENT_ID = 'f40066d2ab3c4244ad647b99dfb85dfc'
    INSTAGRAM_CLIENT_SECRET = '82a6d0217d6d43cb802336deb651fe9f'



class DevConfig(Config):
    """Development configurations"""
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost/buyorbid'
    DATABASE = SQLALCHEMY_DATABASE_URI


class TestProdConfig(Config):
    """Development configurations"""
    DEBUG = False
    PROTOCOL = "http://"
    DOMAIN = "buyorbidng.com"
    CANONICAL_URL = "%s%s" % (PROTOCOL, DOMAIN)
    P_CANONICAL_URL = CANONICAL_URL
    # SQLALCHEMY_DATABASE_URI = 'mysql+mysqldb://buyorbid:buyorbid@localhost/buyorbid'
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost/buyorbid'
    DATABASE = SQLALCHEMY_DATABASE_URI
    LOGFILE_NAME = "buyorbid"


class ProdConfig(Config):
    """Development configurations"""
    DEBUG = False
    PROTOCOL = "http://"
    DOMAIN = "buyorbidng.com"
    CANONICAL_URL = "%s%s" % (PROTOCOL, DOMAIN)
    P_CANONICAL_URL = CANONICAL_URL
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost/buyorbid'
    DATABASE = SQLALCHEMY_DATABASE_URI
    LOGFILE_NAME = "buyorbid"

