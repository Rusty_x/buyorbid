"""
domains.py

@Author:    rusty & kunsam002

Handles subdomain parsing and extraction for each individual shop

"""

from flask import g, request, render_template, redirect, current_app
import htmlmin

from .utils import detect_user_agent


def prepare_subdomain():
    """ Functionality to prepare information for a subdomain request. """

    app = current_app

    host = request.host.split(":")[0]
    domain = app.config.get("DOMAIN")  # Must be present!!
    subdomain = host[:-len(domain)].rstrip('.')

    ua = detect_user_agent(request.user_agent.string)

    g.subdomain = subdomain.lower()

    # g.template_path = "mobile" if subdomain == "m" else "main"

    g.template_path = "mobile" if ua.is_mobile or g.subdomain is "m" else "main"


def rewrite_uri():
    """
    Functionality to determine if the application is accessed by a mobile device and redirect to the mobile version
    """

    app = current_app

    ua = detect_user_agent(request.user_agent.string)
    mobile_domain = app.config.get("MOBILE_DOMAIN")  # Must be present!!

    # print request.scheme, request.host, request.path, request.query_string, request.remote_addr

    if ua.is_mobile:
        # redirect to a new url scheme
        _protocol = request.host.split(":")[1] if len(request.host.split(":")) == 2 else None
        mobile_domain = "%s:%s" % (mobile_domain, _protocol) if _protocol else mobile_domain

        mobile_url = "%s://%s%s" % (request.scheme, mobile_domain, request.path)
        if request.query_string:
            mobile_url = "%s?%s" % (mobile_url, request.query_string)

        return redirect(mobile_url)


def prepare_template_name(name):
    """ Helper function to prepare the template name """
    return "%s/%s" % (g.template_path, name)


def render_domain_template(name, **kwargs):
    """ Functionality to select whether to render the main or mobile template, depending on  user agent access """
    prepare_subdomain()

    template_name = prepare_template_name(name)

    _html = render_template(template_name, **kwargs)

    # return render_template(template_name, **kwargs)

    return htmlmin.minify(_html, remove_empty_space=True, remove_comments=True)


def snippet(name):
    return prepare_template_name("snippets/%s.html" % name)
