"""
excel.py

@Author: rusty & kunsam002

This module creates a pre-styled workbook
"""

from openpyxl import Workbook
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font

title_font = Font(name="Calibri", color="000000", size=10, bold=True)
text_font = Font(name="Calibri", color="000000", size=10)
sub_text_font = Font(name="Calibri", color="000000", size=10)
fill = PatternFill(fill_type=None, start_color='FFFFFF', end_color='F2F2F2')
border = Border(left=Side(border_style=None, color='000000'), right=Side(border_style=None, color='000000'),
                top=Side(border_style='thin', color='666666'), bottom=Side(border_style=None, color='000000'),
                diagonal=Side(border_style=None, color='000000'), diagonal_direction=0,
                outline=Side(border_style='thin', color='000000'), vertical=Side(border_style='thin', color='000000'),
                horizontal=Side(border_style='thin', color='000000'))

title_border = Border(left=Side(border_style=None, color='000000'), right=Side(border_style=None, color='000000'),
                      top=Side(border_style='thin', color='666666'), bottom=Side(border_style='medium', color='000000'),
                      diagonal=Side(border_style=None, color='000000'), diagonal_direction=0,
                      outline=Side(border_style='thin', color='000000'), vertical=Side(border_style='thin', color='000000'),
                      horizontal=Side(border_style='thin', color='000000'))

alignment = Alignment(horizontal='general', vertical='center', text_rotation=0, wrap_text=True, shrink_to_fit=False, indent=0)
right_alignment = Alignment(horizontal='right', vertical='center', text_rotation=0, wrap_text=False, shrink_to_fit=False, indent=0)
number_format = "#,##0.00"
protection = Protection(locked=True, hidden=False)


class TitleStyle:
    def __init__(self):
        self.style = Style(font=title_font, border=title_border, alignment=alignment, protection=protection, fill=fill, number_format=number_format)


class RightTitleStyle:
    def __init__(self):
        self.style = Style(font=title_font, border=title_border, alignment=right_alignment, protection=protection, fill=fill, number_format=number_format)


class TextStyle:
    def __init__(self):
        self.style = Style(font=text_font, border=border, alignment=alignment, protection=protection, fill=fill, number_format=number_format)


class SubTextStyle:
    def __init__(self):
        self.style = Style(font=sub_text_font, border=Border(), alignment=alignment, protection=protection, fill=fill, number_format=number_format)
