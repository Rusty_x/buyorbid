"""
pdf.py

@Author: rusty & kunsam002

This module provides weasy_print printing functionality
"""

from flask_weasyprint import HTML, render_pdf
from weasyprint import HTML as _HTML
import os
from flask import render_template, current_app as app


def pdf_response(html_string):
    """
    converts the html string into a pdf document and returns a HTTP response for it
    :param html_string: String to be converted to pdf document
    """

    return render_pdf(HTML(string=html_string))


def pdf_stream(html_string):
    """
    converts the html string into a pdf stream. Will be used along with a file structure
    :param html_string: String to be converted to pdf document
    """
    return HTML(string=html_string).write_pdf()


def pdf_file(html_string, filename):
    """
    converts the html string into a pdf temporary file and returns the source. Useful for email attachements
    :param html_string: String to be converted to pdf document
    :param filename: filename of generated pdf
    """
    if not filename.endswith(".pdf"):
        filename += ".pdf"

    _filename = os.path.join(app.config.get("DOWNLOADS_FOLDER"), "pdf/%s" % filename)
    _doc = open(_filename, "wb")
    _pdf = _HTML(string=html_string).write_pdf()
    _doc.write(_pdf)
    _doc.close()
    os.chdir(os.path.join(app.config.get("DOWNLOADS_FOLDER"), "pdf"))
    filepath = os.path.join(os.path.basename(os.getcwd()), filename)

    return filepath

def create_pdf(template_path, filename, data={}):

    """ create a temporary pdf file from an html template """

    html = render_template(template_path, **data)

    _pdf = pdf_file(html, filename)

    return _pdf
