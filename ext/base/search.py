from elasticsearch_dsl.field import InnerObject, InnerObjectWrapper, Field as _Field
from elasticsearch_dsl import Index, DocType, Boolean, Integer, Float, String, Date, Object, MetaField
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import class_mapper
import importlib
import inspect
import pprint


class CustomInnerObject(InnerObject):
    def __init__(self, *args, **kwargs):
        self._doc_class = kwargs.pop('doc_class', InnerObjectWrapper)
        super(CustomInnerObject, self).__init__(*args, **kwargs)

    def _serialize(self, data):
        if data is None:
            return None
        return data.to_dict()


class CustomNested(CustomInnerObject, _Field):
    name = 'nested'

    def __init__(self, *args, **kwargs):
        # change the default for Nested fields
        kwargs.setdefault('multi', True)
        super(CustomNested, self).__init__(*args, **kwargs)


class DynamicNested(CustomInnerObject, _Field):
    name = 'nested'
    _param_defs = {'properties': {'type': 'field', 'hash': True}}

    class Meta:
        dynamic = MetaField('false')

    def __init__(self, *args, **kwargs):
        # change the default for Nested fields
        kwargs.setdefault('multi', False)
        kwargs.setdefault('dynamic', True)
        super(DynamicNested, self).__init__()


class StandardType(DocType):

    def __init__(self, obj=None, extras=[], **kwargs):
        super(StandardType, self).__init__(**kwargs)
        objects = self._doc_type.mapping.properties._params
        properties = objects['properties']
        if obj:
            extras = extras + ['pk'] # adding support for dynamic primary key discovery
            _data = obj.as_dict(extras=extras)
            self.meta.id = _data.get("pk", None)
            for key, value in _data.items():
                if properties.has_key(key):
                    setattr(self, key, value)

    def save(self, **kwargs):
        try:
            _obj = self.__class__.get(self.meta.id)
            _obj.delete()
        except Exception as e:
            print e, 'Creating it now'
        finally:
            return super(StandardType, self).save(**kwargs)


class DynamicField(Object, _Field):

    class Meta:
        all = MetaField(enabled=True)
        dynamic = MetaField(True)


def create_index(name, settings={}, ignore=None):
    """ Experiment to install index with settings """
    index = Index(name)
    index.settings(**settings)
    index.create(ignore=ignore)

    return index

def delete_index(name, settings={}, ignore=None):
    """ Experiment to install index with settings """
    index = Index(name)
    index.settings(**settings)
    index.delete(ignore=ignore)


def index_object(model_module_name, search_module_name, class_name, obj_id, skip=False):

    module = importlib.import_module(model_module_name)
    mappings = importlib.import_module(search_module_name)

    model_class = getattr(module, class_name)

    extras = getattr(model_class, "extra_fields", [])

    if not model_class:
        return False

    search_class = getattr(mappings, class_name)

    if not search_class:
        return False

    if skip is True and search_class.get(obj_id, ignore=[404]) is not None:
        print "Skipping [%s] with id [%s]. It already exists" % (class_name, obj_id)
        return search_class.get(obj_id)

    obj = model_class.query.get(obj_id)

    if not obj:
        return False

    search_obj = search_class(obj=obj, extras=extras)
    return search_obj.save()


def delete_indexed_object(search_module_name, class_name, obj_id):

    mappings = importlib.import_module(search_module_name)
    search_class = getattr(mappings, class_name)

    if not search_class:
        return False

    search_obj = search_class.get(obj_id)

    if not search_obj:
        return False

    return search_obj.delete()


SEARCH_FIELDS = {
    'String': String,
    'Integer': Integer,
    'Float': Float,
    'Date': Date,
    'DateTime': Date,
    'Text': String,
    'Boolean': Boolean
}


def generate_mapping(module_name, model_class, suffix=''):
    """ Dynamically generate all search classes based on the models that are being fed into it """

    _name = "%s%s" % (model_class.__name__, suffix)
    mapper = class_mapper(model_class)

    mappings = importlib.import_module(module_name)

    attrs = mapper.attrs
    columns = mapper.columns

    mapped_data = dict(__module__=module_name)

    # find all properties and hybrid properties
    props = inspect.getmembers(model_class, lambda x: isinstance(x, (property,)))

    for x in attrs:
        if x.__class__.__name__ == 'RelationshipProperty':
            new_mappers = [y for y in x.mapper.attrs if y.__class__.__name__ != 'RelationshipProperty' and x.uselist is False]
            _properties = dict()

            for c in new_mappers:
                _NESTED_MAPPING_MODEL = SEARCH_FIELDS.get(x.mapper.columns[c.key].type.__class__.__name__)

                _NESTED_MAPPING = _NESTED_MAPPING_MODEL(required=False, fields={"raw": String(index="not_analyzed")}) if _NESTED_MAPPING_MODEL is String \
                    else _NESTED_MAPPING_MODEL(required=False)
                _properties[c.key] = _NESTED_MAPPING

            mapped_data[x.key] = CustomNested(doc_class=CustomInnerObject, properties=_properties, multi=x.uselist, required=False)

        else:
            _MAPPING_MODEL = SEARCH_FIELDS.get(columns[x.key].type.__class__.__name__)

            _MAPPING = _MAPPING_MODEL(required=False, fields={"raw": String(index="not_analyzed")}) if _MAPPING_MODEL is String else _MAPPING_MODEL(required=False)
            mapped_data[x.key] = _MAPPING

    for _p_name, _p_value in props:
        _str_names = ['name', 'url', 'description', 'code', 'full_name', 'platform_name', "origin_description",
                      'short_description', 'long_description', 'approx_name', "destination_description"]
        if _p_name in _str_names:  # hack to support name value as it is quite popular
            mapped_data[_p_name] = String(required=False, fields={"raw": String(index="not_analyzed")})

    cls = type(_name, (mappings.Standard,), mapped_data)

    # cls.init()

    setattr(mappings, cls.__name__, cls)

    return cls
