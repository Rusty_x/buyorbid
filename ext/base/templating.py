"""
templating.py

@Author:    rusty & kunsam002

This module is responsible for converting templates into string (either html or text)
"""

from jinja2 import TemplateNotFound
from flask import render_template, current_app
import os


def generate_content(template_dir, template_name, data={}):
    """
    Generates email content by using the template name and the given data
    email templates are stored as either .html or .txt in the templates email directory.
    When a template name is given, it locates the html version by concatenating the template name with '.html'
    and the text template with '.txt'.

    :param template_dir: The path to the template file
    :param template_name: The template name
    :param data: Template data to merge with

    :returns: rendered text
    """

    logger = current_app.logger

    _template = os.path.join(template_dir, template_name)

    try:
        _text = render_template(_template, **data)
        return _text

    except TemplateNotFound, e:
        logger.info(e)
