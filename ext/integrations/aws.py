"""
s3.py

@Author: kunsam002 & rusty

"""

import os
import boto3.session
import mimetypes
from flask import current_app
from premailer import transform
import htmlmin
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.utils import COMMASPACE
import re

mimetypes.init()
FOLDER_MAPS = {'image': 'images', 'application': 'documents'}

EMAIL_REGEX = re.compile(r'^.+@([^.@][^@]+)$', re.IGNORECASE)

class AWS(object):
    """ Core amazon services to integrate """

    def __init__(self, app=None, aws_access_key=None, aws_access_secret=None, aws_region=None, aws_bucket=None, sender=None, **kwargs):
        self.app = app if app else current_app
        self.aws_access_key = aws_access_key if aws_access_key else self.app.config.get("AWS_ACCESS_KEY", aws_access_key)
        self.aws_access_secret = aws_access_secret if aws_access_secret else self.app.config.get("AWS_ACCESS_SECRET", aws_access_secret)
        self.aws_region = aws_region if aws_region else self.app.config.get("AWS_REGION", aws_region)
        self.aws_bucket = aws_bucket if aws_bucket else self.app.config.get("AWS_BUCKET", aws_bucket)
        self.sender = sender if sender else self.app.config.get("ZOHO_SENDER")

        self.session = boto3.session.Session(aws_access_key_id=self.aws_access_key, aws_secret_access_key=self.aws_access_secret,
                                             region_name=self.aws_region)
        self.logger = self.app.logger

    def initialize_client(self, name):
        """ Initialize and return an amazon aws client """
        return self.session.client(name)

    def initialize_resource(self, name):
        """ Initialize an amazon aws resource """
        return self.session.resource(name)


class S3(AWS):
    """ Amazon S3 Integration """

    def __init__(self, app, **kwargs):
        super(S3, self).__init__(app=app, **kwargs)

        try:
            self.s3 = self.initialize_resource('s3')
            self.bucket = self.create_bucket(self.aws_bucket)
        except Exception, e:
            self.logger.error(e)

    def create_bucket(self, name):
        """ creates a new amazon s3 bucket or returns the bucket if it exists """
        if not name:
            raise Exception('Bucket name is required')

        try:
            bucket = self.s3.create_bucket(Bucket=name)
            return bucket
        except Exception:
            raise

    def upload_object(self, file_path, directory=None, auto_delete=False, acl='public-read'):
        """ Creates an amazon s3 object and adds it to the bucket. If auto_delete is True, then the uploaded file is deleted from disk """
        try:

            mimetype, extra = mimetypes.guess_type(file_path)
            directory = '%s/' % directory if directory else ''
            filename = os.path.basename(str(file_path))
            key = "%s%s" % (directory, filename)
            self.s3.Object(self.bucket.name, key).put(Body=open(file_path, 'rb'), ContentType=mimetype)
            acl_obj = self.s3.ObjectAcl(self.bucket.name, key)
            acl_obj.put(ACL=acl)

            url = "%s/%s/%s" % (self.s3.meta.client.meta.endpoint_url, self.bucket.name, key)

            if auto_delete and os.path.exists(file_path):
                os.remove(file_path)

            # return the data for this file
            ext = os.path.splitext(file_path)[1]
            ext = ext.replace(".", "")
            return {'mimetype': mimetype, 'ext': ext, 'url': url, 'name': filename}

        except Exception, e:
            print e
            raise

    def delete_object(self, file_path, directory=None):
        """ Delete an amazon s3 object from the bucket """

        try:
            directory = '%s/' % directory if directory else ''
            key = "%s%s" % (directory, file_path)

            obj = self.s3.Object(self.bucket.name, key)
            obj.delete()

        except:
            raise


class SES(AWS):

    """
    Amazon Simple Email Service Integration
    """

    def __init__(self, app, **kwargs):
        super(SES, self).__init__(app=app, **kwargs)

        try:
            self.ses = self.initialize_client('ses')
            self.app = app
        except Exception, e:
            self.logger.error(e)

    def send(self, from_=None, recipients=None, cc=None, bcc=None, reply_to=None, subject="", text="", html=None, files=None, delete_files=False, **kwargs):
        """
        Proxy email function. This email function serves as a proxy to specific implementations supported within this module

        :param sender: Sender email address
        :param to: List of email addresses to send to
        :param reply_to: email address to messages to
        :param subject: subject of the email
        :param text: text email content
        :param html: html email content
        :param files: attachment files
        :param delete_files: delete files after sending emails
        :return:
        """
        if recipients:
            recipients = str(recipients)
            recipients = [r.strip() for r in recipients.split(',')]

        if not recipients:
            recipients = []

        _dev_mode = self.app.config.get("EMAIL_DEV_ONLY", False)
        _dev_emails = self.app.config.get("DEV_EMAILS", [])

        if _dev_mode:
            recipients = _dev_emails

        if not from_:
            print "ses if block", self.sender
            from_ = self.sender

        # if not from_:
        #     from_ = self.app.config.get("EMAIL_DEFAULT_SENDER", "Traclist <alerts@emails.traclist.com>")

        # Use premailer to inline the css
        if html:
            html = transform(html, base_url=self.app.config.get("CANONICAL_URL"))
            html = htmlmin.minify(html, remove_empty_space=True, remove_comments=True)

        message = MIMEMultipart()

        message['Subject'] = subject
        message['Reply-To'] = reply_to or from_
        message['From'] = from_
        message['Cc'] = cc
        message['Bcc'] = bcc

        if isinstance(recipients, (str, unicode)):
            recipients = recipients.split(",")

        if text:
            _text = MIMEText(text, 'text')
            message.attach(_text)

        if html:
            html = html.encode('utf-8')
            _html = MIMEText(html, 'html')
            message.attach(_html)

        if files:
            for item in files:
                _pdf = MIMEApplication(open(item[1], 'rb').read())
                _pdf.add_header('Content-Disposition', 'attachment', filename='%s' % item[0])
                message.attach(_pdf)

        print recipients, "recipients===="
        for value in recipients:
            if value and isinstance(value, (str, unicode)) and EMAIL_REGEX.match(value):
                self.ses.send_raw_email(Destinations=[value], RawMessage={'Data': message.as_string()})

        if delete_files and files:
            for _, f in files:
                os.remove(f)
