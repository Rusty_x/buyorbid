

from flask import current_app, request
import requests
import json
from pprint import pprint

from ext.base.exceptions import FurtherActionException, ValidationFailed
from ext.base.utils import encrypt_data, encrypt_data_pyaes


class Billing:

    def __init__(self, app, uri=None, private_key=None, public_key=None, username=None):
        self.app = app if app else current_app
        self.public_key = public_key if public_key else self.app.config.get("BILLING_PUBLIC_KEY", public_key)
        self.private_key = private_key if private_key else self.app.config.get("BILLING_PRIVATE_SBKEY", private_key)
        self.username = username if username else self.app.config.get("BILLING_USERNAME", username)

        self.uri = uri if uri else self.app.config.get("BILLING_BASE_URI", uri)
        self.headers = {
            'Content-Type': 'application/json',
            "Authorization": self.public_key
        }
        self.auth = (self.username, self.public_key)

    def request(self, **data):
        url = "/".join([self.uri, "requests"])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def charge(self, **data):
        card_key = data.get('card_key')
        url = "/".join([self.uri, "cards/%s/charge" % card_key.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def preauthorize(self, **data):
        card_key = data.get('card_key')
        url = "/".join([self.uri, "cards/%s/preauthorize" % card_key.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def capture(self, **data):
        trans_code = data.get('trans_code')
        url = "/".join([self.uri, "transactions/%s/capture" % trans_code.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def refund(self, **data):
        trans_code = data.get('trans_code')
        url = "/".join([self.uri, "transactions/%s/refund" % trans_code.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def fl_refund(self, **data):
        trans_code = data.get('trans_code')
        url = "/".join([self.uri, "transactions/%s/fl_refund" % trans_code.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def void(self, **data):
        trans_code = data.get('trans_code')
        url = "/".join([self.uri, "transactions/%s/void" % trans_code.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def pay(self, **data):  # would need redirect
        url = "/".join([self.uri, "card_pay"])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def add_card(self, **data): # would need redirect
        url = "/".join([self.uri, "register_card"])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def charge_wallet(self, **data):
        token = data.get('token', None)
        if token is None:
            raise FurtherActionException({"No token": "You need to setup your wallet first"})
        url = "/".join([self.uri, "wallets/%s/charge" % token.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def load_wallet(self, **data):
        token = data.get('token', None)
        if token is None:
            raise ValidationFailed({"No token": "You need to setup your wallet first"})
        url = "/".join([self.uri, "wallets/%s/load" % token.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def wallet_transfer(self, **data):
        token = data.get('token', None)
        if token is None:
            raise FurtherActionException({"No token": "You need to setup your wallet first"})
        url = "/".join([self.uri, "wallets/%s/transfer" % token.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def refund_wallet(self, **data):
        token = data.get('token', None)
        if token is None:
            raise FurtherActionException({"No token": "You need to setup your wallet first"})
        url = "/".join([self.uri, "wallets/%s/refund" % token.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def topup_charge_wallet(self, **data):
        token = data.get('token', None)
        if token is None:
            raise FurtherActionException({"No token": "You need to setup your wallet first"})
        url = "/".join([self.uri, "wallets/%s/topup_charge" % token.lower()])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def create_customer(self, **data):
        url = "/".join([self.uri, "customers"])
        encrypted_data = encrypt_data_pyaes(self.private_key, data)
        return self.post(url, encrypted_data)

    def post(self, url, encrypted_data):
        """post the encrypted data using requests and catch errors"""

        try:
            resp = requests.post(url, headers=self.headers, data=json.dumps(encrypted_data))
            pprint("Inside post method of billing ")
            pprint(url)
            pprint(resp.content)
            res = resp.json()
            pprint(res)

            return res

        except Exception, e:
            print e
            return {"status": "error", "message": "An internal error occured. Please contact the administrator", "status_code": "-RR"}
