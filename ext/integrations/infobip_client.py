"""
infobip_client.py

@Author: kunsam002 & rusty

Infobip SMS Flask integration.
"""
import base64
import json

from infobip.clients import *
from infobip.api.model.sms.mt.send.textual.SMSTextualRequest import SMSTextualRequest
from flask import current_app, render_template_string
from ext.base import utils, templating
import requests


class InfoBipConfig:
    def __init__(self, base_url, username, password, api_key, token):
        self.username = username
        self.password = password
        self.base_url = base_url
        self.api_key = api_key
        self.token = token


class InfoBip:
    """
    Infobip SMS integration class. This is used in sending out sms messages
    """

    def __init__(self, app=None, base_url=None, username=None, password=None, sender=None, template_dir="",
                 template_source=None, api_key=None, token=None):
        # If parameters are passed into __init__, override default config values
        self.app = app if app else current_app
        self.base_url = base_url if base_url else self.app.config.get("INFOBIP_SMS_URL", base_url)
        self.username = username if username else self.app.config.get("INFOBIP_USERNAME", username)
        self.password = password if password else self.app.config.get("INFOBIP_PASSWORD", password)
        self.sender = sender if sender else self.app.config.get("INFOBIP_DEFAULT_SENDER", "SENDBOX")
        self.template_dir = template_dir if template_dir else self.app.config.get("INFOBIP_TEMPLATE_DIR", template_dir)
        self.template_source = template_source if template_source else self.app.config.get("INFOBIP_TEMPLATE_SOURCE",
                                                                                           "directory")
        self.api_key = api_key
        self.token = token
        self.config = InfoBipConfig(self.base_url, self.username, self.password, self.api_key, self.token)

    # def generate_template_sms(self, template, phone_numbers, message_data, code, sender):
    #
    #     # format phone numbers to valid country formats
    #     numbers = utils.format_phone_numbers(phone_numbers, code)
    #
    #     # Generate message body
    #     text = templating.generate_content(self.template_dir, template, data=message_data)
    #
    #     _data = {"numbers": numbers, "sender": sender, "text": text, "username": self.username,
    #              "password": self.password}
    #
    #     return _data
    #
    def generate_string_sms(self, template_string, phone_numbers, message_data, code, sender):

        # format phone numbers to valid country formats
        numbers = utils.format_phone_numbers(phone_numbers, code)

        # Generate message body using template string
        text = render_template_string(template_string, message_data)

        return text
    #

    def send(self, text=None, phone_numbers=None, from_=None, template_string=None, code='NG', voice=False, language='en'):

        send_sms_client = send_single_textual_sms(self.config)
        request = SMSTextualRequest()

        # verify that a sender value is used
        if from_ is None:
            from_ = str(self.sender)
        print from_

        if not phone_numbers:
            raise Exception("Destination phone number(s) is required")

        # if self.template_source is "directory" and template_name is not None:
        #     _message_data = self.generate_template_sms(template_name, phone_numbers, text, code, from_)
        #
        if voice:
            headers = {}
            auth = base64.encodestring('%s:%s' % (self.username, self.password)).replace('\n', '')
            headers["Authorization"] = "Basic %s" % auth
            headers["content-type"] = "application/json"
            url = "https://api.infobip.com/tts/3/single"
            numbers = utils.format_phone_numbers(phone_numbers, code)
            print numbers

            data = {"to":numbers[0], "text":text, "from":from_, "language":language}

            res = requests.post(url, headers=headers, data=json.dumps(data))
            return res

        if self.template_source is "string" and template_string is not None:
            _message_data = self.generate_string_sms(template_string, phone_numbers, text, code, from_)
            request.text = _message_data
            request.to = phone_numbers
            request.from_ = from_
            response = send_sms_client.execute(request)
            return response

        numbers = utils.format_phone_numbers(phone_numbers, code)
        request.text = text
        request.to = numbers
        request.from_ = from_
        response = send_sms_client.execute(request)
        return response

    def retrieve(self, limit=None, msg_id=None, bulk_id=None):
        """method that pulls all messages recieved"""

        params = dict()
        params['limit'] = limit
        params['msg_id'] = msg_id
        params['bulk_id'] = bulk_id
        get_received_messages_client = get_received_messages(self.config)
        messages = get_received_messages_client.execute(params)
        print messages
        return messages

    def balance(self):
        """this gets the account balance of the user"""

        get_account_balance_client = get_account_balance(self.config)
        balance = get_account_balance_client.execute()
        print balance
        return balance

    def get_sms_log_reports(self, limit=None, msg_id=None, bulk_id=None, from_=None, to=None):
        """use this method to get sms log(s) of sent messages"""

        params = dict()
        params['limit'] = limit
        params['messageId'] = msg_id
        params['bulk_id'] = bulk_id
        params['from'] = from_
        params['to'] = to

        for i in params.keys():
            if params[i] is None:
                params.pop(i)

        get_sms_log_reports_client = get_sent_sms_logs(self.config)
        response = get_sms_log_reports_client.execute(params)

        return response

    def get_del_reports(self, limit=None, msg_id=None, bulk_id=None):
        """use this method to get delivery reports of sent messages"""

        params = dict()
        params['limit'] = limit
        params['messageId'] = msg_id
        params['bulk_id'] = bulk_id

        for i in params.keys():
            if params[i] is None:
                params.pop(i)

        get_delivery_reports_client = get_sent_sms_delivery_reports(self.config)
        response = get_delivery_reports_client.execute(params)
        print(unicode(response))
        return unicode(response)

    def on_recieve_sms(self, from_, text, callback):
        """the method recieves incoming messages from a view
        and passes it to a callback function for processing"""

        sender = from_
        message_body = text
        callback(sender, message_body)
        return callback
