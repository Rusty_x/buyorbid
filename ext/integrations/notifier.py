"""
notifier.py

@Author: rusty & kunsam002

script that integrates notifications.
"""

from flask import current_app
import requests
import json

from ext.base.exceptions import ValidationFailed
# from notification.schemas import *
# from notification.services.notification import *


class Notifier:

    def __init__(self, sender=None):
        self.app = current_app
        # self.api_key = api_key if api_key else self.app.config.get("NOTIFIER_API_KEY", api_key)
        # self.app_id = app_id if app_id else self.app.config.get("NOTIFIER_APP_ID", app_id)
        # self.username = username if username else self.app.config.get("NOTIFIER_USERNAME", username)
        self.sender = sender if sender else self.app.config.get("NOTIFIER_SENDER", "Buy-OrBi-d")
        self.sender = "buyorbid"

        # self.uri = uri if uri else self.app.config.get("NOTIFIER_BASE_URI", uri)
        # self.headers = {
        #     'Content-Type': 'application/json',
        # }
        # self.auth = (self.username, self.api_key)

    def send(self, code, email=None, sms=None, push=None, email_service=None, sms_service=None, email_schema=None,
             sms_schema=None):
        """
        method that initiates the sending of an email/sms/push message
        :param code: used to fetch a preconfigured notification
        :param email: message of type email
        :param sms: message of type sms
        :param push: message of type push

        :returns: resp
        """

        resp = {}
        if email:
            email_resp = self.send_email(code, email_schema, email_service, email)
            resp.update(email=email_resp)

        if sms:
            sms_resp = self.send_sms(code, sms_schema, sms_service, sms)
            resp.update(sms=sms_resp)

        # if push:
        #     push_resp = self.send_push(code, push)
        #     resp.update(push=push_resp)

        return resp

    def send_email(self, code, _schema, service, data=None):
        """
        Send email notification
        :param code: preconfigured notification code
        :param data: email message data

        :returns: resp
        """

        if not data:
            raise Exception("No data specified")

        if not data.get('from_'):
            print "notifier if block", self.sender
            data['from_'] = self.sender

        app_data = dict(code=code, data=data)

        # url = "/".join([self.uri, "emails"])

        return self.post("email", app_data, _schema, service)

    def send_sms(self, code, schema, service, data=None):
        """ Send sms notification
        :param code: preconfigured notification code
        :param data: sms message data

        :returns: resp
        """
        print data, '===data==='
        if not data:
            raise Exception("No data specified")

        if not data.has_key('from_'):
            if type(self.sender) is dict:
                data['from_'] = self.sender['name']
            else:
                data['from_'] = self.sender

        app_data = dict(code=code, data=data)
        # url = "/".join([self.uri, "sms"])

        return self.post("sms", app_data, schema, service)

    # def send_push(self, code, data=None):
    #     """
    #     Send push notification
    #     :param code: preconfigured notification code
    #     :param data: push message data
    #
    #     :returns: resp
    #     """
    #
    #     if not data:
    #         raise Exception("No data specified")
    #
    #     if not data['from_']:
    #         data['from_'] = self.sender
    #
    #     app_data = dict(app_id=self.app_id, code=code, data=data)
    #     url = "/".join([self.uri, "push"])
    #
    #     return self.post(url, app_data)

    def post(self, type_, app_data, schema_, service):
        """post the app data using requests and catch errors"""

        data, errors = schema_().load(app_data)

        if errors:
            print "Errors", errors
            raise ValidationFailed(errors)

        if type_ == "email":
            service.create(**data)
        else:
            service.create(**data)

        # try:
        #     resp = requests.post(url, headers=self.headers, data=json.dumps(app_data), auth=self.auth)
        #     return resp.json()
        #
        # except Exception, e:
        #     print e
        #     raise
        return "name"
