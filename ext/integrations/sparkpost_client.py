"""
sparkpost_client.py

@Author: rusty & kunsam002

Sparkpost email Flask integration.
"""

import os
from flask import current_app
from sparkpost import SparkPost
import requests
import mime
import json
from ext.base import utils


class Sparkpost:
    """base class that will be used for the integration into the app"""

    def __init__(self, app=None, sender=None, template_dir="", api_key=None, domain=None):
        self.app = app if app else current_app
        self.sender = sender if sender else self.app.config.get("SPARKPOST_SENDER", "SENDBOX")
        self.template_dir = template_dir if template_dir else self.app.config.get("SPARKPOST_TEMPLATE_DIR", template_dir)
        self.api_key = api_key if api_key else self.app.config.get("SPARKPOST_API_KEY", api_key)

        self.sp = SparkPost(self.api_key)
        self.inbound_domain = InboundDomain(self.api_key)
        self.relay_webhook = RelayWebhook(self.api_key)


    def send(self, from_=None, recipients=None, subject=None, html=None, text=None, attachments=[], images=None, delete_files=False):

        """
        Method that sends emails to clients
        :param from_: string or dict(name, email)
        :param to: person(s) the email is to be sent to
        :param subject: the title of the email
        :param html: html content of the email
        :param text: text content of teh email
        :param attachments: email attachment(s)
        :param images: inline image(s)
        :return: response
        """

        if not from_:
            from_ = self.sender

        data = dict(recipients=recipients, from_email=from_, subject=subject, html=html, text=text)

        _attachments = []
        _images = []

        if not recipients:
            raise Exception('please enter email recipient(s)')

        if not html and not text and not attachments:
            raise Exception('Your email is missing content')

        if images:
            parent_dir = os.path.dirname(os.path.realpath(__file__))
            for _name, _filename in images:
                _file = dict()
                _file['name'] = _name
                _file['filename'] = _filename
                _type = mime.Types.of(_name)[0]
                _file['type'] = _type.simplified
                _images.append(_file)

            if len(_images) > 0:
                data["inline_images"] = _images

        if attachments:
            _attachments = []
            parent_dir = os.path.dirname(os.path.realpath(__file__))
            for _name, _filename in attachments:
                _file = dict()
                _file['name'] = _name
                _file['filename'] = _filename
                _type = mime.Types.of(_name)[0]
                _file['type'] = _type.simplified
                _attachments.append(_file)

            if len(_attachments) > 0:
                data["attachments"] = _attachments

        cleaned_data = utils.remove_empty_keys(data)
        response = self.sp.transmission.send(**cleaned_data)

        if delete_files and attachments:
            for _, f in attachments:
                os.remove(f)

        print response
        return response


class InboundDomain:
    """class used to manage inbound domains used to recieve emails
    methods include: create, list, retrieve, delete"""

    def __init__(self, api_key=None):
        self.api_key = api_key

        self.uri = 'https://api.sparkpost.com/api/v1/inbound-domains'
        self.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': self.api_key
        }

    def create(self, domain=None):

        body = {'domain': domain}
        result = requests.post(self.uri, headers=self.headers, data=json.dumps(body))
        return result.json()

    def list(self):

        result = requests.get(self.uri, headers=self.headers)
        return result.json()

    def delete(self, domain):
        result = requests.delete(url=self.uri+'/'+domain, headers=self.headers)
        return result.json()

    def retrieve(self, domain):
        result = requests.get(url=self.uri + '/' + domain, headers=self.headers)
        return result.json()


class RelayWebhook:
    """class used to manage relay webhooks that connects inbound domains
    to a specified view that recieves fowarded emails
    methods include: create, list, retrieve, delete and update"""

    def __init__(self, api_key=None):
        self.api_key = api_key

        self.uri = 'https://api.sparkpost.com/api/v1/relay-webhooks'
        self.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': self.api_key
        }

    def create(self, name=None, target=None, domain=None, auth_token=None, **kwargs):

        if not target:
            raise Exception('target is required')

        if not domain:
            raise Exception('Domain is required')

        if not auth_token:
            raise Exception('auth_token is required')

        match = dict(protocol=kwargs.get('protocol'), domain=domain)
        body = dict(name=name, match=match, target=target, auth_token=auth_token)

        result = requests.post(self.uri, headers=self.headers, data=json.dumps(body))
        return result.json()

    def list(self):

        result = requests.get(self.uri, headers=self.headers)
        return result.json()

    def delete(self, webhook_id):
        result = requests.delete(url=self.uri+'/'+webhook_id, headers=self.headers)
        return result.json()

    def retrieve(self, webhook_id):
        result = requests.get(url=self.uri+'/'+webhook_id, headers=self.headers)
        return result.json()

    def update(self, webhook_id, domain=None, target=None, auth_token=None, name=None):

        content = self.retrieve(webhook_id).json()
        content_ = content['results']
        if not domain:
            domain = content_['match']['domain']
        if not target:
            target = content_['target']
        if not auth_token:
            auth_token = content_['auth_token']
        if name:
            name = name
        else:
            name = content_['name']

        match = dict(protocol=content_['match']['protocol'], domain=domain)
        body = dict(name=name, match=match, target=target, auth_token=auth_token)

        result = requests.put(url=self.uri+'/'+webhook_id, headers=self.headers, data=json.dumps(body))

        return result.json()
