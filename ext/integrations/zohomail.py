import os
from flask import current_app
from sparkpost import SparkPost
import requests
import mime
import json
from ext.base import utils


class ZohoMail:
    """base class that will be used for the integration into the app"""

    def __init__(self, app=None, sender=None, api_key=None, domain=None):
        self.app = app if app else current_app
        self.sender = sender if sender else self.app.config.get("ZOHO_SENDER")
        # self.template_dir = template_dir if template_dir else self.app.config.get("SPARKPOST_TEMPLATE_DIR",
        #                                                                           template_dir)
        self.base = "http://mail.zoho.com/api/accounts/%s/messages" % self.app.config.get("ZOHO_ACC_ID")
        print self.base, '==base=='
        self.api_key = api_key if api_key else self.app.config.get("ZOHO_AUTH_TOKEN", api_key)
        #
        # self.sp = SparkPost(self.api_key)
        # self.inbound_domain = InboundDomain(self.api_key)
        # self.relay_webhook = RelayWebhook(self.api_key)
        self.headers = {
            'Content-Type': 'application/json',
            'Authorization': self.api_key
        }
        print self.headers, '==header=='

    def send(self, from_=None, recipients=None, subject=None, html=None, text=None, attachments=[], images=None,
             delete_files=False):
        """
        Method that sends emails to clients
        :param from_: string or dict(name, email)
        :param recipients: person(s) the email is to be sent to
        :param subject: the title of the email
        :param html: html content of the email
        :param text: text content of teh email
        :param attachments: email attachment(s)
        :param images: inline image(s)
        :return: response
        """

        if not from_:
            from_ = self.sender

        data = dict(toAddress=recipients, fromAddress=from_, subject=subject, content=html)

        _attachments = []
        _images = []

        if not recipients:
            raise Exception('please enter email recipient(s)')

        if not html and not text and not attachments:
            raise Exception('Your email is missing content')

        # if images:
        #     parent_dir = os.path.dirname(os.path.realpath(__file__))
        #     for _name, _filename in images:
        #         _file = dict()
        #         _file['name'] = _name
        #         _file['filename'] = _filename
        #         _type = mime.Types.of(_name)[0]
        #         _file['type'] = _type.simplified
        #         _images.append(_file)
        #
        #     if len(_images) > 0:
        #         data["inline_images"] = _images
        #
        # if attachments:
        #     _attachments = []
        #     parent_dir = os.path.dirname(os.path.realpath(__file__))
        #     for _name, _filename in attachments:
        #         _file = dict()
        #         _file['name'] = _name
        #         _file['filename'] = _filename
        #         _type = mime.Types.of(_name)[0]
        #         _file['type'] = _type.simplified
        #         _attachments.append(_file)
        #
        #     if len(_attachments) > 0:
        #         data["attachments"] = _attachments

        cleaned_data = utils.remove_empty_keys(data)
        print json.dumps(data), '===data=='
        result = requests.post(self.base, headers=self.headers, data=json.dumps(data))

        return result.json()
