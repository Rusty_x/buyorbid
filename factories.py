"""
factories.py

@Author: rusty & kunsam002

This module contains application factories.
"""

import os

import googlemaps
from celery import Celery
from elasticsearch import Elasticsearch
from elasticsearch_dsl import connections, Search
from flask import Flask
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_principal import Principal
from flask_sqlalchemy import SQLAlchemy
from flask_assets import Environment, Bundle
from flask_uploads import UploadSet, IMAGES, ARCHIVES, SCRIPTS, EXECUTABLES, AllExcept, ALL, configure_uploads, \
    patch_request_class
import redis
from flask.ext.cache import Cache
from flask_restful import Api
from flask_alembic import Alembic
import wtforms_json
from celery import Celery
from ext.integrations.aws import S3, SES
from ext.integrations.infobip_client import InfoBip
from ext.integrations.notifier import Notifier
from ext.integrations.sparkpost_client import Sparkpost
from ext.integrations.zohomail import ZohoMail
from ext.integrations.billing import Billing
# from ext.plugins.webpack import Webpack
from rq_scheduler.scheduler import Scheduler
from rq import Queue
from flutterwave import Flutterwave
from flask_moment import Moment
from raven.contrib.flask import Sentry
from flask_oauth import OAuth

# Monkey patch wtforms to support json data
wtforms_json.init()


def initialize_api(app, api):
    """ Register all resources for the API """
    api.init_app(app=app)  # Initialize api first
    _resources = getattr(app, "api_registry", None)
    if _resources and isinstance(_resources, (list, tuple,)):
        for cls, args, kwargs in _resources:
            api.add_resource(cls, *args, **kwargs)


def initialize_blueprints(app, *blueprints):
    """Registers a set of blueprints to an application"""

    for blueprint in blueprints:
        app.register_blueprint(blueprint)


def make_celery(app):
    """ make celery inside the application possible """
    celery = Celery(app.import_name, backend=app.config.get("CELERY_RESULT_BACKEND"),
                    broker=app.config.get("CELERY_BROKER_URL"))

    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def apply_static_rule(app):
    """ Apply the static url endpoint based on the config file """

    static_folder = app.config.get("STATIC_FOLDER", os.path.dirname(os.path.abspath(__name__)))
    static_url_path = app.config.get("STATIC_URL_PATH", "/static")

    app.static_folder = static_folder
    app.static_url_path = static_url_path

    # rule to append to the app
    def fetch_from_static(path):
        return app.send_from_directory(static_folder, path)

    # app.add_url_rule('/static/<path:path>', 'static', fetch_from_static)

    return app


def create_app(app_name, config_obj, api_prefix='/api/v1'):
    """ Generates and configures the main shop application. All additional """

    # API application
    # app = Eve(name, settings=api_settings)

    # Launching application
    app = Flask(app_name)  # So the engine would recognize the root package

    # Load Configuration
    app.config.from_object(config_obj)

    # Initialize Mail
    # app.mail = Mail(app)

    assets = Environment(app)
    assets.from_yaml('assets.yaml')
    app.assets = assets

    # Include a connector registry to handle all connectors
    app.connector_registry = {}

    # Initializing login manager
    login_manager = LoginManager()
    login_manager.login_view = app.config.get('LOGIN_VIEW', '.login')
    # login_manager.login_message = 'You need to be logged in to access this page'
    login_manager.session_protection = 'strong'
    login_manager.init_app(app)
    app.login_manager = login_manager

    # Initializing principal manager
    app.principal = Principal(app)

    # Initializing bcrypt password encryption
    bcrypt = Bcrypt(app)
    app.bcrypt = bcrypt

    # Initializing Database
    db = SQLAlchemy(app)
    app.db = db

    moment = Moment(app)
    app.moment = moment

    # Install webpack
    # app.webpack = Webpack(app)

    # Initializing Alembic
    alembic = Alembic()
    alembic.init_app(app)
    app.alembic = alembic

    app.oauth = OAuth()

    photos = UploadSet('photos', IMAGES)
    archives = UploadSet('archives', ARCHIVES)
    configure_uploads(app, (photos, archives))
    patch_request_class(app, 16 * 1024 * 1024)  # Patches to 16MB file uploads max.
    app.photos = photos
    app.archives = archives

    # uploads = UploadSet('uploads', AllExcept(SCRIPTS + EXECUTABLES))
    # configure_uploads(app, uploads)
    # patch_request_class(app, 4 * 1024 * 1024)  # Patches to 2MB file uploads max.
    #
    # app.uploads = uploads

    # fixing links for static files
    app = apply_static_rule(app)

    es_config = app.config.get("ES_CONFIG")
    app.es = Elasticsearch(es_config)
    connections.connections.create_connection(hosts=[app.config.get("ES_HOST")], timeout=60, retry_on_timeout=True)
    # search = Search(app.es)
    # app.search = search

    # Caching
    app.cache = Cache(app)

    # Redis
    app.redis = redis.StrictRedis().from_url(app.config.get('REDIS_URL'))

    # Redis store for session management
    # The process running Flask needs write access to this directory:
    # store = RedisStore(redis.StrictRedis())

    # # this will replace the app's session handling
    # KVSessionExtension(store, app)

    # # configure sentry
    # if not app.config.get("DEBUG", True):
    # 	sentry = Sentry(app)
    #
    # 	app.sentry = sentry

    # inject celery into the app
    app.celery = make_celery(app)

    # app.s3 = S3(app)

    app.ses = SES(app)

    app.infobip = InfoBip(app, template_source='string')
    app.zohomail = ZohoMail(app)

    # Sparkpost
    # app.sparkpost = Sparkpost(app)

    # notification engine
    app.notifier = Notifier(app)

    # Initialize Flutterwave
    app.flutterwave = Flutterwave(app)

    app.billing = Billing(app)

    api = Api(app, prefix=api_prefix)
    app.api = api

    # google maps api
    app.gmaps = googlemaps.Client(key=app.config.get('GOOGLE_API_KEY'))

    # Initialize Logging
    if not app.debug:
        import logging
        from logging.handlers import RotatingFileHandler
        file_handler = RotatingFileHandler("/var/log/%s/%s.log" % (app_name, app.config.get("LOGFILE_NAME", app_name)),
                                           maxBytes=500 * 1024)
        file_handler.setLevel(logging.ERROR)
        from logging import Formatter
        file_handler.setFormatter(Formatter(
            '%(asctime)s %(levelname)s: %(message)s '
            '[in %(pathname)s:%(lineno)d]'
        ))
        app.logger.addHandler(file_handler)

        # Implement Sentry logging here
        sentry = Sentry(app)
        app.sentry = sentry

    # include an api_registry to the application
    app.api_registry = []  # a simple list holding the values to be registered

    return app
