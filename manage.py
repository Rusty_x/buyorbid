#! /usr/bin/env python
# coding: utf-8

from functools import partial

from flask_script import Manager, prompt, prompt_pass, prompt_bool
import os, json
import flask

from factories import create_app, initialize_blueprints, initialize_api

APP_NAME = "buyorbid"

# Use partials to include the app_name parameter in the product create_app function
app_factory = partial(create_app, APP_NAME)

manager = Manager(app_factory)

# Allow the manager access config files on the fly
manager.add_option('-c', '--config', dest='config_obj', default='config.DevConfig', required=False)


@manager.command
def runsite():
    """ Main management command to run the tracking site application """
    app = flask.current_app
    with app.app_context():
        from buyorbid.views import admin
        from buyorbid import api

        # Import all subscriptions to initialize them
        from buyorbid.subscribers import search

        initialize_api(app, api)

        from flask_debugtoolbar import DebugToolbarExtension
        from flask_debugtoolbar_lineprofilerpanel.profile import line_profile

        app.config['DEBUG_TB_PANELS'] = [
            'flask_debugtoolbar.panels.versions.VersionDebugPanel',
            'flask_debugtoolbar.panels.timer.TimerDebugPanel',
            'flask_debugtoolbar.panels.headers.HeaderDebugPanel',
            'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
            'flask_debugtoolbar.panels.template.TemplateDebugPanel',
            'flask_debugtoolbar.panels.sqlalchemy.SQLAlchemyDebugPanel',
            'flask_debugtoolbar.panels.logger.LoggingPanel',
            'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',
            # Add the line profiling
            'flask_debugtoolbar_lineprofilerpanel.panels.LineProfilerPanel'
        ]

        toolbar = DebugToolbarExtension(app)

        port = int(os.environ.get('PORT', 5700))
        app.run(host='0.0.0.0', port=port, debug=True)


@manager.command
def runserver():
    """ Main management command to run the tracking site application """
    app = flask.current_app
    with app.app_context():
        from buyorbid.views import public
        from buyorbid import api

        # Import all subscriptions to initialize them
        from buyorbid.subscribers import search

        initialize_api(app, api)

        from flask_debugtoolbar import DebugToolbarExtension
        from flask_debugtoolbar_lineprofilerpanel.profile import line_profile

        app.config['DEBUG_TB_PANELS'] = [
            'flask_debugtoolbar.panels.versions.VersionDebugPanel',
            'flask_debugtoolbar.panels.timer.TimerDebugPanel',
            'flask_debugtoolbar.panels.headers.HeaderDebugPanel',
            'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
            'flask_debugtoolbar.panels.template.TemplateDebugPanel',
            'flask_debugtoolbar.panels.sqlalchemy.SQLAlchemyDebugPanel',
            'flask_debugtoolbar.panels.logger.LoggingPanel',
            'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',
            # Add the line profiling
            'flask_debugtoolbar_lineprofilerpanel.panels.LineProfilerPanel'
        ]

        toolbar = DebugToolbarExtension(app)

        port = int(os.environ.get('PORT', 5400))
        app.run(host='0.0.0.0', port=port, debug=True)


@manager.command
def runwebsite():
    """ Main management command to run the tracking site application """
    app = flask.current_app
    with app.app_context():
        from buyorbid import api

        initialize_api(app, api)

        port = int(os.environ.get('PORT', 5200))
        app.run(host='0.0.0.0', port=port, debug=True)


@manager.command
def alembic(action, message=""):
    """ alembic integration using Flask-Alembic. Should provide us with more control over migrations """

    app = flask.current_app
    with app.app_context():
        from buyorbid import alembic as _alembic
        from buyorbid.models import *

        if action == "migrate":
            app.logger.info("Generating migration")
            _alembic.revision(message)
            app.logger.info("Migration complete")

        elif action == "upgrade":
            app.logger.info("Executing upgrade")
            _alembic.upgrade()
            app.logger.info("Upgrade complete")

        elif action == 'update':
            app.logger.info("Executing upgrade")
            _alembic.upgrade()
            _alembic.revision("Generating migration")
            _alembic.upgrade()
            app.logger.info("Upgrade complete")


@manager.command
def create_user():
    """ Admin setup to create """
    app = flask.current_app
    with app.app_context():

        from buyorbid.services.accounts import UserService
        from buyorbid.models import *

    print "Enter the following parameters to created a new user account"

    username = prompt("Username(*)")
    email = prompt("Email(*)")
    password = prompt_pass("Password(*)")
    retype_password = prompt_pass("Retype Password(*)")
    first_name = prompt("First Name")
    last_name = prompt("Last Name")
    is_staff = prompt_bool("Staff Account (yes/no)", default=False)
    is_enabled = prompt_bool("Enable Account (yes/no)", default=False)
    is_admin = prompt_bool("Admin Account (yes/no)", default=False)

    if username and email and password and retype_password and (password == retype_password):
        user = UserService.create(include_address=False, username=username, email=email, first_name=first_name,
                                  last_name=last_name, is_admin=is_admin,
                                  password=password, is_staff=is_staff, is_enabled=is_enabled)

        if user:
            user.user_earning = 2500
            user.purchased_credit = 4000
            user.awarded_credit = 950
            db.session.add(user)
            db.session.commit()
            return user.id

    else:
        print "Account could not be created. Please supply all required (*) parameters"
        return None


@manager.command
def create_admin_user(id=None):
    """ Admin setup to create """
    app = flask.current_app
    with app.app_context():
        from buyorbid.services.accounts import UserService
        user_id = create_user()
        if user_id:
            user = UserService.update(user_id, is_staff=True, is_enabled=True, is_admin=True)
            if user:
                return user.id

            else:
                print "Admin Account could not be created. Please supply all required (*) parameters"
                return None

        else:
            print "Account could not be created. Please supply all required (*) parameters"
            return None


@manager.command
def install_index():
    app = flask.current_app
    with app.app_context():
        from buyorbid import logger
        from buyorbid.models import *
        from ext.base.search import generate_mapping, create_index
        import importlib

        # first create the new index
        settings = dict(mapping=dict(nested_fields=dict(limit=1000)))

        create_index(app.config.get("ES_INDEX"), settings, ignore=[400, 404])

        for name, model_class in pyinspect.getmembers(sys.modules["future.models"], pyinspect.isclass):
            if issubclass(model_class, db.Model) and getattr(model_class, "exclude_in_index", False) is False:
                # implement index creattion for all classes
                print "Installing index for [%s]" % name
                cls = generate_mapping('future.search.mappings', model_class)
                cls.init()


@manager.command
def flush_index():
    """ Find all couriers and departments and fix the logic such that it matches properly """

    app = flask.current_app
    with app.app_context():
        from buyorbid.models import *
        from ext.base.search import delete_index

        delete_index(app.config.get("ES_INDEX"))


# @manager.command
# def rebuild_index(skip=False):
# 	app = flask.current_app
# 	with app.app_context():
# 		from buyorbid import logger
# 		from buyorbid.models import *
# 		from ext.base.search import generate_mapping
# 		import importlib
#
# 		install_index()
#
# 		for name, model_class in pyinspect.getmembers(sys.modules["future.models"], pyinspect.isclass):
# 			if issubclass(model_class, db.Model) and getattr(model_class, "exclude_in_index", False) is False:
# 				# implement index creattion for all classes
# 				rebuild_model_index(name, skip=skip)


@manager.command
def rebuild_index(delete=False, skip=False):
    from buyorbid import logger
    from buyorbid.services import search

    logger.info("Rebuilding search index, this might take a while. Please wait...")

    search.rebuild_index(_delete=delete, skip=skip)

    logger.info("Done")


# @manager.command
# def rebuild_model_index(doc_type, skip=False, purge=False):
#     from buyorbid import models, logger, db
#     from ext.base.search import index_object, generate_mapping
#     #
#     model_class = getattr(models, doc_type)
#     #
#     logger.info(model_class)
#
#     logger.info("Building index for [%s]" % doc_type)
#
#     generate_mapping('buyorbid.subscribers.search', model_class)
#
#     for obj in db.session.query(model_class.primary_key()).distinct().all():
#         # print "Attempting to index for %s - %s" % (model_class.__name__, obj.id)
#         print obj
#         index_object('buyorbid.models', 'buyorbid.subscribers.search', model_class.__name__, obj[0], skip=skip)

@manager.command
def rebuild_model_index(doc_type, skip=False, purge=False):
    from buyorbid import models, logger, db
    from buyorbid.services.search import rebuild_model_index
    #
    model_class = getattr(models, doc_type)
    #
    logger.info(model_class)

    logger.info("Building index for [%s]" % doc_type)

    rebuild_model_index(model_class)


@manager.command
def install_assets(name):
    """ load startup data for a particular module """

    app = flask.current_app
    with app.app_context():
        from buyorbid import db, models, logger
        from ext.base import loader

        setup_dir = app.config.get("SETUP_DIR")  # Should be present in config

        filename = "%s.json" % name

        src = os.path.join(setup_dir, filename)
        logger.info(src)

        loader.load_data(models, db, src)


@manager.command
def setup_app():
    """ load startup data for a particular module """
    app = flask.current_app

    with app.app_context():
        from buyorbid import db, models, logger, alembic as _alembic
        from ext.base import loader
        from buyorbid.models import Image, Variant
        from buyorbid.services.product import ImageService, ProductService
        from buyorbid.services import search

        logger.info("Generating all tables ")
        db.create_all()

        logger.info("Relax and Enjoy the Ride....")
        # For Database Update
        # actions = ['upgrade', 'migrate', 'upgrade']
        # for i in actions:
        #     _alembic(action=i)

        SETUP_DIR = app.config.get("SETUP_DIR")  # Should be present in config

        files = ['countries', 'currencies', 'timezones', 'transaction_statuses',
                 'payment_options', 'delivery_options', 'payment_statuses','delivery_statuses',
                 'payment_channels', 'order_statuses', "categories", "product_detail_types"]

    for name in files:
        filename = "%s.json" % name

        src = os.path.join(SETUP_DIR, filename)
        logger.info(src)

        loader.load_data(models, db, src)

    loader.load_states()
    loader.load_cities()
    loader.load_banks()

    user_id = create_user()
    # data = {"name": "Apple iPhone 6s 128GB", "description": "Apple iPhone 6s 128GB Grey & Rose Gold",
    #         "sku": "123ED", "quantity": 889, "regular_price": 300000, "sale_price": 250000, "weight": 0.8,
    #         "has_variants": False}
    # product = ProductService.create(**data)
    # image = Image(name="Apple iPhone 6s 128GB Grey", product_id=product.id,
    #               url="https://i.gadgets360cdn.com/large/iphone_6_full_1489986905312.jpg?output-quality=80",
    #               is_cover=True)
    # db.session.add(image)
    # image1 = Image(name="Apple iPhone 6s 128GB Rose Gold", product_id=product.id,
    #                url="https://qq2you.com/7612-thickbox_default/iphone-6s-plus-32gb-lte-gold-hk-spec-mn2x2zp-a.jpg",
    #                is_cover=False)
    # db.session.add(image1)
    # variant = Variant(name="Apple iPhone 6s 128GB Rose Gold", product_id=product.id, sku="123EDRG", quantity=75,
    #                   regular_price=350000, sale_price=280000)
    # db.session.add(variant)
    # db.session.commit()
    #
    # data2 = {"name": "Apple Mac Book Pro 1TB", "description": "Apple Mac Book Pro 1TB",
    #          "sku": "123ED", "quantity": 889, "regular_price": 600000, "sale_price": 580000, "weight": 0.8,
    #          "has_variants": False}
    # product2 = ProductService.create(**data2)
    # image2 = Image(name="Apple Mac Book Pro 1TB", product_id=product2.id,
    #                url="https://www.bhphotovideo.com/images/images2500x2500/Apple_MC024LL_A_17_MacBook_Pro_Notebook_684525.jpg",
    #                is_cover=True)
    # db.session.add(image2)
    # image3 = Image(name="Apple Mac Book Pro 1TB", product_id=product2.id,
    #                url="http://descr.provis.ru/descr/109466/i/macbook_pro_13_inch_retina.jpeg",
    #                is_cover=False)
    # db.session.add(image3)
    # image4 = Image(name="Apple Mac Book Pro 1TB", product_id=product2.id,
    #                url="https://s-media-cache-ak0.pinimg.com/originals/36/14/91/3614913a0a5208cd329b929ca8a005fb.jpg",
    #                is_cover=False)
    # db.session.add(image4)
    # db.session.commit()
    #
    # search.custom_index_setup()

    app.logger.info("App is Setup and Ready to Go....Have Fun!!!")


# @manager.command
# def update_record_states():
#     from model_migrations import create_cities
#     create_cities()
#
#
# @manager.command
# def generate_setup_data():
#     from future import logger
#     from model_migrations import generate_cities_json, generate_states_json
#
#     generate_states_json()
#     generate_cities_json()
#
#     logger.info("Setup Data Generation Successful")
#
#
# @manager.command
# def worker():
#     app = flask.current_app
#     with app.app_context():
#         import workers

@manager.command
def fix_product_category():
    app = flask.current_app

    with app.app_context():
        from buyorbid import db, models, logger, alembic as _alembic
        from ext.base import loader
        from buyorbid.models import Product, Category
        from buyorbid.services import search

        for product in Product.query.all():
            categories = product.categories

            new_category_ids =[]
            for cat in categories:
                new_category_ids.append(cat.id)
                if cat.parent_id:
                    parent_cat = Category.query.get(cat.parent_id)
                    if parent_cat:
                        new_category_ids.append(cat.parent_id)
                        if parent_cat.parent_id:
                            new_category_ids.append(parent_cat.parent_id)

            product.categories = Category.query.filter(Category.id.in_(new_category_ids)).all()
            db.session.add(product)
            db.session.commit()
            search.index(product)





if __name__ == "__main__":
    manager.run()
