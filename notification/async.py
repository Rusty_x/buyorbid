from buyorbid import infobip, celery, app, zohomail, ses
import ast


@celery.task
def send_email_notification(subject, html=None, recipients=[], attachments=[], delete_files=False, **kwargs):
	""" Send out email messages as a celery task """
	ses.send(recipients=recipients, subject=subject, html=html, files=attachments, delete_files=delete_files)


@celery.task
def send_sms_notification(phone_numbers, text, formatted=True, sender=None, **kwargs):
	""" Send out sms messages as a celery task """
	response = infobip.send(from_=sender, phone_numbers=phone_numbers, text=text)
	res = ast.literal_eval(response.to_JSON())


@celery.task
def send_voice_notification(phone_numbers, text, formatted=True, sender=None, **kwargs):
	""" Send out sms messages as a celery task """
	response = infobip.send(from_=sender, phone_numbers=phone_numbers, text=text, voice=True)


@celery.task
def get_sms_log(msg_id):
	""""""
	sender = infobip
	response = sender.get_sms_log_reports(msg_id=msg_id)
	res = ast.literal_eval(response.to_JSON())
