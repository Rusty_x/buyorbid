from marshmallow import Schema, fields
from marshmallow.decorators import validates_schema
from marshmallow.exceptions import ValidationError
from marshmallow.validate import NoneOf, OneOf
# from sendbox_notifications.models import *
from sqlalchemy import and_, or_, func
from flask import g

# , validate=OneOf([c.id for c in App.query.filter(App.account_id)==getattr(g, "account_id")])
class NotificationSchema(Schema):

    name = fields.String(required=True)
    email = fields.String()
    sms = fields.String()
    push = fields.String()
    code = fields.String()
    # app_id = fields.Integer(required=True)


# class AppSchema(Schema):
#     name = fields.String(required=True)


class EmailDataSchema(Schema):
    name = fields.String()
    subject = fields.String(required=True)
    body = fields.String()
    # to = fields.List(fields.String, required=True, many=True)
    to = fields.String(required=True)
    from_ = fields.Raw()
    payload = fields.Dict(required=False)


class EmailSchema(Schema):
    # app_id = fields.Integer(required=True)
    code = fields.String(required=True)
    data = fields.Nested(EmailDataSchema)


class SmsDataSchema(Schema):
    name = fields.String(required=True)
    # to = fields.List(fields.String, required=True, many=True)
    to = fields.String(required=True)
    from_ = fields.String()
    voice = fields.Boolean(default=False)
    payload = fields.Dict(required=False)


class SmsSchema(Schema):
    # app_id = fields.Integer(required=True)
    code = fields.String(required=True)
    data = fields.Nested(SmsDataSchema)


class PushDataShema(Schema):
    payload = fields.Dict(required=False)
    recipient_type = fields.String(required=True)
    recipient_name = fields.String(required=True)
    title = fields.String()

class PushSchema(Schema):
    """Not yet implemented"""
    # app_id = fields.Integer(required=True)
    event = fields.String(required=True)
    data = fields.Nested(PushDataShema)


