from jinja2 import BaseLoader, TemplateNotFound, Environment
import config
from flask import g
from buyorbid.models import Notification


class TemplateLoader(BaseLoader):
    """ Database template loader """

    def __init__(self, model_class, encoding='utf-8'):
        self.encoding = encoding
        self.model_class = model_class

    def get_notification(self, template):
        raise NotImplementedError

    def get_source(self, environment, template):
        """Loads the specified template from the database by querying the data model and reading the content.
        The template will be broken into (courier_id.templatename) """

        notification, template_type = self.get_notification(template)

        if notification is None or hasattr(notification, template_type) is None:
            raise TemplateNotFound(template)

        source = getattr(notification, template_type)

        # source = source.decode(self.encoding)
        source = source.encode(self.encoding, 'ignore').decode(self.encoding)

        return source, template, lambda: notification.last_updated


class NotificationLoader(TemplateLoader):

    def __init__(self):
        super(NotificationLoader, self).__init__(None)

    def get_notification(self, template):

        data = template.split(".")
        print data, "the data in get notification"
        if len(data) < 2:
            raise Exception

        template_type, code = "%s" % data[0], ".".join(data[1:])
        obj = Notification.query.filter(Notification.code == code.strip()).first()

        print template_type, obj, '----------', code

        return obj, template_type

def extends_template(template=None):
    template_name = g.type + '.' + template
    return template_name

def include_template(template=None):
    template_name = g.type + '.' + template
    return template_name


def standard_render_template(loader_class, template_name, data={}):
    """ custom template rendering function for standard notifications"""

    template_context = {
        'config': config,
        'g': g
    }

    env = Environment(loader=loader_class())
    env.globals['extends_template'] = extends_template
    env.globals['include_template'] = include_template
    template = env.get_template(template_name)
    print template, "the final template"
    data.update(template_context)

    return template.render(**data)


def render_standard_email_template(template_name, **kwargs):
    name = "email.%s" % template_name
    return standard_render_template(NotificationLoader, name, data=kwargs)


def render_standard_sms_template(template_name, **kwargs):
    name = "sms.%s" % template_name
    return standard_render_template(NotificationLoader, name, data=kwargs)
