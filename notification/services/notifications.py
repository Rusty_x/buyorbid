"""
notification.py

@Author: Rotola Akinsowon
@Date: 24 August, 2016
@Time: 11:24 AM

notification service to handle
all application specific notifications
"""
import time

import datetime
from pprint import pprint

from rq.decorators import job
from rq.queue import Queue
from rq_scheduler.scheduler import Scheduler
from rq_scheduler.scripts import rqscheduler

from notification.async import send_email_notification, send_sms_notification  # , send_email_notification, send_voice_notification
from buyorbid.models import Notification, db, Email, Sms
from ext.services.orm import ServiceFactory
from loader import render_standard_email_template, render_standard_sms_template
from flask import g
from buyorbid import infobip, redis, celery, app
import ast

BaseNotificationService = ServiceFactory.create_service(Notification, db)
BaseEmailService = ServiceFactory.create_service(Email, db)
BaseSmsService = ServiceFactory.create_service(Sms, db)

class NotificationService(BaseNotificationService):

    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        notification = BaseNotificationService.create(ignored_args=ignored_args, **kwargs)
        return  notification

    @classmethod
    def update(cls, obj_id, ignored_args=None, **kwargs):
        obj = BaseNotificationService.update(obj_id, ignored_args, **kwargs)
        return obj

    @classmethod
    def compile(cls, code_, **data):
        """
        merge jinja template with user data to construct
        a notification message

        :param code_: unique notification code
        :param data: instance specific data

         :returns: instance specific message
        """

        print " Compiling email for %s" % code_

        obj = cls.query.filter_by(code=code_).first()

        # template_name = str(account_id) +'.'+str(app_id) +'.'+ code_
        template_name = code_

        notification = dict(id=obj.id)
        if obj.email is not None:
            g.type = 'email'
            email = render_standard_email_template(template_name, **data)
            # print email
            notification.update(email=email)

        if obj.sms is not None:
            g.type = 'sms'
            sms = render_standard_sms_template(template_name, **data)
            notification.update(sms=sms)

        return notification


class EmailService(BaseEmailService):
    """
    email service to manage email requirements
    """
    @classmethod
    def create(cls, code=None, data=None):
        """
        compile and persist email messages to the database
        :param code: notification code
        :param data: instance specific data to be merged with the template at compile
        :return: email
        """
        payload = data.get('payload')
        payload['name'] = data.get('name')
        message = NotificationService.compile(code, **payload)
        id = message.get('id')

        if message.has_key('email'):
            html = message.get('email')

            if data.get('from_'):
                from_ = data['from_']
                print "Notification if block", from_
                if type(from_) is dict:
                    from_ = str(from_)
            else:
                from_ = None

            email = BaseEmailService.create(subject=data['subject'], html=html,
                                            notification_code=code, notification_id=id, to=data['to'], from_=from_)
            cls.send(email.id)

        else:
            return Exception("Email notification has not been configured")

        return email

    @classmethod
    def send(cls, obj_id):
        obj = BaseEmailService.get(obj_id)
        # print obj.to, obj.html, obj.subject
        # response = sender.send(from_=obj.from_, recipients=[obj.to], subject=obj.subject, html=obj.html,
        #                        text=obj.text, attachments=[], images=None, delete_files=False)

        html = obj.html.encode('utf-8', 'ignore').decode('utf-8')

        send_email_notification.delay(from_=obj.from_, recipients=obj.to, subject=obj.subject, html=html,
                               text=obj.text, attachments=[], images=None, delete_files=False)

        # if response.has_key('id'):
        #     cls.update(obj.id, status='sent', message_id=response['id'])
        # else:
        #     cls.update(obj.id, status='NOT sent')
        # print response
        # obj = Email.query.get(obj_id)
        return obj

class SmsService(BaseSmsService):
    @classmethod
    def create(cls, code=None, data=None):
        """
        compile and persist sms messages to the database
        :param code: notification code
        :param data: instance specific data to be merged with the template at compile
        :return: sms
        """
        payload = data.get('payload')
        payload['name'] = data.get('name')
        voice = data.get('voice')
        message = NotificationService.compile(code, **payload)
        id = message.get('id')


        if message.has_key('sms'):
            text = message.get('sms')
            print("im noti sms:",data)

            if data.get('from_'):
                from_ = data['from_']
            else: from_ = None

            sms = BaseSmsService.create(to=data['to'], notification_code=code,
                                        text=text, from_=from_, notification_id=id)
            cls.send(sms.id, voice)
        else:
            return Exception("Sms notification has not been configured")

        return sms

    @classmethod
    def send(cls, obj_id, voice=False):
        obj = BaseSmsService.get(obj_id)
        print("im text:", obj.text)
        print("sender:", app.config.get('INFOBIP_VOICE_NUMBER'))
        if voice:
            pass
            # send_voice_notification.delay(text=obj.text, phone_numbers=obj.to,
            #                               sender=app.config.get('INFOBIP_VOICE_NUMBER'))
            # send_voice_notification(text=obj.text, phone_numbers=obj.to,
            #                               sender=app.config.get('INFOBIP_VOICE_NUMBER'))
        else:
            # send_sms_notification.delay(text=obj.text, phone_numbers=obj.to, sender=obj.from_)
            send_sms_notification.delay(text=obj.text, phone_numbers=obj.to, sender=obj.from_)
        # # cls.update(obj.id, status='sent')
        # reso = ast.literal_eval(response.to_JSON())
        # print("im reso:", reso)
        # msg_id = reso.get('messages')[0].get('messageId')
        # msg_log = cls.send_log(msg_id)
        # pprint(msg_log)
        # status = 'pending'
        # if msg_log:
        #     status = msg_log['results'][0]['status']['groupName']
        # cls.update(obj.id, status=status, message_id=msg_id)
        # obj = Sms.query.get(obj_id)
        return obj


    @classmethod
    def send_log(cls, msg_id):
        print msg_id
        sender = infobip
        response = sender.get_sms_log_reports(msg_id=msg_id)
        return ast.literal_eval(response.to_JSON())

    @classmethod
    def log(cls, msg_id, limit=None, bulk_id=None, from_=None, to=None):
        print msg_id
        obj = cls.get(msg_id)
        sender = infobip
        response = sender.get_sms_log_reports(limit=limit, msg_id=obj.message_id, bulk_id=bulk_id, from_=from_, to=to)
        res = ast.literal_eval(response.to_JSON())
        status = res['results'][0]['status']['groupName']
        obj = Sms(status=status, message_id=obj.message_id, to=obj.to, last_updated=obj.last_updated,
                  text=obj.text, date_created=obj.date_created, id=obj.id)
        return obj


# class PushService:
#     """does not use any model class so no service factory creation"""
#
#     @classmethod
#     def create(cls, ignore_args=None, **kwargs):
#         """
#         :param kwargs: data you want to push
#         :return: data
#         """
#         app_id = kwargs.get('app_id')
#
#         data = kwargs.get('data')
#         recipient_type = data.get('recipient_type') #the type of receiver e.g courier, merchant etc
#         recipient_name = data.get('recipient_name') #the name of receiver e.g oragon, sneakersden etc
#         event = kwargs.get('event')
#         room = "%s@%s.%s" % (recipient_name, recipient_type, app_id)
#         socketIO.emit(event, data, room=room)
#
#
#         return kwargs


# res = notifier.send(code='sw', sms_service=SmsService, sms_schema=SmsSchema
#                     ...:, sms = sms)