from factories import create_app
import socket

# config_obj = 'config.StagingConfig'
# switch to live configuration based on system name
config_obj = 'config.ProdConfig'

# Use partials to include the app_name parameter in the product create_app function
app = create_app('buyorbid', config_obj)

with app.app_context():
   from buyorbid.async import *
   from notification.async import *
   from buyorbid.services.product import *
   from buyorbid.subscribers.search import *
   # from buyorbid.services.admin import *
   from buyorbid.models import *

   celery_app = app.celery
